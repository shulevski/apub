update:
	git pull
	apub-update

push:
	apub-export > data/apub-export.csv
	git add data/apub.db data/apub-export.csv
	git commit -m "Regular db update"
	git push

refresh:
	# Update the metadata by re-fetching all entries from the ADS API
	# this will e.g. update the citation counts and bibcodes
	apub-export > data/apub-export.csv
	rm data/apub.db
	apub-import data/apub-export.csv


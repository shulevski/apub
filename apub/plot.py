"""Creates beautiful visualizations of the publication database."""
import datetime
import numpy as np
from astropy import log
import collections

from matplotlib import pyplot as pl
import matplotlib.patheffects as path_effects
import matplotlib as mpl
from mpl_chord_diagram import chord_diagram
from matplotlib.patches import PathPatch
from matplotlib.offsetbox import OffsetImage, AnnotationBbox

import nltk
import apub

from . import SCIENCE

# Configure the aesthetics
mpl.rcParams["figure.figsize"] = (10, 6)
mpl.rcParams["interactive"] = False
mpl.rcParams["lines.antialiased"] = True
# Patches
mpl.rcParams["patch.linewidth"] = 0.5
mpl.rcParams["patch.facecolor"] = "348ABD"
mpl.rcParams["patch.edgecolor"] = "eeeeee"
mpl.rcParams["patch.antialiased"] = True
# Font
mpl.rcParams["font.family"] = "sans-serif"
mpl.rcParams["font.size"] = 16
mpl.rcParams["font.sans-serif"] = "Open Sans"
mpl.rcParams["text.color"] = "333333"
# Axes
mpl.rcParams["axes.facecolor"] = "ecf0f1"
mpl.rcParams["axes.edgecolor"] = "bdc3c7"
mpl.rcParams["axes.linewidth"] = 1.0
mpl.rcParams["axes.grid"] = False
mpl.rcParams["axes.titlesize"] = "x-large"
mpl.rcParams["axes.labelsize"] = "x-large"
mpl.rcParams["axes.labelweight"] = "normal"
mpl.rcParams["axes.labelcolor"] = "333333"
mpl.rcParams["axes.axisbelow"] = True
mpl.rcParams["axes.unicode_minus"] = True
# Ticks
mpl.rcParams["xtick.color"] = "333333"
mpl.rcParams["ytick.color"] = "333333"
mpl.rcParams["xtick.major.size"] = 0
mpl.rcParams["ytick.major.size"] = 0
# Grid
mpl.rcParams["grid.color"] = "bdc3c7"
mpl.rcParams["grid.linestyle"] = "-"
mpl.rcParams["grid.linewidth"] = 1


def plot_by_year(db,
                 output_fn='apub-publication-rate.pdf',
                 first_year=2011,
                 current_year=2022,
                 barwidth=0.75,
                 dpi=200,
                 extrapolate=False,
                 instrument='LOFAR',
                 colors=["#3498db", "#27ae60", "#95a5a6"]):
    """Plots a bar chart showing the number of publications per year.

    Parameters
    ----------
    db : `PublicationDB` object
        Data to plot.

    output_fn : str
        Output filename of the plot.

    first_year : int
        What year should the plot start?

    current_year : int
        What year should the plot end?

    barwidth : float
        Aesthetics -- how wide are the bars?

    dpi : float
        Output resolution.

    extrapolate : boolean
        If `True`, extrapolate the publication count in the current year.

    instrument : str
        'LOFAR'

    colors : list of str
        Define the facecolor for [lofar, extrapolation]
    """
    # Obtain the dictionary which provides the annual counts
    #current_year = datetime.datetime.now().year
    #current_year = current_year - 1 # to limit to end of past year
    counts = db.get_annual_publication_count(year_begin=first_year, year_end=current_year)

    # Now make the actual plot
    fig = pl.figure()
    ax = fig.add_subplot(111)
    pl.bar(np.array(list(counts[instrument].keys())),
            list(counts[instrument].values()),
            label=instrument,
            facecolor=colors[0],
            width=barwidth)
    # Also plot the extrapolated prediction for the current year
    if extrapolate:
        now = datetime.datetime.now()
        fraction_of_year_passed = float(now.strftime("%-j")) / 365.2425
        if instrument == 'LOFAR':
            current_total = (counts['LOFAR'][current_year])
        else:
            current_total = counts[instrument][current_year]
        expected = (1/fraction_of_year_passed - 1) * current_total
        pl.bar(current_year,
               expected,
               bottom=current_total,
               label='Extrapolation',
               facecolor=colors[2],
               width=barwidth)

    # Aesthetics
    pl.ylabel("Publications per year")
    ax.get_xaxis().get_major_formatter().set_useOffset(False)
    pl.xticks(range(first_year - 1, current_year + 1))
    pl.xlim([first_year - 0.75*barwidth, current_year + 0.75*barwidth])
    '''
    pl.legend(bbox_to_anchor=(0.1, 1., 1., 0.),
              loc=3,
              ncol=3,
              borderaxespad=0.,
              handlelength=0.8,
              frameon=False)
    '''
    # Disable spines
    ax.spines["left"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    # Only show bottom and left ticks
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    # Only show horizontal grid lines
    ax.grid(axis='y')
    pl.tight_layout(rect=(0, 0, 1, 0.95), h_pad=1.5)
    log.info("Writing {}".format(output_fn))
    pl.savefig(output_fn, dpi=dpi)
    pl.close()


def plot_science_piechart(db, output_fn="apub-piechart.png", first_year=2011, last_year=None, dpi=150):
    """Plots a piechart showing radio vs astrophysics publications.

    Parameters
    ----------
    db : `PublicationDB` object
        Data to plot.

    output_fn : str
        Output filename of the plot.

    first_year : int
        What year should the plot start?

    last_year : int
        What year should the plot end?

    dpi : float
        Output resolution.
    """
    count = []

    where = "science = ?"
    if last_year is not None:
        years = range(first_year, last_year+1)
        str_year = ["'{}'".format(y) for y in years]
        where += " AND year IN (" + ", ".join(str_year) + ")"
    else:
        where += " AND year = '{}' ".format(first_year)
    
    for science in SCIENCE:
        cur = db.con.execute("SELECT COUNT(*) FROM pubs "
                             "WHERE {} and instrument = 'LOFAR';".format(where), [science])
        rows = list(cur.fetchall())
        count.append(rows[0][0])
    myorder = ['eg', 'ga', 'pt', 'im', 'io', 'cr', 'so', 'pl', 'ex', 'li', 'hI']
    # CATEGORIES and my_order should reflect the content and order of the SCIENCE list.
    CATEGORIES = {
              'eg': 'Extragalactic',
              'ga': 'Galaxy',
              'pt': 'Pulsars & Transients',
              'im': 'Instrumentation & Methods',
              'io': 'Ionosphere',
              'cr': 'Cosmic Rays',
              'so': 'Solar',
              'pl': 'Solar System Planets',
              'ex': 'Exoplanets',
              'li': 'Lightning',
              'hI': 'HI'
            }
    
    categories, im_names, sizes = [], [], []
    for i, label in enumerate(myorder):
        if count[i] != 0:
            categories.append(CATEGORIES[label])
            im_names.append(label)
            sizes.append(count[i])
    
    fig, ax = pl.subplots(figsize=(12, 9))
    wedges, labels, num_labels = ax.pie(sizes, labels=categories, autopct='%1.0f%%', startangle=158,
                            counterclock=False,
                            pctdistance=1.1, labeldistance=1.3,
                            textprops={'fontsize': 10, 'weight': 'normal'})

    # x,y position of the wedge image in canvas units
    positions = [(0.2,0.01), (-0.05,-0.5), (0.13,-0.54), (-0.4,-0.5), (-0.5,-0.3), (-0.6,-0.1), (-0.49,0.06), (-0.5,0.12), (-0.5,0.15), (-0.5,0.15)]
    # Wedge image zoom factors
    zooms = [1.2, 0.55, 0.8, 0.2, 0.4, 0.4, 0.18, 0.2, 0.5, 0.5]

    def img_to_pie(fn, wedge, xy, zoom=1, ax = None):
        if ax==None: ax=pl.gca()
        im = pl.imread(fn, format='jpeg')
        path = wedge.get_path()
        patch = PathPatch(path, facecolor='none')
        ax.add_patch(patch)
        imagebox = OffsetImage(im, zoom=zoom, clip_path=patch, zorder=-10)
        ab = AnnotationBbox(imagebox, xy, xycoords='data', pad=0, frameon=False)
        ax.add_artist(ab)
    '''
    log.info("Len wedges: {}".format(len(wedges)))
    log.info("Len pos: {}".format(len(positions)))
    log.info("Len zoom: {}".format(len(zooms)))
    log.info("Len siz: {}".format(len(sizes)))
    '''
    for i in range(len(sizes)):
        fn = "scripts/science-categories/images/{}.jpeg".format(im_names[i])
        img_to_pie(fn, wedges[i], xy=positions[i], zoom=zooms[i])
        #wedges[i].set_zorder(100)

    ax.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    ttl = pl.title("Papers per Science Area", fontsize=36)
    ttl.set_position([.2, 1.02])
    #ax.text(1, 0.005, 'Last update: 2019 Feb 21',
    #        horizontalalignment='right',
    #        verticalalignment='top',
    #        transform=ax.transAxes,
    #        fontsize=14)
    pl.tight_layout(rect=[0.03, 0.03, 0.97, 0.97])
    #pl.show()
    log.info("Writing {}".format(output_fn))
    pl.savefig(output_fn, dpi=dpi)
    pl.close()

def plot_author_count(db,
                      output_fn='apub-author-count.pdf',
                      first_year=2017,
                      dpi=200,
                      instrument='LOFAR',
                      cumulative=True,
                      colors=["#3498db", "#95a5a6"]):
    """Plots a line chart showing the number of authors over time.

    Parameters
    ----------
    db : `PublicationDB` object
        Data to plot.

    output_fn : str
        Output filename of the plot.

    first_year : int
        What year should the plot start?

    dpi : float
        Output resolution.

    instrument : str
        Instrument to query the database for (LOFAR, WSRT...)

    cumulative : boolean
        Plot cumulative or by year plot.

    colors : list of str
        Define the facecolor for [lofar, extrapolation]
    """
    # Obtain the dictionary which provides the annual counts
    current_year = datetime.datetime.now().year
    if cumulative:
        current_year = current_year - 1 # to limit to end of past year
    # Now make the actual plot
    fig = pl.figure()
    ax = fig.add_subplot(111)

    cumulative_years = []
    paper_counts = []
    author_counts, first_author_counts = [], []

    for year in range(first_year, current_year):
        cumulative_years.append(year)
        if cumulative:
            metrics = db.get_metrics(cumulative_years, instrument)
        else:
            metrics = db.get_metrics(year, instrument)
        paper_counts.append(metrics['publication_count'])
        author_counts.append(metrics['author_count'])
        first_author_counts.append(metrics['first_author_count'])
    if cumulative:
        # +1 because the stats for all of e.g. 2018 should show at Jan 1, 2019
        ax.plot([y+1 for y in cumulative_years], paper_counts, label="{} publications".format(instrument), lw=9)
        #ax.plot(cumulative_years, author_counts, label="Unique authors", lw=6)
        ax.plot([y+1 for y in cumulative_years], first_author_counts, label="Unique first authors", lw=3)
    else:
        #ax.plot(cumulative_years, paper_counts, label="{} publications".format(instrument), lw=9)
        #ax.plot(cumulative_years, author_counts, label="Unique authors", lw=6)
        #ax.plot(cumulative_years, first_author_counts, label="{} unique first authors".format(instrument), lw=9, color="#3498db")
        ax.plot(cumulative_years, author_counts, label="{} unique authors".format(instrument), lw=9, color="#3498db")

    # Aesthetics
    #pl.title("LOFAR scientific output over time")
    if cumulative:
        pl.ylabel("Cumulative count")
    else:
        pl.ylabel("Count")
    ax.get_xaxis().get_major_formatter().set_useOffset(False)
    pl.xticks(range(first_year - 1, current_year + 1))
    if cumulative:
        pl.xlim([first_year + 0.5, current_year + 0.5])
    else:
        pl.xlim([first_year - 0.5, current_year + 0.5])
    #print("Cumulative years: ", cumulative_years)
    pl.ylim([0, 1.05*np.max(paper_counts)])
    #pl.ylim([0, 1.05*np.max(author_counts)])
    pl.legend(bbox_to_anchor=(0.03, 0.95, 0.95, 0.),
              loc="upper left",
              ncol=1,
              borderaxespad=0.,
              handlelength=1.5,
              frameon=True)
    # Disable spines
    ax.spines["left"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    # Only show bottom and left ticks
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    # Only show horizontal grid lines
    ax.grid(axis='y')
    pl.tight_layout(rect=(0, 0, 1, 0.98), h_pad=1.5)
    log.info("Writing {}".format(output_fn))
    pl.savefig(output_fn, dpi=dpi)
    pl.close()

def plot_chord_diagram(db,
                      output_fn='apub-choord-diagram.pdf',
                      first_year=2011,
                      last_year=None,
                      dpi=200,
                      instrument='LOFAR',
                      colors=["#3498db", "#95a5a6"]):
    """Plots a chord diagram of author relations.

    Parameters
    ----------
    db : `PublicationDB` object
        Data to plot.

    output_fn : str
        Output filename of the plot.

    first_year : int
        What year should the plot start?

    dpi : float
        Output resolution.

    instrument : str
        Instrument to query the database for (LOFAR, WSRT...)

    colors : list of str
        Define the facecolor for [lofar, extrapolation]
    """
    locations = []
    try:
        import pandas as pd
    except ImportError:
        log.info('ERROR: pandas needs to be installed for this feature.')
    
    years = np.arange(first_year, last_year + 1).tolist()
    db = apub.PublicationDB()
    all_publications = db.get_all(instrument=instrument, year=years)
    log.info(f"Fetched {len(all_publications)} publications.")
    for publication in all_publications:
        #if (publication['author_norm'][0]).startswith('Abbott'):
        #    continue
        affiliations = publication['aff']
        # Use the first three authors
        for aff in affiliations:
            aff_suffix = ",".join(aff.split(";")[-1].split(",")[-1:]).strip(" ;,-")
            locations.append(aff_suffix)
    unique_locations = nltk.FreqDist(locations)
    log.info("Found {} unique locations".format(len(unique_locations)))
    keys = []
    for loc in unique_locations:
        if len(loc.split(' ')) <= 2:
            if loc.startswith('/') or loc.startswith('the') or loc.startswith('UAE') or loc.endswith('.') or loc.endswith('2')\
            or loc.endswith('5') or loc.endswith('3') or len(loc.split('-')) > 1 or loc.startswith('McGill') or loc.startswith('ROC')\
            or loc.startswith('NM') or loc.endswith('Netherland') or loc.startswith('6') or loc.startswith('2') or loc.startswith('NH')\
            or loc.endswith('Federation') or loc.endswith('U') or loc.startswith('Bologna') or loc.startswith('Manchester') or loc.startswith('México')\
            or loc.startswith('United States') or loc.startswith('Crimea') or loc.startswith('RSA') or loc.startswith('Bonn') or loc.startswith('USVI')\
            or loc.startswith('PR') or loc.startswith('Czech ') or loc.startswith('Hong') or loc == "" or loc.startswith('England') or loc.startswith('Netherlands')\
            or unique_locations.get(loc) < 10:
                continue
            keys.append(loc)
    dim = len(keys)
    corr_mat = np.zeros(shape=(dim, dim), dtype=np.uint8)
    spreadsheet_chord = []
    for publication in all_publications:
        if (publication['author_norm'][0]).startswith('Abbott'):
            print(publication['author_norm'][0])
            print('Break','\n')
            continue
        #else:
        #    print('Not Abbott')
        affiliations = publication['aff']
        first_auth_first_aff_country = affiliations[0].split(';')[0].split(',')[-1].strip(" ;,-")
        ind_first_auth_li = [i for i, x in enumerate(keys) if x == first_auth_first_aff_country]
        if len(ind_first_auth_li) > 0:
            ind_first_auth = ind_first_auth_li[0]
        else:
            continue
        for aff in affiliations[1:]:
            co_auth_first_aff_country = aff.split(';')[0].split(',')[-1].strip(" ;,-")
            ind_co_auth_li = [i for i, x in enumerate(keys) if x == co_auth_first_aff_country]
            if len(ind_co_auth_li) > 0:
                ind_co_auth = ind_co_auth_li[0]
                corr_mat[ind_first_auth, ind_co_auth] += 1
                myrow = collections.OrderedDict([
                ('Author affiliation', first_auth_first_aff_country),
                ('Co-author affiliation', co_auth_first_aff_country)
                ])
                spreadsheet_chord.append(myrow)
            else:
                continue

    auth_country_sum = np.ndarray.sum(corr_mat, axis=0)

    fig = pl.figure()
    ax = fig.add_subplot(111)

    chord_diagram(corr_mat, keys, ax=ax, min_chord_width=500, fontsize=16, rotate_names=True)
    #chord_diagram(corr_mat, keys, ax=ax2, min_chord_width=200)

    pl.savefig(output_fn, dpi=dpi)
    pl.close()

    #
    # For Grafana, we need to export a CSV dile containing two columns per paper: first author affiliation country and all co-authors affiliation country.
    # The first column content is copied for all co-authors.
    #

    output_fn = 'apub-chord-diagram.csv'
    log.info('Writing {}'.format(output_fn))
    pd.DataFrame(spreadsheet_chord).to_csv(output_fn, index=False, encoding='utf-8')
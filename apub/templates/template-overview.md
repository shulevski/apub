Title: Publications
Save_as: publications.html

[TOC]

We request that scientific publications using data obtained from ASTRON instruments or surveys include one of the following acknowledgments:

*LOFAR, the Low Frequency Array designed and constructed by ASTRON, has facilities in several countries, that are owned by various parties (each with their own funding sources), and that are collectively operated by the International LOFAR Telescope (ILT) foundation under a joint scientific policy.*

*This work makes use of data from the Apertif system installed at the Westerbork Synthesis Radio Telescope owned by ASTRON.*

*Data taken with the Westerbork Synthesis Radio Telescope were the basis of this work. It is operated by ASTRON (Netherlands Institute for
Radio Astronomy) with support from the Netherlands Foundation for Scientific Research (NWO).*

## Publication database

The SDCO curates a list of scientific publications
pertaining to ASTRON.
The database contains {{ metrics["publication_count"] }} publications,
of which {{ metrics["refereed_count"] }} are peer-reviewed.
It demonstrates the important impact of LOFAR data
on astronomical research.

You can access the publication list by instrument:

 * <a href="apub-lofar.html">LOFAR publications &raquo;</a>

Or by topic:

 * <a href="apub-imaging.html">Radio publications &raquo;</a>
 * <a href="apub-time-domain.html">Astrophysics publications &raquo;</a>

You also can mine the database yourself by accessing a spreadsheet of the publications: [astron-publications.xls](/data/astron-publications.xls)

If you spot an error, such as a missing entry,
please get in touch or open an issue in the <a href="https://support.astron.nl/sdchelpdesk">SDCO Helpdesk</a>.

Last update: {{ now.strftime('%d %b %Y') }}.

<hr/>

## Breakdown by year & instrument

The graph below shows the number of publications as a function
of year and instrument.
The publication count for LOFAR is {{ metrics["LOFAR_count"] }}
while that of WSRT is {{ metrics["WSRT_count"] }}
and of APERTIF: {{ metrics["APERTIF_count"] }}.
The number of refereed papers is {{ metrics["LOFAR_refereed_count"]}} for LOFAR and {{ metrics["WSRT_refereed_count"] }} for WSRT.

[![Publication rate by instrument and year](/images/apub/apub-publication-rate-without-extrapolation.png)](/images/apub/apub-publication-rate-without-extrapolation.png)

<hr/>

## Number of authors

The entries in the publication database have been authored and co-authored
by a total of {{ metrics["author_count"] }} unique author names.
We define the author name at "last name, first initial".
Slight variations in the spelling may increase the number of unique names,
while common names with the same first initial may result in undercounting.

[![Number of LOFAR, WSRT and APERTIF papers and unique authors over time](/images/apub/apub-author-count.png)](/images/apub/apub-author-count.png)

<hr/>

## Breakdown by subject

ASTRON data have been used for scientific applications
across radio astronomy research.
<!--
While {{ metrics["Imaging_count"] }} works relate to interferometry
({{ "%.0f"|format(metrics["Imaging_fraction"]*100) }}%),
a total of {{ metrics["Time Domain_count"] }}
pertain to other areas of radio astronomy
({{ "%.0f"|format(metrics["Time Domain_fraction"]*100) }}%).
-->
The graph below details the breakdown of ASTRON papers by science topic.

[![Publications by subject](/plots/apub-piechart.png)](/plots/apub-piechart.png)

<hr/>

## Most-cited publications

ASTRON publications have cumulatively been cited
{{ metrics["citation_count"] }} times.
The list below shows the most-cited publications,
based on the citation count obtained from NASA ADS.

{% for art in most_cited %}
{{loop.index}}. {{art['title'][0].upper()}}  
{{ ', '.join(art['author'][0:3]) }}{% if art['author']|length > 3 %}, et al.{% endif %}    
{{ '[{bibcode}](http://adsabs.harvard.edu/abs/{bibcode})'.format(**art) }}
<span class="badge">{{ art['citation_count'] }} citations</span>
{% endfor -%}

<hr/>

<!-- 
## Most-read publications

The read count shown below is obtained from the ADS API
and indicates the number of times the article has been downloaded
within the last 90 days.

{% for art in most_read %}
{{loop.index}}. {{art['title'][0].upper()}}  
{{ ', '.join(art['author'][0:3]) }}{% if art['author']|length > 3 %}, et al.{% endif %}    
{{ '[{bibcode}](http://adsabs.harvard.edu/abs/{bibcode})'.format(**art) }}
<span class="badge">{{ "%.0f"|format(art['read_count']) }} reads</span>
{% endfor -%}

<hr/>

-->

## Most-active authors

Below we list the most-active authors, defined as those with six or more first-author publications in our database.

{% for author in most_active_first_authors %}
 * {{author[0]}} ({{ "%.0f"|format(author[1]) }} publications)
{% endfor -%}

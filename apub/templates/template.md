Title: {{ title }}
Save_as: {{ save_as }}

[TOC]
{% for month in articles %}

{{ month }}
{{ "-" * month|length }}
{% for art in articles[month] %}
{{loop.index}}. {{ ', '.join(art['author'][0:3]) }}{% if art['author']|length > 3 %}, et al.{% endif %}: [{{ art['title'][0] }}](http://adsabs.harvard.edu/abs/{{ art['bibcode'] }}), {{ art['year'] }}, {{ art['pub'] }}, {{ art['volume'] }}, {{ art['page'][0] }}
{% endfor -%}
{% endfor -%}
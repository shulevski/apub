"""Build and maintain a database of ASTRON publications.
"""
from __future__ import print_function, division, unicode_literals

# Standard library
import os
import glob
import re
import sys
import json
import datetime
import argparse
import collections
import sqlite3 as sql
import numpy as np
import nltk

try:
    import ads
except Exception:
    ads = None

# External dependencies
import jinja2
from six.moves import input  # needed to support Python 2
from astropy import log
from astropy.utils.console import ProgressBar

from . import plot, PACKAGEDIR, INSTRUMENT, SCIENCE, MODE
from matplotlib import pyplot as pl

# Where is the default location of the SQLite database?
DEFAULT_DB = os.path.expanduser("data/.apub.db")

# Which metadata fields do we want to retrieve from the ADS API?
# (basically everything apart from 'body' to reduce data volume)
FIELDS = ['date', 'pub', 'id', 'volume', 'links_data', 'citation', 'doi',
          'eid', 'keyword_schema', 'citation_count', 'data', 'data_facet',
          'year', 'identifier', 'keyword_norm', 'reference', 'abstract', 'recid',
          'alternate_bibcode', 'arxiv_class', 'bibcode', 'first_author_norm',
          'pubdate', 'reader', 'doctype', 'doctype_facet_hier', 'title', 'pub_raw', 'property',
          'author', 'email', 'orcid', 'keyword', 'author_norm',
          'cite_read_boost', 'database', 'classic_factor', 'ack', 'page',
          'first_author', 'reader', 'read_count', 'indexstamp', 'issue', 'keyword_facet',
          'aff', 'facility', 'simbid']


class Highlight:
    """Defines colors for highlighting words in the terminal."""
    RED = "\033[4;31m"
    GREEN = "\033[4;32m"
    YELLOW = "\033[4;33m"
    BLUE = "\033[4;34m"
    PURPLE = "\033[4;35m"
    CYAN = "\033[4;36m"
    END = '\033[0m'


class PublicationDB(object):
    """Class wrapping the SQLite database containing the publications.

    Parameters
    ----------
    filename : str
        Path to the SQLite database file.
    """
    def __init__(self, filename=DEFAULT_DB):
        self.filename = filename
        self.con = sql.connect(filename)
        pubs_table_exists = self.con.execute(
                                """
                                   SELECT COUNT(*) FROM sqlite_master
                                   WHERE type='table' AND name='pubs';
                                """).fetchone()[0]
        if not pubs_table_exists:
            self.create_table()

    def create_table(self):
        self.con.execute("""CREATE TABLE pubs(
                                id UNIQUE,
                                bibcode UNIQUE,
                                year,
                                month,
                                date,
                                instrument,
                                ksp,
                                science,
                                metrics,
                                mode)""")

    def add(self, article, instrument="LOFAR", ksp="SURVEYS", science="Extragalactic", mode="Interferometric(IF)"):
        """Adds a single article object to the database.
        Parameters
        ----------
        article : `ads.Article` object.
            An article object as returned by `ads.SearchQuery`.
        """
        log.debug('Ingesting {}'.format(article.bibcode))
        # Also store the extra metadata in the json string
        month = article.pubdate[0:7]
        article._raw['instrument'] = instrument
        article._raw['ksp'] = ksp
        article._raw['science'] = science
        article._raw['mode'] = mode
        try:
            cur = self.con.execute("INSERT INTO pubs "
                                   "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                   [article.id, article.bibcode,
                                    article.year, month, article.pubdate,
                                    instrument, ksp, science, json.dumps(article._raw),
                                    mode])
            log.info('Inserted {} row(s).'.format(cur.rowcount))
            self.con.commit()
        except sql.IntegrityError:
            log.warning('{} was already ingested.'.format(article.bibcode))

    def add_interactively(self, article, statusmsg=""):
        """Adds an article by prompting the user for the classification.

        Parameters
        ----------
        article : `ads.Article` object
        """
        # Do not show an article that is already in the database
        if article in self:
            log.info("{} is already in the database "
                     "-- skipping.".format(article.bibcode))
            return

        # Print paper information to stdout
        print(chr(27) + "[2J")  # Clear screen
        print(statusmsg)
        display_abstract(article._raw)

        # Prompt the user to classify the paper by mission and science
        print("=> LOFAR [1], WSRT [2], APERTIF [3], unrelated [4], or skip [any key]? ",
              end="")
        prompt = input()
        if prompt == "1":
            instrument = "LOFAR"
        elif prompt == "2":
            instrument = "WSRT"
        elif prompt == "3":
            instrument = "APERTIF"
        elif prompt == "4":
            instrument = "unrelated"
        else:
            return
        print(instrument)

        # Now classify by KSP
        ksp = ""
        if instrument != "unrelated":
            print('=> SURVEYS [1], PULSARS & TRANSIENTS [2], COSMIC RAYS [3], TECHNICAL [4], EoR [5], MAGNETISM [6], SOLAR [7] or OTHER [8]? ', end='')
            prompt = input()
            if prompt == "1":
                ksp = "SURVEYS"
            elif prompt == "2":
                ksp = "PULSARS & TRANSIENTS"
            elif prompt == "3":
                ksp = "COSMIC RAYS"
            elif prompt == "4":
                ksp = "TECHNICAL"
            elif prompt == "5":
                ksp = "EoR"
            elif prompt == "6":
                ksp = "MAGNETISM"
            elif prompt == "7":
                ksp = "SOLAR"
            elif prompt == "8":
                ksp = "OTHER"
            print(ksp)

        # Now classify by science area
        science = ""
        if instrument != "unrelated":
            print('=> Extragalactic [1], Galaxy [2], Pulsars & Transients [3], Instrumentation & Methods [4], Ionosphere [5], Cosmic Rays [6],\n')
            print('Solar and Space Weather [7], Solar System Planets [8], Exo-planets [9], Lightning [10] or HI [11]? ', end='')
            prompt = input()
            if prompt == "1":
                science = "Extragalactic"
            elif prompt == "2":
                science = "Galaxy"
            elif prompt == "3":
                science = "Pulsars & Transients"
            elif prompt == "4":
                science = "Instrumentation & Methods"
            elif prompt == "5":
                science = "Ionosphere"
            elif prompt == "6":
                science = "Cosmic Rays"
            elif prompt == "7":
                science = "Solar"
            elif prompt == "8":
                science = "Solar System Planets"
            elif prompt == "9":
                science = "Exo-planets"
            elif prompt == "10":
                science = "Lightning"
            elif prompt == "11":
                science = "HI"
            print(science)

        # Now classify by mode
        mode = ""
        if instrument != "unrelated":
            print('=> Interferometric(IF) [1], Beam-formed(BF) [2], TBB [3], IF+BF [4], Single Station [5], Other [6]? ', end='')
            prompt = input()
            if prompt == "1":
                mode = "Interferometric(IF)"
            elif prompt == "2":
                mode = "Beam-formed(BF)"
            elif prompt == "3":
                mode = "TBB"
            elif prompt == "4":
                mode = "IF+BF"
            elif prompt == "5":
                mode = "Single Station"
            elif prompt == "6":
                mode = "Other"
            print(mode)

        self.add(article, instrument=instrument, ksp=ksp, science=science, mode=mode)

    def add_by_bibcode(self, bibcode, interactive=False, **kwargs):
        if ads is None:
            log.error("This action requires the ADS key to be setup.")
            return

        q = ads.SearchQuery(q="identifier:{}".format(bibcode), fl=FIELDS)
        for article in q:
            # Print useful warnings
            if bibcode != article.bibcode:
                log.warning("Requested {} but ADS API returned {}".format(bibcode, article.bibcode))
            if interactive and ('NONARTICLE' in article.property):
                # Note: data products are sometimes tagged as NONARTICLE
                log.warning("{} is not an article.".format(article.bibcode))

            if article in self:
                log.warning("{} is already in the db.".format(article.bibcode))
            else:
                if interactive:
                    self.add_interactively(article)
                else:
                    self.add(article, **kwargs)

    def delete_by_bibcode(self, bibcode):
        cur = self.con.execute("DELETE FROM pubs WHERE bibcode = ?;", [bibcode])
        log.info('Deleted {} row(s).'.format(cur.rowcount))
        self.con.commit()

    def __contains__(self, article):
        count = self.con.execute("SELECT COUNT(*) FROM pubs WHERE id = ? OR bibcode = ?;",
                                 [article.id, article.bibcode]).fetchone()[0]
        return bool(count)

    def query(self, instrument=None, science=None, ksp=None, year=None, mode=None):
        """Query the database by instrument and/or science and/or year.

        Parameters
        ----------
        instrument : str
            'LOFAR'
        science : str
            'Imaging' or 'Time Domain'
        year : int or list of int
            Examples: 2009, 2010, [2009, 2010], ...
        mode : str
            'Interferometric(IF)', 'Beam-formed(BF)', 'TBB', 'Single Station', 'Other', 'IF+BF'
        Returns
        -------
        rows : list
            List of SQLite result rows.
        """
        # Build the query
        if instrument is None:
            where = "(instrument = 'LOFAR') "
        else:
            where = "(instrument = '{}') ".format(instrument)

        if science is not None:
            where += " AND science = '{}' ".format(science)

        if mode is not None:
            where += " AND mode = '{}' ".format(mode)

        if year is not None:
            if isinstance(year, (list, tuple)):  # Multiple years?
                str_year = ["'{}'".format(y) for y in year]
                where += " AND year IN (" + ", ".join(str_year) + ")"
            else:
                where += " AND year = '{}' ".format(year)
        
        cur = self.con.execute("SELECT year, month, metrics, bibcode "
                               "FROM pubs "
                               "WHERE {} "
                               "ORDER BY date DESC; ".format(where))
        return cur.fetchall()

    def get_metadata(self, bibcode):
        """Returns a dictionary of the raw metadata given a bibcode."""
        cur = self.con.execute("SELECT metrics FROM pubs WHERE bibcode = ?;", [bibcode])
        return json.loads(cur.fetchone()[0])

    def to_markdown(self, title="Publications",
                    group_by_month=False, save_as=None, **kwargs):
        """Returns the publication list in markdown format.
        """
        if group_by_month:
            group_idx = 1
        else:
            group_idx = 0  # by year

        articles = collections.OrderedDict({})
        for row in self.query(**kwargs):
            group = row[group_idx]
            if group.endswith("-00"):
                group = group[:-3] + "-01"
            if group not in articles:
                articles[group] = []
            art = json.loads(row[2])
            # The markdown template depends on "property" being iterable
            if art["property"] is None:
                art["property"] = []
            articles[group].append(art)

        templatedir = os.path.join(PACKAGEDIR, 'templates')
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(templatedir))
        template = env.get_template('template.md')
        markdown = template.render(title=title, save_as=save_as,
                                   articles=articles)
        if sys.version_info >= (3, 0):
            return markdown  # Python 3
        else:
            return markdown.encode("utf-8")  # Python 2

    def save_markdown(self, output_fn, **kwargs):
        """Saves the database to a text file in markdown format.

        Parameters
        ----------
        output_fn : str
            Path of the file to write.
        """
        markdown = self.to_markdown(save_as=output_fn.replace("md", "html"),
                                    **kwargs)
        log.info('Writing {}'.format(output_fn))
        f = open(output_fn, 'w')
        f.write(markdown)
        f.close()

    def plot(self, year_from, year_to):
        """Saves beautiful plot of the database."""
        for extension in ['png']:
            #plot.plot_by_year(self,
            #                  "apub-publication-rate-lofar.{}".format(extension),
            #                  first_year=2017,
            #                  instrument='LOFAR')
            
            plot.plot_by_year(self,
                              "apub-publication-rate-lofar.{}".format(extension),
                              first_year = year_from,
                              current_year = year_to,
                              instrument='LOFAR')
            '''
            plot.plot_by_year(self,
                              "apub-publication-rate-wsrt.{}".format(extension),
                              instrument='WSRT')
            plot.plot_by_year(self,
                              "apub-publication-rate-apertif.{}".format(extension),
                              first_year=2018,
                              instrument='APERTIF')
            plot.plot_by_year(self,
                              "apub-publication-rate-without-extrapolation.{}".format(extension),
                              first_year=2017,
                              instrument='APERTIF',
                              extrapolate=False)
            '''
            plot.plot_science_piechart(self,
                                       "apub-piechart.{}".format(extension),
                                       first_year = year_from,
                                       last_year = year_to)
            
            plot.plot_chord_diagram(self,
                                    "apub-chord-diagram.{}".format(extension),
                                    first_year = year_from,
                                    last_year = year_to,
                                    instrument='LOFAR')
            #'''
            plot.plot_author_count(self,
                                   "apub-author-count.{}".format(extension),
                                   first_year=2017,
                                   instrument='LOFAR',
                                   cumulative=True)
            #'''
    def get_metrics(self, year=None, instrument=None):
        """Returns a dictionary of overall publication statistics.
        The metrics include:
        * # of publications since XX.
        * # of unique author surnames.
        * # of citations.
        * # of peer-reviewed pubs.
        * # of LOFAR,WSRT,APERTIF,Imaging,Time Domain.
        """
        metrics = {
                   "publication_count": 0,
                   "LOFAR_count": 0,
                   "WSRT_count": 0,
                   "APERTIF_count": 0,
                   "refereed_count": 0,
                   "LOFAR_refereed_count": 0,
                   "WSRT_refereed_count": 0,
                   "APERTIF_refereed_count": 0,
                   "citation_count": 0,
                   "LOFAR_citation_count": 0,
                   "WSRT_citation_count": 0,
                   "APERTIF_citation_count": 0
                   }
        authors, first_authors = [], []
        lofar_authors, wsrt_authors, apertif_authors = [], [], []
        lofar_first_authors, wsrt_first_authors, apertif_first_authors = [], [], []
        
        for article in self.query(year=year, instrument=instrument):   
            api_response = article[2]
            js = json.loads(api_response)
            metrics["publication_count"] += 1
            metrics["{}_count".format(js["instrument"])] += 1
            try:
                metrics["{}_count".format(js["science"])] += 1
            except KeyError:
                log.warning("{}: no science category".format(js["bibcode"]))
            authors.extend(js["author_norm"])
            first_authors.append(js["first_author_norm"])
            if js["instrument"] == 'LOFAR':
                lofar_authors.extend(js["author_norm"])
                lofar_first_authors.append(js["first_author_norm"])
            elif js["instrument"] == 'WSRT':
                wsrt_authors.extend(js["author_norm"])
                wsrt_first_authors.append(js["first_author_norm"])
            else:
                apertif_authors.extend(js["author_norm"])
                apertif_first_authors.append(js["first_author_norm"])
            try:
                if "REFEREED" in js["property"]:
                    metrics["refereed_count"] += 1
                    metrics["{}_refereed_count".format(js["instrument"])] += 1
            except TypeError:  # proprety is None
                pass
            try:
                metrics["citation_count"] += js["citation_count"]
                metrics["{}_citation_count".format(js["instrument"])] += js["citation_count"]
            except (KeyError, TypeError):
                log.warning("{}: no citation_count".format(js["bibcode"]))
        metrics["author_count"] = np.unique(authors).size
        metrics["first_author_count"] = np.unique(first_authors).size
        metrics["LOFAR_author_count"] = np.unique(lofar_authors).size
        metrics["LOFAR_first_author_count"] = np.unique(lofar_first_authors).size
        metrics["WSRT_author_count"] = np.unique(wsrt_authors).size
        metrics["WSRT_first_author_count"] = np.unique(wsrt_first_authors).size
        metrics["APERTIF_author_count"] = np.unique(apertif_authors).size
        metrics["APERTIF_first_author_count"] = np.unique(apertif_first_authors).size
        # Also compute fractions
        for frac in ["LOFAR", "WSRT", "APERTIF"]:
            metrics[frac+"_fraction"] = metrics[frac+"_count"] / metrics["publication_count"]
        return metrics

    def get_all(self, instrument=None, ksp=None, science=None, year=None, mode=None):
        """Returns a list of dictionaries, one entry per publication."""
        articles = self.query(instrument=instrument, ksp=ksp, science=science, year=year, mode=mode)
        return [json.loads(art[2]) for art in articles]

    def get_most_cited(self, instrument=None, ksp=None, science=None, mode=None, top=10):
        """Returns the most-cited publications."""
        bibcodes, citations = [], []
        articles = self.query(instrument=instrument, ksp=ksp, mode=mode, science=science)
        for article in articles:
            api_response = article[2]
            js = json.loads(api_response)
            bibcodes.append(article[3])
            if js["citation_count"] is None:
                citations.append(0)
            else:
                citations.append(js["citation_count"])
        idx_top = np.argsort(citations)[::-1][0:top]
        return [json.loads(articles[idx][2]) for idx in idx_top]

    def get_most_read(self, instrument=None, ksp=None, science=None, mode=None, top=10):
        """Returns the most-cited publications."""
        bibcodes, citations = [], []
        articles = self.query(instrument=instrument, ksp=ksp, science=science, mode=mode)
        for article in articles:
            api_response = article[2]
            js = json.loads(api_response)
            bibcodes.append(article[3])
            citations.append(js["read_count"])
        idx_top = np.argsort(citations)[::-1][0:top]
        return [json.loads(articles[idx][2]) for idx in idx_top]

    def get_most_active_first_authors(self, min_papers=6):
        """Returns names and paper counts of the most active first authors."""
        articles = self.query()
        authors = {}
        for article in articles:
            api_response = article[2]
            js = json.loads(api_response)
            first_author = js["first_author_norm"]
            try:
                authors[first_author] += 1
            except KeyError:
                authors[first_author] = 1
        names = np.array(list(authors.keys()))
        paper_count = np.array(list(authors.values()))
        idx_top = np.argsort(paper_count)[::-1]
        mask = paper_count[idx_top] >= min_papers
        return zip(names[idx_top], paper_count[idx_top[mask]])

    def get_all_authors(self, top=20):
        articles = self.query()
        authors = {}
        for article in articles:
            api_response = article[2]
            js = json.loads(api_response)
            for auth in js["author_norm"]:
                try:
                    authors[auth] += 1
                except KeyError:
                    authors[auth] = 1
        names = np.array(list(authors.keys()))
        paper_count = np.array(list(authors.values()))
        idx_top = np.argsort(paper_count)[::-1][:top]
        return names[idx_top], paper_count[idx_top]

    def get_annual_publication_count(self, year_begin=2009, year_end=datetime.datetime.now().year):
        """Returns a dict containing the number of publications per year per instrument.

        Parameters
        ----------
        year_begin : int
            Year to start counting. (default: 2009)

        year_end : int
            Year to end counting. (default: current year)
        """
        # Initialize a dictionary to contain the data to plot
        result = {}
        for instrument in INSTRUMENT:
            result[instrument] = {}
            for year in range(year_begin, year_end + 1):
                result[instrument][year] = 0
            cur = self.con.execute("SELECT year, COUNT(*) FROM pubs "
                                   "WHERE instrument = ? "
                                   "AND year >= '2010' "
                                   "GROUP BY year;",
                                   [instrument])
            rows = list(cur.fetchall())
            for row in rows:
                if int(row[0]) <= year_end:
                    result[instrument][int(row[0])] = row[1]
        # Also combine counts
        result['both'] = {}
        for year in range(year_begin, year_end + 1):
            result['both'][year] = sum(result[instrument][year] for instrument in INSTRUMENT)
        return result

    def get_annual_publication_count_cumulative(self, year_begin=2009, year_end=datetime.datetime.now().year):
        """Returns a dict containing the cumulative number of publications per year per instrument.

        Parameters
        ----------
        year_begin : int
            Year to start counting. (default: 2009)

        year_end : int
            Year to end counting. (default: current year)
        """
        # Initialize a dictionary to contain the data to plot
        result = {}
        for instrument in INSTRUMENT:
            result[instrument] = {}
            for year in range(year_begin, year_end + 1):
                cur = self.con.execute("SELECT COUNT(*) FROM pubs "
                                       "WHERE instrument = ? "
                                       "AND year <= ?;",
                                       [mission, str(year)])
                result[instrument][year] = cur.fetchone()[0]
        # Also combine counts
        result['both'] = {}
        for year in range(year_begin, year_end + 1):
            result['both'][year] = sum(result[instrument][year] for instrument in INSTRUMENT)
        return result

    def update(self, month=None,
               #exclude=['keplerian', 'johannes', 'k<sub>2</sub>',
               #        "kepler equation", "kepler's equation", "xmm-newton",
               #         "kepler's law", "kepler's third law", "kepler problem",
               #         "kepler crater", "kepler's supernova", "kepler's snr"]
                exclude=["VLT/SPHERE"]
               ):
        """Query ADS for new publications.

        Parameters
        ----------
        month : str
            Of the form "YYYY-MM".

        exclude : list of str
            Ignore articles if they contain any of the strings given
            in this list. (Case-insensitive.)
        """
        if ads is None:
            log.error("This action requires the ADS key to be setup.")
            return

        print(Highlight.YELLOW +
              "Reminder: did you `git pull` apub before running "
              "this command? [y/n] " +
              Highlight.END,
              end='')
        if input() == 'n':
            return

        if month is None:
            month = datetime.datetime.now().strftime("%Y-%m")

        # First show all the papers with the various instruments funding messages in the ack
        log.info("Querying ADS for acknowledgements (month={}).".format(month))
        database = "astronomy"
        qry = ads.SearchQuery(q="""(ack:"LOFAR"
                                    OR ack:"International LOFAR Telescope"
                                    OR ack:"designed and constructed by ASTRON")
                                    OR ack:"WSRT"
                                    OR ack:"Westerbork Synthesis Radio Telescope"
                                    OR ack:"operated by ASTRON"
                                    OR ack:"Apertif system"
                                    OR ack:"owned by ASTRON"
                                   -ack:"partial support from"
                                   pubdate:"{}"
                                   database:"{}"
                                """.format(month, database),
                              fl=FIELDS,
                              rows=9999999999)
        articles = list(qry)
        for idx, article in enumerate(articles):
            statusmsg = ("Showing article {} out of {} that mentions ASTRON instruments "
                         "in the acknowledgements.\n\n".format(
                            idx+1, len(articles)))
            self.add_interactively(article, statusmsg=statusmsg)

        # Then search for keywords in the title and abstracts
        log.info("Querying ADS for titles and abstracts "
                 "(month={}).".format(month))
        qry = ads.SearchQuery(q="""(
                                    abs:"LOFAR"
                                    OR abs:"WSRT"
                                    OR abs:"Westerbork Synthesis Radio Telescope"
                                    OR abs:"Apertif"
                                    OR abs:"APERTIF"
                                    OR title:"LOFAR"
                                    OR title:"WSRT"
                                    OR title:"Westerbork Synthesis Radio Telescope"
                                    OR title:"Apertif"
                                    OR title:"APERTIF"
                                    OR full:"designed and constructed by ASTRON"
                                    OR full:"operated by ASTRON"
                                    OR full:"owned by ASTRON"
                                    )
                                   pubdate:"{}"
                                   database:"{}"
                                """.format(month, database),
                              fl=FIELDS,
                              rows=9999999999)
        articles = list(qry)

        for idx, article in enumerate(articles):
            # Ignore articles without abstract
            if not hasattr(article, 'abstract') or article.abstract is None:
                continue
            abstract_lower = article.abstract.lower()

            ignore = False

            # Ignore articles containing any of the excluded terms
            for term in exclude:
                if term.lower() in abstract_lower:
                    ignore = True

            # Ignore articles already in the database
            if article in self:
                ignore = True

            # Ignore all the unrefereed non-arxiv stuff
            try:
                if "NOT REFEREED" in article.property and article.pub.lower() != "arxiv e-prints":
                    ignore = True
            except (AttributeError, TypeError, ads.exceptions.APIResponseError):
                pass  # no .pub attribute or .property not iterable

            # Ignore proposals and cospar abstracts
            if ".prop." in article.bibcode or "cosp.." in article.bibcode:
                ignore = True

            if not ignore:  # Propose to the user
                statusmsg = '(Reviewing article {} out of {}.)\n\n'.format(
                                idx+1, len(articles))
                self.add_interactively(article, statusmsg=statusmsg)
        log.info('Finished reviewing all articles for {}.'.format(month))


##################
# Helper functions
##################

def display_abstract(article_dict):
    """Prints the title and abstract of an article to the terminal,
    given a dictionary of the article metadata.

    Parameters
    ----------
    article : `dict` containing standard ADS metadata keys
    """
    # Highlight keywords in the title and abstract
    colors = {'LOFAR': Highlight.RED,
              'WSRT': Highlight.RED,
              'Apertif': Highlight.RED,
              'APERTIF': Highlight.RED,
              'low frequency': Highlight.BLUE,
              'H I': Highlight.BLUE,
              'Westerbork': Highlight.YELLOW,
              'LBA': Highlight.YELLOW,
              'HBA': Highlight.YELLOW}

    title = article_dict['title'][0]
    try:
        abstract = article_dict['abstract']
    except KeyError:
        abstract = ""

    for word in colors:
        pattern = re.compile(word, re.IGNORECASE)
        title = pattern.sub(colors[word] + word + Highlight.END, title)
        abstract = pattern.sub(colors[word]+word+Highlight.END, str(abstract))

    try:
        print(title)
        print('-'*len(title))
        print(abstract)
        print('')
        print('Authors: ' + ', '.join(article_dict['author']))
        print('Date: ' + article_dict['pubdate'])
        print('Status: ' + str(article_dict['property']))
        print('URL: http://adsabs.harvard.edu/abs/' + article_dict['bibcode'])
        print('')
    except TypeError:
        print(title)
        print('-'*len(title))
        print(abstract)
        print('')
        print('Date: ' + article_dict['pubdate'])
        print('Status: ' + str(article_dict['property']))
        print('URL: http://adsabs.harvard.edu/abs/' + article_dict['bibcode'])
        print('')

def plot_citations(output_fn='apub-citation-rate-lofar.png',
                   first_year = 2011,
                   last_year = 2022,
                   barwidth=0.75, 
                   dpi=200, 
                   instrument='LOFAR', 
                   colors=["#3498db", "#27ae60", "#95a5a6"]):

    for file in glob.glob(os.path.expanduser("data/publication_references*.npy")):
        with open(file, 'rb') as c:
            citations = np.load(c, allow_pickle=True)
    citations_dict = {}
    for citation in citations:
        year = int(citation._raw['year'])
        if year in citations_dict:
            citations_dict[year] = citations_dict[year] + 1
        else:
            citations_dict[year] = 0
    
    first_year = first_year
    last_year = last_year
    #first_year = min(citations_dict.keys())
    #last_year = max(citations_dict.keys())
    #'''
    # Now make the actual plot
    fig = pl.figure()
    ax = fig.add_subplot(111)
    pl.bar(np.array(list(citations_dict.keys())),
            list(citations_dict.values()),
            label=instrument,
            facecolor=colors[0],
            width=barwidth)

    # Aesthetics
    pl.ylabel("Number of publications which cite LOFAR publications")
    ax.get_xaxis().get_major_formatter().set_useOffset(False)
    pl.xticks(range(first_year - 1, last_year + 1))
    pl.xlim([first_year - 0.75*barwidth, last_year + 0.75*barwidth])
    #'''
    '''
    pl.legend(bbox_to_anchor=(0.1, 1., 1., 0.),
              loc=3,
              ncol=3,
              borderaxespad=0.,
              handlelength=0.8,
              frameon=False)
    '''
    #'''
    # Disable spines
    ax.spines["left"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    # Only show bottom and left ticks
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    # Only show horizontal grid lines
    ax.grid(axis='y')
    pl.tight_layout(rect=(0, 0, 1, 0.95), h_pad=1.5)
    log.info("Writing {}".format(output_fn))
    pl.savefig(output_fn, dpi=dpi)
    pl.close() 
    #'''


#########################
# Command-line interfaces
#########################

def apub(args=None):
    """Lists the publications in the database in Markdown format."""
    parser = argparse.ArgumentParser(
        description="View the ASTRON instruments and surveys publication list in markdown format.")
    parser.add_argument('-f', metavar='dbfile',
                        type=str, default=DEFAULT_DB,
                        help="Location of the ASTRON publication list db. "
                             "Defaults to ~/.apub.db.")
    parser.add_argument('-l', '--lofar', action='store_true',
                        help='Only show LOFAR publications.')
    parser.add_argument('-w', '--wsrt', action='store_true',
                        help='Only show WSRT publications.')
    parser.add_argument('-a', '--apertif', action='store_true',
                        help='Only show APERTIF publications.')
    parser.add_argument('-m', '--month', action='store_true',
                        help='Group the papers by month rather than year.')
    parser.add_argument('-s', '--save', action='store_true',
                        help='Save the output and plots in the current directory.')
    args = parser.parse_args(args)

    db = PublicationDB(args.f)

    if args.save:
        for bymonth in [True, False]:
            if bymonth:
                suffix = "-by-month"
                title_suffix = " by month"
            else:
                suffix = ""
                title_suffix = ""
            output_fn = 'apub{}.md'.format(suffix)
            db.save_markdown(output_fn,
                             group_by_month=bymonth,
                             title="ASTRON owned instruments and surveys publications{}".format(title_suffix))
            for mode in MODE:
                output_fn = 'apub-{}{}.md'.format(mode, suffix)
                db.save_markdown(output_fn,
                                 group_by_month=bymonth,
                                 mode=mode,
                                 title="ASTRON owned instruments and surveys {} publications{}".format(mode, title_suffix))
            for science in SCIENCE:
                output_fn = 'apub-{}{}.md'.format(science, suffix)
                db.save_markdown(output_fn,
                                 group_by_month=bymonth,
                                 science=science,
                                 title="ASTRON owned instruments and surveys {} publications{}".format(science, title_suffix))
            for instrument in ['LOFAR','WSRT','APERTIF']:
                output_fn = 'apub-{}{}.md'.format(instrument, suffix)
                db.save_markdown(output_fn,
                                 group_by_month=bymonth,
                                 instrument=instrument,
                                 title="{} publications{}".format(instrument.capitalize(), title_suffix))

        # Finally, produce an overview page
        templatedir = os.path.join(PACKAGEDIR, 'templates')
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(templatedir))
        template = env.get_template('template-overview.md')
        markdown = template.render(metrics=db.get_metrics(),
                                   most_cited=db.get_most_cited(top=20),
                                   most_active_first_authors=db.get_most_active_first_authors(),
                                   now=datetime.datetime.now())
        # most_read=db.get_most_read(20),
        filename = 'publications.md'
        log.info('Writing {}'.format(filename))
        f = open(filename, 'w')
        if sys.version_info >= (3, 0):
            f.write(markdown)  # Python 3
        else:
            f.write(markdown.encode("utf-8"))  # Legacy Python
        f.close()

    else:
        if args.lofar and not args.wsrt:
            instrument = "LOFAR"
        elif args.wsrt and not args.lofar:
            instrument = "WSRT"
        elif args.apertif:
            instrument = "APERTIF"
        else:
            instrument = None

        output = db.to_markdown(group_by_month=args.month,
                                instrument=instrument)
        from signal import signal, SIGPIPE, SIG_DFL
        signal(SIGPIPE, SIG_DFL)
        print(output)


def apub_plot(args=None):
    """Creates beautiful plots of the database."""
    parser = argparse.ArgumentParser(
        description="Creates beautiful plots of the database.")
    parser.add_argument('-f', metavar='dbfile',
                        type=str, default=DEFAULT_DB,
                        help="Location of the ASTRON publication list db. "
                             "Defaults to ~/.apub.db.")
    parser.add_argument('year_from', nargs='?', type=int, default=None,
                        help='Year to query from, e.g. 2011.')
    parser.add_argument('year_to', nargs='?', type=int, default=None,
                        help='Year to query to, e.g. 2022.')
    args = parser.parse_args(args)

    if args.year_from is None:
        first_year = 2011
    else:
        first_year = args.year_from
    if args.year_to is None:
        last_year = datetime.datetime.now().year
    else:
        last_year = args.year_to
    PublicationDB(args.f).plot(first_year, last_year)
    plot_citations(first_year = first_year, last_year = last_year)


def apub_update(args=None):
    """Interactively query ADS for new publications."""
    parser = argparse.ArgumentParser(
        description="Interactively query ADS for new publications.")
    parser.add_argument('-f', metavar='dbfile',
                        type=str, default=DEFAULT_DB,
                        help="Location of the ASTRON publication list db. "
                             "Defaults to ~/.apub.db.")
    parser.add_argument('month', nargs='?', default=None,
                        help='Month to query, e.g. 2015-06.')
    args = parser.parse_args(args)

    PublicationDB(args.f).update(month=args.month)


def apub_add(args=None):
    """Add a publication with a known ADS bibcode."""
    parser = argparse.ArgumentParser(
        description="Add a paper to the ASTRON publication list.")
    parser.add_argument('-f', metavar='dbfile',
                        type=str, default=DEFAULT_DB,
                        help="Location of the ASTRON publication list db. "
                             "Defaults to ~/.apub.db.")
    parser.add_argument('bibcode', nargs='+',
                        help='ADS bibcode that identifies the publication.')
    args = parser.parse_args(args)

    db = PublicationDB(args.f)
    for bibcode in args.bibcode:
        db.add_by_bibcode(bibcode, interactive=True)


def apub_delete(args=None):
    """Deletes a publication using its ADS bibcode."""
    parser = argparse.ArgumentParser(
        description="Deletes a paper from the ASTRON publication list.")
    parser.add_argument('-f', metavar='dbfile',
                        type=str, default=DEFAULT_DB,
                        help="Location of the ASTRON publication list db. "
                             "Defaults to ~/.apub.db.")
    parser.add_argument('bibcode', nargs='+',
                        help='ADS bibcode that identifies the publication.')
    args = parser.parse_args(args)

    db = PublicationDB(args.f)
    for bibcode in args.bibcode:
        db.delete_by_bibcode(bibcode)


def apub_import(args=None):
    """Import publications from a csv file.

    The csv file must contain entries of the form "bibcode,instrument,ksp,science".
    The actual metadata of each publication will be grabbed using the ADS API,
    hence this routine may take 10-20 minutes to complete.
    """
    parser = argparse.ArgumentParser(
        description="Batch-import papers into the ASTRON publication list "
                    "from a CSV file. The CSV file must have five columns "
                    "(bibcode,instrument,ksp,science,mode) separated by commas. "
                    "For example: '2004ApJ...610.1199G,LOFAR,SURVEYS,Extragalactic,Interferometric'.")
    parser.add_argument('-f', metavar='dbfile',
                        type=str, default=DEFAULT_DB,
                        help="Location of the ASTRON publication list db. "
                             "Defaults to ~/.apub.db.")
    parser.add_argument('csvfile',
                        help="Filename of the csv file to ingest.")
    args = parser.parse_args(args)

    db = PublicationDB(args.f)
    import time
    for line in ProgressBar(open(args.csvfile, 'r').readlines()):
        for attempt in range(5):
            try:
                col = line.split(',')  # Naive csv parsing
                db.add_by_bibcode(col[0], instrument=col[1], ksp=col[2].strip(), science=col[3].strip(), mode=col[4].strip())
                time.sleep(0.1)
                break
            except Exception as e:
                print("Warning: attempt #{} for {}: error '{}'".format(attempt, col[0], e))


def apub_export(args=None):
    """Export the bibcodes and classifications in CSV format."""
    parser = argparse.ArgumentParser(
        description="Export the ASTRON publication list in CSV format")
    parser.add_argument('-f', metavar='dbfile',
                        type=str, default=DEFAULT_DB,
                        help="Location of the ASTRON publication list db. "
                             "Defaults to ~/.apub.db.")
    args = parser.parse_args(args)

    db = PublicationDB(args.f)
    cur = db.con.execute("SELECT bibcode, instrument, ksp, science, mode "
                         "FROM pubs ORDER BY bibcode;")
    for row in cur.fetchall():
        print('{0},{1},{2},{3},{4}'.format(row[0], row[1], row[2], row[3], row[4]))

def apub_citations(args=None):
    """Return publications which cite LOFAR publications."""
    parser = argparse.ArgumentParser(
        description="Return publications which cite LOFAR publications.")
    parser.add_argument('month_start', type=str, default='2011-01',
                        help='Start month, e.g. 2011-01.')
    parser.add_argument('month_end', nargs='?', default=None,
                        help='End month, e.g. 2015-06.')
    args = parser.parse_args(args)
    print(args)
    
    if ads is None:
        log.error("This action requires the ADS key to be setup.")
        return
    '''
    if input() == 'n':
        return
    '''
    month_start = args.month_start
    month_end = args.month_end
    
    if month_end is None:
        month_end = datetime.datetime.now().strftime("%Y-%m")
    
    # Search for suitable publications
    log.info("Querying ADS...")
    database = "astronomy"
    
    qry = ads.SearchQuery(q="""(
                                citations(title: "LOFAR" OR abstract: "LOFAR") 
                                AND pubdate:[{} TO {}]
                                )
                                database:"{}"
                            """.format(month_start, month_end, database),
                            fl=FIELDS,
                            rows=9999999999)
    
    articles = list(qry)

    for idx, article in enumerate(articles):
        # Ignore articles without abstract
        if not hasattr(article, 'abstract') or article.abstract is None:
            continue
        abstract_lower = article.abstract.lower()

        ignore = False

        # Ignore articles already in the database (add this back when this becomes a class methoid)
        #if article in self:
        #    ignore = True

        # Ignore all the unrefereed non-arxiv stuff
        try:
            if "NOT REFEREED" in article.property and article.pub.lower() != "arxiv e-prints":
                ignore = True
        except (AttributeError, TypeError, ads.exceptions.APIResponseError):
            pass  # no .pub attribute or .property not iterable

        # Ignore proposals and cospar abstracts
        if ".prop." in article.bibcode or "cosp.." in article.bibcode:
            ignore = True

        if not ignore:  # Propose to the user
            articles.pop()
            '''
            statusmsg = '(Reviewing "external" article {} out of {}.)\n\n'.format(
                            idx+1, len(articles))
            title = article._raw['title'][0]
            print(title)
            print('-'*len(title))
            print(article._raw['abstract'])
            print('')
            print('Authors: ' + ', '.join(article._raw['author']))
            print('Date: ' + article._raw['pubdate'])
            print('Status: ' + str(article._raw['property']))
            print('URL: http://adsabs.harvard.edu/abs/' + article._raw['bibcode'])
            print(article._raw['year'])
            print()
            break
            #print(articles)
            '''
            for f in glob.glob(os.path.expanduser("data/publication_references*.npy")):
                os.remove(f)
            np.save(os.path.expanduser("data/publication_references_{:}_{:}.npy".format(month_start, month_end)), np.array(articles))


def apub_spreadsheet(args=None):
    """Export the publication database to an Excel spreadsheet."""
    try:
        import pandas as pd
    except ImportError:
        print('ERROR: pandas needs to be installed for this feature.')

    parser = argparse.ArgumentParser(
        description="Export the ASTRON publication list in XLS format.")
    parser.add_argument('-f', metavar='dbfile',
                        type=str, default=DEFAULT_DB,
                        help="Location of the ASTRON publication list db. "
                             "Defaults to ~/.apub.db.")
    parser.add_argument('-l', '--lofar', action='store_true',
                        help='Only show LOFAR publications.')
    parser.add_argument('-w', '--wsrt', action='store_true',
                        help='Only show WSRT publications.')
    parser.add_argument('-a', '--apertif', action='store_true',
                        help='Only show APERTIF publications.')
    parser.add_argument('-c', '--citations', action='store_true',
                        help='Write out a separate ASTRON publication citations spreadsheet.')
    args = parser.parse_args(args)

    db = PublicationDB(args.f)
    spreadsheet, cit_spreadsheet = [], []
    if args.lofar and not args.wsrt:
        instrument = "LOFAR"
    elif args.wsrt and not args.lofar:
        instrument = "WSRT"
    elif args.apertif:
        instrument = "APERTIF"
    else:
        instrument = None
    if instrument is None:
        cur = db.con.execute("SELECT bibcode, year, month, date, instrument, ksp, science, metrics, mode "
                            "FROM pubs WHERE instrument != 'unrelated' ORDER BY bibcode;")
    else:
        cur = db.con.execute("SELECT bibcode, year, month, date, instrument, ksp, science, metrics, mode "
                            "FROM pubs WHERE instrument == '" + instrument + "' ORDER BY bibcode;")
    for row in cur.fetchall():
        metrics = json.loads(row[7])
        # Fix for the Excell cell overflow export exception (cells containing too many characters - co-author 
        # and affiliation fields for papers of large collaborations)
        '''
        ch, chaf = 0, 0
        mod_metrics, mod_aff = [], []
        for el in metrics['author_norm']:
            ch+=len(el)
            if ch > 22000:
                break
            else:
                mod_metrics.append(el)
        for af in metrics['aff']:
            chaf+=len(af)
            if chaf > 28000:
                break
            else:
                mod_aff.append(af)
        '''
        try:
            if 'REFEREED' in metrics['property']:
                refereed = 'REFEREED'
            elif 'NOT REFEREED' in metrics['property']:
                refereed = 'NOT REFEREED'
            else:
                refereed = ''
        except TypeError:  # .property is None
            refereed = ''
        # Compute citations per year
        try:
            dateobj = datetime.datetime.strptime(row[3], '%Y-%m-00')
        except ValueError:
            dateobj = datetime.datetime.strptime(row[3], '%Y-00-00')
        publication_age = datetime.datetime.now() - dateobj
        try:
            citations_per_year = metrics['citation_count'] / (publication_age.days / 365)
        except (TypeError, ZeroDivisionError):
            citations_per_year = 0
        #print("----- Pub ----")
        loc = []
        for aff in metrics['aff'][1:]:
            loc.append(aff.split(';')[0].split(',')[-1])
        unique_locations = nltk.FreqDist(loc)
        countries =	{
            "NL": 0,
            "DE": 0,
            "FR": 0,
            "GB": 0,
            "IE": 0,
            "IT": 0,
            "PL": 0,
            "BG": 0,
            "SE": 0,
            "LV": 0,
            "CN": 0,
            "CA": 0,
            "US": 0,
            "AU": 0,
            "ES": 0,
            "SA": 0,
            "Other": 0
        }
        for loc in unique_locations:
            '''
            print('\n')
            print("Location: ", loc)
            print("Occurence: ", unique_locations.get(loc))
            print(loc.split(' '))
            '''
            locst = loc.strip(" ;,-")
            #print("Location stripped:", locst)
            if locst == "the Netherlands":
                countries["NL"] += unique_locations.get(loc)
            elif locst == "The Netherlands":
                countries["NL"] += unique_locations.get(loc)
            elif locst == "Germany":
                countries["DE"] += unique_locations.get(loc)
            elif locst == "France":
                countries["FR"] += unique_locations.get(loc)
            elif locst == "UK":
                countries["GB"] += unique_locations.get(loc)
            elif locst == "Ireland":
                countries["IE"] += unique_locations.get(loc)
            elif locst == "Italy":
                countries["IT"] += unique_locations.get(loc)
            elif locst == "Poland":
                countries["PL"] += unique_locations.get(loc)
            elif locst == "Bulgaria":
                countries["BG"] += unique_locations.get(loc)
            elif locst == "Sweden":
                countries["SE"] += unique_locations.get(loc)
            elif locst == "Latvia":
                countries["LV"] += unique_locations.get(loc)
            elif locst == "China":
                countries["CN"] += unique_locations.get(loc)
            elif locst == "People's Republic of China":
                countries["CN"] += unique_locations.get(loc)
            elif locst == "Canada":
                countries["CA"] += unique_locations.get(loc)
            elif locst == "USA":
                countries["US"] += unique_locations.get(loc)
            elif locst == "Australia":
                countries["AU"] += unique_locations.get(loc)
            elif locst == "Spain":
                countries["ES"] += unique_locations.get(loc)
            elif locst == "South Africa":
                countries["SA"] += unique_locations.get(loc)
            elif locst == "south africa":
                countries["SA"] += unique_locations.get(loc)
            else:
                countries["Other"] += unique_locations.get(loc)                
        #print("---------------")
        #print('\n')
        myrow = collections.OrderedDict([
                    ('bibcode', 'https://ui.adsabs.harvard.edu/abs/'+row[0]),
                    ('Year', row[1]),
                    #('date', row[3]),
                    ('Instrument', row[4]),
                    ('KSP', row[5]),
                    ('Science', row[6]),
                    ('Mode', row[8]),
                    ('doi', metrics['doi']),
                    #('refereed', refereed),
                    #('citation_count', metrics['citation_count']),
                    #('citations_per_year', round(citations_per_year, 2)),
                    #('read_count', metrics['read_count']),
                    #('first_author_norm', metrics['first_author_norm']),
                    ('title', metrics['title'][0]),
                    #('keyword_norm', metrics['keyword_norm']),
                    #('abstract', metrics['abstract'])
                    #('co_author_norm', mod_metrics),
                    ('Nationality of first author', metrics['aff'][0].split(';')[0].split(',')[-1]),
                    ('Number co-authors', len(metrics['author_norm']) - 1),
                    ("NL", countries["NL"]),
                    ("DE", countries["DE"]),
                    ("FR", countries["FR"]),
                    ("GB", countries["GB"]),
                    ("IE", countries["IE"]),
                    ("IT", countries["IT"]),
                    ("PL", countries["PL"]),
                    ("BG", countries["BG"]),
                    ("SE", countries["SE"]),
                    ("LV", countries["LV"]),
                    ("CN", countries["CN"]),
                    ("CA", countries["CA"]),
                    ("US", countries["US"]),
                    ("AU", countries["AU"]),
                    ("ES", countries["ES"]),
                    ("SA", countries["SA"]),
                    ("Other", countries["Other"])
                    ])
                    #('affiliations', mod_aff)])
        spreadsheet.append(myrow)
    output_fn = 'astron-publications.xls'
    log.info('Writing {}'.format(output_fn))
    pd.DataFrame(spreadsheet).to_excel(output_fn, index=False, engine="xlsxwriter")
    if args.citations:
        # Add papers per year spreadsheet export for Grafana

        for file in glob.glob(os.path.expanduser("data/publication_references*.npy")):
            with open(file, 'rb') as c:
                citations = np.load(c, allow_pickle=True)
        for citation in citations:
            citrow = collections.OrderedDict([
                        ('bibcode', 'https://ui.adsabs.harvard.edu/abs/'+citation._raw['bibcode']),
                        ('Year', citation._raw['year'])])
            cit_spreadsheet.append(citrow)
        output_fn = 'astron-cited.xls'
        log.info('Writing {}'.format(output_fn))
        pd.DataFrame(cit_spreadsheet).to_excel(output_fn, index=False, engine="xlsxwriter")
if __name__ == '__main__':
    pass

import apub


def test_annual_count():
    # Does the cumulative count match the annual count?
    db = apub.PublicationDB()
    annual = db.get_annual_publication_count()
    cumul = db.get_annual_publication_count_cumulative()
    assert annual['LOFAR'][2007] == 0  # LOFAR didn't exist in 2007
    # The first LOFAR papers started appearing in 2010; the cumulative counts should reflect that:
    assert (annual['LOFAR'][2014] + annual['LOFAR'][2015]) == cumul['LOFAR'][2015]
    assert (annual['LOFAR'][2014] + annual['LOFAR'][2015] + annual['LOFAR'][2016]) == cumul['LOFAR'][2016]
    # Are the values returned by get_metrics consistent?
    for year in range(2010, 2019):
        metrics = db.get_metrics(year=year)
        assert metrics['publication_count'] == annual['LOFAR'][year]
        assert metrics['lofar_count'] == annual['LOFAR'][year]
    # Can we pass multiple years to get_metrics?
    metrics = db.get_metrics(year=[2011, 2012])
    assert metrics['publication_count'] == annual['LOFAR'][2011] + annual['LOFAR'][2012]

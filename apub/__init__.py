__version__ = "0.1.dev"

import os

# Useful constants
PACKAGEDIR = os.path.abspath(os.path.dirname(__file__))
INSTRUMENT = ['LOFAR', 'WSRT', 'APERTIF']
SCIENCE = ['Extragalactic', 'Galaxy', 'Pulsars & Transients', 'Instrumentation & Methods', 'Ionosphere', 'Cosmic Rays',\
'Solar', 'Solar System Planets', 'Exoplanets', 'Lightning', 'HI']
MODE = ['Interferometric(IF)', 'Beam-formed(BF)', 'TBB', 'IF+BF', 'Single Station', 'Other']
from .apub import *

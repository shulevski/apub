"""Make a pie chart plot showing the distribution of ASTRON papers by science theme.
"""
import pandas as pd
import matplotlib.pyplot as pl
from matplotlib.patches import PathPatch
from matplotlib.offsetbox import OffsetImage, AnnotationBbox

from categorize import CATEGORIES

df = pd.read_csv("astron-categories_2022.csv", names=["bibcode", "category"])

counts = df.category.value_counts()

myorder = ['eg', 'pt', 'im', 'ga', 'hI', 'er', 'li', 'so', 'pl']

categories = [CATEGORIES[lab] for lab in myorder]
im_names = [lab for lab in myorder]
sizes = [counts[lab] for lab in myorder]

fig, ax = pl.subplots(figsize=(12, 9))
wedges, labels, num_labels = ax.pie(sizes, labels=categories, autopct='%1.0f%%', startangle=158,
                        counterclock=False,
                        pctdistance=1.1, labeldistance=1.3,
                        textprops={'fontsize': 20, 'weight': 'normal'})

# x,y position of the wedge image in canvas units
positions = [(0.2,0.01), (-0.05,-0.5), (-0.3,-0.5), (-0.4,-0.2), (-0.6,-0.1), (-0.6,-0.1), (-0.6,0.0), (-0.6,0.1), (-0.5,0.2)]
# Wedge image zoom factors
zooms = [1.2, 0.55, 0.2, 0.2, 0.4, 0.4, 0.4, 0.2, 0.2]

def img_to_pie(fn, wedge, xy, zoom=1, ax = None):
    if ax==None: ax=pl.gca()
    im = pl.imread(fn, format='jpeg')
    path = wedge.get_path()
    patch = PathPatch(path, facecolor='none')
    ax.add_patch(patch)
    imagebox = OffsetImage(im, zoom=zoom, clip_path=patch, zorder=-10)
    ab = AnnotationBbox(imagebox, xy, xycoords='data', pad=0, frameon=False)
    ax.add_artist(ab)

for i in range(len(sizes)):
    fn = "images/{}.jpeg".format(im_names[i])
    img_to_pie(fn, wedges[i], xy=positions[i], zoom=zooms[i])
    #wedges[i].set_zorder(100)

ax.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
ttl = pl.title("Papers per Science Area", fontsize=36)
ttl.set_position([.5, 1.05])
#ax.text(1, 0.005, 'Last update: 2019 Feb 21',
#        horizontalalignment='right',
#        verticalalignment='top',
#        transform=ax.transAxes,
#        fontsize=14)
pl.tight_layout(rect=[0.03, 0.03, 0.97, 0.97])
#pl.show()
pl.savefig("astron-science-piechart.png")
pl.close("all")

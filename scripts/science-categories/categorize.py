"""Categorize publications into fine-grained categories
"""
from pprint import pprint
import pandas as pd
import apub

FILENAME = "astron-categories.csv"

CATEGORIES = {
              'eg': 'Extragalactic', 
              'pt': 'Pulsars & Transients', 
              'im': 'Instrumentation & Methods', 
              'cr': 'Cosmic Rays', 
              'ga': 'Galaxy', 
              'hI': 'HI', 
              'er': 'EoR',
              'io': 'Ionosphere', 
              'li': 'Lightning',
              'so': 'Solar', 
              'pl': 'Solar System Planets',
              'ex': 'Exoplanets'
            }


if __name__ == "__main__":
    # Read in existing db
    try:
        catdb = pd.read_csv(FILENAME, names=["bibcode", "cat"])
        classified_bibcodes = catdb.bibcode.values
    except FileNotFoundError:
        classified_bibcodes = []

    # Open db in append mode
    out = open(FILENAME, 'a')

    db = apub.PublicationDB()
    pubs = db.get_all(instrument="LOFAR", year=2022)
    for idx, pub in enumerate(pubs):
        # Skip articles already categorizes
        if pub['bibcode'] in classified_bibcodes:
            continue

        # Clear screen and show the article
        print(chr(27) + "[2J")
        print("Article {} out of {}".format(idx+1, len(pubs)))
        apub.display_abstract(pub)

        # Prompt the user to classify the paper
        print("Categories:")
        pprint(CATEGORIES)
        while True:
            print("=> Enter code: ", end="")
            prompt = input()
            if prompt not in CATEGORIES.keys():
                print("Error: invalid category")
            else:
                out.write("{},{}\n".format(pub['bibcode'], prompt))
                out.flush()
                break

    out.close()

# apub: ASTRON publication database

***A database of scientific publications related to ASTRON (radio) telescopes and surveys.***

`apub` is a telescope/survey-specific tool that enables ASTRON SDCO to keep 
track of scientific publications in an easy way. 
It leverages SQLite and the [ADS API](https://github.com/adsabs/adsabs-dev-api)
(using Andy Casey's [awesome Python client](https://github.com/andycasey/ads)
as well as being based on the [Kepler publication database](https://github.com/KeplerGO/kpub) repository.) 
to create and curate a database that contains the metadata of telescope/survey-related articles.

## Example use

Print a nicely-formatted list of ASTRON-related LOFAR publications in markdown format:
```
apub --lofar
```

Add a new article to the database using its bibcode.
This command will display the article's metadata and ask the user to
classify the science:
```
apub-add 2015arXiv150204715F
```

Remove an article using its bibcode:
```
apub-delete 2015ApJ...800...46B
```

Search ADS interactively for new ASTRON-related articles and try to add them:
```
apub-update 2023-01
```

For example output, see the `data/output/` sub-directory in this repository.

## Installation

To install the latest version from source:
```
git clone https://git.astron.nl/shulevski/apub
cd apub
python setup.py install
```

Note that the `apub` tools will use `~/.apub.db` as the default database file.
This repository contains a recent version
of the database file (`data/apub.db`),
which you may want to link to the default file as follows:
```
ln -s </path/to/git/repo>/data/apub.db ~/.apub.db
```

The `apub-add`and `apub-update` tools that come with this package require
an api key from NASA ADS labs to retrieve publication meta-data.
You need to follow the installation instructions of the [ads client](https://github.com/andycasey/ads) by @andycasey to make this work.

## Usage

`apub` adds a number of tools to the command line (described below).

There is a `Makefile` which makes your life easy if you work
for the GO office and are updating the database. 
Simply type:
* `make update` to search for new publications;
* `make push` to push the updated database to the git repo;
* `make refresh` to export and import all publications, this is slow and necessary only if you want to remove duplicates and fetch fresh citation statistics.

## Command-line tools

After installation, this package adds the following command-line tools to your path:
* `apub` prints the list of publications in markdown format;
* `apub-update` adds new publications by searching ADS (interactive);
* `apub-citations` search ADS for publications which cite LOFAR publications and store them for use;
* `apub-add` adds a publication using its ADS bibcode;
* `apub-delete` deletes a publication using its ADS bibcode;
* `apub-import` imports bibcodes from a csv file;
* `apub-export` exports bibcodes to a csv file;
* `apub-plot` creates a visualization of the database;
* `apub-spreadsheet` exports the publications to an Excel spreadsheet.

Listed below are the usage instructions for each command:

*apub*
```
$ apub --help
usage: apub [-h] [-f dbfile] [-l] [-w] [-a] [-m] [-s]

View the ASTRON instruments and surveys publication list in markdown format.

options:
  -h, --help     show this help message and exit
  -f dbfile      Location of the ASTRON publication list db. Defaults to ~/.apub.db.
  -l, --lofar    Only show LOFAR publications.
  -w, --wsrt     Only show WSRT publications.
  -a, --apertif  Only show APERTIF publications.
  -m, --month    Group the papers by month rather than year.
  -s, --save     Save the output and plots in the current directory.
```

*apub-update*
```
$ apub-update --help
usage: apub-update [-h] [-f dbfile] [month]

Interactively query ADS for new publications.

positional arguments:
  month       Month to query, e.g. 2015-06.

optional arguments:
  -h, --help  show this help message and exit
  -f dbfile   Location of the ASTRON publication list db. Defaults to
              ~/.apub.db.
```

*apub-citations*
```
$ apub-citations --help
usage: apub-citations [-h] month_start [month_end]

Return publications which cite LOFAR publications.

positional arguments:
  month_start  Start month, e.g. 2011-01.
  month_end    End month, e.g. 2015-06.

options:
  -h, --help   show this help message and exit
```

*apub-add*
```
$ apub-add --help
usage: apub-add [-h] [-f dbfile] bibcode [bibcode ...]

Add a paper to the ASTRON publication list.

positional arguments:
  bibcode     ADS bibcode that identifies the publication.

optional arguments:
  -h, --help  show this help message and exit
  -f dbfile   Location of the ASTRON publication list db. Defaults to
              ~/.apub.db.
```

*apub-delete*
```
$ apub-delete --help
usage: apub-delete [-h] [-f dbfile] bibcode [bibcode ...]

Deletes a paper from the ASTRON publication list.

positional arguments:
  bibcode     ADS bibcode that identifies the publication.

optional arguments:
  -h, --help  show this help message and exit
  -f dbfile   Location of the ASTRON publication list db. Defaults to
              ~/.apub.db.
```

*apub-import*
```
$ apub-import --help
usage: apub-import [-h] [-f dbfile] csvfile

Batch-import papers into the ASTRON publication list from a CSV file.
The CSV file must have five columns (bibcode,instrument,ksp,science,mode) separated by commas.
For example: '2004ApJ...610.1199G,LOFAR,SURVEYS,Extragalactic,Interferometric'.

positional arguments:
  csvfile     Filename of the csv file to ingest.

options:
  -h, --help  show this help message and exit
  -f dbfile   Location of the ASTRON publication list db. Defaults to ~/.apub.db.
```

*apub-export*
```
$ apub-export --help
usage: apub-export [-h] [-f dbfile]

Export the ASTRON publication list in CSV format.

optional arguments:
  -h, --help  show this help message and exit
  -f dbfile   Location of the ASTRON publication list db. Defaults to
              ~/.apub.db.
```

*apub-spreadsheet*
```
$ apub-spreadsheet --help
usage: apub-spreadsheet [-h] [-f dbfile] [-l] [-w] [-a] [-c]

Export the ASTRON publication list in XLS format.

options:
  -h, --help       show this help message and exit
  -f dbfile        Location of the ASTRON publication list db. Defaults to ~/.apub.db.
  -l, --lofar      Only show LOFAR publications.
  -w, --wsrt       Only show WSRT publications.
  -a, --apertif    Only show APERTIF publications.
  -c, --citations  Write out a separate ASTRON publication citations spreadsheet.
```

*apub-plot*
```
apub-plot --help
usage: apub-plot [-h] [-f dbfile] [year_from] [year_to]

Creates beautiful plots of the database.

positional arguments:
  year_from   Year to query from, e.g. 2011.
  year_to     Year to query to, e.g. 2022.

options:
  -h, --help  show this help message and exit
  -f dbfile   Location of the ASTRON publication list db. Defaults to ~/.apub.db.
```

## Author
Adapted for ASTRON by Aleksandar Shulevski on behalf of
ASTRON SDCO based on the Kepler/K2 publication database
package, created by Geert Barentsen (geert.barentsen at nasa.gov).

## Acknowledgements
This tool is made possible thanks to the efforts made by NASA ADS to
provide a web API, and thanks to the excellent Python client that Andy Casey
(@andycasey) wrote to use the API. Also, thanks to Geert who extended
Andy's client. Thanks ADS, thanks Andy, thanks Geert!



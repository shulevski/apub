[console_scripts]
apub = apub:apub
apub-add = apub:apub_add
apub-citations = apub:apub_citations
apub-delete = apub:apub_delete
apub-export = apub:apub_export
apub-import = apub:apub_import
apub-plot = apub:apub_plot
apub-spreadsheet = apub:apub_spreadsheet
apub-update = apub:apub_update

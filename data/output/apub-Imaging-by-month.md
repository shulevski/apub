Title: ASTRON owned instruments and surveys Imaging publications by month
Save_as: apub-Imaging-by-month.html

[TOC]


2023-02
-------

1. Ubertosi, F., Gitti, M., Brighenti, F.: [Chasing ICM cooling and AGN feedback from the macro to the meso scales in the galaxy cluster ZwCl 235](http://adsabs.harvard.edu/abs/2023A&A...670A..23U), 2023, Astronomy and Astrophysics, 670, A23

2. Stein, M., Heesen, V., Dettmar, R. -J., et al.: [CHANG-ES. XXVI. Insights into cosmic-ray transport from radio halos in edge-on galaxies](http://adsabs.harvard.edu/abs/2023A&A...670A.158S), 2023, Astronomy and Astrophysics, 670, A158

3. Callingham, J. R., Shimwell, T. W., Vedantham, H. K., et al.: [V-LoTSS: The circularly polarised LOFAR Two-metre Sky Survey](http://adsabs.harvard.edu/abs/2023A&A...670A.124C), 2023, Astronomy and Astrophysics, 670, A124

4. Giarratana, S., Giroletti, M., Spingola, C., et al.: [Multi-scale VLBI observations of the candidate host galaxy of GRB 200716C](http://adsabs.harvard.edu/abs/2023A&A...670A..35G), 2023, Astronomy and Astrophysics, 670, A35

5. Dutta, Sushant, Singh, Veeresh, Chandra, C. H. Ishwara, et al.: [Search and Characterization of Remnant Radio Galaxies in the XMM-LSS Deep Field](http://adsabs.harvard.edu/abs/2023ApJ...944..176D), 2023, The Astrophysical Journal, 944, 176

6. Mahatma, V. H., Basu, A., Hardcastle, M. J., et al.: [A low frequency sub-arcsecond view of powerful radio galaxies in rich-cluster environments: 3C 34 and 3C 320](http://adsabs.harvard.edu/abs/2023MNRAS.tmp..404M), 2023, Monthly Notices of the Royal Astronomical Society, None, 

7. Heesen, V., O'Sullivan, S. P., Brüggen, M., et al.: [Detection of magnetic fields in the circumgalactic medium of nearby galaxies using Faraday rotation](http://adsabs.harvard.edu/abs/2023A&A...670L..23H), 2023, Astronomy and Astrophysics, 670, L23

8. Fine, Maxwell A., Van Eck, Cameron L., Pratley, Luke: [Correcting Bandwidth Depolarization by Extreme Faraday Rotation](http://adsabs.harvard.edu/abs/2023MNRAS.tmp..438F), 2023, Monthly Notices of the Royal Astronomical Society, None, 


2023-01
-------

1. Heesen, V., Klocke, T. -L., Brüggen, M., et al.: [Nearby galaxies in the LOFAR Two-metre Sky Survey. II. The magnetic field-gas relation](http://adsabs.harvard.edu/abs/2023A&A...669A...8H), 2023, Astronomy and Astrophysics, 669, A8

2. Hare, B. M., Scholten, O., Buitink, S., et al.: [Characteristics of recoil leaders as observed by LOFAR](http://adsabs.harvard.edu/abs/2023PhRvD.107b3025H), 2023, Physical Review D, 107, 023025

3. Rajpurohit, K., Osinga, E., Brienza, M., et al.: [Deep low-frequency radio observations of Abell 2256. II. The ultra-steep spectrum radio halo](http://adsabs.harvard.edu/abs/2023A&A...669A...1R), 2023, Astronomy and Astrophysics, 669, A1

4. Xu, Xiaoyu, Wang, Junfeng: [Ghost in the Shell: Evidence for Past Active Galactic Nucleus Activities in NGC 5195 from a Newly Discovered Large-scale Ionized Structure](http://adsabs.harvard.edu/abs/2023ApJ...943...28X), 2023, The Astrophysical Journal, 943, 28

5. Cassano, R., Cuciti, V., Brunetti, G., et al.: [The Planck clusters in the LOFAR sky: IV: LoTSS-DR2: statistics of radio halos and re-acceleration models](http://adsabs.harvard.edu/abs/2023arXiv230108052C), 2023, arXiv e-prints, None, arXiv:2301.08052

6. Bruno, L., Brunetti, G., Botteon, A., et al.: [The Planck clusters in the LOFAR sky. II. LoTSS-DR2: Recovering diffuse extended emission with LOFAR](http://adsabs.harvard.edu/abs/2023arXiv230108121B), 2023, arXiv e-prints, None, arXiv:2301.08121

7. de Ruiter, Iris, Nyamai, Miriam M., Rowlinson, Antonia, et al.: [Low-frequency radio observations of recurrent nova RS Ophiuchi with MeerKAT and LOFAR](http://adsabs.harvard.edu/abs/2023arXiv230110552D), 2023, arXiv e-prints, None, arXiv:2301.10552

8. Jones, A., de Gasperin, F., Cuciti, V., et al.: [The Planck clusters in the LOFAR sky VI. LoTSS-DR2: Properties of radio relics](http://adsabs.harvard.edu/abs/2023arXiv230107814J), 2023, arXiv e-prints, None, arXiv:2301.07814

9. Carretti, E., O'Sullivan, S. P., Vacca, V., et al.: [Magnetic field evolution in cosmic filaments with LOFAR data](http://adsabs.harvard.edu/abs/2023MNRAS.518.2273C), 2023, Monthly Notices of the Royal Astronomical Society, 518, 2273

10. Gan, H., Mertens, F. G., Koopmans, L. V. E., et al.: [Assessing the impact of two independent direction-dependent calibration algorithms on the LOFAR 21 cm signal power spectrum. And applications to an observation of a field flanking the north celestial pole](http://adsabs.harvard.edu/abs/2023A&A...669A..20G), 2023, Astronomy and Astrophysics, 669, A20

11. Oei, Martijn S. S. L., van Weeren, Reinout J., Hardcastle, Martin J., et al.: [An intergalactic medium temperature from a giant radio galaxy](http://adsabs.harvard.edu/abs/2023MNRAS.518..240O), 2023, Monthly Notices of the Royal Astronomical Society, 518, 240


2022-12
-------

1. Cooper, A. J., Rowlinson, A., Wijers, R. A. M. J., et al.: [Testing afterglow models of FRB 200428 with early post-burst observations of SGR 1935 + 2154](http://adsabs.harvard.edu/abs/2022MNRAS.517.5483C), 2022, Monthly Notices of the Royal Astronomical Society, 517, 5483

2. Timmerman, R., van Weeren, R. J., Botteon, A., et al.: [Measuring cavity powers of active galactic nuclei in clusters using a hybrid X-ray-radio method. A new window on feedback opened by subarcsecond LOFAR-VLBI observations](http://adsabs.harvard.edu/abs/2022A&A...668A..65T), 2022, Astronomy and Astrophysics, 668, A65

3. Gloudemans, A. J., Duncan, K. J., Saxena, A., et al.: [Discovery of 24 radio-bright quasars at 4.9 ≤ z ≤ 6.6 using low-frequency radio observations](http://adsabs.harvard.edu/abs/2022A&A...668A..27G), 2022, Astronomy and Astrophysics, 668, A27

4. de Jong, J. M. G. H. J., van Weeren, R. J., Botteon, A., et al.: [Deep study of A399-401: Application of a wide-field facet calibration](http://adsabs.harvard.edu/abs/2022A&A...668A.107D), 2022, Astronomy and Astrophysics, 668, A107

5. Mostert, Rafaël I. J., Duncan, Kenneth J., Alegre, Lara, et al.: [Radio source-component association for the LOFAR Two-metre Sky Survey with region-based convolutional neural networks](http://adsabs.harvard.edu/abs/2022A&A...668A..28M), 2022, Astronomy and Astrophysics, 668, A28

6. Slob, M. M., Callingham, J. R., Röttgering, H. J. A., et al.: [Extragalactic peaked-spectrum radio sources at low frequencies are young radio galaxies](http://adsabs.harvard.edu/abs/2022A&A...668A.186S), 2022, Astronomy and Astrophysics, 668, A186

7. Dabhade, P., Shimwell, T. W., Bagchi, J., et al.: [Barbell-shaped giant radio galaxy with ∼100 kpc kink in the jet](http://adsabs.harvard.edu/abs/2022A&A...668A..64D), 2022, Astronomy and Astrophysics, 668, A64

8. Radiconi, Federico, Vacca, Valentina, Battistelli, Elia, et al.: [The thermal and non-thermal components within and between galaxy clusters Abell 399 and Abell 401](http://adsabs.harvard.edu/abs/2022MNRAS.517.5232R), 2022, Monthly Notices of the Royal Astronomical Society, 517, 5232

9. Kayal, Abhijit, Singh, Veeresh, Chandra, C. H. Ishwara, et al.: [Detection of radio-AGN in dust-obscured galaxies using deep uGMRT radio continuum observations](http://adsabs.harvard.edu/abs/2022JApA...43...84K), 2022, Journal of Astrophysics and Astronomy, 43, 84

10. Gao, F., Wang, L., Ramos Padilla, A. F., et al.: [Probing the megaparsec-scale environment of hyperluminous infrared galaxies at 2 &lt; z &lt; 4](http://adsabs.harvard.edu/abs/2022A&A...668A..54G), 2022, Astronomy and Astrophysics, 668, A54

11. Roberts, Ian D., Lang, Maojin, Trotsenko, Daria, et al.: [LoTSS Jellyfish Galaxies. IV. Enhanced Star Formation on the Leading Half of Cluster Galaxies and Gas Compression in IC3949](http://adsabs.harvard.edu/abs/2022ApJ...941...77R), 2022, The Astrophysical Journal, 941, 77


2022-11
-------

1. Arias, M., Botteon, A., Bassa, C. G., et al.: [Possible discovery of Calvera's supernova remnant](http://adsabs.harvard.edu/abs/2022A&A...667A..71A), 2022, Astronomy and Astrophysics, 667, A71

2. Alegre, Lara, Sabater, Jose, Best, Philip, et al.: [A machine-learning classifier for LOFAR radio galaxy cross-matching techniques](http://adsabs.harvard.edu/abs/2022MNRAS.516.4716A), 2022, Monthly Notices of the Royal Astronomical Society, 516, 4716

3. Rezaei, S., McKean, J. P., Biehl, M., et al.: [A machine learning based approach to gravitational lens identification with the International LOFAR Telescope](http://adsabs.harvard.edu/abs/2022MNRAS.517.1156R), 2022, Monthly Notices of the Royal Astronomical Society, 517, 1156

4. Mesarcik, Michael, Boonstra, Albert-Jan, Ranguelova, Elena, et al.: [Learning to detect radio frequency interference in radio astronomy without seeing it](http://adsabs.harvard.edu/abs/2022MNRAS.516.5367M), 2022, Monthly Notices of the Royal Astronomical Society, 516, 5367

5. Omar, Amitesh: [LOFAR discovery of rare large FR I jets in the low-luminosity radio galaxy NGC 5322](http://adsabs.harvard.edu/abs/2022MNRAS.517L..81O), 2022, Monthly Notices of the Royal Astronomical Society, 517, L81

6. Botteon, Andrea, van Weeren, Reinout J., Brunetti, Gianfranco, et al.: [Magnetic fields and relativistic electrons fill entire galaxy cluster](http://adsabs.harvard.edu/abs/2022SciA....8.7623B), 2022, Science Advances, 8, eabq7623

7. Fallows, R. A., Forte, B., Mevius, M., et al.: [The scintillating tail of comet C/2020 F3 (Neowise)](http://adsabs.harvard.edu/abs/2022A&A...667A..57F), 2022, Astronomy and Astrophysics, 667, A57

8. Zhou, Yihao, Xu, Haiguang, Zhu, Zhenghao, et al.: [A semi-analytical model for the formation and evolution of radio relics in galaxy clusters](http://adsabs.harvard.edu/abs/2022MNRAS.517.1299Z), 2022, Monthly Notices of the Royal Astronomical Society, 517, 1299


2022-10
-------

1. Morabito, Leah K., Sweijen, F., Radcliffe, J. F., et al.: [Identifying active galactic nuclei via brightness temperature with sub-arcsecond international LOFAR telescope observations](http://adsabs.harvard.edu/abs/2022MNRAS.515.5758M), 2022, Monthly Notices of the Royal Astronomical Society, 515, 5758

2. Petley, J. W., Morabito, L. K., Alexander, D. M., et al.: [Connecting radio emission to AGN wind properties with broad absorption line quasars](http://adsabs.harvard.edu/abs/2022MNRAS.515.5159P), 2022, Monthly Notices of the Royal Astronomical Society, 515, 5159

3. Ignesti, Alessandro, Vulcani, Benedetta, Poggianti, Bianca M., et al.: [Walk on the Low Side: LOFAR Explores the Low-frequency Radio Emission of GASP Jellyfish Galaxies](http://adsabs.harvard.edu/abs/2022ApJ...937...58I), 2022, The Astrophysical Journal, 937, 58

4. Edler, H. W., de Gasperin, F., Brunetti, G., et al.: [Abell 1033: Radio halo and gently reenergized tail at 54 MHz](http://adsabs.harvard.edu/abs/2022A&A...666A...3E), 2022, Astronomy and Astrophysics, 666, A3

5. Sasmal, Tapan K., Bera, Soumen, Mondal, Soumen: [Miscellaneous radio galaxies from LOFAR survey](http://adsabs.harvard.edu/abs/2022AN....34310083S), 2022, Astronomische Nachrichten, 343, e20210083

6. Kayama, Kazuho, Tanaka, Takaaki, Uchida, Hiroyuki, et al.: [Spatially resolved study of the SS 433/W 50 west region with Chandra: X-ray structure and spectral variation of non-thermal emission](http://adsabs.harvard.edu/abs/2022PASJ...74.1143K), 2022, Publications of the Astronomical Society of Japan, 74, 1143

7. Su, Renzhi, Sadler, Elaine M., Allison, James R., et al.: [FLASH pilot survey: detections of associated 21 cm H I absorption in GAMA galaxies at 0.42 &lt; z &lt; 1.00](http://adsabs.harvard.edu/abs/2022MNRAS.516.2947S), 2022, Monthly Notices of the Royal Astronomical Society, 516, 2947


2022-09
-------

1. Palaiologou, E. V., Leonidaki, I., Kopsacheili, M.: [First optical identification of the SRG/eROSITA-detected supernova remnant G 116.6 - 26.1. I. Preliminary results](http://adsabs.harvard.edu/abs/2022MNRAS.515..339P), 2022, Monthly Notices of the Royal Astronomical Society, 515, 339

2. Cuciti, V., de Gasperin, F., Brüggen, M., et al.: [Galaxy clusters enveloped by vast volumes of relativistic electrons](http://adsabs.harvard.edu/abs/2022Natur.609..911C), 2022, Nature, 609, 911

3. Simonte, M., Andernach, H., Brüggen, M., et al.: [Giant radio galaxies in the LOw-Frequency ARray Two-metre Sky Survey Boötes deep field](http://adsabs.harvard.edu/abs/2022MNRAS.515.2032S), 2022, Monthly Notices of the Royal Astronomical Society, 515, 2032

4. Hoang, D. N., Brüggen, M., Botteon, A., et al.: [Diffuse radio emission from non-Planck galaxy clusters in the LoTSS-DR2 fields](http://adsabs.harvard.edu/abs/2022A&A...665A..60H), 2022, Astronomy and Astrophysics, 665, A60

5. Hajduk, Marcin, Leto, Paolo, Vedantham, Harish, et al.: [Radio detection of chemically peculiar stars with LOFAR](http://adsabs.harvard.edu/abs/2022A&A...665A.152H), 2022, Astronomy and Astrophysics, 665, A152

6. Pomakov, V. P., O'Sullivan, S. P., Brüggen, M., et al.: [The redshift evolution of extragalactic magnetic fields](http://adsabs.harvard.edu/abs/2022MNRAS.515..256P), 2022, Monthly Notices of the Royal Astronomical Society, 515, 256

7. Mulcahey, C. R., Leslie, S. K., Jackson, T. M., et al.: [Star formation and AGN feedback in the local Universe: Combining LOFAR and MaNGA](http://adsabs.harvard.edu/abs/2022A&A...665A.144M), 2022, Astronomy and Astrophysics, 665, A144

8. Zheng, X. C., Röttgering, H. J. A., van der Wel, A., et al.: [Shapes of galaxies hosting radio-loud AGNs with z ≤ 1](http://adsabs.harvard.edu/abs/2022A&A...665A.114Z), 2022, Astronomy and Astrophysics, 665, A114


2022-08
-------

1. Kukreti, P., Morganti, R., Bondi, M., et al.: [Seeing the forest and the trees: A radio investigation of the ULIRG Mrk 273](http://adsabs.harvard.edu/abs/2022A&A...664A..25K), 2022, Astronomy and Astrophysics, 664, A25

2. Pajdosz-Śmierciak, Urszula, Śmierciak, Bartosz, Jamrozy, Marek: [Possible jet reorientation in low-frequency radio structures of blazars](http://adsabs.harvard.edu/abs/2022MNRAS.514.2122P), 2022, Monthly Notices of the Royal Astronomical Society, 514, 2122

3. Smith, D. J. B., Krause, M. G., Hardcastle, M. J., et al.: [Relic jet activity in 'Hanny's Voorwerp' revealed by the LOFAR two metre sky survey](http://adsabs.harvard.edu/abs/2022MNRAS.514.3879S), 2022, Monthly Notices of the Royal Astronomical Society, 514, 3879

4. Emig, K. L., White, G. J., Salas, P., et al.: [Filamentary structures of ionized gas in Cygnus X](http://adsabs.harvard.edu/abs/2022A&A...664A..88E), 2022, Astronomy and Astrophysics, 664, A88

5. Heesen, V., Staffehl, M., Basu, A., et al.: [Nearby galaxies in the LOFAR Two-metre Sky Survey. I. Insights into the non-linearity of the radio-SFR relation](http://adsabs.harvard.edu/abs/2022A&A...664A..83H), 2022, Astronomy and Astrophysics, 664, A83

6. Maschmann, Daniel, Melchior, Anne-Laure, Combes, Francoise, et al.: [Central star formation in double-peak, gas-rich radio galaxies](http://adsabs.harvard.edu/abs/2022A&A...664A.125M), 2022, Astronomy and Astrophysics, 664, A125

7. Vacca, Valentina, Govoni, Federica, Murgia, Matteo, et al.: [Puzzling large-scale polarization in the galaxy cluster Abell 523](http://adsabs.harvard.edu/abs/2022MNRAS.514.4969V), 2022, Monthly Notices of the Royal Astronomical Society, 514, 4969

8. Rudnick, L., Brüggen, M., Brunetti, G., et al.: [Intracluster Magnetic Filaments and an Encounter with a Radio Jet](http://adsabs.harvard.edu/abs/2022ApJ...935..168R), 2022, The Astrophysical Journal, 935, 168


2022-07
-------

1. Bonafede, A., Brunetti, G., Rudnick, L., et al.: [The Coma Cluster at LOFAR Frequencies. II. The Halo, Relic, and a New Accretion Relic](http://adsabs.harvard.edu/abs/2022ApJ...933..218B), 2022, The Astrophysical Journal, 933, 218

2. Erceg, Ana, Jelić, Vibor, Haverkorn, Marijke, et al.: [Faraday tomography of LoTSS-DR2 data. I. Faraday moments in the high-latitude outer Galaxy and revealing Loop III in polarisation](http://adsabs.harvard.edu/abs/2022A&A...663A...7E), 2022, Astronomy and Astrophysics, 663, A7

3. Retana-Montenegro, E.: [What is the origin of the stacked radio emission in radio-undetected quasars?. Insights from a radio-infrared image stacking analysis](http://adsabs.harvard.edu/abs/2022A&A...663A.153R), 2022, Astronomy and Astrophysics, 663, A153

4. Kondapally, Rohit, Best, Philip N., Cochrane, Rachel K., et al.: [Cosmic evolution of low-excitation radio galaxies in the LOFAR two-metre sky survey deep fields](http://adsabs.harvard.edu/abs/2022MNRAS.513.3742K), 2022, Monthly Notices of the Royal Astronomical Society, 513, 3742

5. Kappes, A., Burd, P. R., Kadler, M., et al.: [Subarcsecond view on the high-redshift blazar GB 1508+5714 by the International LOFAR Telescope](http://adsabs.harvard.edu/abs/2022A&A...663A..44K), 2022, Astronomy and Astrophysics, 663, A44

6. Pasini, T., Edler, H. W., Brüggen, M., et al.: [Particle re-acceleration and diffuse radio sources in the galaxy cluster Abell 1550](http://adsabs.harvard.edu/abs/2022A&A...663A.105P), 2022, Astronomy and Astrophysics, 663, A105

7. Bracco, Andrea, Ntormousi, Evangelia, Jelić, Vibor, et al.: [First look at the multiphase interstellar medium using synthetic observations from low-frequency Faraday tomography](http://adsabs.harvard.edu/abs/2022A&A...663A..37B), 2022, Astronomy and Astrophysics, 663, A37

8. Gopal Krishna, Dabhade, Pratik: [X-shaped radio galaxy 3C 223.1: A `double boomerang' with an anomalous spectral gradient](http://adsabs.harvard.edu/abs/2022A&A...663L...8G), 2022, Astronomy and Astrophysics, 663, L8


2022-06
-------

1. Gehlot, B. K., Koopmans, L. V. E., Offringa, A. R., et al.: [Degree-scale galactic radio emission at 122 MHz around the North Celestial Pole with LOFAR-AARTFAAC](http://adsabs.harvard.edu/abs/2022A&A...662A..97G), 2022, Astronomy and Astrophysics, 662, A97

2. McCheyne, I., Oliver, S., Sargent, M., et al.: [The LOFAR Two-metre Sky Survey Deep fields. The mass dependence of the far-infrared radio correlation at 150 MHz using deblended Herschel fluxes](http://adsabs.harvard.edu/abs/2022A&A...662A.100M), 2022, Astronomy and Astrophysics, 662, A100

3. Churazov, E. M., Khabibullin, I. I., Bykov, A. M., et al.: [LOFAR detection of faint radio emission from the supernova remnant SRGe J0023+3625 = G116.6-26.1: probing the Milky Way synchrotron halo](http://adsabs.harvard.edu/abs/2022MNRAS.513L..83C), 2022, Monthly Notices of the Royal Astronomical Society, 513, L83

4. Shulevski, A., Franzen, T. M. O., Williams, W. L., et al.: [Characterization of the AARTFAAC-12 aperture array: radio source counts at 42 and 61 MHz](http://adsabs.harvard.edu/abs/2022MNRAS.513.1036S), 2022, Monthly Notices of the Royal Astronomical Society, 513, 1036

5. Bruni, G., Bassani, L., Persic, M., et al.: [IGR J18249-3243: a new GeV-emitting FR II and the emerging population of high-energy radio galaxies](http://adsabs.harvard.edu/abs/2022MNRAS.513..886B), 2022, Monthly Notices of the Royal Astronomical Society, 513, 886

6. Oei, Martijn S. S. L., van Weeren, Reinout J., Vazza, Franco, et al.: [Filamentary baryons and where to find them. A forecast of synchrotron radiation from merger and accretion shocks in the local Cosmic Web](http://adsabs.harvard.edu/abs/2022A&A...662A..87O), 2022, Astronomy and Astrophysics, 662, A87

7. Pascale, Massimo, Frye, Brenda L., Dai, Liang, et al.: [Possible Ongoing Merger Discovered by Photometry and Spectroscopy in the Field of the Galaxy Cluster PLCK G165.7+67.0](http://adsabs.harvard.edu/abs/2022ApJ...932...85P), 2022, The Astrophysical Journal, 932, 85


2022-05
-------

1. Sobey, C., Bassa, C. G., O'Sullivan, S. P., et al.: [Searching for pulsars associated with polarised point sources using LOFAR: Initial discoveries from the TULIPP project](http://adsabs.harvard.edu/abs/2022A&A...661A..87S), 2022, Astronomy and Astrophysics, 661, A87

2. Pasini, T., Brüggen, M., Hoang, D. N., et al.: [The eROSITA Final Equatorial-Depth Survey (eFEDS). LOFAR view of brightest cluster galaxies and AGN feedback](http://adsabs.harvard.edu/abs/2022A&A...661A..13P), 2022, Astronomy and Astrophysics, 661, A13

3. Carretti, Ettore, Vacca, V., O'Sullivan, S. P., et al.: [Magnetic field strength in cosmic web filaments](http://adsabs.harvard.edu/abs/2022MNRAS.512..945C), 2022, Monthly Notices of the Royal Astronomical Society, 512, 945

4. Brienza, M., Lovisari, L., Rajpurohit, K., et al.: [The galaxy group NGC 507: Newly detected AGN remnant plasma transported by sloshing](http://adsabs.harvard.edu/abs/2022A&A...661A..92B), 2022, Astronomy and Astrophysics, 661, A92

5. Endsley, Ryan, Stark, Daniel P., Fan, Xiaohui, et al.: [Radio and far-IR emission associated with a massive star-forming galaxy candidate at z ≃ 6.8: a radio-loud AGN in the reionization era?](http://adsabs.harvard.edu/abs/2022MNRAS.512.4248E), 2022, Monthly Notices of the Royal Astronomical Society, 512, 4248

6. Riseley, C. J., Rajpurohit, K., Loi, F., et al.: [A MeerKAT-meets-LOFAR study of MS 1455.0 + 2232: a 590 kiloparsec 'mini'-halo in a sloshing cool-core cluster](http://adsabs.harvard.edu/abs/2022MNRAS.512.4210R), 2022, Monthly Notices of the Royal Astronomical Society, 512, 4210

7. Bera, Soumen, Sasmal, Tapan K., Patra, Dusmanta, et al.: ["Winged" Radio Sources from the LOFAR Two-meter Sky Survey First Data Release (LoTSS DR1)](http://adsabs.harvard.edu/abs/2022ApJS..260....7B), 2022, The Astrophysical Journal Supplement Series, 260, 7

8. Punsly, Brian, Groeneveld, Christian, Hill, Gary J., et al.: [The Energetics of the Central Engine in the Powerful Quasar 3C 298](http://adsabs.harvard.edu/abs/2022AJ....163..194P), 2022, The Astronomical Journal, 163, 194

9. Sun, Haomin, Deng, Hui, Wang, Feng, et al.: [A robust RFI identification for radio interferometry based on a convolutional neural network](http://adsabs.harvard.edu/abs/2022MNRAS.512.2025S), 2022, Monthly Notices of the Royal Astronomical Society, 512, 2025


2022-04
-------

1. Vacca, Valentina, Shimwell, Timothy, Perley, Richard A., et al.: [Spectral study of the diffuse synchrotron source in the galaxy cluster Abell 523](http://adsabs.harvard.edu/abs/2022MNRAS.511.3389V), 2022, Monthly Notices of the Royal Astronomical Society, 511, 3389

2. Capetti, A., Brienza, M., Balmaverde, B., et al.: [The LOFAR view of giant, early-type galaxies: Radio emission from active nuclei and star formation](http://adsabs.harvard.edu/abs/2022A&A...660A..93C), 2022, Astronomy and Astrophysics, 660, A93

3. Wagenveld, J. D., Saxena, A., Duncan, K. J., et al.: [Revealing new high-redshift quasar populations through Gaussian mixture model selection](http://adsabs.harvard.edu/abs/2022A&A...660A..22W), 2022, Astronomy and Astrophysics, 660, A22

4. Machado, J. G. O., Hare, B. M., Scholten, O., et al.: [The Relationship of Lightning Radio Pulse Amplitudes and Source Altitudes as Observed by LOFAR](http://adsabs.harvard.edu/abs/2022E&SS....901958M), 2022, Earth and Space Science, 9, e01958

5. Mingo, B., Croston, J. H., Best, P. N., et al.: [Accretion mode versus radio morphology in the LOFAR Deep Fields](http://adsabs.harvard.edu/abs/2022MNRAS.511.3250M), 2022, Monthly Notices of the Royal Astronomical Society, 511, 3250

6. Botteon, A., Shimwell, T. W., Cassano, R., et al.: [The Planck clusters in the LOFAR sky. I. LoTSS-DR2: New detections and sample overview](http://adsabs.harvard.edu/abs/2022A&A...660A..78B), 2022, Astronomy and Astrophysics, 660, A78

7. Oei, Martijn S. S. L., van Weeren, Reinout J., Hardcastle, Martin J., et al.: [The discovery of a radio galaxy of at least 5 Mpc](http://adsabs.harvard.edu/abs/2022A&A...660A...2O), 2022, Astronomy and Astrophysics, 660, A2

8. Davis, F., Kaviraj, S., Hardcastle, M. J., et al.: [Radio AGN in nearby dwarf galaxies: the important role of AGN in dwarf galaxy evolution](http://adsabs.harvard.edu/abs/2022MNRAS.511.4109D), 2022, Monthly Notices of the Royal Astronomical Society, 511, 4109


2022-03
-------

1. Tiwari, Prabhakar, Zhao, Ruiyang, Zheng, Jinglan, et al.: [Galaxy Power Spectrum and Biasing Results from the LOFAR Two-meter Sky Survey (First Data Release)](http://adsabs.harvard.edu/abs/2022ApJ...928...38T), 2022, The Astrophysical Journal, 928, 38

2. Rajpurohit, K., van Weeren, R. J., Hoeft, M., et al.: [Deep Low-frequency Radio Observations of A2256. I. The Filamentary Radio Relic](http://adsabs.harvard.edu/abs/2022ApJ...927...80R), 2022, The Astrophysical Journal, 927, 80

3. Scholten, O., Hare, B. M., Dwyer, J., et al.: [Interferometric imaging of intensely radiating negative leaders](http://adsabs.harvard.edu/abs/2022PhRvD.105f2007S), 2022, Physical Review D, 105, 062007

4. Ignesti, A., Brunetti, G., Shimwell, T., et al.: [A LOFAR view into the stormy environment of the galaxy cluster 2A0335+096](http://adsabs.harvard.edu/abs/2022A&A...659A..20I), 2022, Astronomy and Astrophysics, 659, A20

5. Shimwell, T. W., Hardcastle, M. J., Tasse, C., et al.: [The LOFAR Two-metre Sky Survey. V. Second data release](http://adsabs.harvard.edu/abs/2022A&A...659A...1S), 2022, Astronomy and Astrophysics, 659, A1

6. Liu, Ningyu Y., Scholten, Olaf, Hare, Brian M., et al.: [LOFAR Observations of Lightning Initial Breakdown Pulses](http://adsabs.harvard.edu/abs/2022GeoRL..4998073L), 2022, Geophysical Research Letters, 49, e98073


2022-02
-------

1. Badole, S., Venkattu, D., Jackson, N., et al.: [High-resolution imaging with the International LOFAR Telescope: Observations of the gravitational lenses MG 0751+2716 and CLASS B1600+434](http://adsabs.harvard.edu/abs/2022A&A...658A...7B), 2022, Astronomy and Astrophysics, 658, A7

2. Gourdji, K., Rowlinson, A., Wijers, R. A. M. J., et al.: [Searching for low radio-frequency gravitational wave counterparts in wide-field LOFAR data](http://adsabs.harvard.edu/abs/2022MNRAS.509.5018G), 2022, Monthly Notices of the Royal Astronomical Society, 509, 5018

3. Vedantham, H. K., Callingham, J. R., Shimwell, T. W., et al.: [Peculiar Radio-X-Ray Relationship in Active Stars](http://adsabs.harvard.edu/abs/2022ApJ...926L..30V), 2022, The Astrophysical Journal, 926, L30

4. Jackson, N., Badole, S., Morgan, J., et al.: [Sub-arcsecond imaging with the International LOFAR Telescope. II. Completion of the LOFAR Long-Baseline Calibrator Survey](http://adsabs.harvard.edu/abs/2022A&A...658A...2J), 2022, Astronomy and Astrophysics, 658, A2

5. Kukreti, Pranav, Morganti, Raffaella, Shimwell, Timothy W., et al.: [Unmasking the history of 3C 293 with LOFAR sub-arcsecond imaging](http://adsabs.harvard.edu/abs/2022A&A...658A...6K), 2022, Astronomy and Astrophysics, 658, A6

6. Sweijen, F., Morabito, L. K., Harwood, J., et al.: [High-resolution international LOFAR observations of 4C 43.15. Spectral ages and injection indices in a high-z radio galaxy](http://adsabs.harvard.edu/abs/2022A&A...658A...3S), 2022, Astronomy and Astrophysics, 658, A3

7. Gordovskyy, Mykola, Kontar, Eduard P., Clarkson, Daniel L., et al.: [Sizes and Shapes of Sources in Solar Metric Radio Bursts](http://adsabs.harvard.edu/abs/2022ApJ...925..140G), 2022, The Astrophysical Journal, 925, 140

8. Timmerman, R., van Weeren, R. J., Callingham, J. R., et al.: [Origin of the ring structures in Hercules A. Sub-arcsecond 144 MHz to 7 GHz observations](http://adsabs.harvard.edu/abs/2022A&A...658A...5T), 2022, Astronomy and Astrophysics, 658, A5

9. Roberts, I. D., van Weeren, R. J., Timmerman, R., et al.: [LoTSS jellyfish galaxies. III. The first identification of jellyfish galaxies in the Perseus cluster](http://adsabs.harvard.edu/abs/2022A&A...658A..44R), 2022, Astronomy and Astrophysics, 658, A44

10. Bonnassieux, Etienne, Sweijen, Frits, Brienza, Marisa, et al.: [Spectral analysis of spatially resolved 3C295 (sub-arcsecond resolution) with the International LOFAR Telescope](http://adsabs.harvard.edu/abs/2022A&A...658A..10B), 2022, Astronomy and Astrophysics, 658, A10

11. Morabito, L. K., Jackson, N. J., Mooney, S., et al.: [Sub-arcsecond imaging with the International LOFAR Telescope. I. Foundational calibration strategy and pipeline](http://adsabs.harvard.edu/abs/2022A&A...658A...1M), 2022, Astronomy and Astrophysics, 658, A1

12. Groeneveld, C., van Weeren, R. J., Miley, G. K., et al.: [Pushing sub-arcsecond resolution imaging down to 30 MHz with the trans-European International LOFAR Telescope](http://adsabs.harvard.edu/abs/2022A&A...658A...9G), 2022, Astronomy and Astrophysics, 658, A9

13. Harwood, J. J., Mooney, S., Morabito, L. K., et al.: [The resolved jet of 3C 273 at 150 MHz. Sub-arcsecond imaging with the LOFAR international baselines](http://adsabs.harvard.edu/abs/2022A&A...658A...8H), 2022, Astronomy and Astrophysics, 658, A8

14. Ramírez-Olivencia, N., Varenius, E., Pérez-Torres, M., et al.: [Sub-arcsecond LOFAR imaging of Arp 299 at 150 MHz. Tracing the nuclear and diffuse extended emission of a bright LIRG](http://adsabs.harvard.edu/abs/2022A&A...658A...4R), 2022, Astronomy and Astrophysics, 658, A4


2022-01
-------

1. Offringa, A. R., Singal, J., Heston, S., et al.: [Measurement of the anisotropy power spectrum of the radio synchrotron background](http://adsabs.harvard.edu/abs/2022MNRAS.509..114O), 2022, Monthly Notices of the Royal Astronomical Society, 509, 114

2. Heald, G. H., Heesen, V., Sridhar, S. S., et al.: [CHANG-ES XXIII: influence of a galactic wind in NGC 5775](http://adsabs.harvard.edu/abs/2022MNRAS.509..658H), 2022, Monthly Notices of the Royal Astronomical Society, 509, 658

3. Ignesti, Alessandro, Vulcani, Benedetta, Poggianti, Bianca M., et al.: [GASP XXXVIII: The LOFAR-MeerKAT-VLA View on the Nonthermal Side of a Jellyfish Galaxy](http://adsabs.harvard.edu/abs/2022ApJ...924...64I), 2022, The Astrophysical Journal, 924, 64

4. Roshi, D. Anish, Peters, W. M., Emig, K. L., et al.: [Arecibo-Green Bank-LOFAR Carbon Radio Recombination Line Observations toward Cold H I Clouds](http://adsabs.harvard.edu/abs/2022ApJ...925....7R), 2022, The Astrophysical Journal, 925, 7

5. Hutschenreuter, S., Anderson, C. S., Betti, S., et al.: [The Galactic Faraday rotation sky 2020](http://adsabs.harvard.edu/abs/2022A&A...657A..43H), 2022, Astronomy and Astrophysics, 657, A43

6. Sweijen, F., van Weeren, R. J., Röttgering, H. J. A., et al.: [Deep sub-arcsecond wide-field imaging of the Lockman Hole field at 144 MHz](http://adsabs.harvard.edu/abs/2022NatAs...6..350S), 2022, Nature Astronomy, 6, 350

7. Barkus, B., Croston, J. H., Piotrowska, J., et al.: [The application of ridgelines in extended radio source cross-identification](http://adsabs.harvard.edu/abs/2022MNRAS.509....1B), 2022, Monthly Notices of the Royal Astronomical Society, 509, 1

8. Cho, Hyejeon, James Jee, M., Smith, Rory, et al.: [Multiwavelength Analysis of A1240, the Double Radio-relic Merging Galaxy Cluster Embedded in an 80 Mpc-long Cosmic Filament](http://adsabs.harvard.edu/abs/2022ApJ...925...68C), 2022, The Astrophysical Journal, 925, 68

9. Mevius, M., Mertens, F., Koopmans, L. V. E., et al.: [A numerical study of 21-cm signal suppression and noise increase in direction-dependent calibration of LOFAR data](http://adsabs.harvard.edu/abs/2022MNRAS.509.3693M), 2022, Monthly Notices of the Royal Astronomical Society, 509, 3693

10. Virone, Giuseppe, Paonessa, Fabio, Ciorba, Lorenzo, et al.: [Measurement of the LOFAR-HBA beam patterns using an unmanned aerial vehicle in the near field](http://adsabs.harvard.edu/abs/2022JATIS...8a1005V), 2022, Journal of Astronomical Telescopes, Instruments, and Systems, 8, 011005


2021-12
-------

1. Webster, B., Croston, J. H., Harwood, J. J., et al.: [Investigating the spectra and physical nature of galaxy scale jets](http://adsabs.harvard.edu/abs/2021MNRAS.508.5972W), 2021, Monthly Notices of the Royal Astronomical Society, 508, 5972

2. Biava, N., de Gasperin, F., Bonafede, A., et al.: [The ultra-steep diffuse radio emission observed in the cool-core cluster RX J1720.1+2638 with LOFAR at 54 MHz](http://adsabs.harvard.edu/abs/2021MNRAS.508.3995B), 2021, Monthly Notices of the Royal Astronomical Society, 508, 3995

3. Gloudemans, A. J., Duncan, K. J., Röttgering, H. J. A., et al.: [Low frequency radio properties of the z &gt; ​5 quasar population](http://adsabs.harvard.edu/abs/2021A&A...656A.137G), 2021, Astronomy and Astrophysics, 656, A137

4. Roy, Namrata, Moravec, Emily, Bundy, Kevin, et al.: [Radio Morphology of Red Geysers](http://adsabs.harvard.edu/abs/2021ApJ...922..230R), 2021, The Astrophysical Journal, 922, 230

5. de Ruiter, Iris, Leseigneur, Guillaume, Rowlinson, Antonia, et al.: [Limits on long-time-scale radio transients at 150 MHz using the TGSS ADR1 and LoTSS DR2 catalogues](http://adsabs.harvard.edu/abs/2021MNRAS.508.2412D), 2021, Monthly Notices of the Royal Astronomical Society, 508, 2412

6. Callingham, J. R., Vedantham, H. K., Shimwell, T. W., et al.: [The population of M dwarfs observed at low radio frequencies](http://adsabs.harvard.edu/abs/2021NatAs...5.1233C), 2021, Nature Astronomy, 5, 1233

7. Mooney, Seán, Massaro, Francesco, Quinn, John, et al.: [Characterising the Extended Morphologies of BL Lacertae Objects at 144 MHz with LOFAR](http://adsabs.harvard.edu/abs/2021ApJS..257...30M), 2021, The Astrophysical Journal Supplement Series, 257, 30

8. Bonato, M., Prandoni, I., De Zotti, G., et al.: [The LOFAR Two-metre Sky Survey Deep Fields. A new analysis of low-frequency radio luminosity as a star-formation tracer in the Lockman Hole region](http://adsabs.harvard.edu/abs/2021A&A...656A..48B), 2021, Astronomy and Astrophysics, 656, A48

9. Gürkan, Gülay, Croston, Judith, Hardcastle, Martin J., et al.: [Finding Rare Quasars: VLA Snapshot Continuum Survey of FRI Quasar Candidates Selected from the LOFAR Two-Metre Sky Survey (LoTSS)](http://adsabs.harvard.edu/abs/2021Galax..10....2G), 2021, Galaxies, 10, 2

10. Sterpka, C., Dwyer, J., Liu, N., et al.: [The Spontaneous Nature of Lightning Initiation Revealed](http://adsabs.harvard.edu/abs/2021GeoRL..4895511S), 2021, Geophysical Research Letters, 48, e95511

11. Hoang, D. N., Zhang, X., Stuardi, C., et al.: [A 3.5 Mpc long radio relic in the galaxy cluster ClG 0217+70](http://adsabs.harvard.edu/abs/2021A&A...656A.154H), 2021, Astronomy and Astrophysics, 656, A154

12. Richards, Gordon T., McCaffrey, Trevor V., Kimball, Amy, et al.: [Probing the Wind Component of Radio Emission in Luminous High-redshift Quasars](http://adsabs.harvard.edu/abs/2021AJ....162..270R), 2021, The Astronomical Journal, 162, 270


2021-11
-------

1. Williams, W. L., de Gasperin, F., Hardcastle, M. J. H., et al.: [The LOFAR LBA Sky Survey: Deep Fields. I. The Boötes Field](http://adsabs.harvard.edu/abs/2021A&A...655A..40W), 2021, Astronomy and Astrophysics, 655, A40

2. Murphy, P. C., Callanan, P., McCauley, J., et al.: [First results from the REAL-time Transient Acquisition backend (REALTA) at the Irish LOFAR station](http://adsabs.harvard.edu/abs/2021A&A...655A..16M), 2021, Astronomy and Astrophysics, 655, A16

3. Morganti, Raffaella, Jurlin, Nika, Oosterloo, Tom, et al.: [Combining LOFAR and Apertif Data for Understanding the Life Cycle of Radio Galaxies](http://adsabs.harvard.edu/abs/2021Galax...9...88M), 2021, Galaxies, 9, 88


2021-10
-------

1. Turić, Luka, Jelić, Vibor, Jaspers, Rutger, et al.: [Multi-tracer analysis of straight depolarisation canals in the surroundings of the 3C 196 field](http://adsabs.harvard.edu/abs/2021A&A...654A...5T), 2021, Astronomy and Astrophysics, 654, A5

2. Toet, S. E. B., Vedantham, H. K., Callingham, J. R., et al.: [Coherent radio emission from a population of RS Canum Venaticorum systems](http://adsabs.harvard.edu/abs/2021A&A...654A..21T), 2021, Astronomy and Astrophysics, 654, A21

3. Rowlinson, A., Starling, R. L. C., Gourdji, K., et al.: [LOFAR early-time search for coherent radio emission from short GRB 181123B](http://adsabs.harvard.edu/abs/2021MNRAS.506.5268R), 2021, Monthly Notices of the Royal Astronomical Society, 506, 5268

4. Macfarlane, C., Best, P. N., Sabater, J., et al.: [The radio loudness of SDSS quasars from the LOFAR Two-metre Sky Survey: ubiquitous jet activity and constraints on star formation](http://adsabs.harvard.edu/abs/2021MNRAS.506.5888M), 2021, Monthly Notices of the Royal Astronomical Society, 506, 5888

5. Scholten, O., Hare, B. M., Dwyer, J., et al.: [Distinguishing features of high altitude negative leaders as observed with LOFAR](http://adsabs.harvard.edu/abs/2021AtmRe.26005688S), 2021, Atmospheric Research, 260, 105688

6. Hajduk, Marcin, Haverkorn, Marijke, Shimwell, Timothy, et al.: [Evidence for Cold Plasma in Planetary Nebulae From Radio Observations With the LOw Frequency ARray (LOFAR)](http://adsabs.harvard.edu/abs/2021ApJ...919..121H), 2021, The Astrophysical Journal, 919, 121

7. Di Gennaro, G., van Weeren, R. J., Cassano, R., et al.: [A LOFAR-uGMRT spectral index study of distant radio halos](http://adsabs.harvard.edu/abs/2021A&A...654A.166D), 2021, Astronomy and Astrophysics, 654, A166

8. Hoeft, M., Dumba, C., Drabent, A., et al.: [Abell 1430: A merging cluster with exceptional diffuse radio emission](http://adsabs.harvard.edu/abs/2021A&A...654A..68H), 2021, Astronomy and Astrophysics, 654, A68

9. Carvajal, Rodrigo, Matute, Israel, Afonso, José, et al.: [Exploring New Redshift Indicators for Radio-Powerful AGN](http://adsabs.harvard.edu/abs/2021Galax...9...86C), 2021, Galaxies, 9, 86

10. Nikiel-Wroczyński, Błażej: [Somewhere in between: Tracing the Radio Emission from Galaxy Groups (or Why Does the Future of Observing Galaxy Groups with Radio Telescopes Look Promising?)](http://adsabs.harvard.edu/abs/2021Galax...9...84N), 2021, Galaxies, 9, 84

11. Paul, Surajit, Gupta, Prateek, Salunkhe, Sameer, et al.: [uGMRT detection of cluster radio emission in low-mass Planck Sunyaev-Zel'dovich clusters](http://adsabs.harvard.edu/abs/2021MNRAS.506.5389P), 2021, Monthly Notices of the Royal Astronomical Society, 506, 5389


2021-09
-------

1. Feeney-Johansson, A., Purser, S. J. D., Ray, T. P., et al.: [Detection of coherent low-frequency radio bursts from weak-line T Tauri stars](http://adsabs.harvard.edu/abs/2021A&A...653A.101F), 2021, Astronomy and Astrophysics, 653, A101

2. Jurlin, N., Brienza, M., Morganti, R., et al.: [Multi-frequency characterisation of remnant radio galaxies in the Lockman Hole field](http://adsabs.harvard.edu/abs/2021A&A...653A.110J), 2021, Astronomy and Astrophysics, 653, A110

3. Scholten, O., Hare, B. M., Dwyer, J., et al.: [Time resolved 3D interferometric imaging of a section of a negative leader with LOFAR](http://adsabs.harvard.edu/abs/2021PhRvD.104f3022S), 2021, Physical Review D, 104, 063022

4. de Vos, K., Hatch, N. A., Merrifield, M. R., et al.: [Clusters' far-reaching influence on narrow-angle tail radio galaxies](http://adsabs.harvard.edu/abs/2021MNRAS.506L..55D), 2021, Monthly Notices of the Royal Astronomical Society, 506, L55

5. Järvelä, Emilia, Berton, Marco, Crepaldi, Luca: [Narrow-line Seyfert 1 galaxies with absorbed jets -insights from radio spectral index maps](http://adsabs.harvard.edu/abs/2021FrASS...8..147J), 2021, Frontiers in Astronomy and Space Sciences, 8, 147


2021-08
-------

1. Pastor-Marazuela, Inés, Connor, Liam, van Leeuwen, Joeri, et al.: [Chromatic periodic activity down to 120 megahertz in a fast radio burst](http://adsabs.harvard.edu/abs/2021Natur.596..505P), 2021, Nature, 596, 505

2. Kuiack, Mark, Wijers, Ralph A. M. J., Shulevski, Aleksandar, et al.: [The AARTFAAC 60 MHz transients survey](http://adsabs.harvard.edu/abs/2021MNRAS.505.2966K), 2021, Monthly Notices of the Royal Astronomical Society, 505, 2966

3. Locatelli, N., Vazza, F., Bonafede, A., et al.: [New constraints on the magnetic field in cosmic web filaments](http://adsabs.harvard.edu/abs/2021A&A...652A..80L), 2021, Astronomy and Astrophysics, 652, A80

4. Roberts, I. D., van Weeren, R. J., McGee, S. L., et al.: [LoTSS jellyfish galaxies. II. Ram pressure stripping in groups versus clusters](http://adsabs.harvard.edu/abs/2021A&A...652A.153R), 2021, Astronomy and Astrophysics, 652, A153

5. Jones, A., de Gasperin, F., Cuciti, V., et al.: [Radio relics in PSZ2 G096.88+24.18: a connection with pre-existing plasma](http://adsabs.harvard.edu/abs/2021MNRAS.505.4762J), 2021, Monthly Notices of the Royal Astronomical Society, 505, 4762

6. Edler, H. W., de Gasperin, F., Rafferty, D.: [Investigating ionospheric calibration for LOFAR 2.0 with simulated observations](http://adsabs.harvard.edu/abs/2021A&A...652A..37E), 2021, Astronomy and Astrophysics, 652, A37

7. Gordon, Yjan A., Boyce, Michelle M., O'Dea, Christopher P., et al.: [A Quick Look at the 3 GHz Radio Sky. I. Source Statistics from the Very Large Array Sky Survey](http://adsabs.harvard.edu/abs/2021ApJS..255...30G), 2021, The Astrophysical Journal Supplement Series, 255, 30


2021-07
-------

1. Kuiack, Mark J., Wijers, Ralph A. M. J., Shulevski, Aleksandar, et al.: [Apparent radio transients mapping the near-Earth plasma environment](http://adsabs.harvard.edu/abs/2021MNRAS.504.4706K), 2021, Monthly Notices of the Royal Astronomical Society, 504, 4706

2. van Weeren, R. J., Shimwell, T. W., Botteon, A., et al.: [LOFAR observations of galaxy clusters in HETDEX. Extraction and self-calibration of individual LOFAR targets](http://adsabs.harvard.edu/abs/2021A&A...651A.115V), 2021, Astronomy and Astrophysics, 651, A115

3. Hare, Brian M., Edens, Harald, Krehbiel, Paul, et al.: [Timing Calibration and Windowing Technique Comparison for Lightning Mapping Arrays](http://adsabs.harvard.edu/abs/2021E&SS....801523H), 2021, Earth and Space Science, 8, e01523

4. McKean, J. P., Luichies, R., Drabent, A., et al.: [Gravitational lensing in LoTSS DR2: extremely faint 144-MHz radio emission from two highly magnified quasars](http://adsabs.harvard.edu/abs/2021MNRAS.505L..36M), 2021, Monthly Notices of the Royal Astronomical Society, 505, L36


2021-06
-------

1. Biava, Nadia, Brienza, Marisa, Bonafede, Annalisa, et al.: [Constraining the AGN duty cycle in the cool-core cluster MS 0735.6+7421 with LOFAR data](http://adsabs.harvard.edu/abs/2021A&A...650A.170B), 2021, Astronomy and Astrophysics, 650, A170

2. Masini, Alberto, Celotti, Annalisa, Grandi, Paola, et al.: [A new distant giant radio galaxy in the Boötes field serendipitously detected by Chandra](http://adsabs.harvard.edu/abs/2021A&A...650A..51M), 2021, Astronomy and Astrophysics, 650, A51

3. Broderick, J. W., Russell, T. D., Fender, R. P., et al.: [Strong low-frequency radio flaring from Cygnus X-3 observed with LOFAR](http://adsabs.harvard.edu/abs/2021MNRAS.504.1482B), 2021, Monthly Notices of the Royal Astronomical Society, 504, 1482

4. Davis, I., Vedantham, H. K., Callingham, J. R., et al.: [Large closed-field corona of WX Ursae Majoris evidenced from radio observations](http://adsabs.harvard.edu/abs/2021A&A...650L..20D), 2021, Astronomy and Astrophysics, 650, L20

5. Roberts, I. D., van Weeren, R. J., McGee, S. L., et al.: [LoTSS jellyfish galaxies. I. Radio tails in low redshift clusters](http://adsabs.harvard.edu/abs/2021A&A...650A.111R), 2021, Astronomy and Astrophysics, 650, A111

6. Botteon, A., Cassano, R., van Weeren, R. J., et al.: [Discovery of a Radio Halo (and Relic) in a M<SUB>500</SUB>&lt;2×10<SUP>14</SUP> M<SUB>⊙</SUB> Cluster](http://adsabs.harvard.edu/abs/2021ApJ...914L..29B), 2021, The Astrophysical Journal, 914, L29

7. Bruno, L., Rajpurohit, K., Brunetti, G., et al.: [The LOFAR and JVLA view of the distant steep spectrum radio halo in MACS J1149.5+2223](http://adsabs.harvard.edu/abs/2021A&A...650A..44B), 2021, Astronomy and Astrophysics, 650, A44

8. Bruni, G., Brienza, M., Panessa, F., et al.: [Hard X-ray selected giant radio galaxies - III. The LOFAR view](http://adsabs.harvard.edu/abs/2021MNRAS.503.4681B), 2021, Monthly Notices of the Royal Astronomical Society, 503, 4681


2021-05
-------

1. Corstanje, A., Buitink, S., Falcke, H., et al.: [Depth of shower maximum and mass composition of cosmic rays from 50 PeV to 2 EeV measured with the LOFAR radio telescope](http://adsabs.harvard.edu/abs/2021PhRvD.103j2006C), 2021, Physical Review D, 103, 102006

2. Botteon, A., Giacintucci, S., Gastaldello, F., et al.: [Nonthermal phenomena in the center of Abell 1775. An 800 kpc head-tail, revived fossil plasma and slingshot radio halo](http://adsabs.harvard.edu/abs/2021A&A...649A..37B), 2021, Astronomy and Astrophysics, 649, A37


2021-04
-------

1. Morganti, R., Oosterloo, T. A., Brienza, M., et al.: [The best of both worlds: Combining LOFAR and Apertif to derive resolved radio spectral index images](http://adsabs.harvard.edu/abs/2021A&A...648A...9M), 2021, Astronomy and Astrophysics, 648, A9

2. Ryan, A. M., Gallagher, P. T., Carley, E. P., et al.: [LOFAR imaging of the solar corona during the 2015 March 20 solar eclipse](http://adsabs.harvard.edu/abs/2021A&A...648A..43R), 2021, Astronomy and Astrophysics, 648, A43

3. Hardcastle, M. J., Shimwell, T. W., Tasse, C., et al.: [The contribution of discrete sources to the sky temperature at 144 MHz](http://adsabs.harvard.edu/abs/2021A&A...648A..10H), 2021, Astronomy and Astrophysics, 648, A10

4. Mandal, S., Prandoni, I., Hardcastle, M. J., et al.: [Extremely deep 150 MHz source counts from the LoTSS Deep Fields](http://adsabs.harvard.edu/abs/2021A&A...648A...5M), 2021, Astronomy and Astrophysics, 648, A5

5. Rankine, Amy L., Matthews, James H., Hewett, Paul C., et al.: [Placing LOFAR-detected quasars in C IV emission space: implications for winds, jets and star formation](http://adsabs.harvard.edu/abs/2021MNRAS.502.4154R), 2021, Monthly Notices of the Royal Astronomical Society, 502, 4154

6. Sabater, J., Best, P. N., Tasse, C., et al.: [The LOFAR Two-meter Sky Survey: Deep Fields Data Release 1. II. The ELAIS-N1 LOFAR deep field](http://adsabs.harvard.edu/abs/2021A&A...648A...2S), 2021, Astronomy and Astrophysics, 648, A2

7. Herrera Ruiz, N., O'Sullivan, S. P., Vacca, V., et al.: [LOFAR Deep Fields: probing a broader population of polarized radio galaxies in ELAIS-N1](http://adsabs.harvard.edu/abs/2021A&A...648A..12H), 2021, Astronomy and Astrophysics, 648, A12

8. Ramasawmy, J., Geach, J. E., Hardcastle, M. J., et al.: [Low-frequency radio spectra of submillimetre galaxies in the Lockman Hole](http://adsabs.harvard.edu/abs/2021A&A...648A..14R), 2021, Astronomy and Astrophysics, 648, A14

9. Tasse, C., Shimwell, T., Hardcastle, M. J., et al.: [The LOFAR Two-meter Sky Survey: Deep Fields Data Release 1. I. Direction-dependent calibration and imaging](http://adsabs.harvard.edu/abs/2021A&A...648A...1T), 2021, Astronomy and Astrophysics, 648, A1

10. Smith, D. J. B., Haskell, P., Gürkan, G., et al.: [The LOFAR Two-metre Sky Survey Deep Fields. The star-formation rate-radio luminosity relation at low frequencies](http://adsabs.harvard.edu/abs/2021A&A...648A...6S), 2021, Astronomy and Astrophysics, 648, A6

11. Osinga, E., van Weeren, R. J., Boxelaar, J. M., et al.: [Diffuse radio emission from galaxy clusters in the LOFAR Two-metre Sky Survey Deep Fields](http://adsabs.harvard.edu/abs/2021A&A...648A..11O), 2021, Astronomy and Astrophysics, 648, A11

12. Pleunis, Z., Michilli, D., Bassa, C. G., et al.: [LOFAR Detection of 110-188 MHz Emission and Frequency-dependent Activity from FRB 20180916B](http://adsabs.harvard.edu/abs/2021ApJ...911L...3P), 2021, The Astrophysical Journal, 911, L3

13. Wang, L., Gao, F., Best, P. N., et al.: [The bright end of the infrared luminosity functions and the abundance of hyperluminous infrared galaxies](http://adsabs.harvard.edu/abs/2021A&A...648A...8W), 2021, Astronomy and Astrophysics, 648, A8

14. Gloudemans, A. J., Duncan, K. J., Kondapally, R., et al.: [LOFAR properties of SILVERRUSH Lyα emitter candidates in the ELAIS-N1 field](http://adsabs.harvard.edu/abs/2021A&A...648A...7G), 2021, Astronomy and Astrophysics, 648, A7

15. Lan 藍鼎文, Ting-Wen, Xavier Prochaska, J.: [On the environments of giant radio galaxies](http://adsabs.harvard.edu/abs/2021MNRAS.502.5104L), 2021, Monthly Notices of the Royal Astronomical Society, 502, 5104

16. Duncan, K. J., Kondapally, R., Brown, M. J. I., et al.: [The LOFAR Two-meter Sky Survey: Deep Fields Data Release 1. IV. Photometric redshifts and stellar masses](http://adsabs.harvard.edu/abs/2021A&A...648A...4D), 2021, Astronomy and Astrophysics, 648, A4

17. Kondapally, R., Best, P. N., Hardcastle, M. J., et al.: [The LOFAR Two-meter Sky Survey: Deep Fields Data Release 1. III. Host-galaxy identifications and value added catalogues](http://adsabs.harvard.edu/abs/2021A&A...648A...3K), 2021, Astronomy and Astrophysics, 648, A3

18. Callingham, J. R., Pope, B. J. S., Feinstein, A. D., et al.: [Low-frequency monitoring of flare star binary CR Draconis: long-term electron-cyclotron maser emission](http://adsabs.harvard.edu/abs/2021A&A...648A..13C), 2021, Astronomy and Astrophysics, 648, A13

19. Sudoh, Takahiro, Linden, Tim, Beacom, John F.: [Millisecond pulsars modify the radio-star-formation-rate correlation in quiescent galaxies](http://adsabs.harvard.edu/abs/2021PhRvD.103h3017S), 2021, Physical Review D, 103, 083017

20. Ntwaetsile, Kushatha, Geach, James E.: [Rapid sorting of radio galaxy morphology using Haralick features](http://adsabs.harvard.edu/abs/2021MNRAS.502.3417N), 2021, Monthly Notices of the Royal Astronomical Society, 502, 3417


2021-03
-------

1. Mahatma, V. H., Hardcastle, M. J., Harwood, J., et al.: [A low-frequency study of linear polarization in radio galaxies](http://adsabs.harvard.edu/abs/2021MNRAS.502..273M), 2021, Monthly Notices of the Royal Astronomical Society, 502, 273

2. Alonso, David, Bellini, Emilio, Hale, Catherine, et al.: [Cross-correlating radio continuum surveys and CMB lensing: constraining redshift distributions, galaxy bias, and cosmology](http://adsabs.harvard.edu/abs/2021MNRAS.502..876A), 2021, Monthly Notices of the Royal Astronomical Society, 502, 876

3. Ghirardini, V., Bulbul, E., Hoang, D. N., et al.: [Discovery of a supercluster in the eROSITA Final Equatorial Depth Survey: X-ray properties, radio halo, and double relics](http://adsabs.harvard.edu/abs/2021A&A...647A...4G), 2021, Astronomy and Astrophysics, 647, A4

4. Wolf, J., Nandra, K., Salvato, M., et al.: [First constraints on the AGN X-ray luminosity function at z   6 from an eROSITA-detected quasar](http://adsabs.harvard.edu/abs/2021A&A...647A...5W), 2021, Astronomy and Astrophysics, 647, A5


2021-02
-------

1. Rajpurohit, K., Brunetti, G., Bonafede, A., et al.: [Physical insights from the spectrum of the radio halo in MACS J0717.5+3745](http://adsabs.harvard.edu/abs/2021A&A...646A.135R), 2021, Astronomy and Astrophysics, 646, A135

2. Jimenez-Gallardo, A., Massaro, F., Paggi, A., et al.: [Extended X-Ray Emission around FR II Radio Galaxies: Hot Spots, Lobes, and Galaxy Clusters](http://adsabs.harvard.edu/abs/2021ApJS..252...31J), 2021, The Astrophysical Journal Supplement Series, 252, 31

3. Scholten, O., Hare, B. M., Dwyer, J., et al.: [The Initial Stage of Cloud Lightning Imaged in High Resolution](http://adsabs.harvard.edu/abs/2021JGRD..12633126S), 2021, Journal of Geophysical Research (Atmospheres), 126, e2020JD033126

4. Hoang, D. N., Shimwell, T. W., Osinga, E., et al.: [LOFAR detection of a low-power radio halo in the galaxy cluster Abell 990](http://adsabs.harvard.edu/abs/2021MNRAS.501..576H), 2021, Monthly Notices of the Royal Astronomical Society, 501, 576

5. Marecki, A., Jamrozy, M., Machalski, J., et al.: [Multifrequency study of a double-double radio galaxy J0028+0035](http://adsabs.harvard.edu/abs/2021MNRAS.501..853M), 2021, Monthly Notices of the Royal Astronomical Society, 501, 853

6. Rajpurohit, K., Wittor, D., van Weeren, R. J., et al.: [Understanding the radio relic emission in the galaxy cluster MACS J0717.5+3745: Spectral analysis](http://adsabs.harvard.edu/abs/2021A&A...646A..56R), 2021, Astronomy and Astrophysics, 646, A56


2021-01
-------

1. Di Gennaro, Gabriella, van Weeren, Reinout J., Brunetti, Gianfranco, et al.: [Fast magnetic field amplification in distant galaxy clusters](http://adsabs.harvard.edu/abs/2021NatAs...5..268D), 2021, Nature Astronomy, 5, 268

2. Bonafede, A., Brunetti, G., Vazza, F., et al.: [The Coma Cluster at LOw Frequency ARray Frequencies. I. Insights into Particle Acceleration Mechanisms in the Radio Bridge](http://adsabs.harvard.edu/abs/2021ApJ...907...32B), 2021, The Astrophysical Journal, 907, 32

3. Webster, B., Croston, J. H., Mingo, B., et al.: [A population of galaxy-scale jets discovered using LOFAR](http://adsabs.harvard.edu/abs/2021MNRAS.500.4921W), 2021, Monthly Notices of the Royal Astronomical Society, 500, 4921

4. Mostert, Rafaël I. J., Duncan, Kenneth J., Röttgering, Huub J. A., et al.: [Unveiling the rarest morphologies of the LOFAR Two-metre Sky Survey radio source population with self-organised maps](http://adsabs.harvard.edu/abs/2021A&A...645A..89M), 2021, Astronomy and Astrophysics, 645, A89

5. Murphy, Pearse C., Carley, Eoin P., Ryan, Aoife Maria, et al.: [LOFAR observations of radio burst source sizes and scattering in the solar corona](http://adsabs.harvard.edu/abs/2021A&A...645A..11M), 2021, Astronomy and Astrophysics, 645, A11

6. Hothi, Ian, Chapman, Emma, Pritchard, Jonathan R., et al.: [Comparing foreground removal techniques for recovery of the LOFAR-EoR 21 cm power spectrum](http://adsabs.harvard.edu/abs/2021MNRAS.500.2264H), 2021, Monthly Notices of the Royal Astronomical Society, 500, 2264

7. Greig, Bradley, Mesinger, Andrei, Koopmans, Léon V. E., et al.: [Interpreting LOFAR 21-cm signal upper limits at z ≈ 9.1 in the context of high-z galaxy and reionization observations](http://adsabs.harvard.edu/abs/2021MNRAS.501....1G), 2021, Monthly Notices of the Royal Astronomical Society, 501, 1


2020-12
-------

1. Bracco, A., Jelić, V., Marchal, A., et al.: [The multiphase and magnetized neutral hydrogen seen by LOFAR](http://adsabs.harvard.edu/abs/2020A&A...644L...3B), 2020, Astronomy and Astrophysics, 644, L3

2. Gehlot, B. K., Mertens, F. G., Koopmans, L. V. E., et al.: [The AARTFAAC Cosmic Explorer: observations of the 21-cm power spectrum in the EDGES absorption trough](http://adsabs.harvard.edu/abs/2020MNRAS.499.4158G), 2020, Monthly Notices of the Royal Astronomical Society, 499, 4158

3. Zheng, X. C., Röttgering, H. J. A., Best, P. N., et al.: [Link between radio-loud AGNs and host-galaxy shape](http://adsabs.harvard.edu/abs/2020A&A...644A..12Z), 2020, Astronomy and Astrophysics, 644, A12

4. Botteon, A., van Weeren, R. J., Brunetti, G., et al.: [A giant radio bridge connecting two galaxy clusters in Abell 1758](http://adsabs.harvard.edu/abs/2020MNRAS.499L..11B), 2020, Monthly Notices of the Royal Astronomical Society, 499, L11

5. de Gasperin, F., Lazio, T. J. W., Knapp, M.: [Radio observations of HD 80606 near planetary periastron. II. LOFAR low band antenna observations at 30-78 MHz](http://adsabs.harvard.edu/abs/2020A&A...644A.157D), 2020, Astronomy and Astrophysics, 644, A157


2020-11
-------

1. Vedantham, H. K., Callingham, J. R., Shimwell, T. W., et al.: [Direct Radio Discovery of a Cold Brown Dwarf](http://adsabs.harvard.edu/abs/2020ApJ...903L..33V), 2020, The Astrophysical Journal, 903, L33

2. Siewert, T. M., Hale, C., Bhardwaj, N., et al.: [One- and two-point source statistics from the LOFAR Two-metre Sky Survey first data release](http://adsabs.harvard.edu/abs/2020A&A...643A.100S), 2020, Astronomy and Astrophysics, 643, A100

3. Ignesti, A., Shimwell, T., Brunetti, G., et al.: [The great Kite in the sky: A LOFAR observation of the radio source in Abell 2626](http://adsabs.harvard.edu/abs/2020A&A...643A.172I), 2020, Astronomy and Astrophysics, 643, A172

4. Mondal, R., Fialkov, A., Fling, C., et al.: [Tight constraints on the excess radio background at z = 9.1 from LOFAR](http://adsabs.harvard.edu/abs/2020MNRAS.498.4178M), 2020, Monthly Notices of the Royal Astronomical Society, 498, 4178


2020-10
-------

1. Capetti, A., Brienza, M., Baldi, R. D., et al.: [The LOFAR view of FR 0 radio galaxies](http://adsabs.harvard.edu/abs/2020A&A...642A.107C), 2020, Astronomy and Astrophysics, 642, A107

2. Osinga, E., Miley, G. K., van Weeren, R. J., et al.: [Alignment in the orientation of LOFAR radio sources](http://adsabs.harvard.edu/abs/2020A&A...642A..70O), 2020, Astronomy and Astrophysics, 642, A70

3. de Gasperin, F., Brunetti, G., Brüggen, M., et al.: [Reaching thermal noise at ultra-low radio frequencies. Toothbrush radio relic downstream of the shock front](http://adsabs.harvard.edu/abs/2020A&A...642A..85D), 2020, Astronomy and Astrophysics, 642, A85


2020-09
-------

1. Kuiack, Mark, Wijers, Ralph A. M. J., Rowlinson, Antonia, et al.: [Long-term study of extreme giant pulses from PSR B0950+08 with AARTFAAC](http://adsabs.harvard.edu/abs/2020MNRAS.497..846K), 2020, Monthly Notices of the Royal Astronomical Society, 497, 846


2020-08
-------

1. Shabala, Stanislav S., Jurlin, Nika, Morganti, Raffaella, et al.: [The duty cycle of radio galaxies revealed by LOFAR: remnant and restarted radio source populations in the Lockman Hole](http://adsabs.harvard.edu/abs/2020MNRAS.496.1706S), 2020, Monthly Notices of the Royal Astronomical Society, 496, 1706

2. Bîrzan, L., Rafferty, D. A., Brüggen, M., et al.: [LOFAR observations of X-ray cavity systems](http://adsabs.harvard.edu/abs/2020MNRAS.496.2613B), 2020, Monthly Notices of the Royal Astronomical Society, 496, 2613

3. Vollmann, Martin, Heesen, Volker, W. Shimwell, Timothy, et al.: [Radio constraints on dark matter annihilation in Canes Venatici I with LOFAR](http://adsabs.harvard.edu/abs/2020MNRAS.496.2663V), 2020, Monthly Notices of the Royal Astronomical Society, 496, 2663

4. Mesarcik, Michael, Boonstra, Albert-Jan, Meijer, Christiaan, et al.: [Deep learning assisted data inspection for radio astronomy](http://adsabs.harvard.edu/abs/2020MNRAS.496.1517M), 2020, Monthly Notices of the Royal Astronomical Society, 496, 1517

5. Moravec, Emily, Gonzalez, Anthony H., Dicker, Simon, et al.: [The Massive and Distant Clusters of WISE Survey. IX. High Radio Activity in a Merging Cluster](http://adsabs.harvard.edu/abs/2020ApJ...898..145M), 2020, The Astrophysical Journal, 898, 145


2020-07
-------

1. Botteon, A., Brunetti, G., van Weeren, R. J., et al.: [The Beautiful Mess in Abell 2255](http://adsabs.harvard.edu/abs/2020ApJ...897...93B), 2020, The Astrophysical Journal, 897, 93

2. Locatelli, Nicola T., Rajpurohit, Kamlesh, Vazza, Franco, et al.: [Discovering the most elusive radio relic in the sky: diffuse shock acceleration caught in the act?](http://adsabs.harvard.edu/abs/2020MNRAS.496L..48L), 2020, Monthly Notices of the Royal Astronomical Society, 496, L48

3. O'Sullivan, S. P., Brüggen, M., Vazza, F., et al.: [New constraints on the magnetization of the cosmic web using LOFAR Faraday rotation observations](http://adsabs.harvard.edu/abs/2020MNRAS.495.2607O), 2020, Monthly Notices of the Royal Astronomical Society, 495, 2607

4. Stein, Y., Dettmar, R. -J., Beck, R., et al.: [CHANG-ES. XXI. Transport processes and the X-shaped magnetic field of NGC 4217: off-center superbubble structure](http://adsabs.harvard.edu/abs/2020A&A...639A.111S), 2020, Astronomy and Astrophysics, 639, A111


2020-06
-------

1. Starling, R. L. C., Rowlinson, A., van der Horst, A. J., et al.: [LOFAR detectability of prompt low-frequency radio emission during gamma-ray burst X-ray flares](http://adsabs.harvard.edu/abs/2020MNRAS.494.5787S), 2020, Monthly Notices of the Royal Astronomical Society, 494, 5787

2. Jurlin, N., Morganti, R., Brienza, M., et al.: [The life cycle of radio galaxies in the LOFAR Lockman Hole field](http://adsabs.harvard.edu/abs/2020A&A...638A..34J), 2020, Astronomy and Astrophysics, 638, A34

3. Brienza, M., Morganti, R., Harwood, J., et al.: [Radio spectral properties and jet duty cycle in the restarted radio galaxy 3C388](http://adsabs.harvard.edu/abs/2020A&A...638A..29B), 2020, Astronomy and Astrophysics, 638, A29

4. Broderick, J. W., Shimwell, T. W., Gourdji, K., et al.: [LOFAR 144-MHz follow-up observations of GW170817](http://adsabs.harvard.edu/abs/2020MNRAS.494.5110B), 2020, Monthly Notices of the Royal Astronomical Society, 494, 5110

5. Chawla, P., Andersen, B. C., Bhardwaj, M., et al.: [Detection of Repeating FRB 180916.J0158+65 Down to Frequencies of 300 MHz](http://adsabs.harvard.edu/abs/2020ApJ...896L..41C), 2020, The Astrophysical Journal, 896, L41

6. Stuardi, C., O'Sullivan, S. P., Bonafede, A., et al.: [The LOFAR view of intergalactic magnetic fields with giant radio galaxies](http://adsabs.harvard.edu/abs/2020A&A...638A..48S), 2020, Astronomy and Astrophysics, 638, A48

7. Cantwell, T. M., Bray, J. D., Croston, J. H., et al.: [Low-frequency observations of the giant radio galaxy NGC 6251](http://adsabs.harvard.edu/abs/2020MNRAS.495..143C), 2020, Monthly Notices of the Royal Astronomical Society, 495, 143


2020-05
-------

1. Bonnassieux, Etienne, Edge, Alastair, Morabito, Leah, et al.: [Decoherence in LOFAR-VLBI beamforming](http://adsabs.harvard.edu/abs/2020A&A...637A..51B), 2020, Astronomy and Astrophysics, 637, A51

2. Rosario, D. J., Fawcett, V. A., Klindt, L., et al.: [Fundamental differences in the radio properties of red and blue quasars: insight from the LOFAR Two-metre Sky Survey (LoTSS)](http://adsabs.harvard.edu/abs/2020MNRAS.494.3061R), 2020, Monthly Notices of the Royal Astronomical Society, 494, 3061


2020-04
-------

1. Retana-Montenegro, E., Röttgering, H. J. A.: [The optical luminosity function of LOFAR radio-selected quasars at 1.4 ≤ z ≤ 5.0 in the NDWFS-Boötes field](http://adsabs.harvard.edu/abs/2020A&A...636A..12R), 2020, Astronomy and Astrophysics, 636, A12

2. Nikolajevs, A., Prūsis, K.: [The LOFAR Long-Baseline Calibrator Survey Classification](http://adsabs.harvard.edu/abs/2020LatJP..57a..34N), 2020, Latvian Journal of Physics and Technical Sciences, 57, 34

3. Rajpurohit, K., Hoeft, M., Vazza, F., et al.: [New mysteries and challenges from the Toothbrush relic: wideband observations from 550 MHz to 8 GHz](http://adsabs.harvard.edu/abs/2020A&A...636A..30R), 2020, Astronomy and Astrophysics, 636, A30


2020-03
-------

1. de Gasperin, F., Vink, J., McKean, J. P., et al.: [Cassiopeia A, Cygnus A, Taurus A, and Virgo A at ultra-low radio frequencies](http://adsabs.harvard.edu/abs/2020A&A...635A.150D), 2020, Astronomy and Astrophysics, 635, A150

2. Dabhade, P., Röttgering, H. J. A., Bagchi, J., et al.: [Giant radio galaxies in the LOFAR Two-metre Sky Survey. I. Radio and environmental properties](http://adsabs.harvard.edu/abs/2020A&A...635A...5D), 2020, Astronomy and Astrophysics, 635, A5


2020-02
-------

1. Sridhar, Sarrvesh S., Morganti, Raffaella, Nyland, Kristina, et al.: [LOFAR view of NGC 3998, a sputtering AGN](http://adsabs.harvard.edu/abs/2020A&A...634A.108S), 2020, Astronomy and Astrophysics, 634, A108

2. Mandal, S., Intema, H. T., van Weeren, R. J., et al.: [Revived fossil plasma sources in galaxy clusters](http://adsabs.harvard.edu/abs/2020A&A...634A...4M), 2020, Astronomy and Astrophysics, 634, A4

3. Vedantham, H. K., Callingham, J. R., Shimwell, T. W., et al.: [Coherent radio emission from a quiescent red dwarf indicative of star-planet interaction](http://adsabs.harvard.edu/abs/2020NatAs...4..577V), 2020, Nature Astronomy, 4, 577

4. Emig, K. L., Salas, P., de Gasperin, F., et al.: [Searching for the largest bound atoms in space](http://adsabs.harvard.edu/abs/2020A&A...634A.138E), 2020, Astronomy and Astrophysics, 634, A138

5. Ghara, R., Giri, S. K., Mellema, G., et al.: [Constraining the intergalactic medium at z ≈ 9.1 using LOFAR Epoch of Reionization observations](http://adsabs.harvard.edu/abs/2020MNRAS.493.4728G), 2020, Monthly Notices of the Royal Astronomical Society, 493, 4728


2020-01
-------

1. Broekema, P. Chris, Allan, Verity, van Nieuwpoort, Rob V., et al.: [On optimising cost and value in compute systems for radio astronomy](http://adsabs.harvard.edu/abs/2020A&C....3000337B), 2020, Astronomy and Computing, 30, 100337

2. Paul, Surajit, Salunkhe, Sameer, Sonkamble, Satish, et al.: [Radio relic and the diffuse emission trail discovered in low-mass galaxy cluster Abell 1697](http://adsabs.harvard.edu/abs/2020A&A...633A..59P), 2020, Astronomy and Astrophysics, 633, A59


2019-12
-------

1. Arias, Maria, Vink, Jacco, Zhou, Ping, et al.: [Low-frequency Radio Absorption in Tycho’s Supernova Remnant](http://adsabs.harvard.edu/abs/2019AJ....158..253A), 2019, The Astronomical Journal, 158, 253

2. Rowlinson, A., Gourdji, K., van der Meulen, K., et al.: [LOFAR early-time search for coherent radio emission from GRB 180706A](http://adsabs.harvard.edu/abs/2019MNRAS.490.3483R), 2019, Monthly Notices of the Royal Astronomical Society, 490, 3483

3. Stein, Y., Dettmar, R. -J., Weżgowiec, M., et al.: [CHANG-ES. XIX. Galaxy NGC 4013: a diffusion-dominated radio halo with plane-parallel disk and vertical halo magnetic fields](http://adsabs.harvard.edu/abs/2019A&A...632A..13S), 2019, Astronomy and Astrophysics, 632, A13


2019-11
-------

1. Feeney-Johansson, Anton, Purser, Simon J. D., Ray, Tom P., et al.: [The First Detection of a Low-frequency Turnover in Nonthermal Emission from the Jet of a Young Star](http://adsabs.harvard.edu/abs/2019ApJ...885L...7F), 2019, The Astrophysical Journal, 885, L7

2. Wang, L., Gao, F., Duncan, K. J., et al.: [A LOFAR-IRAS cross-match study: the far-infrared radio correlation and the 150 MHz luminosity as a star-formation rate tracer](http://adsabs.harvard.edu/abs/2019A&A...631A.109W), 2019, Astronomy and Astrophysics, 631, A109

3. Kappes, A., Perucho, M., Kadler, M., et al.: [LOFAR measures the hotspot advance speed of the high-redshift blazar S5 0836+710](http://adsabs.harvard.edu/abs/2019A&A...631A..49K), 2019, Astronomy and Astrophysics, 631, A49

4. Offringa, A. R., Mertens, F., van der Tol, S., et al.: [Precision requirements for interferometric gridding in the analysis of a 21 cm power spectrum](http://adsabs.harvard.edu/abs/2019A&A...631A..12O), 2019, Astronomy and Astrophysics, 631, A12


2019-10
-------

1. Botteon, A., Cassano, R., Eckert, D., et al.: [Particle acceleration in a nearby galaxy cluster pair: the role of cluster dynamics](http://adsabs.harvard.edu/abs/2019A&A...630A..77B), 2019, Astronomy and Astrophysics, 630, A77


2019-09
-------

1. Gehlot, B. K., Mertens, F. G., Koopmans, L. V. E., et al.: [The first power spectrum limit on the 21-cm signal of neutral hydrogen during the Cosmic Dawn at z = 20-25 from LOFAR](http://adsabs.harvard.edu/abs/2019MNRAS.488.4271G), 2019, Monthly Notices of the Royal Astronomical Society, 488, 4271

2. Mulrey, K., Bonardi, A., Buitink, S., et al.: [Calibration of the LOFAR low-band antennas using the Galaxy and a model of the signal chain](http://adsabs.harvard.edu/abs/2019APh...111....1M), 2019, Astroparticle Physics, 111, 1

3. Hardcastle, M. J., Croston, J. H., Shimwell, T. W., et al.: [NGC 326: X-shaped no more](http://adsabs.harvard.edu/abs/2019MNRAS.488.3416H), 2019, Monthly Notices of the Royal Astronomical Society, 488, 3416

4. Mingo, B., Croston, J. H., Hardcastle, M. J., et al.: [Revisiting the Fanaroff-Riley dichotomy and radio-galaxy morphology with the LOFAR Two-Metre Sky Survey (LoTSS)](http://adsabs.harvard.edu/abs/2019MNRAS.488.2701M), 2019, Monthly Notices of the Royal Astronomical Society, 488, 2701

5. Urdampilleta, I., Mernier, F., Kaastra, J. S., et al.: [Iron abundance distribution in the hot gas of merging galaxy clusters](http://adsabs.harvard.edu/abs/2019A&A...629A..31U), 2019, Astronomy and Astrophysics, 629, A31


2019-08
-------

1. Shulevski, A., Barthel, P. D., Morganti, R., et al.: [First look at the giant radio galaxy <ASTROBJ>3C 236</ASTROBJ> with LOFAR](http://adsabs.harvard.edu/abs/2019A&A...628A..69S), 2019, Astronomy and Astrophysics, 628, A69

2. Heesen, V., Whitler, L., Schmidt, P., et al.: [Warped diffusive radio halo around the quiescent spiral edge-on galaxy NGC 4565](http://adsabs.harvard.edu/abs/2019A&A...628L...3H), 2019, Astronomy and Astrophysics, 628, L3

3. Lukic, V., Brüggen, M., Mingo, B., et al.: [Morphological classification of radio galaxies: capsule networks versus convolutional neural networks](http://adsabs.harvard.edu/abs/2019MNRAS.487.1729L), 2019, Monthly Notices of the Royal Astronomical Society, 487, 1729

4. Bîrzan, L., Rafferty, D. A., Cassano, R., et al.: [A massive cluster at z = 0.288 caught in the process of formation: The case of Abell 959](http://adsabs.harvard.edu/abs/2019MNRAS.487.4775B), 2019, Monthly Notices of the Royal Astronomical Society, 487, 4775

5. Cassano, R., Botteon, A., Di Gennaro, G., et al.: [LOFAR Discovery of a Radio Halo in the High-redshift Galaxy Cluster PSZ2 G099.86+58.45](http://adsabs.harvard.edu/abs/2019ApJ...881L..18C), 2019, The Astrophysical Journal, 881, L18


2019-07
-------

1. Clarke, A. O., Scaife, A. M. M., Shimwell, T., et al.: [Signatures from a merging galaxy cluster and its AGN population: LOFAR observations of Abell 1682](http://adsabs.harvard.edu/abs/2019A&A...627A.176C), 2019, Astronomy and Astrophysics, 627, A176

2. Krause, Martin G. H., Hardcastle, Martin J., Shabala, Stanislav S.: [Probing gaseous halos of galaxies with radio jets](http://adsabs.harvard.edu/abs/2019A&A...627A.113K), 2019, Astronomy and Astrophysics, 627, A113

3. Creaner, O., Carozzi, T. D.: [beamModelTester: Software framework for testing radio telescope beams](http://adsabs.harvard.edu/abs/2019A&C....2800311C), 2019, Astronomy and Computing, 28, 100311

4. Mechev, A. P., Shimwell, T. W., Plaat, A., et al.: [Scalability model for the LOFAR direction independent pipeline](http://adsabs.harvard.edu/abs/2019A&C....2800293M), 2019, Astronomy and Computing, 28, 100293


2019-06
-------

1. Gu, Liyi, Akamatsu, Hiroki, Shimwell, Timothy W., et al.: [Observations of a pre-merger shock in colliding clusters of galaxies](http://adsabs.harvard.edu/abs/2019NatAs...3..838G), 2019, Nature Astronomy, 3, 838

2. Salas, P., Oonk, J. B. R., Emig, K. L., et al.: [Carbon radio recombination lines from gigahertz to megahertz frequencies towards Orion A](http://adsabs.harvard.edu/abs/2019A&A...626A..70S), 2019, Astronomy and Astrophysics, 626, A70

3. Govoni, F., Orrù, E., Bonafede, A., et al.: [A radio ridge connecting two galaxy clusters in a filament of the cosmic web](http://adsabs.harvard.edu/abs/2019Sci...364..981G), 2019, Science, 364, 981


2019-05
-------

1. Thwala, S. A., Shafi, N., Colafrancesco, S., et al.: [The study of extended emission in a radio galaxy detected in the LOFAR Two-Metre Sky Survey](http://adsabs.harvard.edu/abs/2019MNRAS.485.1938T), 2019, Monthly Notices of the Royal Astronomical Society, 485, 1938


2019-04
-------

1. Rowlinson, A., Stewart, A. J., Broderick, J. W., et al.: [Identifying transient and variable sources in radio images](http://adsabs.harvard.edu/abs/2019A&C....27..111R), 2019, Astronomy and Computing, 27, 111


2019-03
-------

1. Van Eck, C. L., Haverkorn, M., Alves, M. I. R., et al.: [Diffuse polarized emission in the LOFAR Two-meter Sky Survey](http://adsabs.harvard.edu/abs/2019A&A...623A..71V), 2019, Astronomy and Astrophysics, 623, A71

2. Straal, S. M., van Leeuwen, J.: [A LOFAR search for steep-spectrum pulsars in supernova remnants and pulsar wind nebulae](http://adsabs.harvard.edu/abs/2019A&A...623A..90S), 2019, Astronomy and Astrophysics, 623, A90

3. Harris, D. E., Moldón, J., Oonk, J. R. R., et al.: [LOFAR Observations of 4C+19.44: On the Discovery of Low-frequency Spectral Curvature in Relativistic Jet Knots](http://adsabs.harvard.edu/abs/2019ApJ...873...21H), 2019, The Astrophysical Journal, 873, 21

4. Wei, Liying, Wijnholds, Stefan J.: [Joint calibration and imaging for phased array radio telescopes](http://adsabs.harvard.edu/abs/2019MNRAS.483.5672W), 2019, Monthly Notices of the Royal Astronomical Society, 483, 5672


2019-02
-------

1. Heesen, V., Buie, E., II, Huff, C. J., et al.: [Calibrating the relation of low-frequency radio continuum to star formation rate at 1 kpc scale with LOFAR](http://adsabs.harvard.edu/abs/2019A&A...622A...8H), 2019, Astronomy and Astrophysics, 622, A8

2. Nikiel-Wroczyński, B., Berger, A., Herrera Ruiz, N., et al.: [Exploring the properties of low-frequency radio emission and magnetic fields in a sample of compact galaxy groups using the LOFAR Two-Metre Sky Survey (LoTSS)](http://adsabs.harvard.edu/abs/2019A&A...622A..23N), 2019, Astronomy and Astrophysics, 622, A23

3. de Gasperin, F., Dijkema, T. J., Drabent, A., et al.: [Systematic effects in LOFAR data: A unified calibration strategy](http://adsabs.harvard.edu/abs/2019A&A...622A...5D), 2019, Astronomy and Astrophysics, 622, A5

4. Arias, M., Vink, J., Iacobelli, M., et al.: [A low-frequency view of mixed-morphology supernova remnant VRO 42.05.01, and its neighbourhood](http://adsabs.harvard.edu/abs/2019A&A...622A...6A), 2019, Astronomy and Astrophysics, 622, A6

5. Wilber, A., Brüggen, M., Bonafede, A., et al.: [Evolutionary phases of merging clusters as seen by LOFAR](http://adsabs.harvard.edu/abs/2019A&A...622A..25W), 2019, Astronomy and Astrophysics, 622, A25

6. Mandal, S., Intema, H. T., Shimwell, T. W., et al.: [Ultra-steep spectrum emission in the merging galaxy cluster Abell 1914](http://adsabs.harvard.edu/abs/2019A&A...622A..22M), 2019, Astronomy and Astrophysics, 622, A22

7. Savini, F., Bonafede, A., Brüggen, M., et al.: [A LOFAR study of non-merging massive galaxy clusters](http://adsabs.harvard.edu/abs/2019A&A...622A..24S), 2019, Astronomy and Astrophysics, 622, A24

8. Mahatma, V. H., Hardcastle, M. J., Williams, W. L., et al.: [LoTSS DR1: Double-double radio galaxies in the HETDEX field](http://adsabs.harvard.edu/abs/2019A&A...622A..13M), 2019, Astronomy and Astrophysics, 622, A13

9. Hoang, D. N., Shimwell, T. W., van Weeren, R. J., et al.: [Characterizing the radio emission from the binary galaxy cluster merger Abell 2146](http://adsabs.harvard.edu/abs/2019A&A...622A..21H), 2019, Astronomy and Astrophysics, 622, A21

10. Stacey, H. R., McKean, J. P., Jackson, N. J., et al.: [LoTSS/HETDEX: Disentangling star formation and AGN activity in gravitationally lensed radio-quiet quasars](http://adsabs.harvard.edu/abs/2019A&A...622A..18S), 2019, Astronomy and Astrophysics, 622, A18

11. Botteon, A., Shimwell, T. W., Bonafede, A., et al.: [The spectacular cluster chain Abell 781 as observed with LOFAR, GMRT, and XMM-Newton](http://adsabs.harvard.edu/abs/2019A&A...622A..19B), 2019, Astronomy and Astrophysics, 622, A19

12. Hale, C. L., Williams, W., Jarvis, M. J., et al.: [LOFAR observations of the XMM-LSS field](http://adsabs.harvard.edu/abs/2019A&A...622A...4H), 2019, Astronomy and Astrophysics, 622, A4

13. Hoang, D. N., Shimwell, T. W., van Weeren, R. J., et al.: [Radio observations of the merging galaxy cluster Abell 520](http://adsabs.harvard.edu/abs/2019A&A...622A..20H), 2019, Astronomy and Astrophysics, 622, A20

14. Shimwell, T. W., Tasse, C., Hardcastle, M. J., et al.: [The LOFAR Two-metre Sky Survey. II. First data release](http://adsabs.harvard.edu/abs/2019A&A...622A...1S), 2019, Astronomy and Astrophysics, 622, A1

15. Miskolczi, A., Heesen, V., Horellou, C., et al.: [CHANG-ES XII. A LOFAR and VLA view of the edge-on star-forming galaxy NGC 3556](http://adsabs.harvard.edu/abs/2019A&A...622A...9M), 2019, Astronomy and Astrophysics, 622, A9

16. Emig, K. L., Salas, P., de Gasperin, F., et al.: [The first detection of radio recombination lines at cosmological distances](http://adsabs.harvard.edu/abs/2019A&A...622A...7E), 2019, Astronomy and Astrophysics, 622, A7

17. Duncan, K. J., Sabater, J., Röttgering, H. J. A., et al.: [The LOFAR Two-metre Sky Survey. IV. First Data Release: Photometric redshifts and rest-frame magnitudes](http://adsabs.harvard.edu/abs/2019A&A...622A...3D), 2019, Astronomy and Astrophysics, 622, A3

18. Mooney, S., Quinn, J., Callingham, J. R., et al.: [Blazars in the LOFAR Two-Metre Sky Survey first data release](http://adsabs.harvard.edu/abs/2019A&A...622A..14M), 2019, Astronomy and Astrophysics, 622, A14

19. Hardcastle, M. J., Williams, W. L., Best, P. N., et al.: [Radio-loud AGN in the first LoTSS data release. The lifetimes and environmental impact of jet-driven sources](http://adsabs.harvard.edu/abs/2019A&A...622A..12H), 2019, Astronomy and Astrophysics, 622, A12

20. Morabito, L. K., Matthews, J. H., Best, P. N., et al.: [The origin of radio emission in broad absorption line quasars: Results from the LOFAR Two-metre Sky Survey](http://adsabs.harvard.edu/abs/2019A&A...622A..15M), 2019, Astronomy and Astrophysics, 622, A15

21. Croston, J. H., Hardcastle, M. J., Mingo, B., et al.: [The environments of radio-loud AGN from the LOFAR Two-Metre Sky Survey (LoTSS)](http://adsabs.harvard.edu/abs/2019A&A...622A..10C), 2019, Astronomy and Astrophysics, 622, A10

22. Williams, W. L., Hardcastle, M. J., Best, P. N., et al.: [The LOFAR Two-metre Sky Survey. III. First data release: Optical/infrared identifications and value-added catalogue](http://adsabs.harvard.edu/abs/2019A&A...622A...2W), 2019, Astronomy and Astrophysics, 622, A2

23. Sabater, J., Best, P. N., Hardcastle, M. J., et al.: [The LoTSS view of radio AGN in the local Universe. The most massive galaxies are always switched on](http://adsabs.harvard.edu/abs/2019A&A...622A..17S), 2019, Astronomy and Astrophysics, 622, A17

24. Gürkan, Gülay, Hardcastle, M. J., Best, P. N., et al.: [LoTSS/HETDEX: Optical quasars. I. Low-frequency radio properties of optically selected quasars](http://adsabs.harvard.edu/abs/2019A&A...622A..11G), 2019, Astronomy and Astrophysics, 622, A11

25. O'Sullivan, S. P., Machalski, J., Van Eck, C. L., et al.: [The intergalactic magnetic field probed by a giant radio galaxy](http://adsabs.harvard.edu/abs/2019A&A...622A..16O), 2019, Astronomy and Astrophysics, 622, A16


2019-01
-------

1. Kuiack, Mark, Huizinga, Folkert, Molenaar, Gijs, et al.: [AARTFAAC flux density calibration and Northern hemisphere catalogue at 60 MHz](http://adsabs.harvard.edu/abs/2019MNRAS.482.2502K), 2019, Monthly Notices of the Royal Astronomical Society, 482, 2502


2018-12
-------

1. Retana-Montenegro, E., Röttgering, H. J. A., Shimwell, T. W., et al.: [Deep LOFAR 150 MHz imaging of the Boötes field: Unveiling the faint low-frequency sky](http://adsabs.harvard.edu/abs/2018A&A...620A..74R), 2018, Astronomy and Astrophysics, 620, A74


2018-11
-------

1. Chyży, K. T., Jurusik, W., Piotrowska, J., et al.: [LOFAR MSSS: Flattening low-frequency radio continuum spectra of nearby galaxies](http://adsabs.harvard.edu/abs/2018A&A...619A..36C), 2018, Astronomy and Astrophysics, 619, A36

2. Read, S. C., Smith, D. J. B., Gürkan, G., et al.: [The Far-Infrared Radio Correlation at low radio frequency with LOFAR/H-ATLAS](http://adsabs.harvard.edu/abs/2018MNRAS.480.5625R), 2018, Monthly Notices of the Royal Astronomical Society, 480, 5625

3. O'Sullivan, Shane, Brüggen, Marcus, Van Eck, Cameron, et al.: [Untangling Cosmic Magnetic Fields: Faraday Tomography at Metre Wavelengths with LOFAR](http://adsabs.harvard.edu/abs/2018Galax...6..126O), 2018, Galaxies, 6, 126

4. Farnes, Jamie, Mort, Ben, Dulwich, Fred, et al.: [Science Pipelines for the Square Kilometre Array](http://adsabs.harvard.edu/abs/2018Galax...6..120F), 2018, Galaxies, 6, 120


2018-10
-------

1. Brienza, M., Morganti, R., Murgia, M., et al.: [Duty cycle of the radio galaxy B2 0258+35](http://adsabs.harvard.edu/abs/2018A&A...618A..45B), 2018, Astronomy and Astrophysics, 618, A45

2. Neld, A., Horellou, C., Mulcahy, D. D., et al.: [Reliable detection and characterization of low-frequency polarized sources in the LOFAR M51 field](http://adsabs.harvard.edu/abs/2018A&A...617A.136N), 2018, Astronomy and Astrophysics, 617, A136

3. Sridhar, S. S., Heald, G., van der Hulst, J. M.: [cuFFS: A GPU-accelerated code for Fast Faraday rotation measure Synthesis](http://adsabs.harvard.edu/abs/2018A&C....25..205S), 2018, Astronomy and Computing, 25, 205

4. Van Eck, Cameron: [The Power of Low Frequencies: Faraday Tomography in the Sub-GHz Regime](http://adsabs.harvard.edu/abs/2018Galax...6..112V), 2018, Galaxies, 6, 112

5. Naghibzadeh, Shahrzad, van der Veen, Alle-Jan: [PRIFIRA: General regularization using prior-conditioning for fast radio interferometric imaging](http://adsabs.harvard.edu/abs/2018MNRAS.479.5638N), 2018, Monthly Notices of the Royal Astronomical Society, 479, 5638


2018-09
-------

1. Di Gennaro, G., van Weeren, R. J., Hoeft, M., et al.: [Deep Very Large Array Observations of the Merging Cluster CIZA J2242.8+5301: Continuum and Spectral Imaging](http://adsabs.harvard.edu/abs/2018ApJ...865...24D), 2018, The Astrophysical Journal, 865, 24


2018-08
-------

1. Gehlot, B. K., Koopmans, L. V. E., de Bruyn, A. G., et al.: [Wide-field LOFAR-LBA power-spectra analyses: impact of calibration, polarization leakage, and ionosphere](http://adsabs.harvard.edu/abs/2018MNRAS.478.1484G), 2018, Monthly Notices of the Royal Astronomical Society, 478, 1484

2. Savini, F., Bonafede, A., Brüggen, M., et al.: [First evidence of diffuse ultra-steep-spectrum radio emission surrounding the cool core of a cluster](http://adsabs.harvard.edu/abs/2018MNRAS.478.2234S), 2018, Monthly Notices of the Royal Astronomical Society, 478, 2234

3. de Gasperin, F., Mevius, M., Rafferty, D. A., et al.: [The effect of the ionosphere on ultra-low-frequency radio-interferometric observations](http://adsabs.harvard.edu/abs/2018A&A...615A.179D), 2018, Astronomy and Astrophysics, 615, A179

4. Bonafede, A., Brüggen, M., Rafferty, D., et al.: [LOFAR discoveryof radio emission in MACS J0717.5+3745](http://adsabs.harvard.edu/abs/2018MNRAS.478.2927B), 2018, Monthly Notices of the Royal Astronomical Society, 478, 2927

5. Hoang, D. N., Shimwell, T. W., van Weeren, R. J., et al.: [Radio observations of the double-relic galaxy cluster Abell 1240](http://adsabs.harvard.edu/abs/2018MNRAS.478.2218H), 2018, Monthly Notices of the Royal Astronomical Society, 478, 2218


2018-07
-------

1. Jelić, Vibor, Prelogović, David, Haverkorn, Marijke, et al.: [Magnetically aligned straight depolarization canals and the rolling Hough transform](http://adsabs.harvard.edu/abs/2018A&A...615L...3J), 2018, Astronomy and Astrophysics, 615, L3

2. König, S., Aalto, S., Muller, S., et al.: [Major impact from a minor merger. The extraordinary hot molecular gas flow in the Eye of the NGC 4194 Medusa galaxy](http://adsabs.harvard.edu/abs/2018A&A...615A.122K), 2018, Astronomy and Astrophysics, 615, A122

3. Botteon, A., Shimwell, T. W., Bonafede, A., et al.: [LOFAR discovery of a double radio halo system in Abell 1758 and radio/X-ray study of the cluster pair](http://adsabs.harvard.edu/abs/2018MNRAS.478..885B), 2018, Monthly Notices of the Royal Astronomical Society, 478, 885

4. Brüggen, M., Rafferty, D., Bonafede, A., et al.: [Discovery of large-scale diffuse radio emission in low-mass galaxy cluster Abell 1931](http://adsabs.harvard.edu/abs/2018MNRAS.477.3461B), 2018, Monthly Notices of the Royal Astronomical Society, 477, 3461

5. Mulcahy, D. D., Horneffer, A., Beck, R., et al.: [Investigation of the cosmic ray population and magnetic field strength in the halo of NGC 891](http://adsabs.harvard.edu/abs/2018A&A...615A..98M), 2018, Astronomy and Astrophysics, 615, A98

6. Bonnassieux, Etienne, Tasse, Cyril, Smirnov, Oleg, et al.: [The variance of radio interferometric calibration solutions. Quality-based weighting schemes](http://adsabs.harvard.edu/abs/2018A&A...615A..66B), 2018, Astronomy and Astrophysics, 615, A66

7. Mechev, A. P., Plaat, A., Oonk, J. B. Raymond, et al.: [Pipeline Collector: Gathering performance data for distributed astronomical pipelines](http://adsabs.harvard.edu/abs/2018A&C....24..117M), 2018, Astronomy and Computing, 24, 117


2018-06
-------

1. Driessen, Laura N., Domček, Vladimír, Vink, Jacco, et al.: [Investigating Galactic Supernova Remnant Candidates Using LOFAR](http://adsabs.harvard.edu/abs/2018ApJ...860..133D), 2018, The Astrophysical Journal, 860, 133

2. Vocks, C., Mann, G., Breitling, F., et al.: [LOFAR observations of the quiet solar corona](http://adsabs.harvard.edu/abs/2018A&A...614A..54V), 2018, Astronomy and Astrophysics, 614, A54

3. Van Eck, C. L., Haverkorn, M., Alves, M. I. R., et al.: [Polarized point sources in the LOFAR Two-meter Sky Survey: A preliminary catalog](http://adsabs.harvard.edu/abs/2018A&A...613A..58V), 2018, Astronomy and Astrophysics, 613, A58


2018-05
-------

1. Heesen, V., Rafferty, D. A., Horneffer, A., et al.: [Exploring the making of a galactic wind in the starbursting dwarf irregular galaxy IC 10 with LOFAR](http://adsabs.harvard.edu/abs/2018MNRAS.476.1756H), 2018, Monthly Notices of the Royal Astronomical Society, 476, 1756

2. Wilber, A., Brüggen, M., Bonafede, A., et al.: [Search for low-frequency diffuse radio emission around a shock in the massive galaxy cluster MACS J0744.9+3927](http://adsabs.harvard.edu/abs/2018MNRAS.476.3415W), 2018, Monthly Notices of the Royal Astronomical Society, 476, 3415

3. Asad, K. M. B., Koopmans, L. V. E., Jelić, V., et al.: [Polarization leakage in epoch of reionization windows - III. Wide-field effects of narrow-field arrays](http://adsabs.harvard.edu/abs/2018MNRAS.476.3051A), 2018, Monthly Notices of the Royal Astronomical Society, 476, 3051


2018-04
-------

1. Salas, P., Oonk, J. B. R., van Weeren, R. J., et al.: [Mapping low-frequency carbon radio recombination lines towards Cassiopeia A at 340, 148, 54, and 43 MHz](http://adsabs.harvard.edu/abs/2018MNRAS.475.2496S), 2018, Monthly Notices of the Royal Astronomical Society, 475, 2496

2. O'Gorman, E., Coughlan, C. P., Vlemmings, W., et al.: [A search for radio emission from exoplanets around evolved stars](http://adsabs.harvard.edu/abs/2018A&A...612A..52O), 2018, Astronomy and Astrophysics, 612, A52

3. Arias, M., Vink, J., de Gasperin, F., et al.: [Low-frequency radio absorption in Cassiopeia A](http://adsabs.harvard.edu/abs/2018A&A...612A.110A), 2018, Astronomy and Astrophysics, 612, A110

4. Carbone, D., Garsden, H., Spreeuw, H., et al.: [PySE: Software for extracting sources from radio images](http://adsabs.harvard.edu/abs/2018A&C....23...92C), 2018, Astronomy and Computing, 23, 92

5. Gürkan, G., Hardcastle, M. J., Smith, D. J. B., et al.: [LOFAR/H-ATLAS: the low-frequency radio luminosity-star formation rate relation](http://adsabs.harvard.edu/abs/2018MNRAS.475.3010G), 2018, Monthly Notices of the Royal Astronomical Society, 475, 3010

6. Williams, W. L., Calistro Rivera, G., Best, P. N., et al.: [LOFAR-Boötes: properties of high- and low-excitation radio galaxies at 0.5 &lt; z &lt; 2.0](http://adsabs.harvard.edu/abs/2018MNRAS.475.3429W), 2018, Monthly Notices of the Royal Astronomical Society, 475, 3429

7. Broderick, J. W., Fender, R. P., Miller-Jones, J. C. A., et al.: [LOFAR 150-MHz observations of SS 433 and W 50](http://adsabs.harvard.edu/abs/2018MNRAS.475.5360B), 2018, Monthly Notices of the Royal Astronomical Society, 475, 5360

8. Mahatma, V. H., Hardcastle, M. J., Williams, W. L., et al.: [Remnant radio-loud AGN in the Herschel-ATLAS field](http://adsabs.harvard.edu/abs/2018MNRAS.475.4557M), 2018, Monthly Notices of the Royal Astronomical Society, 475, 4557

9. Broekema, P. Chris, Mol, J. Jan David, Nijboer, R., et al.: [Cobalt: A GPU-based correlator and beamformer for LOFAR](http://adsabs.harvard.edu/abs/2018A&C....23..180B), 2018, Astronomy and Computing, 23, 180


2018-03
-------

1. Farnes, J. S., Heald, G., Junklewitz, H., et al.: [Source finding in linear polarization for LOFAR, and SKA predecessor surveys, using Faraday moments](http://adsabs.harvard.edu/abs/2018MNRAS.474.3280F), 2018, Monthly Notices of the Royal Astronomical Society, 474, 3280

2. Heesen, V., Croston, J. H., Morganti, R., et al.: [LOFAR reveals the giant: a low-frequency radio continuum study of the outflow in the nearby FR I radio galaxy 3C 31](http://adsabs.harvard.edu/abs/2018MNRAS.474.5049H), 2018, Monthly Notices of the Royal Astronomical Society, 474, 5049

3. Savini, F., Bonafede, A., Brüggen, M., et al.: [Studying the late evolution of a radio-loud AGN in a galaxy group with LOFAR](http://adsabs.harvard.edu/abs/2018MNRAS.474.5023S), 2018, Monthly Notices of the Royal Astronomical Society, 474, 5023

4. Retana-Montenegro, Edwin, Röttgering, Huub: [On the selection of high-z quasars using LOFAR observations](http://adsabs.harvard.edu/abs/2018FrASS...5....5R), 2018, Frontiers in Astronomy and Space Sciences, 5, 5

5. Ramírez-Olivencia, N., Varenius, E., Pérez-Torres, M., et al.: [Sub-arcsecond imaging of Arp 299-A at 150 MHz with LOFAR: Evidence for a starburst-driven outflow](http://adsabs.harvard.edu/abs/2018A&A...610L..18R), 2018, Astronomy and Astrophysics, 610, L18


2018-02
-------

1. Chiarucci, Simone, Wijnholds, Stefan J.: [Blind calibration of radio interferometric arrays using sparsity constraints and its implications for self-calibration](http://adsabs.harvard.edu/abs/2018MNRAS.474.1028C), 2018, Monthly Notices of the Royal Astronomical Society, 474, 1028


2018-01
-------

1. Wilber, A., Brüggen, M., Bonafede, A., et al.: [LOFAR discovery of an ultra-steep radio halo and giant head-tail radio galaxy in Abell 1132](http://adsabs.harvard.edu/abs/2018MNRAS.473.3536W), 2018, Monthly Notices of the Royal Astronomical Society, 473, 3536

2. Rajpurohit, K., Hoeft, M., van Weeren, R. J., et al.: [Deep VLA Observations of the Cluster 1RXS J0603.3+4214 in the Frequency Range of 1-2 GHz](http://adsabs.harvard.edu/abs/2018ApJ...852...65R), 2018, The Astrophysical Journal, 852, 65


2017-12
-------

1. Pan, Hanjie, Simeoni, Matthieu, Hurley, Paul, et al.: [LEAP: Looking beyond pixels with continuous-space EstimAtion of Point sources](http://adsabs.harvard.edu/abs/2017A&A...608A.136P), 2017, Astronomy and Astrophysics, 608, A136


2017-11
-------

1. Vedantham, H. K., de Bruyn, A. G., Macquart, J. -P.: [A Dense Plasma Globule in the Solar Neighborhood](http://adsabs.harvard.edu/abs/2017ApJ...849L...3V), 2017, The Astrophysical Journal, 849, L3


2017-10
-------

1. Reid, Hamish A. S., Kontar, Eduard P.: [Imaging spectroscopy of type U and J solar radio bursts with LOFAR](http://adsabs.harvard.edu/abs/2017A&A...606A.141R), 2017, Astronomy and Astrophysics, 606, A141

2. Brienza, M., Godfrey, L., Morganti, R., et al.: [Search and modelling of remnant radio galaxies in the LOFAR Lockman Hole field](http://adsabs.harvard.edu/abs/2017A&A...606A..98B), 2017, Astronomy and Astrophysics, 606, A98

3. Hoang, D. N., Shimwell, T. W., Stroe, A., et al.: [Deep LOFAR observations of the merging galaxy cluster CIZA J2242.8+5301](http://adsabs.harvard.edu/abs/2017MNRAS.471.1107H), 2017, Monthly Notices of the Royal Astronomical Society, 471, 1107

4. de Gasperin, Francesco, Intema, Huib T., Shimwell, Timothy W., et al.: [Gentle reenergization of electrons in merging galaxy clusters](http://adsabs.harvard.edu/abs/2017SciA....3E1634D), 2017, Science Advances, 3, e1701634

5. Abbott, B. P., Abbott, R., Abbott, T. D., et al.: [Multi-messenger Observations of a Binary Neutron Star Merger](http://adsabs.harvard.edu/abs/2017ApJ...848L..12A), 2017, The Astrophysical Journal, 848, L12


2017-09
-------

1. Kokotanekov, G., Wise, M., Heald, G. H., et al.: [LOFAR MSSS: The scaling relation between AGN cavity power and radio luminosity at low radio frequencies](http://adsabs.harvard.edu/abs/2017A&A...605A..48K), 2017, Astronomy and Astrophysics, 605, A48


2017-08
-------

1. Morabito, Leah K., Williams, W. L., Duncan, Kenneth J., et al.: [Investigating the unification of LOFAR-detected powerful AGN in the Boötes field](http://adsabs.harvard.edu/abs/2017MNRAS.469.1883M), 2017, Monthly Notices of the Royal Astronomical Society, 469, 1883

2. Calistro Rivera, G., Williams, W. L., Hardcastle, M. J., et al.: [The LOFAR window on star-forming galaxies and AGNs - curved radio SEDs and IR-radio correlation at 0&lt;z&lt;2.5](http://adsabs.harvard.edu/abs/2017MNRAS.469.3468C), 2017, Monthly Notices of the Royal Astronomical Society, 469, 3468


2017-07
-------

1. Harwood, Jeremy J., Hardcastle, Martin J., Morganti, Raffaella, et al.: [FR II radio galaxies at low frequencies - II. Spectral ageing and source dynamics](http://adsabs.harvard.edu/abs/2017MNRAS.469..639H), 2017, Monthly Notices of the Royal Astronomical Society, 469, 639

2. Venturi, T., Rossetti, M., Brunetti, G., et al.: [The two-component giant radio halo in the galaxy cluster Abell 2142](http://adsabs.harvard.edu/abs/2017A&A...603A.125V), 2017, Astronomy and Astrophysics, 603, A125


2017-05
-------

1. Salas, P., Oonk, J. B. R., van Weeren, R. J., et al.: [LOFAR observations of decameter carbon radio recombination lines towards Cassiopeia A](http://adsabs.harvard.edu/abs/2017MNRAS.467.2274S), 2017, Monthly Notices of the Royal Astronomical Society, 467, 2274

2. Clarke, A. O., Heald, G., Jarrett, T., et al.: [LOFAR MSSS: Discovery of a 2.56 Mpc giant radio galaxy associated with a disturbed galaxy group](http://adsabs.harvard.edu/abs/2017A&A...601A..25C), 2017, Astronomy and Astrophysics, 601, A25


2017-04
-------

1. Trinh, T. N. G., Scholten, O., Bonardi, A., et al.: [Thunderstorm electric fields probed by extensive air showers through their polarized radio emission](http://adsabs.harvard.edu/abs/2017PhRvD..95h3004T), 2017, Physical Review D, 95, 083004

2. Shulevski, A., Morganti, R., Harwood, J. J., et al.: [Radiative age mapping of the remnant radio galaxy B2 0924+30: the LOFAR perspective](http://adsabs.harvard.edu/abs/2017A&A...600A..65S), 2017, Astronomy and Astrophysics, 600, A65

3. Sabater, J., Sánchez-Expósito, S., Best, P., et al.: [Calibration of LOFAR data on the cloud](http://adsabs.harvard.edu/abs/2017A&C....19...75S), 2017, Astronomy and Computing, 19, 75


2017-03
-------

1. Morganti, R.: [Synergy with new radio facilities: From LOFAR to SKA](http://adsabs.harvard.edu/abs/2017AN....338..165M), 2017, Astronomische Nachrichten, 338, 165

2. Patil, A. H., Yatawatta, S., Koopmans, L. V. E., et al.: [Upper Limits on the 21 cm Epoch of Reionization Power Spectrum from One Night with LOFAR](http://adsabs.harvard.edu/abs/2017ApJ...838...65P), 2017, The Astrophysical Journal, 838, 65


2017-02
-------

1. Oonk, J. B. R., van Weeren, R. J., Salas, P., et al.: [Carbon and hydrogen radio recombination lines from the cold clouds towards Cassiopeia A](http://adsabs.harvard.edu/abs/2017MNRAS.465.1066O), 2017, Monthly Notices of the Royal Astronomical Society, 465, 1066

2. Shimwell, T. W., Röttgering, H. J. A., Best, P. N., et al.: [The LOFAR Two-metre Sky Survey. I. Survey description and preliminary data release](http://adsabs.harvard.edu/abs/2017A&A...598A.104S), 2017, Astronomy and Astrophysics, 598, A104


2017-01
-------

1. Van Eck, C. L., Haverkorn, M., Alves, M. I. R., et al.: [Faraday tomography of the local interstellar medium with LOFAR: Galactic foregrounds towards IC 342](http://adsabs.harvard.edu/abs/2017A&A...597A..98V), 2017, Astronomy and Astrophysics, 597, A98

2. Coughlan, Colm P., Ainsworth, Rachael E., Eislöffel, Jochen, et al.: [A LOFAR Detection of the Low-mass Young Star T Tau at 149 MHz](http://adsabs.harvard.edu/abs/2017ApJ...834..206C), 2017, The Astrophysical Journal, 834, 206


2016-12
-------

1. Mahony, E. K., Morganti, R., Prandoni, I., et al.: [The Lockman Hole project: LOFAR observations and spectral index properties of low-frequency radio sources](http://adsabs.harvard.edu/abs/2016MNRAS.463.2997M), 2016, Monthly Notices of the Royal Astronomical Society, 463, 2997

2. Burningham, Ben, Hardcastle, M., Nichols, J. D., et al.: [A LOFAR mini-survey for low-frequency radio emission from the nearest brown dwarfs](http://adsabs.harvard.edu/abs/2016MNRAS.463.2202B), 2016, Monthly Notices of the Royal Astronomical Society, 463, 2202

3. McKean, J. P., Godfrey, L. E. H., Vegetti, S., et al.: [LOFAR imaging of Cygnus A - direct detection of a turnover in the hotspot radio spectra](http://adsabs.harvard.edu/abs/2016MNRAS.463.3143M), 2016, Monthly Notices of the Royal Astronomical Society, 463, 3143

4. Patil, Ajinkya H., Yatawatta, Sarod, Zaroubi, Saleem, et al.: [Systematic biases in low-frequency radio interferometric data due to calibration: the LOFAR-EoR case](http://adsabs.harvard.edu/abs/2016MNRAS.463.4317P), 2016, Monthly Notices of the Royal Astronomical Society, 463, 4317


2016-11
-------

1. Jackson, N., Tagore, A., Deller, A., et al.: [LBCS: The LOFAR Long-Baseline Calibrator Survey](http://adsabs.harvard.edu/abs/2016A&A...595A..86J), 2016, Astronomy and Astrophysics, 595, A86

2. Scholten, O., Trinh, T. N. G., Bonardi, A., et al.: [Measurement of the circular polarization in radio emission from extensive air showers confirms emission mechanisms](http://adsabs.harvard.edu/abs/2016PhRvD..94j3010S), 2016, Physical Review D, 94, 103010


2016-10
-------

1. Hardcastle, M. J., Gürkan, G., van Weeren, R. J., et al.: [LOFAR/H-ATLAS: a deep low-frequency survey of the Herschel-ATLAS North Galactic Pole field](http://adsabs.harvard.edu/abs/2016MNRAS.462.1910H), 2016, Monthly Notices of the Royal Astronomical Society, 462, 1910

2. Crosley, M. K., Osten, R. A., Broderick, J. W., et al.: [The Search for Signatures of Transient Mass Loss in Active Stars](http://adsabs.harvard.edu/abs/2016ApJ...830...24C), 2016, The Astrophysical Journal, 830, 24


2016-09
-------

1. Morabito, Leah K., Deller, Adam T., Röttgering, Huub, et al.: [LOFAR VLBI studies at 55 MHz of 4C 43.15, a z = 2.4 radio galaxy](http://adsabs.harvard.edu/abs/2016MNRAS.461.2676M), 2016, Monthly Notices of the Royal Astronomical Society, 461, 2676

2. Varenius, E., Conway, J. E., Martí-Vidal, I., et al.: [Subarcsecond international LOFAR radio images of Arp 220 at 150 MHz. A kpc-scale star forming disk surrounding nuclei with shocked outflows](http://adsabs.harvard.edu/abs/2016A&A...593A..86V), 2016, Astronomy and Astrophysics, 593, A86


2016-08
-------

1. Williams, W. L., van Weeren, R. J., Röttgering, H. J. A., et al.: [LOFAR 150-MHz observations of the Boötes field: catalogue and source counts](http://adsabs.harvard.edu/abs/2016MNRAS.460.2385W), 2016, Monthly Notices of the Royal Astronomical Society, 460, 2385

2. Mulcahy, D. D., Fletcher, A., Beck, R., et al.: [Modelling the cosmic ray electron propagation in M 51](http://adsabs.harvard.edu/abs/2016A&A...592A.123M), 2016, Astronomy and Astrophysics, 592, A123


2016-07
-------

1. Carbone, D., van der Horst, A. J., Wijers, R. A. M. J., et al.: [New methods to constrain the radio transient rate: results from a survey of four fields with LOFAR](http://adsabs.harvard.edu/abs/2016MNRAS.459.3161C), 2016, Monthly Notices of the Royal Astronomical Society, 459, 3161

2. Abbott, B. P., Abbott, R., Abbott, T. D., et al.: [Supplement: “Localization and Broadband Follow-up of the Gravitational-wave Transient GW150914” (2016, ApJL, 826, L13)](http://adsabs.harvard.edu/abs/2016ApJS..225....8A), 2016, The Astrophysical Journal Supplement Series, 225, 8


2016-06
-------

1. Shimwell, T. W., Luckin, J., Brüggen, M., et al.: [A plethora of diffuse steep spectrum radio sources in Abell 2034 revealed by LOFAR](http://adsabs.harvard.edu/abs/2016MNRAS.459..277S), 2016, Monthly Notices of the Royal Astronomical Society, 459, 277

2. Harwood, Jeremy J., Croston, Judith H., Intema, Huib T., et al.: [FR II radio galaxies at low frequencies - I. Morphology, magnetic field strength and energetics](http://adsabs.harvard.edu/abs/2016MNRAS.458.4443H), 2016, Monthly Notices of the Royal Astronomical Society, 458, 4443


2016-05
-------

1. Rossetto, L., Buitink, S., Corstanje, A., et al.: [Measurement of cosmic rays with LOFAR](http://adsabs.harvard.edu/abs/2016JPhCS.718e2035R), 2016, Journal of Physics Conference Series, 718, 052035


2016-03
-------

1. van Weeren, R. J., Williams, W. L., Hardcastle, M. J., et al.: [LOFAR Facet Calibration](http://adsabs.harvard.edu/abs/2016ApJS..223....2V), 2016, The Astrophysical Journal Supplement Series, 223, 2

2. Buitink, S., Corstanje, A., Falcke, H., et al.: [A large light-mass component of cosmic rays at 10<SUP>17</SUP>-10<SUP>17.5</SUP> electronvolts from radio observations](http://adsabs.harvard.edu/abs/2016Natur.531...70B), 2016, Nature, 531, 70

3. Stewart, A. J., Fender, R. P., Broderick, J. W., et al.: [LOFAR MSSS: detection of a low-frequency radio transient in 400 h of monitoring of the North Celestial Pole](http://adsabs.harvard.edu/abs/2016MNRAS.456.2321S), 2016, Monthly Notices of the Royal Astronomical Society, 456, 2321

4. Girard, J. N., Zarka, P., Tasse, C., et al.: [Imaging Jupiter's radiation belts down to 127 MHz with LOFAR](http://adsabs.harvard.edu/abs/2016A&A...587A...3G), 2016, Astronomy and Astrophysics, 587, A3

5. Vrbanec, Dijana, Ciardi, Benedetta, Jelić, Vibor, et al.: [Predictions for the 21 cm-galaxy cross-power spectrum observable with LOFAR and Subaru](http://adsabs.harvard.edu/abs/2016MNRAS.457..666V), 2016, Monthly Notices of the Royal Astronomical Society, 457, 666


2016-02
-------

1. van Weeren, R. J., Brunetti, G., Brüggen, M., et al.: [LOFAR, VLA, and Chandra Observations of the Toothbrush Galaxy Cluster](http://adsabs.harvard.edu/abs/2016ApJ...818..204V), 2016, The Astrophysical Journal, 818, 204

2. Marcote, B., Ribó, M., Paredes, J. M., et al.: [Orbital and superorbital variability of LS I +61 303 at low radio frequencies with GMRT and LOFAR](http://adsabs.harvard.edu/abs/2016MNRAS.456.1791M), 2016, Monthly Notices of the Royal Astronomical Society, 456, 1791


2016-01
-------

1. Thoudam, S., Buitink, S., Corstanje, A., et al.: [Measurement of the cosmic-ray energy spectrum above 10<SUP>16</SUP> eV with the LOFAR Radboud Air Shower Array](http://adsabs.harvard.edu/abs/2016APh....73...34T), 2016, Astroparticle Physics, 73, 34

2. Brienza, M., Godfrey, L., Morganti, R., et al.: [LOFAR discovery of a 700-kpc remnant radio galaxy at low redshift](http://adsabs.harvard.edu/abs/2016A&A...585A..29B), 2016, Astronomy and Astrophysics, 585, A29

3. Trinh, T. N. G., Scholten, O., Buitink, S., et al.: [Influence of atmospheric electric fields on the radio emission from extensive air showers](http://adsabs.harvard.edu/abs/2016PhRvD..93b3003T), 2016, Physical Review D, 93, 023003


2015-12
-------

1. Orrù, E., van Velzen, S., Pizzo, R. F., et al.: [Wide-field LOFAR imaging of the field around the double-double radio galaxy B1834+620. A fresh view on a restarted AGN and doubeltjes](http://adsabs.harvard.edu/abs/2015A&A...584A.112O), 2015, Astronomy and Astrophysics, 584, A112


2015-11
-------

1. Zaroubi, S., Jelic, V., de Bruyn, A. G., et al.: [Galactic interstellar filaments as probed by LOFAR and Planck.](http://adsabs.harvard.edu/abs/2015MNRAS.454L..46Z), 2015, Monthly Notices of the Royal Astronomical Society, 454, L46

2. Jelić, V., de Bruyn, A. G., Pandey, V. N., et al.: [Linear polarization structures in LOFAR observations of the interstellar medium in the 3C 196 field](http://adsabs.harvard.edu/abs/2015A&A...583A.137J), 2015, Astronomy and Astrophysics, 583, A137

3. Shulevski, A., Morganti, R., Barthel, P. D., et al.: [AGN duty cycle estimates for the ultra-steep spectrum radio relic VLSS J1431.8+1331](http://adsabs.harvard.edu/abs/2015A&A...583A..89S), 2015, Astronomy and Astrophysics, 583, A89

4. Nelles, A., Hörandel, J. R., Karskens, T., et al.: [Calibrating the absolute amplitude scale for air showers measured at LOFAR](http://adsabs.harvard.edu/abs/2015JInst..10P1005N), 2015, Journal of Instrumentation, 10, P11005


2015-10
-------

1. Heald, G. H., Pizzo, R. F., Orrú, E., et al.: [The LOFAR Multifrequency Snapshot Sky Survey (MSSS). I. Survey description and first results](http://adsabs.harvard.edu/abs/2015A&A...582A.123H), 2015, Astronomy and Astrophysics, 582, A123

2. Ciardi, B., Inoue, S., Abdalla, F. B., et al.: [Simulating the 21 cm forest detectable with LOFAR and SKA in the spectra of high-z GRBs](http://adsabs.harvard.edu/abs/2015MNRAS.453..101C), 2015, Monthly Notices of the Royal Astronomical Society, 453, 101


2015-09
-------

1. Ghosh, Abhik, Koopmans, Léon V. E., Chapman, E., et al.: [A Bayesian analysis of redshifted 21-cm H I signal and foregrounds: simulations for LOFAR](http://adsabs.harvard.edu/abs/2015MNRAS.452.1587G), 2015, Monthly Notices of the Royal Astronomical Society, 452, 1587

2. van Diepen, G. N. J.: [Casacore Table Data System and its use in the MeasurementSet](http://adsabs.harvard.edu/abs/2015A&C....12..174V), 2015, Astronomy and Computing, 12, 174


2015-08
-------

1. Asad, K. M. B., Koopmans, L. V. E., Jelić, V., et al.: [Polarization leakage in epoch of reionization windows - I. Low Frequency Array observations of the 3C196 field](http://adsabs.harvard.edu/abs/2015MNRAS.451.3709A), 2015, Monthly Notices of the Royal Astronomical Society, 451, 3709


2015-07
-------

1. Yoshiura, Shintaro, Shimabukuro, Hayato, Takahashi, Keitaro, et al.: [Sensitivity for 21 cm bispectrum from Epoch of Reionization](http://adsabs.harvard.edu/abs/2015MNRAS.451..266Y), 2015, Monthly Notices of the Royal Astronomical Society, 451, 266

2. Vedantham, H. K., Koopmans, L. V. E., de Bruyn, A. G., et al.: [Lunar occultation of the diffuse radio sky: LOFAR measurements between 35 and 80 MHz](http://adsabs.harvard.edu/abs/2015MNRAS.450.2291V), 2015, Monthly Notices of the Royal Astronomical Society, 450, 2291

3. Shulevski, A., Morganti, R., Barthel, P. D., et al.: [The peculiar radio galaxy 4C 35.06: a case for recurrent AGN activity?](http://adsabs.harvard.edu/abs/2015A&A...579A..27S), 2015, Astronomy and Astrophysics, 579, A27


2015-06
-------

1. Metzger, Brian D., Williams, P. K. G., Berger, Edo: [Extragalactic Synchrotron Transients in the Era of Wide-field Radio Surveys. I. Detection Rates and Light Curve Characteristics](http://adsabs.harvard.edu/abs/2015ApJ...806..224M), 2015, The Astrophysical Journal, 806, 224

2. Swinbank, John D., Staley, Tim D., Molenaar, Gijs J., et al.: [The LOFAR Transients Pipeline](http://adsabs.harvard.edu/abs/2015A&C....11...25S), 2015, Astronomy and Computing, 11, 25


2015-05
-------

1. Nelles, A., Buitink, S., Corstanje, A., et al.: [The radio emission pattern of air showers as measured with LOFAR-a tool for the reconstruction of the energy and the shower maximum](http://adsabs.harvard.edu/abs/2015JCAP...05..018N), 2015, Journal of Cosmology and Astroparticle Physics, 2015, 018

2. Nelles, A., Schellart, P., Buitink, S., et al.: [Measuring a Cherenkov ring in the radio emission from air showers at 110-190 MHz with LOFAR](http://adsabs.harvard.edu/abs/2015APh....65...11N), 2015, Astroparticle Physics, 65, 11


2015-03
-------

1. Garsden, H., Girard, J. N., Starck, J. L., et al.: [LOFAR sparse image reconstruction](http://adsabs.harvard.edu/abs/2015A&A...575A..90G), 2015, Astronomy and Astrophysics, 575, A90


2015-02
-------

1. Corstanje, A., Schellart, P., Nelles, A., et al.: [The shape of the radio wavefront of extensive air showers as measured with LOFAR](http://adsabs.harvard.edu/abs/2015APh....61...22C), 2015, Astroparticle Physics, 61, 22

2. Varenius, E., Conway, J. E., Martí-Vidal, I., et al.: [Subarcsecond international LOFAR radio images of the M82 nucleus at 118 MHz and 154 MHz](http://adsabs.harvard.edu/abs/2015A&A...574A.114V), 2015, Astronomy and Astrophysics, 574, A114

3. Moldón, J., Deller, A. T., Wucknitz, O., et al.: [The LOFAR long baseline snapshot calibrator survey](http://adsabs.harvard.edu/abs/2015A&A...574A..73M), 2015, Astronomy and Astrophysics, 574, A73


2015-01
-------

1. Nelles, Anna, Buitink, Stijn, Falcke, Heino, et al.: [A parameterization for the radio emission of air showers as predicted by CoREAS simulations and applied to LOFAR measurements](http://adsabs.harvard.edu/abs/2015APh....60...13N), 2015, Astroparticle Physics, 60, 13


2014-12
-------

1. Fallows, R. A., Coles, W. A., McKay-Bukowski, D., et al.: [Broadband meter-wavelength observations of ionospheric scintillation](http://adsabs.harvard.edu/abs/2014JGRA..11910544F), 2014, Journal of Geophysical Research (Space Physics), 119, 10,544


2014-11
-------

1. Patel, Prina, Abdalla, Filipe B., Bacon, David J., et al.: [Weak lensing measurements in simulations of radio images](http://adsabs.harvard.edu/abs/2014MNRAS.444.2893P), 2014, Monthly Notices of the Royal Astronomical Society, 444, 2893

2. Salvini, Stefano, Wijnholds, Stefan J.: [Fast gain calibration in radio astronomy using alternating direction implicit methods: Analysis and applications](http://adsabs.harvard.edu/abs/2014A&A...571A..97S), 2014, Astronomy and Astrophysics, 571, A97


2014-10
-------

1. Buitink, S., Corstanje, A., Enriquez, J. E., et al.: [Method for high precision reconstruction of air shower X<SUB>max</SUB> using two-dimensional radio intensity profiles](http://adsabs.harvard.edu/abs/2014PhRvD..90h2003B), 2014, Physical Review D, 90, 082003

2. Schellart, P., Buitink, S., Corstanje, A., et al.: [Polarized radio emission from extensive air showers measured with LOFAR](http://adsabs.harvard.edu/abs/2014JCAP...10..014S), 2014, Journal of Cosmology and Astroparticle Physics, 2014, 014

3. van Weeren, R. J., Williams, W. L., Tasse, C., et al.: [LOFAR Low-band Antenna Observations of the 3C 295 and Boötes Fields: Source Counts and Ultra-steep Spectrum Sources](http://adsabs.harvard.edu/abs/2014ApJ...793...82V), 2014, The Astrophysical Journal, 793, 82


2014-09
-------

1. Patil, Ajinkya H., Zaroubi, Saleem, Chapman, Emma, et al.: [Constraining the epoch of reionization with the variance statistic: simulations of the LOFAR case](http://adsabs.harvard.edu/abs/2014MNRAS.443.1113P), 2014, Monthly Notices of the Royal Astronomical Society, 443, 1113


2014-08
-------

1. Jelić, V., de Bruyn, A. G., Mevius, M., et al.: [Initial LOFAR observations of epoch of reionization windows. II. Diffuse polarized emission in the ELAIS-N1 field](http://adsabs.harvard.edu/abs/2014A&A...568A.101J), 2014, Astronomy and Astrophysics, 568, A101

2. Mulcahy, D. D., Horneffer, A., Beck, R., et al.: [The nature of the low-frequency emission of M 51. First observations of a nearby galaxy with LOFAR](http://adsabs.harvard.edu/abs/2014A&A...568A..74M), 2014, Astronomy and Astrophysics, 568, A74

3. Mikhailov, E., Kasparova, A., Moss, D., et al.: [Magnetic fields near the peripheries of galactic discs](http://adsabs.harvard.edu/abs/2014A&A...568A..66M), 2014, Astronomy and Astrophysics, 568, A66

4. Prasad, P., Wijnholds, S. J., Huizinga, F., et al.: [Real-time calibration of the AARTFAAC array for transient detection](http://adsabs.harvard.edu/abs/2014A&A...568A..48P), 2014, Astronomy and Astrophysics, 568, A48


2014-05
-------

1. Vasiliev, Evgenii O., Sethi, Shiv K.: [H I Absorption from the Epoch of Reionization and Primordial Magnetic Fields](http://adsabs.harvard.edu/abs/2014ApJ...786..142V), 2014, The Astrophysical Journal, 786, 142

2. Rubart, Matthias, Bacon, David, Schwarz, Dominik J.: [Impact of local structure on the cosmic radio dipole](http://adsabs.harvard.edu/abs/2014A&A...565A.111R), 2014, Astronomy and Astrophysics, 565, A111


2014-04
-------

1. Mesinger, Andrei, Ewall-Wice, Aaron, Hewitt, Jacqueline: [Reionization and beyond: detecting the peaks of the cosmological 21 cm signal](http://adsabs.harvard.edu/abs/2014MNRAS.439.3262M), 2014, Monthly Notices of the Royal Astronomical Society, 439, 3262

2. Roseboom, I. G., Best, P. N.: [Cosmic star formation probed via parametric stack-fitting of known sources to radio imaging](http://adsabs.harvard.edu/abs/2014MNRAS.439.1286R), 2014, Monthly Notices of the Royal Astronomical Society, 439, 1286


2014-01
-------

1. Vedantham, H. K., Koopmans, L. V. E., de Bruyn, A. G., et al.: [Chromatic effects in the 21 cm global signal from the cosmic dawn](http://adsabs.harvard.edu/abs/2014MNRAS.437.1056V), 2014, Monthly Notices of the Royal Astronomical Society, 437, 1056


2013-12
-------

1. Pfrommer, Christoph: [Toward a Comprehensive Model for Feedback by Active Galactic Nuclei: New Insights from M87 Observations by LOFAR, Fermi, and H.E.S.S.](http://adsabs.harvard.edu/abs/2013ApJ...779...10P), 2013, The Astrophysical Journal, 779, 10

2. Schellart, P., Nelles, A., Buitink, S., et al.: [Detecting cosmic rays with the LOFAR radio telescope](http://adsabs.harvard.edu/abs/2013A&A...560A..98S), 2013, Astronomy and Astrophysics, 560, A98


2013-10
-------

1. Offringa, A. R., de Bruyn, A. G., Zaroubi, S., et al.: [The brightness and spatial distributions of terrestrial radio sources](http://adsabs.harvard.edu/abs/2013MNRAS.435..584O), 2013, Monthly Notices of the Royal Astronomical Society, 435, 584

2. Iacobelli, M., Haverkorn, M., Orrú, E., et al.: [Studying Galactic interstellar turbulence through fluctuations in synchrotron emission. First LOFAR Galactic foreground detection](http://adsabs.harvard.edu/abs/2013A&A...558A..72I), 2013, Astronomy and Astrophysics, 558, A72


2013-08
-------

1. van Haarlem, M. P., Wise, M. W., Gunst, A. W., et al.: [LOFAR: The LOw-Frequency ARray](http://adsabs.harvard.edu/abs/2013A&A...556A...2V), 2013, Astronomy and Astrophysics, 556, A2


2013-05
-------

1. Tasse, C., van der Tol, S., van Zwieten, J., et al.: [Applying full polarization A-Projection to very wide field of view instruments: An imager for LOFAR](http://adsabs.harvard.edu/abs/2013A&A...553A.105T), 2013, Astronomy and Astrophysics, 553, A105


2013-04
-------

1. Prasad, P., Wijnholds, S. J.: [Amsterdam-ASTRON radio transient facility and analysis centre: towards a 24 x 7, all-sky monitor for the low-frequency array (LOFAR)](http://adsabs.harvard.edu/abs/2013RSPTA.37120234P), 2013, Philosophical Transactions of the Royal Society of London Series A, 371, 20120234

2. Sotomayor-Beltran, C., Sobey, C., Hessels, J. W. T., et al.: [Calibrating high-precision Faraday rotation measurements for LOFAR and the next generation of low-frequency radio telescopes](http://adsabs.harvard.edu/abs/2013A&A...552A..58S), 2013, Astronomy and Astrophysics, 552, A58


2013-03
-------

1. Asgekar, A., Oonk, J. B. R., Yatawatta, S., et al.: [LOFAR detections of low-frequency radio recombination lines towards Cassiopeia A](http://adsabs.harvard.edu/abs/2013A&A...551L..11A), 2013, Astronomy and Astrophysics, 551, L11


2013-02
-------

1. Yatawatta, S., de Bruyn, A. G., Brentjens, M. A., et al.: [Initial deep LOFAR observations of epoch of reionization windows. I. The north celestial pole](http://adsabs.harvard.edu/abs/2013A&A...550A.136Y), 2013, Astronomy and Astrophysics, 550, A136


2013-01
-------

1. Offringa, A. R., de Bruyn, A. G., Zaroubi, S., et al.: [The LOFAR radio environment](http://adsabs.harvard.edu/abs/2013A&A...549A..11O), 2013, Astronomy and Astrophysics, 549, A11


2012-11
-------

1. de Gasperin, F., Orrú, E., Murgia, M., et al.: [M 87 at metre wavelengths: the LOFAR picture](http://adsabs.harvard.edu/abs/2012A&A...547A..56D), 2012, Astronomy and Astrophysics, 547, A56


2012-09
-------

1. Noorishad, P., Wijnholds, S. J., van Ardenne, A., et al.: [Redundancy calibration of phased-array stations](http://adsabs.harvard.edu/abs/2012A&A...545A.108N), 2012, Astronomy and Astrophysics, 545, A108


2012-08
-------

1. Buitink, S., Falcke, H., James, C., et al.: [Constraints on ultra-high-energy neutrino flux from radio observations of the Moon](http://adsabs.harvard.edu/abs/2012ASTRA...8...29B), 2012, Astrophysics and Space Sciences Transactions, 8, 29


2012-07
-------

1. Yatawatta, Sarod: [Reduced ambiguity calibration for LOFAR](http://adsabs.harvard.edu/abs/2012ExA....34...89Y), 2012, Experimental Astronomy, 34, 89

2. van Weeren, R. J., Röttgering, H. J. A., Rafferty, D. A., et al.: [First LOFAR observations at very low frequencies of cluster-scale non-thermal emission: the case of Abell 2256](http://adsabs.harvard.edu/abs/2012A&A...543A..43V), 2012, Astronomy and Astrophysics, 543, A43


2012-06
-------

1. Scaife, Anna M. M., Heald, George H.: [A broad-band flux scale for low-frequency radio telescopes](http://adsabs.harvard.edu/abs/2012MNRAS.423L..30S), 2012, Monthly Notices of the Royal Astronomical Society, 423, L30


2012-05
-------

1. Beck, Rainer: [Magnetic Fields in Galaxies](http://adsabs.harvard.edu/abs/2012SSRv..166..215B), 2012, Space Science Reviews, 166, 215


2012-03
-------

1. Offringa, A. R., van de Gronde, J. J., Roerdink, J. B. T. M.: [A morphological algorithm for improving radio-frequency interference detection](http://adsabs.harvard.edu/abs/2012A&A...539A..95O), 2012, Astronomy and Astrophysics, 539, A95


2012-02
-------

1. Singh, K., Mevius, M., Scholten, O., et al.: [Optimized trigger for ultra-high-energy cosmic-ray and neutrino observations with the low frequency radio array](http://adsabs.harvard.edu/abs/2012NIMPA.664..171S), 2012, Nuclear Instruments and Methods in Physics Research A, 664, 171


2012-01
-------

1. Tasse, Cyril, van Diepen, Ger, van der Tol, Sebastiaan, et al.: [LOFAR calibration and wide-field imaging](http://adsabs.harvard.edu/abs/2012CRPhy..13...28T), 2012, Comptes Rendus Physique, 13, 28


2011-12
-------

1. Heald, George, Bell, Michael R., Horneffer, Andreas, et al.: [LOFAR: Recent Imaging Results and Future Prospects](http://adsabs.harvard.edu/abs/2011JApA...32..589H), 2011, Journal of Astrophysics and Astronomy, 32, 589


2011-11
-------

1. Wijnholds, S. J., Bregman, J. D., van Ardenne, A.: [Calibratability and its impact on configuration design for the LOFAR and SKA phased array radio telescopes](http://adsabs.harvard.edu/abs/2011RaSc...46.0F07W), 2011, Radio Science, 46, RS0F07


2011-06
-------

1. Kazemi, S., Yatawatta, S., Zaroubi, S., et al.: [Radio interferometric calibration using the SAGE algorithm](http://adsabs.harvard.edu/abs/2011MNRAS.414.1656K), 2011, Monthly Notices of the Royal Astronomical Society, 414, 1656

2. Wijnholds, Stefan J., van Cappellen, Wim A.: [In Situ Antenna Performance Evaluation of the LOFAR Phased Array Radio Telescope](http://adsabs.harvard.edu/abs/2011ITAP...59.1981W), 2011, IEEE Transactions on Antennas and Propagation, 59, 1981

3. Thoudam, S., Aar, G. V., Akker, M. V. D., et al.: [An air shower array for LOFAR: LORA](http://adsabs.harvard.edu/abs/2011ASTRA...7..195T), 2011, Astrophysics and Space Sciences Transactions, 7, 195

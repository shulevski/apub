Title: ASTRON owned instruments and surveys publications
Save_as: apub.html

[TOC]


2023
----

1. Ubertosi, F., Gitti, M., Brighenti, F.: [Chasing ICM cooling and AGN feedback from the macro to the meso scales in the galaxy cluster ZwCl 235](http://adsabs.harvard.edu/abs/2023A&A...670A..23U), 2023, Astronomy and Astrophysics, 670, A23

2. Stein, M., Heesen, V., Dettmar, R. -J., et al.: [CHANG-ES. XXVI. Insights into cosmic-ray transport from radio halos in edge-on galaxies](http://adsabs.harvard.edu/abs/2023A&A...670A.158S), 2023, Astronomy and Astrophysics, 670, A158

3. Callingham, J. R., Shimwell, T. W., Vedantham, H. K., et al.: [V-LoTSS: The circularly polarised LOFAR Two-metre Sky Survey](http://adsabs.harvard.edu/abs/2023A&A...670A.124C), 2023, Astronomy and Astrophysics, 670, A124

4. Giarratana, S., Giroletti, M., Spingola, C., et al.: [Multi-scale VLBI observations of the candidate host galaxy of GRB 200716C](http://adsabs.harvard.edu/abs/2023A&A...670A..35G), 2023, Astronomy and Astrophysics, 670, A35

5. Dutta, Sushant, Singh, Veeresh, Chandra, C. H. Ishwara, et al.: [Search and Characterization of Remnant Radio Galaxies in the XMM-LSS Deep Field](http://adsabs.harvard.edu/abs/2023ApJ...944..176D), 2023, The Astrophysical Journal, 944, 176

6. Mahatma, V. H., Basu, A., Hardcastle, M. J., et al.: [A low frequency sub-arcsecond view of powerful radio galaxies in rich-cluster environments: 3C 34 and 3C 320](http://adsabs.harvard.edu/abs/2023MNRAS.tmp..404M), 2023, Monthly Notices of the Royal Astronomical Society, None, 

7. Heesen, V., O'Sullivan, S. P., Brüggen, M., et al.: [Detection of magnetic fields in the circumgalactic medium of nearby galaxies using Faraday rotation](http://adsabs.harvard.edu/abs/2023A&A...670L..23H), 2023, Astronomy and Astrophysics, 670, L23

8. Fine, Maxwell A., Van Eck, Cameron L., Pratley, Luke: [Correcting Bandwidth Depolarization by Extreme Faraday Rotation](http://adsabs.harvard.edu/abs/2023MNRAS.tmp..438F), 2023, Monthly Notices of the Royal Astronomical Society, None, 

9. Zhang, Jinge, Reid, Hamish A. S., Krupar, Vratislav, et al.: [Deriving Large Coronal Magnetic Loop Parameters Using LOFAR J Burst Observations](http://adsabs.harvard.edu/abs/2023SoPh..298....7Z), 2023, Solar Physics, 298, 7

10. Heesen, V., Klocke, T. -L., Brüggen, M., et al.: [Nearby galaxies in the LOFAR Two-metre Sky Survey. II. The magnetic field-gas relation](http://adsabs.harvard.edu/abs/2023A&A...669A...8H), 2023, Astronomy and Astrophysics, 669, A8

11. Dabrowski, Bartosz, Mikuła, Katarzyna, Flisek, Paweł, et al.: [Interferometric imaging of the type IIIb and U radio bursts observed with LOFAR on 22 August 2017](http://adsabs.harvard.edu/abs/2023A&A...669A..52D), 2023, Astronomy and Astrophysics, 669, A52

12. van der Wateren, E., Bassa, C. G., Cooper, S., et al.: [The LOFAR Tied-Array All-Sky Survey: Timing of 35 radio pulsars and an overview of the properties of the LOFAR pulsar discoveries](http://adsabs.harvard.edu/abs/2023A&A...669A.160V), 2023, Astronomy and Astrophysics, 669, A160

13. Hare, B. M., Scholten, O., Buitink, S., et al.: [Characteristics of recoil leaders as observed by LOFAR](http://adsabs.harvard.edu/abs/2023PhRvD.107b3025H), 2023, Physical Review D, 107, 023025

14. Rajpurohit, K., Osinga, E., Brienza, M., et al.: [Deep low-frequency radio observations of Abell 2256. II. The ultra-steep spectrum radio halo](http://adsabs.harvard.edu/abs/2023A&A...669A...1R), 2023, Astronomy and Astrophysics, 669, A1

15. Xu, Xiaoyu, Wang, Junfeng: [Ghost in the Shell: Evidence for Past Active Galactic Nucleus Activities in NGC 5195 from a Newly Discovered Large-scale Ionized Structure](http://adsabs.harvard.edu/abs/2023ApJ...943...28X), 2023, The Astrophysical Journal, 943, 28

16. Cassano, R., Cuciti, V., Brunetti, G., et al.: [The Planck clusters in the LOFAR sky: IV: LoTSS-DR2: statistics of radio halos and re-acceleration models](http://adsabs.harvard.edu/abs/2023arXiv230108052C), 2023, arXiv e-prints, None, arXiv:2301.08052

17. Bruno, L., Brunetti, G., Botteon, A., et al.: [The Planck clusters in the LOFAR sky. II. LoTSS-DR2: Recovering diffuse extended emission with LOFAR](http://adsabs.harvard.edu/abs/2023arXiv230108121B), 2023, arXiv e-prints, None, arXiv:2301.08121

18. de Ruiter, Iris, Nyamai, Miriam M., Rowlinson, Antonia, et al.: [Low-frequency radio observations of recurrent nova RS Ophiuchi with MeerKAT and LOFAR](http://adsabs.harvard.edu/abs/2023arXiv230110552D), 2023, arXiv e-prints, None, arXiv:2301.10552

19. Jones, A., de Gasperin, F., Cuciti, V., et al.: [The Planck clusters in the LOFAR sky VI. LoTSS-DR2: Properties of radio relics](http://adsabs.harvard.edu/abs/2023arXiv230107814J), 2023, arXiv e-prints, None, arXiv:2301.07814

20. Carretti, E., O'Sullivan, S. P., Vacca, V., et al.: [Magnetic field evolution in cosmic filaments with LOFAR data](http://adsabs.harvard.edu/abs/2023MNRAS.518.2273C), 2023, Monthly Notices of the Royal Astronomical Society, 518, 2273

21. Gan, H., Mertens, F. G., Koopmans, L. V. E., et al.: [Assessing the impact of two independent direction-dependent calibration algorithms on the LOFAR 21 cm signal power spectrum. And applications to an observation of a field flanking the north celestial pole](http://adsabs.harvard.edu/abs/2023A&A...669A..20G), 2023, Astronomy and Astrophysics, 669, A20

22. Oei, Martijn S. S. L., van Weeren, Reinout J., Hardcastle, Martin J., et al.: [An intergalactic medium temperature from a giant radio galaxy](http://adsabs.harvard.edu/abs/2023MNRAS.518..240O), 2023, Monthly Notices of the Royal Astronomical Society, 518, 240


2022
----

1. Cooper, A. J., Rowlinson, A., Wijers, R. A. M. J., et al.: [Testing afterglow models of FRB 200428 with early post-burst observations of SGR 1935 + 2154](http://adsabs.harvard.edu/abs/2022MNRAS.517.5483C), 2022, Monthly Notices of the Royal Astronomical Society, 517, 5483

2. Timmerman, R., van Weeren, R. J., Botteon, A., et al.: [Measuring cavity powers of active galactic nuclei in clusters using a hybrid X-ray-radio method. A new window on feedback opened by subarcsecond LOFAR-VLBI observations](http://adsabs.harvard.edu/abs/2022A&A...668A..65T), 2022, Astronomy and Astrophysics, 668, A65

3. Gloudemans, A. J., Duncan, K. J., Saxena, A., et al.: [Discovery of 24 radio-bright quasars at 4.9 ≤ z ≤ 6.6 using low-frequency radio observations](http://adsabs.harvard.edu/abs/2022A&A...668A..27G), 2022, Astronomy and Astrophysics, 668, A27

4. de Jong, J. M. G. H. J., van Weeren, R. J., Botteon, A., et al.: [Deep study of A399-401: Application of a wide-field facet calibration](http://adsabs.harvard.edu/abs/2022A&A...668A.107D), 2022, Astronomy and Astrophysics, 668, A107

5. Mostert, Rafaël I. J., Duncan, Kenneth J., Alegre, Lara, et al.: [Radio source-component association for the LOFAR Two-metre Sky Survey with region-based convolutional neural networks](http://adsabs.harvard.edu/abs/2022A&A...668A..28M), 2022, Astronomy and Astrophysics, 668, A28

6. Slob, M. M., Callingham, J. R., Röttgering, H. J. A., et al.: [Extragalactic peaked-spectrum radio sources at low frequencies are young radio galaxies](http://adsabs.harvard.edu/abs/2022A&A...668A.186S), 2022, Astronomy and Astrophysics, 668, A186

7. Dabhade, P., Shimwell, T. W., Bagchi, J., et al.: [Barbell-shaped giant radio galaxy with ∼100 kpc kink in the jet](http://adsabs.harvard.edu/abs/2022A&A...668A..64D), 2022, Astronomy and Astrophysics, 668, A64

8. Radiconi, Federico, Vacca, Valentina, Battistelli, Elia, et al.: [The thermal and non-thermal components within and between galaxy clusters Abell 399 and Abell 401](http://adsabs.harvard.edu/abs/2022MNRAS.517.5232R), 2022, Monthly Notices of the Royal Astronomical Society, 517, 5232

9. Kayal, Abhijit, Singh, Veeresh, Chandra, C. H. Ishwara, et al.: [Detection of radio-AGN in dust-obscured galaxies using deep uGMRT radio continuum observations](http://adsabs.harvard.edu/abs/2022JApA...43...84K), 2022, Journal of Astrophysics and Astronomy, 43, 84

10. Gao, F., Wang, L., Ramos Padilla, A. F., et al.: [Probing the megaparsec-scale environment of hyperluminous infrared galaxies at 2 &lt; z &lt; 4](http://adsabs.harvard.edu/abs/2022A&A...668A..54G), 2022, Astronomy and Astrophysics, 668, A54

11. Roberts, Ian D., Lang, Maojin, Trotsenko, Daria, et al.: [LoTSS Jellyfish Galaxies. IV. Enhanced Star Formation on the Leading Half of Cluster Galaxies and Gas Compression in IC3949](http://adsabs.harvard.edu/abs/2022ApJ...941...77R), 2022, The Astrophysical Journal, 941, 77

12. Arias, M., Botteon, A., Bassa, C. G., et al.: [Possible discovery of Calvera's supernova remnant](http://adsabs.harvard.edu/abs/2022A&A...667A..71A), 2022, Astronomy and Astrophysics, 667, A71

13. Alegre, Lara, Sabater, Jose, Best, Philip, et al.: [A machine-learning classifier for LOFAR radio galaxy cross-matching techniques](http://adsabs.harvard.edu/abs/2022MNRAS.516.4716A), 2022, Monthly Notices of the Royal Astronomical Society, 516, 4716

14. Rezaei, S., McKean, J. P., Biehl, M., et al.: [A machine learning based approach to gravitational lens identification with the International LOFAR Telescope](http://adsabs.harvard.edu/abs/2022MNRAS.517.1156R), 2022, Monthly Notices of the Royal Astronomical Society, 517, 1156

15. Mesarcik, Michael, Boonstra, Albert-Jan, Ranguelova, Elena, et al.: [Learning to detect radio frequency interference in radio astronomy without seeing it](http://adsabs.harvard.edu/abs/2022MNRAS.516.5367M), 2022, Monthly Notices of the Royal Astronomical Society, 516, 5367

16. Omar, Amitesh: [LOFAR discovery of rare large FR I jets in the low-luminosity radio galaxy NGC 5322](http://adsabs.harvard.edu/abs/2022MNRAS.517L..81O), 2022, Monthly Notices of the Royal Astronomical Society, 517, L81

17. Botteon, Andrea, van Weeren, Reinout J., Brunetti, Gianfranco, et al.: [Magnetic fields and relativistic electrons fill entire galaxy cluster](http://adsabs.harvard.edu/abs/2022SciA....8.7623B), 2022, Science Advances, 8, eabq7623

18. Fallows, R. A., Forte, B., Mevius, M., et al.: [The scintillating tail of comet C/2020 F3 (Neowise)](http://adsabs.harvard.edu/abs/2022A&A...667A..57F), 2022, Astronomy and Astrophysics, 667, A57

19. Zhou, Yihao, Xu, Haiguang, Zhu, Zhenghao, et al.: [A semi-analytical model for the formation and evolution of radio relics in galaxy clusters](http://adsabs.harvard.edu/abs/2022MNRAS.517.1299Z), 2022, Monthly Notices of the Royal Astronomical Society, 517, 1299

20. Morabito, Leah K., Sweijen, F., Radcliffe, J. F., et al.: [Identifying active galactic nuclei via brightness temperature with sub-arcsecond international LOFAR telescope observations](http://adsabs.harvard.edu/abs/2022MNRAS.515.5758M), 2022, Monthly Notices of the Royal Astronomical Society, 515, 5758

21. Petley, J. W., Morabito, L. K., Alexander, D. M., et al.: [Connecting radio emission to AGN wind properties with broad absorption line quasars](http://adsabs.harvard.edu/abs/2022MNRAS.515.5159P), 2022, Monthly Notices of the Royal Astronomical Society, 515, 5159

22. Ignesti, Alessandro, Vulcani, Benedetta, Poggianti, Bianca M., et al.: [Walk on the Low Side: LOFAR Explores the Low-frequency Radio Emission of GASP Jellyfish Galaxies](http://adsabs.harvard.edu/abs/2022ApJ...937...58I), 2022, The Astrophysical Journal, 937, 58

23. Edler, H. W., de Gasperin, F., Brunetti, G., et al.: [Abell 1033: Radio halo and gently reenergized tail at 54 MHz](http://adsabs.harvard.edu/abs/2022A&A...666A...3E), 2022, Astronomy and Astrophysics, 666, A3

24. Sasmal, Tapan K., Bera, Soumen, Mondal, Soumen: [Miscellaneous radio galaxies from LOFAR survey](http://adsabs.harvard.edu/abs/2022AN....34310083S), 2022, Astronomische Nachrichten, 343, e20210083

25. Badman, Samuel T., Carley, Eoin, Cañizares, Luis Alberto, et al.: [Tracking a Beam of Electrons from the Low Solar Corona into Interplanetary Space with the Low Frequency Array, Parker Solar Probe, and 1 au Spacecraft](http://adsabs.harvard.edu/abs/2022ApJ...938...95B), 2022, The Astrophysical Journal, 938, 95

26. Kayama, Kazuho, Tanaka, Takaaki, Uchida, Hiroyuki, et al.: [Spatially resolved study of the SS 433/W 50 west region with Chandra: X-ray structure and spectral variation of non-thermal emission](http://adsabs.harvard.edu/abs/2022PASJ...74.1143K), 2022, Publications of the Astronomical Society of Japan, 74, 1143

27. Su, Renzhi, Sadler, Elaine M., Allison, James R., et al.: [FLASH pilot survey: detections of associated 21 cm H I absorption in GAMA galaxies at 0.42 &lt; z &lt; 1.00](http://adsabs.harvard.edu/abs/2022MNRAS.516.2947S), 2022, Monthly Notices of the Royal Astronomical Society, 516, 2947

28. Liu, Hongyu, Zucca, Pietro, Cho, Kyung-Suk, et al.: [Interferometric Imaging, and Beam-Formed Study of a Moving Type-IV Radio Burst with LOFAR](http://adsabs.harvard.edu/abs/2022SoPh..297..115L), 2022, Solar Physics, 297, 115

29. Palaiologou, E. V., Leonidaki, I., Kopsacheili, M.: [First optical identification of the SRG/eROSITA-detected supernova remnant G 116.6 - 26.1. I. Preliminary results](http://adsabs.harvard.edu/abs/2022MNRAS.515..339P), 2022, Monthly Notices of the Royal Astronomical Society, 515, 339

30. Cuciti, V., de Gasperin, F., Brüggen, M., et al.: [Galaxy clusters enveloped by vast volumes of relativistic electrons](http://adsabs.harvard.edu/abs/2022Natur.609..911C), 2022, Nature, 609, 911

31. Simonte, M., Andernach, H., Brüggen, M., et al.: [Giant radio galaxies in the LOw-Frequency ARray Two-metre Sky Survey Boötes deep field](http://adsabs.harvard.edu/abs/2022MNRAS.515.2032S), 2022, Monthly Notices of the Royal Astronomical Society, 515, 2032

32. Hoang, D. N., Brüggen, M., Botteon, A., et al.: [Diffuse radio emission from non-Planck galaxy clusters in the LoTSS-DR2 fields](http://adsabs.harvard.edu/abs/2022A&A...665A..60H), 2022, Astronomy and Astrophysics, 665, A60

33. Hajduk, Marcin, Leto, Paolo, Vedantham, Harish, et al.: [Radio detection of chemically peculiar stars with LOFAR](http://adsabs.harvard.edu/abs/2022A&A...665A.152H), 2022, Astronomy and Astrophysics, 665, A152

34. Pomakov, V. P., O'Sullivan, S. P., Brüggen, M., et al.: [The redshift evolution of extragalactic magnetic fields](http://adsabs.harvard.edu/abs/2022MNRAS.515..256P), 2022, Monthly Notices of the Royal Astronomical Society, 515, 256

35. Mulcahey, C. R., Leslie, S. K., Jackson, T. M., et al.: [Star formation and AGN feedback in the local Universe: Combining LOFAR and MaNGA](http://adsabs.harvard.edu/abs/2022A&A...665A.144M), 2022, Astronomy and Astrophysics, 665, A144

36. Zheng, X. C., Röttgering, H. J. A., van der Wel, A., et al.: [Shapes of galaxies hosting radio-loud AGNs with z ≤ 1](http://adsabs.harvard.edu/abs/2022A&A...665A.114Z), 2022, Astronomy and Astrophysics, 665, A114

37. Kukreti, P., Morganti, R., Bondi, M., et al.: [Seeing the forest and the trees: A radio investigation of the ULIRG Mrk 273](http://adsabs.harvard.edu/abs/2022A&A...664A..25K), 2022, Astronomy and Astrophysics, 664, A25

38. Pajdosz-Śmierciak, Urszula, Śmierciak, Bartosz, Jamrozy, Marek: [Possible jet reorientation in low-frequency radio structures of blazars](http://adsabs.harvard.edu/abs/2022MNRAS.514.2122P), 2022, Monthly Notices of the Royal Astronomical Society, 514, 2122

39. Smith, D. J. B., Krause, M. G., Hardcastle, M. J., et al.: [Relic jet activity in 'Hanny's Voorwerp' revealed by the LOFAR two metre sky survey](http://adsabs.harvard.edu/abs/2022MNRAS.514.3879S), 2022, Monthly Notices of the Royal Astronomical Society, 514, 3879

40. Emig, K. L., White, G. J., Salas, P., et al.: [Filamentary structures of ionized gas in Cygnus X](http://adsabs.harvard.edu/abs/2022A&A...664A..88E), 2022, Astronomy and Astrophysics, 664, A88

41. Heesen, V., Staffehl, M., Basu, A., et al.: [Nearby galaxies in the LOFAR Two-metre Sky Survey. I. Insights into the non-linearity of the radio-SFR relation](http://adsabs.harvard.edu/abs/2022A&A...664A..83H), 2022, Astronomy and Astrophysics, 664, A83

42. Maschmann, Daniel, Melchior, Anne-Laure, Combes, Francoise, et al.: [Central star formation in double-peak, gas-rich radio galaxies](http://adsabs.harvard.edu/abs/2022A&A...664A.125M), 2022, Astronomy and Astrophysics, 664, A125

43. Vacca, Valentina, Govoni, Federica, Murgia, Matteo, et al.: [Puzzling large-scale polarization in the galaxy cluster Abell 523](http://adsabs.harvard.edu/abs/2022MNRAS.514.4969V), 2022, Monthly Notices of the Royal Astronomical Society, 514, 4969

44. Rudnick, L., Brüggen, M., Brunetti, G., et al.: [Intracluster Magnetic Filaments and an Encounter with a Radio Jet](http://adsabs.harvard.edu/abs/2022ApJ...935..168R), 2022, The Astrophysical Journal, 935, 168

45. Bonafede, A., Brunetti, G., Rudnick, L., et al.: [The Coma Cluster at LOFAR Frequencies. II. The Halo, Relic, and a New Accretion Relic](http://adsabs.harvard.edu/abs/2022ApJ...933..218B), 2022, The Astrophysical Journal, 933, 218

46. Wu, Ziwei, Verbiest, Joris P. W., Main, Robert A., et al.: [Pulsar scintillation studies with LOFAR. I. The census](http://adsabs.harvard.edu/abs/2022A&A...663A.116W), 2022, Astronomy and Astrophysics, 663, A116

47. Erceg, Ana, Jelić, Vibor, Haverkorn, Marijke, et al.: [Faraday tomography of LoTSS-DR2 data. I. Faraday moments in the high-latitude outer Galaxy and revealing Loop III in polarisation](http://adsabs.harvard.edu/abs/2022A&A...663A...7E), 2022, Astronomy and Astrophysics, 663, A7

48. Retana-Montenegro, E.: [What is the origin of the stacked radio emission in radio-undetected quasars?. Insights from a radio-infrared image stacking analysis](http://adsabs.harvard.edu/abs/2022A&A...663A.153R), 2022, Astronomy and Astrophysics, 663, A153

49. Kondapally, Rohit, Best, Philip N., Cochrane, Rachel K., et al.: [Cosmic evolution of low-excitation radio galaxies in the LOFAR two-metre sky survey deep fields](http://adsabs.harvard.edu/abs/2022MNRAS.513.3742K), 2022, Monthly Notices of the Royal Astronomical Society, 513, 3742

50. Kappes, A., Burd, P. R., Kadler, M., et al.: [Subarcsecond view on the high-redshift blazar GB 1508+5714 by the International LOFAR Telescope](http://adsabs.harvard.edu/abs/2022A&A...663A..44K), 2022, Astronomy and Astrophysics, 663, A44

51. Pasini, T., Edler, H. W., Brüggen, M., et al.: [Particle re-acceleration and diffuse radio sources in the galaxy cluster Abell 1550](http://adsabs.harvard.edu/abs/2022A&A...663A.105P), 2022, Astronomy and Astrophysics, 663, A105

52. Bracco, Andrea, Ntormousi, Evangelia, Jelić, Vibor, et al.: [First look at the multiphase interstellar medium using synthetic observations from low-frequency Faraday tomography](http://adsabs.harvard.edu/abs/2022A&A...663A..37B), 2022, Astronomy and Astrophysics, 663, A37

53. Gopal Krishna, Dabhade, Pratik: [X-shaped radio galaxy 3C 223.1: A `double boomerang' with an anomalous spectral gradient](http://adsabs.harvard.edu/abs/2022A&A...663L...8G), 2022, Astronomy and Astrophysics, 663, L8

54. Zhang, PeiJin, Zucca, Pietro, Kozarev, Kamen, et al.: [Imaging of the Quiet Sun in the Frequency Range of 20-80 MHz](http://adsabs.harvard.edu/abs/2022ApJ...932...17Z), 2022, The Astrophysical Journal, 932, 17

55. Gehlot, B. K., Koopmans, L. V. E., Offringa, A. R., et al.: [Degree-scale galactic radio emission at 122 MHz around the North Celestial Pole with LOFAR-AARTFAAC](http://adsabs.harvard.edu/abs/2022A&A...662A..97G), 2022, Astronomy and Astrophysics, 662, A97

56. McCheyne, I., Oliver, S., Sargent, M., et al.: [The LOFAR Two-metre Sky Survey Deep fields. The mass dependence of the far-infrared radio correlation at 150 MHz using deblended Herschel fluxes](http://adsabs.harvard.edu/abs/2022A&A...662A.100M), 2022, Astronomy and Astrophysics, 662, A100

57. Churazov, E. M., Khabibullin, I. I., Bykov, A. M., et al.: [LOFAR detection of faint radio emission from the supernova remnant SRGe J0023+3625 = G116.6-26.1: probing the Milky Way synchrotron halo](http://adsabs.harvard.edu/abs/2022MNRAS.513L..83C), 2022, Monthly Notices of the Royal Astronomical Society, 513, L83

58. Shulevski, A., Franzen, T. M. O., Williams, W. L., et al.: [Characterization of the AARTFAAC-12 aperture array: radio source counts at 42 and 61 MHz](http://adsabs.harvard.edu/abs/2022MNRAS.513.1036S), 2022, Monthly Notices of the Royal Astronomical Society, 513, 1036

59. Bruni, G., Bassani, L., Persic, M., et al.: [IGR J18249-3243: a new GeV-emitting FR II and the emerging population of high-energy radio galaxies](http://adsabs.harvard.edu/abs/2022MNRAS.513..886B), 2022, Monthly Notices of the Royal Astronomical Society, 513, 886

60. Oei, Martijn S. S. L., van Weeren, Reinout J., Vazza, Franco, et al.: [Filamentary baryons and where to find them. A forecast of synchrotron radiation from merger and accretion shocks in the local Cosmic Web](http://adsabs.harvard.edu/abs/2022A&A...662A..87O), 2022, Astronomy and Astrophysics, 662, A87

61. Pascale, Massimo, Frye, Brenda L., Dai, Liang, et al.: [Possible Ongoing Merger Discovered by Photometry and Spectroscopy in the Field of the Galaxy Cluster PLCK G165.7+67.0](http://adsabs.harvard.edu/abs/2022ApJ...932...85P), 2022, The Astrophysical Journal, 932, 85

62. Sobey, C., Bassa, C. G., O'Sullivan, S. P., et al.: [Searching for pulsars associated with polarised point sources using LOFAR: Initial discoveries from the TULIPP project](http://adsabs.harvard.edu/abs/2022A&A...661A..87S), 2022, Astronomy and Astrophysics, 661, A87

63. Pasini, T., Brüggen, M., Hoang, D. N., et al.: [The eROSITA Final Equatorial-Depth Survey (eFEDS). LOFAR view of brightest cluster galaxies and AGN feedback](http://adsabs.harvard.edu/abs/2022A&A...661A..13P), 2022, Astronomy and Astrophysics, 661, A13

64. Carretti, Ettore, Vacca, V., O'Sullivan, S. P., et al.: [Magnetic field strength in cosmic web filaments](http://adsabs.harvard.edu/abs/2022MNRAS.512..945C), 2022, Monthly Notices of the Royal Astronomical Society, 512, 945

65. Brienza, M., Lovisari, L., Rajpurohit, K., et al.: [The galaxy group NGC 507: Newly detected AGN remnant plasma transported by sloshing](http://adsabs.harvard.edu/abs/2022A&A...661A..92B), 2022, Astronomy and Astrophysics, 661, A92

66. Endsley, Ryan, Stark, Daniel P., Fan, Xiaohui, et al.: [Radio and far-IR emission associated with a massive star-forming galaxy candidate at z ≃ 6.8: a radio-loud AGN in the reionization era?](http://adsabs.harvard.edu/abs/2022MNRAS.512.4248E), 2022, Monthly Notices of the Royal Astronomical Society, 512, 4248

67. Riseley, C. J., Rajpurohit, K., Loi, F., et al.: [A MeerKAT-meets-LOFAR study of MS 1455.0 + 2232: a 590 kiloparsec 'mini'-halo in a sloshing cool-core cluster](http://adsabs.harvard.edu/abs/2022MNRAS.512.4210R), 2022, Monthly Notices of the Royal Astronomical Society, 512, 4210

68. Bera, Soumen, Sasmal, Tapan K., Patra, Dusmanta, et al.: ["Winged" Radio Sources from the LOFAR Two-meter Sky Survey First Data Release (LoTSS DR1)](http://adsabs.harvard.edu/abs/2022ApJS..260....7B), 2022, The Astrophysical Journal Supplement Series, 260, 7

69. Punsly, Brian, Groeneveld, Christian, Hill, Gary J., et al.: [The Energetics of the Central Engine in the Powerful Quasar 3C 298](http://adsabs.harvard.edu/abs/2022AJ....163..194P), 2022, The Astronomical Journal, 163, 194

70. Sun, Haomin, Deng, Hui, Wang, Feng, et al.: [A robust RFI identification for radio interferometry based on a convolutional neural network](http://adsabs.harvard.edu/abs/2022MNRAS.512.2025S), 2022, Monthly Notices of the Royal Astronomical Society, 512, 2025

71. Morosan, Diana E., Räsänen, Juska E., Kumari, Anshu, et al.: [Exploring the Circular Polarisation of Low-Frequency Solar Radio Bursts with LOFAR](http://adsabs.harvard.edu/abs/2022SoPh..297...47M), 2022, Solar Physics, 297, 47

72. Vacca, Valentina, Shimwell, Timothy, Perley, Richard A., et al.: [Spectral study of the diffuse synchrotron source in the galaxy cluster Abell 523](http://adsabs.harvard.edu/abs/2022MNRAS.511.3389V), 2022, Monthly Notices of the Royal Astronomical Society, 511, 3389

73. Capetti, A., Brienza, M., Balmaverde, B., et al.: [The LOFAR view of giant, early-type galaxies: Radio emission from active nuclei and star formation](http://adsabs.harvard.edu/abs/2022A&A...660A..93C), 2022, Astronomy and Astrophysics, 660, A93

74. Wagenveld, J. D., Saxena, A., Duncan, K. J., et al.: [Revealing new high-redshift quasar populations through Gaussian mixture model selection](http://adsabs.harvard.edu/abs/2022A&A...660A..22W), 2022, Astronomy and Astrophysics, 660, A22

75. Machado, J. G. O., Hare, B. M., Scholten, O., et al.: [The Relationship of Lightning Radio Pulse Amplitudes and Source Altitudes as Observed by LOFAR](http://adsabs.harvard.edu/abs/2022E&SS....901958M), 2022, Earth and Space Science, 9, e01958

76. Mingo, B., Croston, J. H., Best, P. N., et al.: [Accretion mode versus radio morphology in the LOFAR Deep Fields](http://adsabs.harvard.edu/abs/2022MNRAS.511.3250M), 2022, Monthly Notices of the Royal Astronomical Society, 511, 3250

77. Botteon, A., Shimwell, T. W., Cassano, R., et al.: [The Planck clusters in the LOFAR sky. I. LoTSS-DR2: New detections and sample overview](http://adsabs.harvard.edu/abs/2022A&A...660A..78B), 2022, Astronomy and Astrophysics, 660, A78

78. Oei, Martijn S. S. L., van Weeren, Reinout J., Hardcastle, Martin J., et al.: [The discovery of a radio galaxy of at least 5 Mpc](http://adsabs.harvard.edu/abs/2022A&A...660A...2O), 2022, Astronomy and Astrophysics, 660, A2

79. Davis, F., Kaviraj, S., Hardcastle, M. J., et al.: [Radio AGN in nearby dwarf galaxies: the important role of AGN in dwarf galaxy evolution](http://adsabs.harvard.edu/abs/2022MNRAS.511.4109D), 2022, Monthly Notices of the Royal Astronomical Society, 511, 4109

80. Tiwari, Prabhakar, Zhao, Ruiyang, Zheng, Jinglan, et al.: [Galaxy Power Spectrum and Biasing Results from the LOFAR Two-meter Sky Survey (First Data Release)](http://adsabs.harvard.edu/abs/2022ApJ...928...38T), 2022, The Astrophysical Journal, 928, 38

81. Rajpurohit, K., van Weeren, R. J., Hoeft, M., et al.: [Deep Low-frequency Radio Observations of A2256. I. The Filamentary Radio Relic](http://adsabs.harvard.edu/abs/2022ApJ...927...80R), 2022, The Astrophysical Journal, 927, 80

82. Scholten, O., Hare, B. M., Dwyer, J., et al.: [Interferometric imaging of intensely radiating negative leaders](http://adsabs.harvard.edu/abs/2022PhRvD.105f2007S), 2022, Physical Review D, 105, 062007

83. Ignesti, A., Brunetti, G., Shimwell, T., et al.: [A LOFAR view into the stormy environment of the galaxy cluster 2A0335+096](http://adsabs.harvard.edu/abs/2022A&A...659A..20I), 2022, Astronomy and Astrophysics, 659, A20

84. Shimwell, T. W., Hardcastle, M. J., Tasse, C., et al.: [The LOFAR Two-metre Sky Survey. V. Second data release](http://adsabs.harvard.edu/abs/2022A&A...659A...1S), 2022, Astronomy and Astrophysics, 659, A1

85. Liu, Ningyu Y., Scholten, Olaf, Hare, Brian M., et al.: [LOFAR Observations of Lightning Initial Breakdown Pulses](http://adsabs.harvard.edu/abs/2022GeoRL..4998073L), 2022, Geophysical Research Letters, 49, e98073

86. Badole, S., Venkattu, D., Jackson, N., et al.: [High-resolution imaging with the International LOFAR Telescope: Observations of the gravitational lenses MG 0751+2716 and CLASS B1600+434](http://adsabs.harvard.edu/abs/2022A&A...658A...7B), 2022, Astronomy and Astrophysics, 658, A7

87. Gourdji, K., Rowlinson, A., Wijers, R. A. M. J., et al.: [Searching for low radio-frequency gravitational wave counterparts in wide-field LOFAR data](http://adsabs.harvard.edu/abs/2022MNRAS.509.5018G), 2022, Monthly Notices of the Royal Astronomical Society, 509, 5018

88. Vedantham, H. K., Callingham, J. R., Shimwell, T. W., et al.: [Peculiar Radio-X-Ray Relationship in Active Stars](http://adsabs.harvard.edu/abs/2022ApJ...926L..30V), 2022, The Astrophysical Journal, 926, L30

89. Jackson, N., Badole, S., Morgan, J., et al.: [Sub-arcsecond imaging with the International LOFAR Telescope. II. Completion of the LOFAR Long-Baseline Calibrator Survey](http://adsabs.harvard.edu/abs/2022A&A...658A...2J), 2022, Astronomy and Astrophysics, 658, A2

90. Kukreti, Pranav, Morganti, Raffaella, Shimwell, Timothy W., et al.: [Unmasking the history of 3C 293 with LOFAR sub-arcsecond imaging](http://adsabs.harvard.edu/abs/2022A&A...658A...6K), 2022, Astronomy and Astrophysics, 658, A6

91. Sweijen, F., Morabito, L. K., Harwood, J., et al.: [High-resolution international LOFAR observations of 4C 43.15. Spectral ages and injection indices in a high-z radio galaxy](http://adsabs.harvard.edu/abs/2022A&A...658A...3S), 2022, Astronomy and Astrophysics, 658, A3

92. Gordovskyy, Mykola, Kontar, Eduard P., Clarkson, Daniel L., et al.: [Sizes and Shapes of Sources in Solar Metric Radio Bursts](http://adsabs.harvard.edu/abs/2022ApJ...925..140G), 2022, The Astrophysical Journal, 925, 140

93. Timmerman, R., van Weeren, R. J., Callingham, J. R., et al.: [Origin of the ring structures in Hercules A. Sub-arcsecond 144 MHz to 7 GHz observations](http://adsabs.harvard.edu/abs/2022A&A...658A...5T), 2022, Astronomy and Astrophysics, 658, A5

94. Roberts, I. D., van Weeren, R. J., Timmerman, R., et al.: [LoTSS jellyfish galaxies. III. The first identification of jellyfish galaxies in the Perseus cluster](http://adsabs.harvard.edu/abs/2022A&A...658A..44R), 2022, Astronomy and Astrophysics, 658, A44

95. Bonnassieux, Etienne, Sweijen, Frits, Brienza, Marisa, et al.: [Spectral analysis of spatially resolved 3C295 (sub-arcsecond resolution) with the International LOFAR Telescope](http://adsabs.harvard.edu/abs/2022A&A...658A..10B), 2022, Astronomy and Astrophysics, 658, A10

96. Morabito, L. K., Jackson, N. J., Mooney, S., et al.: [Sub-arcsecond imaging with the International LOFAR Telescope. I. Foundational calibration strategy and pipeline](http://adsabs.harvard.edu/abs/2022A&A...658A...1M), 2022, Astronomy and Astrophysics, 658, A1

97. Groeneveld, C., van Weeren, R. J., Miley, G. K., et al.: [Pushing sub-arcsecond resolution imaging down to 30 MHz with the trans-European International LOFAR Telescope](http://adsabs.harvard.edu/abs/2022A&A...658A...9G), 2022, Astronomy and Astrophysics, 658, A9

98. Harwood, J. J., Mooney, S., Morabito, L. K., et al.: [The resolved jet of 3C 273 at 150 MHz. Sub-arcsecond imaging with the LOFAR international baselines](http://adsabs.harvard.edu/abs/2022A&A...658A...8H), 2022, Astronomy and Astrophysics, 658, A8

99. Ramírez-Olivencia, N., Varenius, E., Pérez-Torres, M., et al.: [Sub-arcsecond LOFAR imaging of Arp 299 at 150 MHz. Tracing the nuclear and diffuse extended emission of a bright LIRG](http://adsabs.harvard.edu/abs/2022A&A...658A...4R), 2022, Astronomy and Astrophysics, 658, A4

100. Offringa, A. R., Singal, J., Heston, S., et al.: [Measurement of the anisotropy power spectrum of the radio synchrotron background](http://adsabs.harvard.edu/abs/2022MNRAS.509..114O), 2022, Monthly Notices of the Royal Astronomical Society, 509, 114

101. Heald, G. H., Heesen, V., Sridhar, S. S., et al.: [CHANG-ES XXIII: influence of a galactic wind in NGC 5775](http://adsabs.harvard.edu/abs/2022MNRAS.509..658H), 2022, Monthly Notices of the Royal Astronomical Society, 509, 658

102. Ignesti, Alessandro, Vulcani, Benedetta, Poggianti, Bianca M., et al.: [GASP XXXVIII: The LOFAR-MeerKAT-VLA View on the Nonthermal Side of a Jellyfish Galaxy](http://adsabs.harvard.edu/abs/2022ApJ...924...64I), 2022, The Astrophysical Journal, 924, 64

103. Roshi, D. Anish, Peters, W. M., Emig, K. L., et al.: [Arecibo-Green Bank-LOFAR Carbon Radio Recombination Line Observations toward Cold H I Clouds](http://adsabs.harvard.edu/abs/2022ApJ...925....7R), 2022, The Astrophysical Journal, 925, 7

104. Hutschenreuter, S., Anderson, C. S., Betti, S., et al.: [The Galactic Faraday rotation sky 2020](http://adsabs.harvard.edu/abs/2022A&A...657A..43H), 2022, Astronomy and Astrophysics, 657, A43

105. Sweijen, F., van Weeren, R. J., Röttgering, H. J. A., et al.: [Deep sub-arcsecond wide-field imaging of the Lockman Hole field at 144 MHz](http://adsabs.harvard.edu/abs/2022NatAs...6..350S), 2022, Nature Astronomy, 6, 350

106. Barkus, B., Croston, J. H., Piotrowska, J., et al.: [The application of ridgelines in extended radio source cross-identification](http://adsabs.harvard.edu/abs/2022MNRAS.509....1B), 2022, Monthly Notices of the Royal Astronomical Society, 509, 1

107. Cho, Hyejeon, James Jee, M., Smith, Rory, et al.: [Multiwavelength Analysis of A1240, the Double Radio-relic Merging Galaxy Cluster Embedded in an 80 Mpc-long Cosmic Filament](http://adsabs.harvard.edu/abs/2022ApJ...925...68C), 2022, The Astrophysical Journal, 925, 68

108. Mevius, M., Mertens, F., Koopmans, L. V. E., et al.: [A numerical study of 21-cm signal suppression and noise increase in direction-dependent calibration of LOFAR data](http://adsabs.harvard.edu/abs/2022MNRAS.509.3693M), 2022, Monthly Notices of the Royal Astronomical Society, 509, 3693

109. Virone, Giuseppe, Paonessa, Fabio, Ciorba, Lorenzo, et al.: [Measurement of the LOFAR-HBA beam patterns using an unmanned aerial vehicle in the near field](http://adsabs.harvard.edu/abs/2022JATIS...8a1005V), 2022, Journal of Astronomical Telescopes, Instruments, and Systems, 8, 011005


2021
----

1. Webster, B., Croston, J. H., Harwood, J. J., et al.: [Investigating the spectra and physical nature of galaxy scale jets](http://adsabs.harvard.edu/abs/2021MNRAS.508.5972W), 2021, Monthly Notices of the Royal Astronomical Society, 508, 5972

2. Biava, N., de Gasperin, F., Bonafede, A., et al.: [The ultra-steep diffuse radio emission observed in the cool-core cluster RX J1720.1+2638 with LOFAR at 54 MHz](http://adsabs.harvard.edu/abs/2021MNRAS.508.3995B), 2021, Monthly Notices of the Royal Astronomical Society, 508, 3995

3. Gloudemans, A. J., Duncan, K. J., Röttgering, H. J. A., et al.: [Low frequency radio properties of the z &gt; ​5 quasar population](http://adsabs.harvard.edu/abs/2021A&A...656A.137G), 2021, Astronomy and Astrophysics, 656, A137

4. Roy, Namrata, Moravec, Emily, Bundy, Kevin, et al.: [Radio Morphology of Red Geysers](http://adsabs.harvard.edu/abs/2021ApJ...922..230R), 2021, The Astrophysical Journal, 922, 230

5. de Ruiter, Iris, Leseigneur, Guillaume, Rowlinson, Antonia, et al.: [Limits on long-time-scale radio transients at 150 MHz using the TGSS ADR1 and LoTSS DR2 catalogues](http://adsabs.harvard.edu/abs/2021MNRAS.508.2412D), 2021, Monthly Notices of the Royal Astronomical Society, 508, 2412

6. Callingham, J. R., Vedantham, H. K., Shimwell, T. W., et al.: [The population of M dwarfs observed at low radio frequencies](http://adsabs.harvard.edu/abs/2021NatAs...5.1233C), 2021, Nature Astronomy, 5, 1233

7. Mooney, Seán, Massaro, Francesco, Quinn, John, et al.: [Characterising the Extended Morphologies of BL Lacertae Objects at 144 MHz with LOFAR](http://adsabs.harvard.edu/abs/2021ApJS..257...30M), 2021, The Astrophysical Journal Supplement Series, 257, 30

8. Bonato, M., Prandoni, I., De Zotti, G., et al.: [The LOFAR Two-metre Sky Survey Deep Fields. A new analysis of low-frequency radio luminosity as a star-formation tracer in the Lockman Hole region](http://adsabs.harvard.edu/abs/2021A&A...656A..48B), 2021, Astronomy and Astrophysics, 656, A48

9. Gürkan, Gülay, Croston, Judith, Hardcastle, Martin J., et al.: [Finding Rare Quasars: VLA Snapshot Continuum Survey of FRI Quasar Candidates Selected from the LOFAR Two-Metre Sky Survey (LoTSS)](http://adsabs.harvard.edu/abs/2021Galax..10....2G), 2021, Galaxies, 10, 2

10. Sterpka, C., Dwyer, J., Liu, N., et al.: [The Spontaneous Nature of Lightning Initiation Revealed](http://adsabs.harvard.edu/abs/2021GeoRL..4895511S), 2021, Geophysical Research Letters, 48, e95511

11. Hoang, D. N., Zhang, X., Stuardi, C., et al.: [A 3.5 Mpc long radio relic in the galaxy cluster ClG 0217+70](http://adsabs.harvard.edu/abs/2021A&A...656A.154H), 2021, Astronomy and Astrophysics, 656, A154

12. Richards, Gordon T., McCaffrey, Trevor V., Kimball, Amy, et al.: [Probing the Wind Component of Radio Emission in Luminous High-redshift Quasars](http://adsabs.harvard.edu/abs/2021AJ....162..270R), 2021, The Astronomical Journal, 162, 270

13. Williams, W. L., de Gasperin, F., Hardcastle, M. J. H., et al.: [The LOFAR LBA Sky Survey: Deep Fields. I. The Boötes Field](http://adsabs.harvard.edu/abs/2021A&A...655A..40W), 2021, Astronomy and Astrophysics, 655, A40

14. Agar, C. H., Weltevrede, P., Bondonneau, L., et al.: [A broad-band radio study of PSR J0250+5854: the slowest spinning radio pulsar known](http://adsabs.harvard.edu/abs/2021MNRAS.508.1102A), 2021, Monthly Notices of the Royal Astronomical Society, 508, 1102

15. Murphy, P. C., Callanan, P., McCauley, J., et al.: [First results from the REAL-time Transient Acquisition backend (REALTA) at the Irish LOFAR station](http://adsabs.harvard.edu/abs/2021A&A...655A..16M), 2021, Astronomy and Astrophysics, 655, A16

16. Morganti, Raffaella, Jurlin, Nika, Oosterloo, Tom, et al.: [Combining LOFAR and Apertif Data for Understanding the Life Cycle of Radio Galaxies](http://adsabs.harvard.edu/abs/2021Galax...9...88M), 2021, Galaxies, 9, 88

17. Turić, Luka, Jelić, Vibor, Jaspers, Rutger, et al.: [Multi-tracer analysis of straight depolarisation canals in the surroundings of the 3C 196 field](http://adsabs.harvard.edu/abs/2021A&A...654A...5T), 2021, Astronomy and Astrophysics, 654, A5

18. Toet, S. E. B., Vedantham, H. K., Callingham, J. R., et al.: [Coherent radio emission from a population of RS Canum Venaticorum systems](http://adsabs.harvard.edu/abs/2021A&A...654A..21T), 2021, Astronomy and Astrophysics, 654, A21

19. Rowlinson, A., Starling, R. L. C., Gourdji, K., et al.: [LOFAR early-time search for coherent radio emission from short GRB 181123B](http://adsabs.harvard.edu/abs/2021MNRAS.506.5268R), 2021, Monthly Notices of the Royal Astronomical Society, 506, 5268

20. Macfarlane, C., Best, P. N., Sabater, J., et al.: [The radio loudness of SDSS quasars from the LOFAR Two-metre Sky Survey: ubiquitous jet activity and constraints on star formation](http://adsabs.harvard.edu/abs/2021MNRAS.506.5888M), 2021, Monthly Notices of the Royal Astronomical Society, 506, 5888

21. Scholten, O., Hare, B. M., Dwyer, J., et al.: [Distinguishing features of high altitude negative leaders as observed with LOFAR](http://adsabs.harvard.edu/abs/2021AtmRe.26005688S), 2021, Atmospheric Research, 260, 105688

22. Grießmeier, J. -M., Smith, D. A., Theureau, G., et al.: [Follow-up of 27 radio-quiet gamma-ray pulsars at 110-190 MHz using the international LOFAR station FR606](http://adsabs.harvard.edu/abs/2021A&A...654A..43G), 2021, Astronomy and Astrophysics, 654, A43

23. Hajduk, Marcin, Haverkorn, Marijke, Shimwell, Timothy, et al.: [Evidence for Cold Plasma in Planetary Nebulae From Radio Observations With the LOw Frequency ARray (LOFAR)](http://adsabs.harvard.edu/abs/2021ApJ...919..121H), 2021, The Astrophysical Journal, 919, 121

24. Di Gennaro, G., van Weeren, R. J., Cassano, R., et al.: [A LOFAR-uGMRT spectral index study of distant radio halos](http://adsabs.harvard.edu/abs/2021A&A...654A.166D), 2021, Astronomy and Astrophysics, 654, A166

25. Hoeft, M., Dumba, C., Drabent, A., et al.: [Abell 1430: A merging cluster with exceptional diffuse radio emission](http://adsabs.harvard.edu/abs/2021A&A...654A..68H), 2021, Astronomy and Astrophysics, 654, A68

26. Carvajal, Rodrigo, Matute, Israel, Afonso, José, et al.: [Exploring New Redshift Indicators for Radio-Powerful AGN](http://adsabs.harvard.edu/abs/2021Galax...9...86C), 2021, Galaxies, 9, 86

27. Nikiel-Wroczyński, Błażej: [Somewhere in between: Tracing the Radio Emission from Galaxy Groups (or Why Does the Future of Observing Galaxy Groups with Radio Telescopes Look Promising?)](http://adsabs.harvard.edu/abs/2021Galax...9...84N), 2021, Galaxies, 9, 84

28. Paul, Surajit, Gupta, Prateek, Salunkhe, Sameer, et al.: [uGMRT detection of cluster radio emission in low-mass Planck Sunyaev-Zel'dovich clusters](http://adsabs.harvard.edu/abs/2021MNRAS.506.5389P), 2021, Monthly Notices of the Royal Astronomical Society, 506, 5389

29. Feeney-Johansson, A., Purser, S. J. D., Ray, T. P., et al.: [Detection of coherent low-frequency radio bursts from weak-line T Tauri stars](http://adsabs.harvard.edu/abs/2021A&A...653A.101F), 2021, Astronomy and Astrophysics, 653, A101

30. Jurlin, N., Brienza, M., Morganti, R., et al.: [Multi-frequency characterisation of remnant radio galaxies in the Lockman Hole field](http://adsabs.harvard.edu/abs/2021A&A...653A.110J), 2021, Astronomy and Astrophysics, 653, A110

31. Scholten, O., Hare, B. M., Dwyer, J., et al.: [Time resolved 3D interferometric imaging of a section of a negative leader with LOFAR](http://adsabs.harvard.edu/abs/2021PhRvD.104f3022S), 2021, Physical Review D, 104, 063022

32. de Vos, K., Hatch, N. A., Merrifield, M. R., et al.: [Clusters' far-reaching influence on narrow-angle tail radio galaxies](http://adsabs.harvard.edu/abs/2021MNRAS.506L..55D), 2021, Monthly Notices of the Royal Astronomical Society, 506, L55

33. Järvelä, Emilia, Berton, Marco, Crepaldi, Luca: [Narrow-line Seyfert 1 galaxies with absorbed jets -insights from radio spectral index maps](http://adsabs.harvard.edu/abs/2021FrASS...8..147J), 2021, Frontiers in Astronomy and Space Sciences, 8, 147

34. Pastor-Marazuela, Inés, Connor, Liam, van Leeuwen, Joeri, et al.: [Chromatic periodic activity down to 120 megahertz in a fast radio burst](http://adsabs.harvard.edu/abs/2021Natur.596..505P), 2021, Nature, 596, 505

35. Kuiack, Mark, Wijers, Ralph A. M. J., Shulevski, Aleksandar, et al.: [The AARTFAAC 60 MHz transients survey](http://adsabs.harvard.edu/abs/2021MNRAS.505.2966K), 2021, Monthly Notices of the Royal Astronomical Society, 505, 2966

36. Locatelli, N., Vazza, F., Bonafede, A., et al.: [New constraints on the magnetic field in cosmic web filaments](http://adsabs.harvard.edu/abs/2021A&A...652A..80L), 2021, Astronomy and Astrophysics, 652, A80

37. Clarkson, Daniel L., Kontar, Eduard P., Gordovskyy, Mykola, et al.: [First Frequency-time-resolved Imaging Spectroscopy Observations of Solar Radio Spikes](http://adsabs.harvard.edu/abs/2021ApJ...917L..32C), 2021, The Astrophysical Journal, 917, L32

38. Roberts, I. D., van Weeren, R. J., McGee, S. L., et al.: [LoTSS jellyfish galaxies. II. Ram pressure stripping in groups versus clusters](http://adsabs.harvard.edu/abs/2021A&A...652A.153R), 2021, Astronomy and Astrophysics, 652, A153

39. Scholten, O., Hare, B. M., Dwyer, J., et al.: [A distinct negative leader propagation mode](http://adsabs.harvard.edu/abs/2021NatSR..1116256S), 2021, Scientific Reports, 11, 16256

40. Jones, A., de Gasperin, F., Cuciti, V., et al.: [Radio relics in PSZ2 G096.88+24.18: a connection with pre-existing plasma](http://adsabs.harvard.edu/abs/2021MNRAS.505.4762J), 2021, Monthly Notices of the Royal Astronomical Society, 505, 4762

41. Edler, H. W., de Gasperin, F., Rafferty, D.: [Investigating ionospheric calibration for LOFAR 2.0 with simulated observations](http://adsabs.harvard.edu/abs/2021A&A...652A..37E), 2021, Astronomy and Astrophysics, 652, A37

42. Gordon, Yjan A., Boyce, Michelle M., O'Dea, Christopher P., et al.: [A Quick Look at the 3 GHz Radio Sky. I. Source Statistics from the Very Large Array Sky Survey](http://adsabs.harvard.edu/abs/2021ApJS..255...30G), 2021, The Astrophysical Journal Supplement Series, 255, 30

43. Kuiack, Mark J., Wijers, Ralph A. M. J., Shulevski, Aleksandar, et al.: [Apparent radio transients mapping the near-Earth plasma environment](http://adsabs.harvard.edu/abs/2021MNRAS.504.4706K), 2021, Monthly Notices of the Royal Astronomical Society, 504, 4706

44. van Weeren, R. J., Shimwell, T. W., Botteon, A., et al.: [LOFAR observations of galaxy clusters in HETDEX. Extraction and self-calibration of individual LOFAR targets](http://adsabs.harvard.edu/abs/2021A&A...651A.115V), 2021, Astronomy and Astrophysics, 651, A115

45. Hare, Brian M., Edens, Harald, Krehbiel, Paul, et al.: [Timing Calibration and Windowing Technique Comparison for Lightning Mapping Arrays](http://adsabs.harvard.edu/abs/2021E&SS....801523H), 2021, Earth and Space Science, 8, e01523

46. McKean, J. P., Luichies, R., Drabent, A., et al.: [Gravitational lensing in LoTSS DR2: extremely faint 144-MHz radio emission from two highly magnified quasars](http://adsabs.harvard.edu/abs/2021MNRAS.505L..36M), 2021, Monthly Notices of the Royal Astronomical Society, 505, L36

47. Biava, Nadia, Brienza, Marisa, Bonafede, Annalisa, et al.: [Constraining the AGN duty cycle in the cool-core cluster MS 0735.6+7421 with LOFAR data](http://adsabs.harvard.edu/abs/2021A&A...650A.170B), 2021, Astronomy and Astrophysics, 650, A170

48. Masini, Alberto, Celotti, Annalisa, Grandi, Paola, et al.: [A new distant giant radio galaxy in the Boötes field serendipitously detected by Chandra](http://adsabs.harvard.edu/abs/2021A&A...650A..51M), 2021, Astronomy and Astrophysics, 650, A51

49. Broderick, J. W., Russell, T. D., Fender, R. P., et al.: [Strong low-frequency radio flaring from Cygnus X-3 observed with LOFAR](http://adsabs.harvard.edu/abs/2021MNRAS.504.1482B), 2021, Monthly Notices of the Royal Astronomical Society, 504, 1482

50. Davis, I., Vedantham, H. K., Callingham, J. R., et al.: [Large closed-field corona of WX Ursae Majoris evidenced from radio observations](http://adsabs.harvard.edu/abs/2021A&A...650L..20D), 2021, Astronomy and Astrophysics, 650, L20

51. Roberts, I. D., van Weeren, R. J., McGee, S. L., et al.: [LoTSS jellyfish galaxies. I. Radio tails in low redshift clusters](http://adsabs.harvard.edu/abs/2021A&A...650A.111R), 2021, Astronomy and Astrophysics, 650, A111

52. Maan, Yogesh, van Leeuwen, Joeri, Vohl, Dany: [Fourier domain excision of periodic radio frequency interference](http://adsabs.harvard.edu/abs/2021A&A...650A..80M), 2021, Astronomy and Astrophysics, 650, A80

53. Botteon, A., Cassano, R., van Weeren, R. J., et al.: [Discovery of a Radio Halo (and Relic) in a M<SUB>500</SUB>&lt;2×10<SUP>14</SUP> M<SUB>⊙</SUB> Cluster](http://adsabs.harvard.edu/abs/2021ApJ...914L..29B), 2021, The Astrophysical Journal, 914, L29

54. Bailes, M., Bassa, C. G., Bernardi, G., et al.: [Multifrequency observations of SGR J1935+2154](http://adsabs.harvard.edu/abs/2021MNRAS.503.5367B), 2021, Monthly Notices of the Royal Astronomical Society, 503, 5367

55. Bruno, L., Rajpurohit, K., Brunetti, G., et al.: [The LOFAR and JVLA view of the distant steep spectrum radio halo in MACS J1149.5+2223](http://adsabs.harvard.edu/abs/2021A&A...650A..44B), 2021, Astronomy and Astrophysics, 650, A44

56. Bruni, G., Brienza, M., Panessa, F., et al.: [Hard X-ray selected giant radio galaxies - III. The LOFAR view](http://adsabs.harvard.edu/abs/2021MNRAS.503.4681B), 2021, Monthly Notices of the Royal Astronomical Society, 503, 4681

57. Reid, Hamish A. S., Kontar, Eduard P.: [Fine structure of type III solar radio bursts from Langmuir wave motion in turbulent plasma](http://adsabs.harvard.edu/abs/2021NatAs...5..796R), 2021, Nature Astronomy, 5, 796

58. Corstanje, A., Buitink, S., Falcke, H., et al.: [Depth of shower maximum and mass composition of cosmic rays from 50 PeV to 2 EeV measured with the LOFAR radio telescope](http://adsabs.harvard.edu/abs/2021PhRvD.103j2006C), 2021, Physical Review D, 103, 102006

59. Botteon, A., Giacintucci, S., Gastaldello, F., et al.: [Nonthermal phenomena in the center of Abell 1775. An 800 kpc head-tail, revived fossil plasma and slingshot radio halo](http://adsabs.harvard.edu/abs/2021A&A...649A..37B), 2021, Astronomy and Astrophysics, 649, A37

60. Miraval Zanon, A., D'Avanzo, P., Ridolfi, A., et al.: [Evidence of intra-binary shock emission from the redback pulsar PSR J1048+2339](http://adsabs.harvard.edu/abs/2021A&A...649A.120M), 2021, Astronomy and Astrophysics, 649, A120

61. Morganti, R., Oosterloo, T. A., Brienza, M., et al.: [The best of both worlds: Combining LOFAR and Apertif to derive resolved radio spectral index images](http://adsabs.harvard.edu/abs/2021A&A...648A...9M), 2021, Astronomy and Astrophysics, 648, A9

62. Ryan, A. M., Gallagher, P. T., Carley, E. P., et al.: [LOFAR imaging of the solar corona during the 2015 March 20 solar eclipse](http://adsabs.harvard.edu/abs/2021A&A...648A..43R), 2021, Astronomy and Astrophysics, 648, A43

63. Hardcastle, M. J., Shimwell, T. W., Tasse, C., et al.: [The contribution of discrete sources to the sky temperature at 144 MHz](http://adsabs.harvard.edu/abs/2021A&A...648A..10H), 2021, Astronomy and Astrophysics, 648, A10

64. Mandal, S., Prandoni, I., Hardcastle, M. J., et al.: [Extremely deep 150 MHz source counts from the LoTSS Deep Fields](http://adsabs.harvard.edu/abs/2021A&A...648A...5M), 2021, Astronomy and Astrophysics, 648, A5

65. Rankine, Amy L., Matthews, James H., Hewett, Paul C., et al.: [Placing LOFAR-detected quasars in C IV emission space: implications for winds, jets and star formation](http://adsabs.harvard.edu/abs/2021MNRAS.502.4154R), 2021, Monthly Notices of the Royal Astronomical Society, 502, 4154

66. Sabater, J., Best, P. N., Tasse, C., et al.: [The LOFAR Two-meter Sky Survey: Deep Fields Data Release 1. II. The ELAIS-N1 LOFAR deep field](http://adsabs.harvard.edu/abs/2021A&A...648A...2S), 2021, Astronomy and Astrophysics, 648, A2

67. Herrera Ruiz, N., O'Sullivan, S. P., Vacca, V., et al.: [LOFAR Deep Fields: probing a broader population of polarized radio galaxies in ELAIS-N1](http://adsabs.harvard.edu/abs/2021A&A...648A..12H), 2021, Astronomy and Astrophysics, 648, A12

68. Ramasawmy, J., Geach, J. E., Hardcastle, M. J., et al.: [Low-frequency radio spectra of submillimetre galaxies in the Lockman Hole](http://adsabs.harvard.edu/abs/2021A&A...648A..14R), 2021, Astronomy and Astrophysics, 648, A14

69. Tasse, C., Shimwell, T., Hardcastle, M. J., et al.: [The LOFAR Two-meter Sky Survey: Deep Fields Data Release 1. I. Direction-dependent calibration and imaging](http://adsabs.harvard.edu/abs/2021A&A...648A...1T), 2021, Astronomy and Astrophysics, 648, A1

70. Smith, D. J. B., Haskell, P., Gürkan, G., et al.: [The LOFAR Two-metre Sky Survey Deep Fields. The star-formation rate-radio luminosity relation at low frequencies](http://adsabs.harvard.edu/abs/2021A&A...648A...6S), 2021, Astronomy and Astrophysics, 648, A6

71. Osinga, E., van Weeren, R. J., Boxelaar, J. M., et al.: [Diffuse radio emission from galaxy clusters in the LOFAR Two-metre Sky Survey Deep Fields](http://adsabs.harvard.edu/abs/2021A&A...648A..11O), 2021, Astronomy and Astrophysics, 648, A11

72. Pleunis, Z., Michilli, D., Bassa, C. G., et al.: [LOFAR Detection of 110-188 MHz Emission and Frequency-dependent Activity from FRB 20180916B](http://adsabs.harvard.edu/abs/2021ApJ...911L...3P), 2021, The Astrophysical Journal, 911, L3

73. Wang, L., Gao, F., Best, P. N., et al.: [The bright end of the infrared luminosity functions and the abundance of hyperluminous infrared galaxies](http://adsabs.harvard.edu/abs/2021A&A...648A...8W), 2021, Astronomy and Astrophysics, 648, A8

74. Gloudemans, A. J., Duncan, K. J., Kondapally, R., et al.: [LOFAR properties of SILVERRUSH Lyα emitter candidates in the ELAIS-N1 field](http://adsabs.harvard.edu/abs/2021A&A...648A...7G), 2021, Astronomy and Astrophysics, 648, A7

75. Lan 藍鼎文, Ting-Wen, Xavier Prochaska, J.: [On the environments of giant radio galaxies](http://adsabs.harvard.edu/abs/2021MNRAS.502.5104L), 2021, Monthly Notices of the Royal Astronomical Society, 502, 5104

76. Duncan, K. J., Kondapally, R., Brown, M. J. I., et al.: [The LOFAR Two-meter Sky Survey: Deep Fields Data Release 1. IV. Photometric redshifts and stellar masses](http://adsabs.harvard.edu/abs/2021A&A...648A...4D), 2021, Astronomy and Astrophysics, 648, A4

77. Kondapally, R., Best, P. N., Hardcastle, M. J., et al.: [The LOFAR Two-meter Sky Survey: Deep Fields Data Release 1. III. Host-galaxy identifications and value added catalogues](http://adsabs.harvard.edu/abs/2021A&A...648A...3K), 2021, Astronomy and Astrophysics, 648, A3

78. Callingham, J. R., Pope, B. J. S., Feinstein, A. D., et al.: [Low-frequency monitoring of flare star binary CR Draconis: long-term electron-cyclotron maser emission](http://adsabs.harvard.edu/abs/2021A&A...648A..13C), 2021, Astronomy and Astrophysics, 648, A13

79. Sudoh, Takahiro, Linden, Tim, Beacom, John F.: [Millisecond pulsars modify the radio-star-formation-rate correlation in quiescent galaxies](http://adsabs.harvard.edu/abs/2021PhRvD.103h3017S), 2021, Physical Review D, 103, 083017

80. Ntwaetsile, Kushatha, Geach, James E.: [Rapid sorting of radio galaxy morphology using Haralick features](http://adsabs.harvard.edu/abs/2021MNRAS.502.3417N), 2021, Monthly Notices of the Royal Astronomical Society, 502, 3417

81. Mahatma, V. H., Hardcastle, M. J., Harwood, J., et al.: [A low-frequency study of linear polarization in radio galaxies](http://adsabs.harvard.edu/abs/2021MNRAS.502..273M), 2021, Monthly Notices of the Royal Astronomical Society, 502, 273

82. Alonso, David, Bellini, Emilio, Hale, Catherine, et al.: [Cross-correlating radio continuum surveys and CMB lensing: constraining redshift distributions, galaxy bias, and cosmology](http://adsabs.harvard.edu/abs/2021MNRAS.502..876A), 2021, Monthly Notices of the Royal Astronomical Society, 502, 876

83. Tiburzi, C., Shaifullah, G. M., Bassa, C. G., et al.: [The impact of solar wind variability on pulsar timing](http://adsabs.harvard.edu/abs/2021A&A...647A..84T), 2021, Astronomy and Astrophysics, 647, A84

84. Maguire, Ciara A., Carley, Eoin P., Zucca, Pietro, et al.: [LOFAR Observations of a Jet-driven Piston Shock in the Low Solar Corona](http://adsabs.harvard.edu/abs/2021ApJ...909....2M), 2021, The Astrophysical Journal, 909, 2

85. Ghirardini, V., Bulbul, E., Hoang, D. N., et al.: [Discovery of a supercluster in the eROSITA Final Equatorial Depth Survey: X-ray properties, radio halo, and double relics](http://adsabs.harvard.edu/abs/2021A&A...647A...4G), 2021, Astronomy and Astrophysics, 647, A4

86. Wolf, J., Nandra, K., Salvato, M., et al.: [First constraints on the AGN X-ray luminosity function at z   6 from an eROSITA-detected quasar](http://adsabs.harvard.edu/abs/2021A&A...647A...5W), 2021, Astronomy and Astrophysics, 647, A5

87. Rajpurohit, K., Brunetti, G., Bonafede, A., et al.: [Physical insights from the spectrum of the radio halo in MACS J0717.5+3745](http://adsabs.harvard.edu/abs/2021A&A...646A.135R), 2021, Astronomy and Astrophysics, 646, A135

88. Jimenez-Gallardo, A., Massaro, F., Paggi, A., et al.: [Extended X-Ray Emission around FR II Radio Galaxies: Hot Spots, Lobes, and Galaxy Clusters](http://adsabs.harvard.edu/abs/2021ApJS..252...31J), 2021, The Astrophysical Journal Supplement Series, 252, 31

89. Scholten, O., Hare, B. M., Dwyer, J., et al.: [The Initial Stage of Cloud Lightning Imaged in High Resolution](http://adsabs.harvard.edu/abs/2021JGRD..12633126S), 2021, Journal of Geophysical Research (Atmospheres), 126, e2020JD033126

90. Hoang, D. N., Shimwell, T. W., Osinga, E., et al.: [LOFAR detection of a low-power radio halo in the galaxy cluster Abell 990](http://adsabs.harvard.edu/abs/2021MNRAS.501..576H), 2021, Monthly Notices of the Royal Astronomical Society, 501, 576

91. Marecki, A., Jamrozy, M., Machalski, J., et al.: [Multifrequency study of a double-double radio galaxy J0028+0035](http://adsabs.harvard.edu/abs/2021MNRAS.501..853M), 2021, Monthly Notices of the Royal Astronomical Society, 501, 853

92. Rajpurohit, K., Wittor, D., van Weeren, R. J., et al.: [Understanding the radio relic emission in the galaxy cluster MACS J0717.5+3745: Spectral analysis](http://adsabs.harvard.edu/abs/2021A&A...646A..56R), 2021, Astronomy and Astrophysics, 646, A56

93. Di Gennaro, Gabriella, van Weeren, Reinout J., Brunetti, Gianfranco, et al.: [Fast magnetic field amplification in distant galaxy clusters](http://adsabs.harvard.edu/abs/2021NatAs...5..268D), 2021, Nature Astronomy, 5, 268

94. Turner, Jake D., Zarka, Philippe, Grießmeier, Jean-Mathias, et al.: [The search for radio emission from the exoplanetary systems 55 Cancri, υ Andromedae, and τ Boötis using LOFAR beam-formed observations](http://adsabs.harvard.edu/abs/2021A&A...645A..59T), 2021, Astronomy and Astrophysics, 645, A59

95. Bonafede, A., Brunetti, G., Vazza, F., et al.: [The Coma Cluster at LOw Frequency ARray Frequencies. I. Insights into Particle Acceleration Mechanisms in the Radio Bridge](http://adsabs.harvard.edu/abs/2021ApJ...907...32B), 2021, The Astrophysical Journal, 907, 32

96. Webster, B., Croston, J. H., Mingo, B., et al.: [A population of galaxy-scale jets discovered using LOFAR](http://adsabs.harvard.edu/abs/2021MNRAS.500.4921W), 2021, Monthly Notices of the Royal Astronomical Society, 500, 4921

97. Mostert, Rafaël I. J., Duncan, Kenneth J., Röttgering, Huub J. A., et al.: [Unveiling the rarest morphologies of the LOFAR Two-metre Sky Survey radio source population with self-organised maps](http://adsabs.harvard.edu/abs/2021A&A...645A..89M), 2021, Astronomy and Astrophysics, 645, A89

98. Murphy, Pearse C., Carley, Eoin P., Ryan, Aoife Maria, et al.: [LOFAR observations of radio burst source sizes and scattering in the solar corona](http://adsabs.harvard.edu/abs/2021A&A...645A..11M), 2021, Astronomy and Astrophysics, 645, A11

99. Hothi, Ian, Chapman, Emma, Pritchard, Jonathan R., et al.: [Comparing foreground removal techniques for recovery of the LOFAR-EoR 21 cm power spectrum](http://adsabs.harvard.edu/abs/2021MNRAS.500.2264H), 2021, Monthly Notices of the Royal Astronomical Society, 500, 2264

100. Greig, Bradley, Mesinger, Andrei, Koopmans, Léon V. E., et al.: [Interpreting LOFAR 21-cm signal upper limits at z ≈ 9.1 in the context of high-z galaxy and reionization observations](http://adsabs.harvard.edu/abs/2021MNRAS.501....1G), 2021, Monthly Notices of the Royal Astronomical Society, 501, 1


2020
----

1. Mitra, P., Bonardi, A., Corstanje, A., et al.: [Reconstructing air shower parameters with LOFAR using event specific GDAS atmosphere](http://adsabs.harvard.edu/abs/2020APh...12302470M), 2020, Astroparticle Physics, 123, 102470

2. Bracco, A., Jelić, V., Marchal, A., et al.: [The multiphase and magnetized neutral hydrogen seen by LOFAR](http://adsabs.harvard.edu/abs/2020A&A...644L...3B), 2020, Astronomy and Astrophysics, 644, L3

3. Donner, J. Y., Verbiest, J. P. W., Tiburzi, C., et al.: [Dispersion measure variability for 36 millisecond pulsars at 150 MHz with LOFAR](http://adsabs.harvard.edu/abs/2020A&A...644A.153D), 2020, Astronomy and Astrophysics, 644, A153

4. Gehlot, B. K., Mertens, F. G., Koopmans, L. V. E., et al.: [The AARTFAAC Cosmic Explorer: observations of the 21-cm power spectrum in the EDGES absorption trough](http://adsabs.harvard.edu/abs/2020MNRAS.499.4158G), 2020, Monthly Notices of the Royal Astronomical Society, 499, 4158

5. Zheng, X. C., Röttgering, H. J. A., Best, P. N., et al.: [Link between radio-loud AGNs and host-galaxy shape](http://adsabs.harvard.edu/abs/2020A&A...644A..12Z), 2020, Astronomy and Astrophysics, 644, A12

6. Botteon, A., van Weeren, R. J., Brunetti, G., et al.: [A giant radio bridge connecting two galaxy clusters in Abell 1758](http://adsabs.harvard.edu/abs/2020MNRAS.499L..11B), 2020, Monthly Notices of the Royal Astronomical Society, 499, L11

7. de Gasperin, F., Lazio, T. J. W., Knapp, M.: [Radio observations of HD 80606 near planetary periastron. II. LOFAR low band antenna observations at 30-78 MHz](http://adsabs.harvard.edu/abs/2020A&A...644A.157D), 2020, Astronomy and Astrophysics, 644, A157

8. Vedantham, H. K., Callingham, J. R., Shimwell, T. W., et al.: [Direct Radio Discovery of a Cold Brown Dwarf](http://adsabs.harvard.edu/abs/2020ApJ...903L..33V), 2020, The Astrophysical Journal, 903, L33

9. Siewert, T. M., Hale, C., Bhardwaj, N., et al.: [One- and two-point source statistics from the LOFAR Two-metre Sky Survey first data release](http://adsabs.harvard.edu/abs/2020A&A...643A.100S), 2020, Astronomy and Astrophysics, 643, A100

10. Ignesti, A., Shimwell, T., Brunetti, G., et al.: [The great Kite in the sky: A LOFAR observation of the radio source in Abell 2626](http://adsabs.harvard.edu/abs/2020A&A...643A.172I), 2020, Astronomy and Astrophysics, 643, A172

11. Mulrey, K., Buitink, S., Corstanje, A., et al.: [On the cosmic-ray energy scale of the LOFAR radio telescope](http://adsabs.harvard.edu/abs/2020JCAP...11..017M), 2020, Journal of Cosmology and Astroparticle Physics, 2020, 017

12. Mondal, R., Fialkov, A., Fling, C., et al.: [Tight constraints on the excess radio background at z = 9.1 from LOFAR](http://adsabs.harvard.edu/abs/2020MNRAS.498.4178M), 2020, Monthly Notices of the Royal Astronomical Society, 498, 4178

13. Rożko, K., Kijak, J., Chyży, K., et al.: [The Significance of Low-frequency Interferometric Observations for the GPS Pulsar Flux Estimation: The Case of J1740+1000](http://adsabs.harvard.edu/abs/2020ApJ...903..144R), 2020, The Astrophysical Journal, 903, 144

14. Capetti, A., Brienza, M., Baldi, R. D., et al.: [The LOFAR view of FR 0 radio galaxies](http://adsabs.harvard.edu/abs/2020A&A...642A.107C), 2020, Astronomy and Astrophysics, 642, A107

15. Osinga, E., Miley, G. K., van Weeren, R. J., et al.: [Alignment in the orientation of LOFAR radio sources](http://adsabs.harvard.edu/abs/2020A&A...642A..70O), 2020, Astronomy and Astrophysics, 642, A70

16. Nieder, L., Clark, C. J., Kandel, D., et al.: [Discovery of a Gamma-Ray Black Widow Pulsar by GPU-accelerated Einstein@Home](http://adsabs.harvard.edu/abs/2020ApJ...902L..46N), 2020, The Astrophysical Journal, 902, L46

17. de Gasperin, F., Brunetti, G., Brüggen, M., et al.: [Reaching thermal noise at ultra-low radio frequencies. Toothbrush radio relic downstream of the shock front](http://adsabs.harvard.edu/abs/2020A&A...642A..85D), 2020, Astronomy and Astrophysics, 642, A85

18. Kuiack, Mark, Wijers, Ralph A. M. J., Rowlinson, Antonia, et al.: [Long-term study of extreme giant pulses from PSR B0950+08 with AARTFAAC](http://adsabs.harvard.edu/abs/2020MNRAS.497..846K), 2020, Monthly Notices of the Royal Astronomical Society, 497, 846

19. Shabala, Stanislav S., Jurlin, Nika, Morganti, Raffaella, et al.: [The duty cycle of radio galaxies revealed by LOFAR: remnant and restarted radio source populations in the Lockman Hole](http://adsabs.harvard.edu/abs/2020MNRAS.496.1706S), 2020, Monthly Notices of the Royal Astronomical Society, 496, 1706

20. Bîrzan, L., Rafferty, D. A., Brüggen, M., et al.: [LOFAR observations of X-ray cavity systems](http://adsabs.harvard.edu/abs/2020MNRAS.496.2613B), 2020, Monthly Notices of the Royal Astronomical Society, 496, 2613

21. Vollmann, Martin, Heesen, Volker, W. Shimwell, Timothy, et al.: [Radio constraints on dark matter annihilation in Canes Venatici I with LOFAR](http://adsabs.harvard.edu/abs/2020MNRAS.496.2663V), 2020, Monthly Notices of the Royal Astronomical Society, 496, 2663

22. Mesarcik, Michael, Boonstra, Albert-Jan, Meijer, Christiaan, et al.: [Deep learning assisted data inspection for radio astronomy](http://adsabs.harvard.edu/abs/2020MNRAS.496.1517M), 2020, Monthly Notices of the Royal Astronomical Society, 496, 1517

23. Moravec, Emily, Gonzalez, Anthony H., Dicker, Simon, et al.: [The Massive and Distant Clusters of WISE Survey. IX. High Radio Activity in a Merging Cluster](http://adsabs.harvard.edu/abs/2020ApJ...898..145M), 2020, The Astrophysical Journal, 898, 145

24. Kuznetsov, Alexey A., Chrysaphi, Nicolina, Kontar, Eduard P., et al.: [Radio Echo in the Turbulent Corona and Simulations of Solar Drift-pair Radio Bursts](http://adsabs.harvard.edu/abs/2020ApJ...898...94K), 2020, The Astrophysical Journal, 898, 94

25. Botteon, A., Brunetti, G., van Weeren, R. J., et al.: [The Beautiful Mess in Abell 2255](http://adsabs.harvard.edu/abs/2020ApJ...897...93B), 2020, The Astrophysical Journal, 897, 93

26. Locatelli, Nicola T., Rajpurohit, Kamlesh, Vazza, Franco, et al.: [Discovering the most elusive radio relic in the sky: diffuse shock acceleration caught in the act?](http://adsabs.harvard.edu/abs/2020MNRAS.496L..48L), 2020, Monthly Notices of the Royal Astronomical Society, 496, L48

27. Zhang, PeiJin, Zucca, Pietro, Sridhar, Sarrvesh Seethapuram, et al.: [Interferometric imaging with LOFAR remote baselines of the fine structures of a solar type-IIIb radio burst](http://adsabs.harvard.edu/abs/2020A&A...639A.115Z), 2020, Astronomy and Astrophysics, 639, A115

28. Magdalenić, Jasmina, Marqué, Christophe, Fallows, Richard A., et al.: [Fine Structure of a Solar Type II Radio Burst Observed by LOFAR](http://adsabs.harvard.edu/abs/2020ApJ...897L..15M), 2020, The Astrophysical Journal, 897, L15

29. O'Sullivan, S. P., Brüggen, M., Vazza, F., et al.: [New constraints on the magnetization of the cosmic web using LOFAR Faraday rotation observations](http://adsabs.harvard.edu/abs/2020MNRAS.495.2607O), 2020, Monthly Notices of the Royal Astronomical Society, 495, 2607

30. Stein, Y., Dettmar, R. -J., Beck, R., et al.: [CHANG-ES. XXI. Transport processes and the X-shaped magnetic field of NGC 4217: off-center superbubble structure](http://adsabs.harvard.edu/abs/2020A&A...639A.111S), 2020, Astronomy and Astrophysics, 639, A111

31. Starling, R. L. C., Rowlinson, A., van der Horst, A. J., et al.: [LOFAR detectability of prompt low-frequency radio emission during gamma-ray burst X-ray flares](http://adsabs.harvard.edu/abs/2020MNRAS.494.5787S), 2020, Monthly Notices of the Royal Astronomical Society, 494, 5787

32. Jurlin, N., Morganti, R., Brienza, M., et al.: [The life cycle of radio galaxies in the LOFAR Lockman Hole field](http://adsabs.harvard.edu/abs/2020A&A...638A..34J), 2020, Astronomy and Astrophysics, 638, A34

33. Brienza, M., Morganti, R., Harwood, J., et al.: [Radio spectral properties and jet duty cycle in the restarted radio galaxy 3C388](http://adsabs.harvard.edu/abs/2020A&A...638A..29B), 2020, Astronomy and Astrophysics, 638, A29

34. Broderick, J. W., Shimwell, T. W., Gourdji, K., et al.: [LOFAR 144-MHz follow-up observations of GW170817](http://adsabs.harvard.edu/abs/2020MNRAS.494.5110B), 2020, Monthly Notices of the Royal Astronomical Society, 494, 5110

35. Chawla, P., Andersen, B. C., Bhardwaj, M., et al.: [Detection of Repeating FRB 180916.J0158+65 Down to Frequencies of 300 MHz](http://adsabs.harvard.edu/abs/2020ApJ...896L..41C), 2020, The Astrophysical Journal, 896, L41

36. Stuardi, C., O'Sullivan, S. P., Bonafede, A., et al.: [The LOFAR view of intergalactic magnetic fields with giant radio galaxies](http://adsabs.harvard.edu/abs/2020A&A...638A..48S), 2020, Astronomy and Astrophysics, 638, A48

37. Cantwell, T. M., Bray, J. D., Croston, J. H., et al.: [Low-frequency observations of the giant radio galaxy NGC 6251](http://adsabs.harvard.edu/abs/2020MNRAS.495..143C), 2020, Monthly Notices of the Royal Astronomical Society, 495, 143

38. Polzin, E. J., Breton, R. P., Bhattacharyya, B., et al.: [Study of spider pulsar binary eclipses and discovery of an eclipse mechanism transition](http://adsabs.harvard.edu/abs/2020MNRAS.494.2948P), 2020, Monthly Notices of the Royal Astronomical Society, 494, 2948

39. Bonnassieux, Etienne, Edge, Alastair, Morabito, Leah, et al.: [Decoherence in LOFAR-VLBI beamforming](http://adsabs.harvard.edu/abs/2020A&A...637A..51B), 2020, Astronomy and Astrophysics, 637, A51

40. Rosario, D. J., Fawcett, V. A., Klindt, L., et al.: [Fundamental differences in the radio properties of red and blue quasars: insight from the LOFAR Two-metre Sky Survey (LoTSS)](http://adsabs.harvard.edu/abs/2020MNRAS.494.3061R), 2020, Monthly Notices of the Royal Astronomical Society, 494, 3061

41. Chrysaphi, Nicolina, Reid, Hamish A. S., Kontar, Eduard P.: [First Observation of a Type II Solar Radio Burst Transitioning between a Stationary and Drifting State](http://adsabs.harvard.edu/abs/2020ApJ...893..115C), 2020, The Astrophysical Journal, 893, 115

42. Retana-Montenegro, E., Röttgering, H. J. A.: [The optical luminosity function of LOFAR radio-selected quasars at 1.4 ≤ z ≤ 5.0 in the NDWFS-Boötes field](http://adsabs.harvard.edu/abs/2020A&A...636A..12R), 2020, Astronomy and Astrophysics, 636, A12

43. Nikolajevs, A., Prūsis, K.: [The LOFAR Long-Baseline Calibrator Survey Classification](http://adsabs.harvard.edu/abs/2020LatJP..57a..34N), 2020, Latvian Journal of Physics and Technical Sciences, 57, 34

44. Rajpurohit, K., Hoeft, M., Vazza, F., et al.: [New mysteries and challenges from the Toothbrush relic: wideband observations from 550 MHz to 8 GHz](http://adsabs.harvard.edu/abs/2020A&A...636A..30R), 2020, Astronomy and Astrophysics, 636, A30

45. Lam, M. T., Lazio, T. J. W., Dolch, T., et al.: [On Frequency-dependent Dispersion Measures and Extreme Scattering Events](http://adsabs.harvard.edu/abs/2020ApJ...892...89L), 2020, The Astrophysical Journal, 892, 89

46. de Gasperin, F., Vink, J., McKean, J. P., et al.: [Cassiopeia A, Cygnus A, Taurus A, and Virgo A at ultra-low radio frequencies](http://adsabs.harvard.edu/abs/2020A&A...635A.150D), 2020, Astronomy and Astrophysics, 635, A150

47. Bilous, A. V., Bondonneau, L., Kondratiev, V. I., et al.: [A LOFAR census of non-recycled pulsars: extending to frequencies below 80 MHz](http://adsabs.harvard.edu/abs/2020A&A...635A..75B), 2020, Astronomy and Astrophysics, 635, A75

48. Bondonneau, L., Grießmeier, J. -M., Theureau, G., et al.: [A census of the pulsar population observed with the international LOFAR station FR606 at low frequencies (25-80 MHz)](http://adsabs.harvard.edu/abs/2020A&A...635A..76B), 2020, Astronomy and Astrophysics, 635, A76

49. Dabhade, P., Röttgering, H. J. A., Bagchi, J., et al.: [Giant radio galaxies in the LOFAR Two-metre Sky Survey. I. Radio and environmental properties](http://adsabs.harvard.edu/abs/2020A&A...635A...5D), 2020, Astronomy and Astrophysics, 635, A5

50. Zhang, PeiJin, Zucca, Pietro, Wang, ChuanBing, et al.: [The Frequency Drift and Fine Structures of Solar S-bursts in the High Frequency Band of LOFAR](http://adsabs.harvard.edu/abs/2020ApJ...891...89Z), 2020, The Astrophysical Journal, 891, 89

51. Hare, B. M., Scholten, O., Dwyer, J., et al.: [Radio Emission Reveals Inner Meter-Scale Structure of Negative Lightning Leader Steps](http://adsabs.harvard.edu/abs/2020PhRvL.124j5101H), 2020, Physical Review Letters, 124, 105101

52. Tan, C. M., Bassa, C. G., Cooper, S., et al.: [The LOFAR Tied-Array all-sky survey: Timing of 21 pulsars including the first binary pulsar discovered with LOFAR](http://adsabs.harvard.edu/abs/2020MNRAS.492.5878T), 2020, Monthly Notices of the Royal Astronomical Society, 492, 5878

53. Salas, P., Brentjens, M. A., Bordenave, D. D., et al.: [Tied-array holography with LOFAR](http://adsabs.harvard.edu/abs/2020A&A...635A.207S), 2020, Astronomy and Astrophysics, 635, A207

54. Sridhar, Sarrvesh S., Morganti, Raffaella, Nyland, Kristina, et al.: [LOFAR view of NGC 3998, a sputtering AGN](http://adsabs.harvard.edu/abs/2020A&A...634A.108S), 2020, Astronomy and Astrophysics, 634, A108

55. Mandal, S., Intema, H. T., van Weeren, R. J., et al.: [Revived fossil plasma sources in galaxy clusters](http://adsabs.harvard.edu/abs/2020A&A...634A...4M), 2020, Astronomy and Astrophysics, 634, A4

56. Vedantham, H. K., Callingham, J. R., Shimwell, T. W., et al.: [Coherent radio emission from a quiescent red dwarf indicative of star-planet interaction](http://adsabs.harvard.edu/abs/2020NatAs...4..577V), 2020, Nature Astronomy, 4, 577

57. Emig, K. L., Salas, P., de Gasperin, F., et al.: [Searching for the largest bound atoms in space](http://adsabs.harvard.edu/abs/2020A&A...634A.138E), 2020, Astronomy and Astrophysics, 634, A138

58. van Leeuwen, Joeri, Mikhailov, Klim, Keane, Evan, et al.: [LOFAR radio search for single and periodic pulses from M 31](http://adsabs.harvard.edu/abs/2020A&A...634A...3V), 2020, Astronomy and Astrophysics, 634, A3

59. Fallows, Richard A., Forte, Biagio, Astin, Ivan, et al.: [A LOFAR observation of ionospheric scintillation from two simultaneous travelling ionospheric disturbances](http://adsabs.harvard.edu/abs/2020JSWSC..10...10F), 2020, Journal of Space Weather and Space Climate, 10, 10

60. Ghara, R., Giri, S. K., Mellema, G., et al.: [Constraining the intergalactic medium at z ≈ 9.1 using LOFAR Epoch of Reionization observations](http://adsabs.harvard.edu/abs/2020MNRAS.493.4728G), 2020, Monthly Notices of the Royal Astronomical Society, 493, 4728

61. Xiao, Jiangping, Li, Xiangru, Lin, Haitao, et al.: [Pulsar candidate selection using pseudo-nearest centroid neighbour classifier](http://adsabs.harvard.edu/abs/2020MNRAS.492.2119X), 2020, Monthly Notices of the Royal Astronomical Society, 492, 2119

62. Michilli, D., Bassa, C., Cooper, S., et al.: [The LOFAR tied-array all-sky survey (LOTAAS): Characterization of 20 pulsar discoveries and their single-pulse behaviour](http://adsabs.harvard.edu/abs/2020MNRAS.491..725M), 2020, Monthly Notices of the Royal Astronomical Society, 491, 725

63. Quiroga-Nuñez, L. H., Intema, H. T., Callingham, J. R., et al.: [Differences in radio emission from similar M dwarfs in the binary system Ross 867-8](http://adsabs.harvard.edu/abs/2020A&A...633A.130Q), 2020, Astronomy and Astrophysics, 633, A130

64. Maguire, Ciara A., Carley, Eoin P., McCauley, Joseph, et al.: [Evolution of the Alfvén Mach number associated with a coronal mass ejection shock](http://adsabs.harvard.edu/abs/2020A&A...633A..56M), 2020, Astronomy and Astrophysics, 633, A56

65. Broekema, P. Chris, Allan, Verity, van Nieuwpoort, Rob V., et al.: [On optimising cost and value in compute systems for radio astronomy](http://adsabs.harvard.edu/abs/2020A&C....3000337B), 2020, Astronomy and Computing, 30, 100337

66. Paul, Surajit, Salunkhe, Sameer, Sonkamble, Satish, et al.: [Radio relic and the diffuse emission trail discovered in low-mass galaxy cluster Abell 1697](http://adsabs.harvard.edu/abs/2020A&A...633A..59P), 2020, Astronomy and Astrophysics, 633, A59


2019
----

1. Arias, Maria, Vink, Jacco, Zhou, Ping, et al.: [Low-frequency Radio Absorption in Tycho’s Supernova Remnant](http://adsabs.harvard.edu/abs/2019AJ....158..253A), 2019, The Astronomical Journal, 158, 253

2. Rowlinson, A., Gourdji, K., van der Meulen, K., et al.: [LOFAR early-time search for coherent radio emission from GRB 180706A](http://adsabs.harvard.edu/abs/2019MNRAS.490.3483R), 2019, Monthly Notices of the Royal Astronomical Society, 490, 3483

3. Stein, Y., Dettmar, R. -J., Weżgowiec, M., et al.: [CHANG-ES. XIX. Galaxy NGC 4013: a diffusion-dominated radio halo with plane-parallel disk and vertical halo magnetic fields](http://adsabs.harvard.edu/abs/2019A&A...632A..13S), 2019, Astronomy and Astrophysics, 632, A13

4. Polzin, E. J., Breton, R. P., Stappers, B. W., et al.: [Long-term variability of a black widow's eclipses - A decade of PSR J2051-0827](http://adsabs.harvard.edu/abs/2019MNRAS.490..889P), 2019, Monthly Notices of the Royal Astronomical Society, 490, 889

5. Feeney-Johansson, Anton, Purser, Simon J. D., Ray, Tom P., et al.: [The First Detection of a Low-frequency Turnover in Nonthermal Emission from the Jet of a Young Star](http://adsabs.harvard.edu/abs/2019ApJ...885L...7F), 2019, The Astrophysical Journal, 885, L7

6. Zhang, PeiJin, Yu, SiJie, Kontar, Eduard P., et al.: [On the Source Position and Duration of a Solar Type III Radio Burst Observed by LOFAR](http://adsabs.harvard.edu/abs/2019ApJ...885..140Z), 2019, The Astrophysical Journal, 885, 140

7. Wang, L., Gao, F., Duncan, K. J., et al.: [A LOFAR-IRAS cross-match study: the far-infrared radio correlation and the 150 MHz luminosity as a star-formation rate tracer](http://adsabs.harvard.edu/abs/2019A&A...631A.109W), 2019, Astronomy and Astrophysics, 631, A109

8. Kappes, A., Perucho, M., Kadler, M., et al.: [LOFAR measures the hotspot advance speed of the high-redshift blazar S5 0836+710](http://adsabs.harvard.edu/abs/2019A&A...631A..49K), 2019, Astronomy and Astrophysics, 631, A49

9. Kuznetsov, A. A., Kontar, E. P.: [First imaging spectroscopy observations of solar drift pair bursts](http://adsabs.harvard.edu/abs/2019A&A...631L...7K), 2019, Astronomy and Astrophysics, 631, L7

10. Offringa, A. R., Mertens, F., van der Tol, S., et al.: [Precision requirements for interferometric gridding in the analysis of a 21 cm power spectrum](http://adsabs.harvard.edu/abs/2019A&A...631A..12O), 2019, Astronomy and Astrophysics, 631, A12

11. Botteon, A., Cassano, R., Eckert, D., et al.: [Particle acceleration in a nearby galaxy cluster pair: the role of cluster dynamics](http://adsabs.harvard.edu/abs/2019A&A...630A..77B), 2019, Astronomy and Astrophysics, 630, A77

12. Gehlot, B. K., Mertens, F. G., Koopmans, L. V. E., et al.: [The first power spectrum limit on the 21-cm signal of neutral hydrogen during the Cosmic Dawn at z = 20-25 from LOFAR](http://adsabs.harvard.edu/abs/2019MNRAS.488.4271G), 2019, Monthly Notices of the Royal Astronomical Society, 488, 4271

13. Mulrey, K., Bonardi, A., Buitink, S., et al.: [Calibration of the LOFAR low-band antennas using the Galaxy and a model of the signal chain](http://adsabs.harvard.edu/abs/2019APh...111....1M), 2019, Astroparticle Physics, 111, 1

14. Hardcastle, M. J., Croston, J. H., Shimwell, T. W., et al.: [NGC 326: X-shaped no more](http://adsabs.harvard.edu/abs/2019MNRAS.488.3416H), 2019, Monthly Notices of the Royal Astronomical Society, 488, 3416

15. Mingo, B., Croston, J. H., Hardcastle, M. J., et al.: [Revisiting the Fanaroff-Riley dichotomy and radio-galaxy morphology with the LOFAR Two-Metre Sky Survey (LoTSS)](http://adsabs.harvard.edu/abs/2019MNRAS.488.2701M), 2019, Monthly Notices of the Royal Astronomical Society, 488, 2701

16. Nieder, L., Clark, C. J., Bassa, C. G., et al.: [Detection and Timing of Gamma-Ray Pulsations from the 707 Hz Pulsar J0952-0607](http://adsabs.harvard.edu/abs/2019ApJ...883...42N), 2019, The Astrophysical Journal, 883, 42

17. Urdampilleta, I., Mernier, F., Kaastra, J. S., et al.: [Iron abundance distribution in the hot gas of merging galaxy clusters](http://adsabs.harvard.edu/abs/2019A&A...629A..31U), 2019, Astronomy and Astrophysics, 629, A31

18. Hörandel, Jörg R.: [Radio detection of extensive air showers - Measuring the properties of cosmic rays with the radio technique at LOFAR and the Pierre Auger Observatory](http://adsabs.harvard.edu/abs/2019NPPP..306..108H), 2019, Nuclear and Particle Physics Proceedings, 306-308, 108

19. Shulevski, A., Barthel, P. D., Morganti, R., et al.: [First look at the giant radio galaxy <ASTROBJ>3C 236</ASTROBJ> with LOFAR](http://adsabs.harvard.edu/abs/2019A&A...628A..69S), 2019, Astronomy and Astrophysics, 628, A69

20. Heesen, V., Whitler, L., Schmidt, P., et al.: [Warped diffusive radio halo around the quiescent spiral edge-on galaxy NGC 4565](http://adsabs.harvard.edu/abs/2019A&A...628L...3H), 2019, Astronomy and Astrophysics, 628, L3

21. Lukic, V., Brüggen, M., Mingo, B., et al.: [Morphological classification of radio galaxies: capsule networks versus convolutional neural networks](http://adsabs.harvard.edu/abs/2019MNRAS.487.1729L), 2019, Monthly Notices of the Royal Astronomical Society, 487, 1729

22. Bîrzan, L., Rafferty, D. A., Cassano, R., et al.: [A massive cluster at z = 0.288 caught in the process of formation: The case of Abell 959](http://adsabs.harvard.edu/abs/2019MNRAS.487.4775B), 2019, Monthly Notices of the Royal Astronomical Society, 487, 4775

23. Cassano, R., Botteon, A., Di Gennaro, G., et al.: [LOFAR Discovery of a Radio Halo in the High-redshift Galaxy Cluster PSZ2 G099.86+58.45](http://adsabs.harvard.edu/abs/2019ApJ...881L..18C), 2019, The Astrophysical Journal, 881, L18

24. Tiburzi, C., Verbiest, J. P. W., Shaifullah, G. M., et al.: [On the usefulness of existing solar wind models for pulsar timing corrections](http://adsabs.harvard.edu/abs/2019MNRAS.487..394T), 2019, Monthly Notices of the Royal Astronomical Society, 487, 394

25. Clarke, A. O., Scaife, A. M. M., Shimwell, T., et al.: [Signatures from a merging galaxy cluster and its AGN population: LOFAR observations of Abell 1682](http://adsabs.harvard.edu/abs/2019A&A...627A.176C), 2019, Astronomy and Astrophysics, 627, A176

26. Krause, Martin G. H., Hardcastle, Martin J., Shabala, Stanislav S.: [Probing gaseous halos of galaxies with radio jets](http://adsabs.harvard.edu/abs/2019A&A...627A.113K), 2019, Astronomy and Astrophysics, 627, A113

27. Creaner, O., Carozzi, T. D.: [beamModelTester: Software framework for testing radio telescope beams](http://adsabs.harvard.edu/abs/2019A&C....2800311C), 2019, Astronomy and Computing, 28, 100311

28. Mechev, A. P., Shimwell, T. W., Plaat, A., et al.: [Scalability model for the LOFAR direction independent pipeline](http://adsabs.harvard.edu/abs/2019A&C....2800293M), 2019, Astronomy and Computing, 28, 100293

29. Krishnakumar, M. A., Maan, Yogesh, Joshi, B. C., et al.: [Multi-frequency Scatter-broadening Evolution of Pulsars. II. Scatter-broadening of Nearby Pulsars](http://adsabs.harvard.edu/abs/2019ApJ...878..130K), 2019, The Astrophysical Journal, 878, 130

30. Sanidas, S., Cooper, S., Bassa, C. G., et al.: [The LOFAR Tied-Array All-Sky Survey (LOTAAS): Survey overview and initial pulsar discoveries](http://adsabs.harvard.edu/abs/2019A&A...626A.104S), 2019, Astronomy and Astrophysics, 626, A104

31. Gu, Liyi, Akamatsu, Hiroki, Shimwell, Timothy W., et al.: [Observations of a pre-merger shock in colliding clusters of galaxies](http://adsabs.harvard.edu/abs/2019NatAs...3..838G), 2019, Nature Astronomy, 3, 838

32. Salas, P., Oonk, J. B. R., Emig, K. L., et al.: [Carbon radio recombination lines from gigahertz to megahertz frequencies towards Orion A](http://adsabs.harvard.edu/abs/2019A&A...626A..70S), 2019, Astronomy and Astrophysics, 626, A70

33. Govoni, F., Orrù, E., Bonafede, A., et al.: [A radio ridge connecting two galaxy clusters in a filament of the cosmic web](http://adsabs.harvard.edu/abs/2019Sci...364..981G), 2019, Science, 364, 981

34. Thwala, S. A., Shafi, N., Colafrancesco, S., et al.: [The study of extended emission in a radio galaxy detected in the LOFAR Two-Metre Sky Survey](http://adsabs.harvard.edu/abs/2019MNRAS.485.1938T), 2019, Monthly Notices of the Royal Astronomical Society, 485, 1938

35. Rowlinson, A., Stewart, A. J., Broderick, J. W., et al.: [Identifying transient and variable sources in radio images](http://adsabs.harvard.edu/abs/2019A&C....27..111R), 2019, Astronomy and Computing, 27, 111

36. Sobey, C., Bilous, A. V., Grießmeier, J. -M., et al.: [Low-frequency Faraday rotation measures towards pulsars using LOFAR: probing the 3D Galactic halo magnetic field](http://adsabs.harvard.edu/abs/2019MNRAS.484.3646S), 2019, Monthly Notices of the Royal Astronomical Society, 484, 3646

37. Hare, B. M., Scholten, O., Dwyer, J., et al.: [Needle-like structures discovered on positively charged lightning branches](http://adsabs.harvard.edu/abs/2019Natur.568..360H), 2019, Nature, 568, 360

38. Turner, Jake D., Grießmeier, Jean-Mathias, Zarka, Philippe, et al.: [The search for radio emission from exoplanets using LOFAR beam-formed observations: Jupiter as an exoplanet](http://adsabs.harvard.edu/abs/2019A&A...624A..40T), 2019, Astronomy and Astrophysics, 624, A40

39. Donner, J. Y., Verbiest, J. P. W., Tiburzi, C., et al.: [First detection of frequency-dependent, time-variable dispersion measures](http://adsabs.harvard.edu/abs/2019A&A...624A..22D), 2019, Astronomy and Astrophysics, 624, A22

40. Houben, L. J. M., Spitler, L. G., ter Veen, S., et al.: [Constraints on the low frequency spectrum of FRB 121102](http://adsabs.harvard.edu/abs/2019A&A...623A..42H), 2019, Astronomy and Astrophysics, 623, A42

41. Porayko, N. K., Noutsos, A., Tiburzi, C., et al.: [Testing the accuracy of the ionospheric Faraday rotation corrections through LOFAR observations of bright northern pulsars](http://adsabs.harvard.edu/abs/2019MNRAS.483.4100P), 2019, Monthly Notices of the Royal Astronomical Society, 483, 4100

42. Van Eck, C. L., Haverkorn, M., Alves, M. I. R., et al.: [Diffuse polarized emission in the LOFAR Two-meter Sky Survey](http://adsabs.harvard.edu/abs/2019A&A...623A..71V), 2019, Astronomy and Astrophysics, 623, A71

43. Straal, S. M., van Leeuwen, J.: [A LOFAR search for steep-spectrum pulsars in supernova remnants and pulsar wind nebulae](http://adsabs.harvard.edu/abs/2019A&A...623A..90S), 2019, Astronomy and Astrophysics, 623, A90

44. Harris, D. E., Moldón, J., Oonk, J. R. R., et al.: [LOFAR Observations of 4C+19.44: On the Discovery of Low-frequency Spectral Curvature in Relativistic Jet Knots](http://adsabs.harvard.edu/abs/2019ApJ...873...21H), 2019, The Astrophysical Journal, 873, 21

45. Wei, Liying, Wijnholds, Stefan J.: [Joint calibration and imaging for phased array radio telescopes](http://adsabs.harvard.edu/abs/2019MNRAS.483.5672W), 2019, Monthly Notices of the Royal Astronomical Society, 483, 5672

46. Gordovskyy, Mykola, Kontar, Eduard, Browning, Philippa, et al.: [Frequency-Distance Structure of Solar Radio Sources Observed by LOFAR](http://adsabs.harvard.edu/abs/2019ApJ...873...48G), 2019, The Astrophysical Journal, 873, 48

47. Heesen, V., Buie, E., II, Huff, C. J., et al.: [Calibrating the relation of low-frequency radio continuum to star formation rate at 1 kpc scale with LOFAR](http://adsabs.harvard.edu/abs/2019A&A...622A...8H), 2019, Astronomy and Astrophysics, 622, A8

48. Nikiel-Wroczyński, B., Berger, A., Herrera Ruiz, N., et al.: [Exploring the properties of low-frequency radio emission and magnetic fields in a sample of compact galaxy groups using the LOFAR Two-Metre Sky Survey (LoTSS)](http://adsabs.harvard.edu/abs/2019A&A...622A..23N), 2019, Astronomy and Astrophysics, 622, A23

49. de Gasperin, F., Dijkema, T. J., Drabent, A., et al.: [Systematic effects in LOFAR data: A unified calibration strategy](http://adsabs.harvard.edu/abs/2019A&A...622A...5D), 2019, Astronomy and Astrophysics, 622, A5

50. Arias, M., Vink, J., Iacobelli, M., et al.: [A low-frequency view of mixed-morphology supernova remnant VRO 42.05.01, and its neighbourhood](http://adsabs.harvard.edu/abs/2019A&A...622A...6A), 2019, Astronomy and Astrophysics, 622, A6

51. Wilber, A., Brüggen, M., Bonafede, A., et al.: [Evolutionary phases of merging clusters as seen by LOFAR](http://adsabs.harvard.edu/abs/2019A&A...622A..25W), 2019, Astronomy and Astrophysics, 622, A25

52. Mandal, S., Intema, H. T., Shimwell, T. W., et al.: [Ultra-steep spectrum emission in the merging galaxy cluster Abell 1914](http://adsabs.harvard.edu/abs/2019A&A...622A..22M), 2019, Astronomy and Astrophysics, 622, A22

53. Savini, F., Bonafede, A., Brüggen, M., et al.: [A LOFAR study of non-merging massive galaxy clusters](http://adsabs.harvard.edu/abs/2019A&A...622A..24S), 2019, Astronomy and Astrophysics, 622, A24

54. Mahatma, V. H., Hardcastle, M. J., Williams, W. L., et al.: [LoTSS DR1: Double-double radio galaxies in the HETDEX field](http://adsabs.harvard.edu/abs/2019A&A...622A..13M), 2019, Astronomy and Astrophysics, 622, A13

55. Hoang, D. N., Shimwell, T. W., van Weeren, R. J., et al.: [Characterizing the radio emission from the binary galaxy cluster merger Abell 2146](http://adsabs.harvard.edu/abs/2019A&A...622A..21H), 2019, Astronomy and Astrophysics, 622, A21

56. Clarke, Brendan P., Morosan, Diana E., Gallagher, Peter T., et al.: [Properties and magnetic origins of solar S-bursts](http://adsabs.harvard.edu/abs/2019A&A...622A.204C), 2019, Astronomy and Astrophysics, 622, A204

57. Stacey, H. R., McKean, J. P., Jackson, N. J., et al.: [LoTSS/HETDEX: Disentangling star formation and AGN activity in gravitationally lensed radio-quiet quasars](http://adsabs.harvard.edu/abs/2019A&A...622A..18S), 2019, Astronomy and Astrophysics, 622, A18

58. Morosan, Diana E., Carley, Eoin P., Hayes, Laura A., et al.: [Multiple regions of shock-accelerated particles during a solar coronal mass ejection](http://adsabs.harvard.edu/abs/2019NatAs...3..452M), 2019, Nature Astronomy, 3, 452

59. Botteon, A., Shimwell, T. W., Bonafede, A., et al.: [The spectacular cluster chain Abell 781 as observed with LOFAR, GMRT, and XMM-Newton](http://adsabs.harvard.edu/abs/2019A&A...622A..19B), 2019, Astronomy and Astrophysics, 622, A19

60. Hale, C. L., Williams, W., Jarvis, M. J., et al.: [LOFAR observations of the XMM-LSS field](http://adsabs.harvard.edu/abs/2019A&A...622A...4H), 2019, Astronomy and Astrophysics, 622, A4

61. Hoang, D. N., Shimwell, T. W., van Weeren, R. J., et al.: [Radio observations of the merging galaxy cluster Abell 520](http://adsabs.harvard.edu/abs/2019A&A...622A..20H), 2019, Astronomy and Astrophysics, 622, A20

62. Shimwell, T. W., Tasse, C., Hardcastle, M. J., et al.: [The LOFAR Two-metre Sky Survey. II. First data release](http://adsabs.harvard.edu/abs/2019A&A...622A...1S), 2019, Astronomy and Astrophysics, 622, A1

63. Miskolczi, A., Heesen, V., Horellou, C., et al.: [CHANG-ES XII. A LOFAR and VLA view of the edge-on star-forming galaxy NGC 3556](http://adsabs.harvard.edu/abs/2019A&A...622A...9M), 2019, Astronomy and Astrophysics, 622, A9

64. Emig, K. L., Salas, P., de Gasperin, F., et al.: [The first detection of radio recombination lines at cosmological distances](http://adsabs.harvard.edu/abs/2019A&A...622A...7E), 2019, Astronomy and Astrophysics, 622, A7

65. Duncan, K. J., Sabater, J., Röttgering, H. J. A., et al.: [The LOFAR Two-metre Sky Survey. IV. First Data Release: Photometric redshifts and rest-frame magnitudes](http://adsabs.harvard.edu/abs/2019A&A...622A...3D), 2019, Astronomy and Astrophysics, 622, A3

66. Mooney, S., Quinn, J., Callingham, J. R., et al.: [Blazars in the LOFAR Two-Metre Sky Survey first data release](http://adsabs.harvard.edu/abs/2019A&A...622A..14M), 2019, Astronomy and Astrophysics, 622, A14

67. Hardcastle, M. J., Williams, W. L., Best, P. N., et al.: [Radio-loud AGN in the first LoTSS data release. The lifetimes and environmental impact of jet-driven sources](http://adsabs.harvard.edu/abs/2019A&A...622A..12H), 2019, Astronomy and Astrophysics, 622, A12

68. Morabito, L. K., Matthews, J. H., Best, P. N., et al.: [The origin of radio emission in broad absorption line quasars: Results from the LOFAR Two-metre Sky Survey](http://adsabs.harvard.edu/abs/2019A&A...622A..15M), 2019, Astronomy and Astrophysics, 622, A15

69. Croston, J. H., Hardcastle, M. J., Mingo, B., et al.: [The environments of radio-loud AGN from the LOFAR Two-Metre Sky Survey (LoTSS)](http://adsabs.harvard.edu/abs/2019A&A...622A..10C), 2019, Astronomy and Astrophysics, 622, A10

70. Williams, W. L., Hardcastle, M. J., Best, P. N., et al.: [The LOFAR Two-metre Sky Survey. III. First data release: Optical/infrared identifications and value-added catalogue](http://adsabs.harvard.edu/abs/2019A&A...622A...2W), 2019, Astronomy and Astrophysics, 622, A2

71. Sabater, J., Best, P. N., Hardcastle, M. J., et al.: [The LoTSS view of radio AGN in the local Universe. The most massive galaxies are always switched on](http://adsabs.harvard.edu/abs/2019A&A...622A..17S), 2019, Astronomy and Astrophysics, 622, A17

72. Gürkan, Gülay, Hardcastle, M. J., Best, P. N., et al.: [LoTSS/HETDEX: Optical quasars. I. Low-frequency radio properties of optically selected quasars](http://adsabs.harvard.edu/abs/2019A&A...622A..11G), 2019, Astronomy and Astrophysics, 622, A11

73. O'Sullivan, S. P., Machalski, J., Van Eck, C. L., et al.: [The intergalactic magnetic field probed by a giant radio galaxy](http://adsabs.harvard.edu/abs/2019A&A...622A..16O), 2019, Astronomy and Astrophysics, 622, A16

74. Kuiack, Mark, Huizinga, Folkert, Molenaar, Gijs, et al.: [AARTFAAC flux density calibration and Northern hemisphere catalogue at 60 MHz](http://adsabs.harvard.edu/abs/2019MNRAS.482.2502K), 2019, Monthly Notices of the Royal Astronomical Society, 482, 2502

75. ter Veen, S., Enriquez, J. E., Falcke, H., et al.: [The FRATS project: real-time searches for fast radio bursts and other fast transients with LOFAR at 135 MHz](http://adsabs.harvard.edu/abs/2019A&A...621A..57T), 2019, Astronomy and Astrophysics, 621, A57


2018
----

1. Retana-Montenegro, E., Röttgering, H. J. A., Shimwell, T. W., et al.: [Deep LOFAR 150 MHz imaging of the Boötes field: Unveiling the faint low-frequency sky](http://adsabs.harvard.edu/abs/2018A&A...620A..74R), 2018, Astronomy and Astrophysics, 620, A74

2. Chrysaphi, Nicolina, Kontar, Eduard P., Holman, Gordon D., et al.: [CME-driven Shock and Type II Solar Radio Burst Band Splitting](http://adsabs.harvard.edu/abs/2018ApJ...868...79C), 2018, The Astrophysical Journal, 868, 79

3. Foster, Griffin, Karastergiou, Aris, Geyer, Marisa, et al.: [Verifying and reporting Fast Radio Bursts](http://adsabs.harvard.edu/abs/2018MNRAS.481.2612F), 2018, Monthly Notices of the Royal Astronomical Society, 481, 2612

4. Hermsen, W., Kuiper, L., Basu, R., et al.: [Discovery of synchronous X-ray and radio moding of PSR B0823+26](http://adsabs.harvard.edu/abs/2018MNRAS.480.3655H), 2018, Monthly Notices of the Royal Astronomical Society, 480, 3655

5. Chyży, K. T., Jurusik, W., Piotrowska, J., et al.: [LOFAR MSSS: Flattening low-frequency radio continuum spectra of nearby galaxies](http://adsabs.harvard.edu/abs/2018A&A...619A..36C), 2018, Astronomy and Astrophysics, 619, A36

6. Michilli, D., Hessels, J. W. T., Lyon, R. J., et al.: [Single-pulse classifier for the LOFAR Tied-Array All-sky Survey](http://adsabs.harvard.edu/abs/2018MNRAS.480.3457M), 2018, Monthly Notices of the Royal Astronomical Society, 480, 3457

7. Read, S. C., Smith, D. J. B., Gürkan, G., et al.: [The Far-Infrared Radio Correlation at low radio frequency with LOFAR/H-ATLAS](http://adsabs.harvard.edu/abs/2018MNRAS.480.5625R), 2018, Monthly Notices of the Royal Astronomical Society, 480, 5625

8. O'Sullivan, Shane, Brüggen, Marcus, Van Eck, Cameron, et al.: [Untangling Cosmic Magnetic Fields: Faraday Tomography at Metre Wavelengths with LOFAR](http://adsabs.harvard.edu/abs/2018Galax...6..126O), 2018, Galaxies, 6, 126

9. Farnes, Jamie, Mort, Ben, Dulwich, Fred, et al.: [Science Pipelines for the Square Kilometre Array](http://adsabs.harvard.edu/abs/2018Galax...6..120F), 2018, Galaxies, 6, 120

10. Brienza, M., Morganti, R., Murgia, M., et al.: [Duty cycle of the radio galaxy B2 0258+35](http://adsabs.harvard.edu/abs/2018A&A...618A..45B), 2018, Astronomy and Astrophysics, 618, A45

11. Neld, A., Horellou, C., Mulcahy, D. D., et al.: [Reliable detection and characterization of low-frequency polarized sources in the LOFAR M51 field](http://adsabs.harvard.edu/abs/2018A&A...617A.136N), 2018, Astronomy and Astrophysics, 617, A136

12. Tan, C. M., Bassa, C. G., Cooper, S., et al.: [LOFAR Discovery of a 23.5 s Radio Pulsar](http://adsabs.harvard.edu/abs/2018ApJ...866...54T), 2018, The Astrophysical Journal, 866, 54

13. Dąbrowski, B. P., Morosan, D. E., Fallows, R. A., et al.: [Observations of the Sun using LOFAR Bałdy station](http://adsabs.harvard.edu/abs/2018AdSpR..62.1895D), 2018, Advances in Space Research, 62, 1895

14. Błaszkiewicz, L. P., Lewandowski, W., Krankowski, A., et al.: [PL612 LOFAR station sensitivity measurements in the context of its application for pulsar observations](http://adsabs.harvard.edu/abs/2018AdSpR..62.1904B), 2018, Advances in Space Research, 62, 1904

15. Sridhar, S. S., Heald, G., van der Hulst, J. M.: [cuFFS: A GPU-accelerated code for Fast Faraday rotation measure Synthesis](http://adsabs.harvard.edu/abs/2018A&C....25..205S), 2018, Astronomy and Computing, 25, 205

16. Van Eck, Cameron: [The Power of Low Frequencies: Faraday Tomography in the Sub-GHz Regime](http://adsabs.harvard.edu/abs/2018Galax...6..112V), 2018, Galaxies, 6, 112

17. Naghibzadeh, Shahrzad, van der Veen, Alle-Jan: [PRIFIRA: General regularization using prior-conditioning for fast radio interferometric imaging](http://adsabs.harvard.edu/abs/2018MNRAS.479.5638N), 2018, Monthly Notices of the Royal Astronomical Society, 479, 5638

18. Di Gennaro, G., van Weeren, R. J., Hoeft, M., et al.: [Deep Very Large Array Observations of the Merging Cluster CIZA J2242.8+5301: Continuum and Spectral Imaging](http://adsabs.harvard.edu/abs/2018ApJ...865...24D), 2018, The Astrophysical Journal, 865, 24

19. Gehlot, B. K., Koopmans, L. V. E., de Bruyn, A. G., et al.: [Wide-field LOFAR-LBA power-spectra analyses: impact of calibration, polarization leakage, and ionosphere](http://adsabs.harvard.edu/abs/2018MNRAS.478.1484G), 2018, Monthly Notices of the Royal Astronomical Society, 478, 1484

20. Bilous, A. V.: [PSR B0943+10: low-frequency study of subpulse periodicity in the Bright mode with LOFAR](http://adsabs.harvard.edu/abs/2018A&A...616A.119B), 2018, Astronomy and Astrophysics, 616, A119

21. Savini, F., Bonafede, A., Brüggen, M., et al.: [First evidence of diffuse ultra-steep-spectrum radio emission surrounding the cool core of a cluster](http://adsabs.harvard.edu/abs/2018MNRAS.478.2234S), 2018, Monthly Notices of the Royal Astronomical Society, 478, 2234

22. de Gasperin, F., Mevius, M., Rafferty, D. A., et al.: [The effect of the ionosphere on ultra-low-frequency radio-interferometric observations](http://adsabs.harvard.edu/abs/2018A&A...615A.179D), 2018, Astronomy and Astrophysics, 615, A179

23. Sharykin, I. N., Kontar, E. P., Kuznetsov, A. A.: [LOFAR Observations of Fine Spectral Structure Dynamics in Type IIIb Radio Bursts](http://adsabs.harvard.edu/abs/2018SoPh..293..115S), 2018, Solar Physics, 293, 115

24. Bonafede, A., Brüggen, M., Rafferty, D., et al.: [LOFAR discoveryof radio emission in MACS J0717.5+3745](http://adsabs.harvard.edu/abs/2018MNRAS.478.2927B), 2018, Monthly Notices of the Royal Astronomical Society, 478, 2927

25. Hoang, D. N., Shimwell, T. W., van Weeren, R. J., et al.: [Radio observations of the double-relic galaxy cluster Abell 1240](http://adsabs.harvard.edu/abs/2018MNRAS.478.2218H), 2018, Monthly Notices of the Royal Astronomical Society, 478, 2218

26. Gunst, André W., Kruithof, Gert H.: [Antenna data storage concept for phased array radio astronomical instruments](http://adsabs.harvard.edu/abs/2018ExA....45..351G), 2018, Experimental Astronomy, 45, 351

27. Zucca, P., Morosan, D. E., Rouillard, A. P., et al.: [Shock location and CME 3D reconstruction of a solar type II radio burst with LOFAR](http://adsabs.harvard.edu/abs/2018A&A...615A..89Z), 2018, Astronomy and Astrophysics, 615, A89

28. Jelić, Vibor, Prelogović, David, Haverkorn, Marijke, et al.: [Magnetically aligned straight depolarization canals and the rolling Hough transform](http://adsabs.harvard.edu/abs/2018A&A...615L...3J), 2018, Astronomy and Astrophysics, 615, L3

29. Kolotkov, Dmitrii Y., Nakariakov, Valery M., Kontar, Eduard P.: [Origin of the Modulation of the Radio Emission from the Solar Corona by a Fast Magnetoacoustic Wave](http://adsabs.harvard.edu/abs/2018ApJ...861...33K), 2018, The Astrophysical Journal, 861, 33

30. König, S., Aalto, S., Muller, S., et al.: [Major impact from a minor merger. The extraordinary hot molecular gas flow in the Eye of the NGC 4194 Medusa galaxy](http://adsabs.harvard.edu/abs/2018A&A...615A.122K), 2018, Astronomy and Astrophysics, 615, A122

31. Botteon, A., Shimwell, T. W., Bonafede, A., et al.: [LOFAR discovery of a double radio halo system in Abell 1758 and radio/X-ray study of the cluster pair](http://adsabs.harvard.edu/abs/2018MNRAS.478..885B), 2018, Monthly Notices of the Royal Astronomical Society, 478, 885

32. Brüggen, M., Rafferty, D., Bonafede, A., et al.: [Discovery of large-scale diffuse radio emission in low-mass galaxy cluster Abell 1931](http://adsabs.harvard.edu/abs/2018MNRAS.477.3461B), 2018, Monthly Notices of the Royal Astronomical Society, 477, 3461

33. Mulcahy, D. D., Horneffer, A., Beck, R., et al.: [Investigation of the cosmic ray population and magnetic field strength in the halo of NGC 891](http://adsabs.harvard.edu/abs/2018A&A...615A..98M), 2018, Astronomy and Astrophysics, 615, A98

34. Verbiest, Joris P. W., Shaifullah, G. M.: [Measurement uncertainty in pulsar timing array experiments](http://adsabs.harvard.edu/abs/2018CQGra..35m3001V), 2018, Classical and Quantum Gravity, 35, 133001

35. Perera, B. B. P., Stappers, B. W., Babak, S., et al.: [Improving timing sensitivity in the microhertz frequency regime: limits from PSR J1713+0747 on gravitational waves produced by supermassive black hole binaries](http://adsabs.harvard.edu/abs/2018MNRAS.478..218P), 2018, Monthly Notices of the Royal Astronomical Society, 478, 218

36. Bonnassieux, Etienne, Tasse, Cyril, Smirnov, Oleg, et al.: [The variance of radio interferometric calibration solutions. Quality-based weighting schemes](http://adsabs.harvard.edu/abs/2018A&A...615A..66B), 2018, Astronomy and Astrophysics, 615, A66

37. Mechev, A. P., Plaat, A., Oonk, J. B. Raymond, et al.: [Pipeline Collector: Gathering performance data for distributed astronomical pipelines](http://adsabs.harvard.edu/abs/2018A&C....24..117M), 2018, Astronomy and Computing, 24, 117

38. Driessen, Laura N., Domček, Vladimír, Vink, Jacco, et al.: [Investigating Galactic Supernova Remnant Candidates Using LOFAR](http://adsabs.harvard.edu/abs/2018ApJ...860..133D), 2018, The Astrophysical Journal, 860, 133

39. Vocks, C., Mann, G., Breitling, F., et al.: [LOFAR observations of the quiet solar corona](http://adsabs.harvard.edu/abs/2018A&A...614A..54V), 2018, Astronomy and Astrophysics, 614, A54

40. Reid, Hamish A. S., Kontar, Eduard P.: [Solar type III radio burst time characteristics at LOFAR frequencies and the implications for electron beam transport](http://adsabs.harvard.edu/abs/2018A&A...614A..69R), 2018, Astronomy and Astrophysics, 614, A69

41. Van Eck, C. L., Haverkorn, M., Alves, M. I. R., et al.: [Polarized point sources in the LOFAR Two-meter Sky Survey: A preliminary catalog](http://adsabs.harvard.edu/abs/2018A&A...613A..58V), 2018, Astronomy and Astrophysics, 613, A58

42. Shaifullah, G., Tiburzi, C., Osłowski, S., et al.: [Multifrequency behaviour of the anomalous events of PSR J0922+0638](http://adsabs.harvard.edu/abs/2018MNRAS.477L..25S), 2018, Monthly Notices of the Royal Astronomical Society, 477, L25

43. Lynch, Ryan S., Swiggum, Joseph K., Kondratiev, Vlad I., et al.: [The Green Bank North Celestial Cap Pulsar Survey. III. 45 New Pulsar Timing Solutions](http://adsabs.harvard.edu/abs/2018ApJ...859...93L), 2018, The Astrophysical Journal, 859, 93

44. Białkowski, Sławomir, Lewandowski, Wojciech, Kijak, Jarosław, et al.: [Mode switching characteristics of PSR B0329+54 at 150 MHz](http://adsabs.harvard.edu/abs/2018Ap&SS.363..110B), 2018, Astrophysics and Space Science, 363, 110

45. Polzin, E. J., Breton, R. P., Clarke, A. O., et al.: [The low-frequency radio eclipses of the black widow pulsar J1810+1744](http://adsabs.harvard.edu/abs/2018MNRAS.476.1968P), 2018, Monthly Notices of the Royal Astronomical Society, 476, 1968

46. Heesen, V., Rafferty, D. A., Horneffer, A., et al.: [Exploring the making of a galactic wind in the starbursting dwarf irregular galaxy IC 10 with LOFAR](http://adsabs.harvard.edu/abs/2018MNRAS.476.1756H), 2018, Monthly Notices of the Royal Astronomical Society, 476, 1756

47. Michilli, D., Hessels, J. W. T., Donner, J. Y., et al.: [Low-frequency pulse profile variation in PSR B2217+47: evidence for echoes from the interstellar medium](http://adsabs.harvard.edu/abs/2018MNRAS.476.2704M), 2018, Monthly Notices of the Royal Astronomical Society, 476, 2704

48. Wilber, A., Brüggen, M., Bonafede, A., et al.: [Search for low-frequency diffuse radio emission around a shock in the massive galaxy cluster MACS J0744.9+3927](http://adsabs.harvard.edu/abs/2018MNRAS.476.3415W), 2018, Monthly Notices of the Royal Astronomical Society, 476, 3415

49. Asad, K. M. B., Koopmans, L. V. E., Jelić, V., et al.: [Polarization leakage in epoch of reionization windows - III. Wide-field effects of narrow-field arrays](http://adsabs.harvard.edu/abs/2018MNRAS.476.3051A), 2018, Monthly Notices of the Royal Astronomical Society, 476, 3051

50. Salas, P., Oonk, J. B. R., van Weeren, R. J., et al.: [Mapping low-frequency carbon radio recombination lines towards Cassiopeia A at 340, 148, 54, and 43 MHz](http://adsabs.harvard.edu/abs/2018MNRAS.475.2496S), 2018, Monthly Notices of the Royal Astronomical Society, 475, 2496

51. O'Gorman, E., Coughlan, C. P., Vlemmings, W., et al.: [A search for radio emission from exoplanets around evolved stars](http://adsabs.harvard.edu/abs/2018A&A...612A..52O), 2018, Astronomy and Astrophysics, 612, A52

52. Cendes, Y., Prasad, P., Rowlinson, A., et al.: [RFI flagging implications for short-duration transients](http://adsabs.harvard.edu/abs/2018A&C....23..103C), 2018, Astronomy and Computing, 23, 103

53. Arias, M., Vink, J., de Gasperin, F., et al.: [Low-frequency radio absorption in Cassiopeia A](http://adsabs.harvard.edu/abs/2018A&A...612A.110A), 2018, Astronomy and Astrophysics, 612, A110

54. Carbone, D., Garsden, H., Spreeuw, H., et al.: [PySE: Software for extracting sources from radio images](http://adsabs.harvard.edu/abs/2018A&C....23...92C), 2018, Astronomy and Computing, 23, 92

55. Gürkan, G., Hardcastle, M. J., Smith, D. J. B., et al.: [LOFAR/H-ATLAS: the low-frequency radio luminosity-star formation rate relation](http://adsabs.harvard.edu/abs/2018MNRAS.475.3010G), 2018, Monthly Notices of the Royal Astronomical Society, 475, 3010

56. Williams, W. L., Calistro Rivera, G., Best, P. N., et al.: [LOFAR-Boötes: properties of high- and low-excitation radio galaxies at 0.5 &lt; z &lt; 2.0](http://adsabs.harvard.edu/abs/2018MNRAS.475.3429W), 2018, Monthly Notices of the Royal Astronomical Society, 475, 3429

57. Broderick, J. W., Fender, R. P., Miller-Jones, J. C. A., et al.: [LOFAR 150-MHz observations of SS 433 and W 50](http://adsabs.harvard.edu/abs/2018MNRAS.475.5360B), 2018, Monthly Notices of the Royal Astronomical Society, 475, 5360

58. Mahatma, V. H., Hardcastle, M. J., Williams, W. L., et al.: [Remnant radio-loud AGN in the Herschel-ATLAS field](http://adsabs.harvard.edu/abs/2018MNRAS.475.4557M), 2018, Monthly Notices of the Royal Astronomical Society, 475, 4557

59. Broekema, P. Chris, Mol, J. Jan David, Nijboer, R., et al.: [Cobalt: A GPU-based correlator and beamformer for LOFAR](http://adsabs.harvard.edu/abs/2018A&C....23..180B), 2018, Astronomy and Computing, 23, 180

60. Chen, Xingyao, Kontar, Eduard P., Yu, Sijie, et al.: [Fine Structures of Solar Radio Type III Bursts and Their Possible Relationship with Coronal Density Turbulence](http://adsabs.harvard.edu/abs/2018ApJ...856...73C), 2018, The Astrophysical Journal, 856, 73

61. Farnes, J. S., Heald, G., Junklewitz, H., et al.: [Source finding in linear polarization for LOFAR, and SKA predecessor surveys, using Faraday moments](http://adsabs.harvard.edu/abs/2018MNRAS.474.3280F), 2018, Monthly Notices of the Royal Astronomical Society, 474, 3280

62. Mann, G., Breitling, F., Vocks, C., et al.: [Tracking of an electron beam through the solar corona with LOFAR](http://adsabs.harvard.edu/abs/2018A&A...611A..57M), 2018, Astronomy and Astrophysics, 611, A57

63. Heesen, V., Croston, J. H., Morganti, R., et al.: [LOFAR reveals the giant: a low-frequency radio continuum study of the outflow in the nearby FR I radio galaxy 3C 31](http://adsabs.harvard.edu/abs/2018MNRAS.474.5049H), 2018, Monthly Notices of the Royal Astronomical Society, 474, 5049

64. Savini, F., Bonafede, A., Brüggen, M., et al.: [Studying the late evolution of a radio-loud AGN in a galaxy group with LOFAR](http://adsabs.harvard.edu/abs/2018MNRAS.474.5023S), 2018, Monthly Notices of the Royal Astronomical Society, 474, 5023

65. Tan, C. M., Lyon, R. J., Stappers, B. W., et al.: [Ensemble candidate classification for the LOTAAS pulsar survey](http://adsabs.harvard.edu/abs/2018MNRAS.474.4571T), 2018, Monthly Notices of the Royal Astronomical Society, 474, 4571

66. Retana-Montenegro, Edwin, Röttgering, Huub: [On the selection of high-z quasars using LOFAR observations](http://adsabs.harvard.edu/abs/2018FrASS...5....5R), 2018, Frontiers in Astronomy and Space Sciences, 5, 5

67. Ramírez-Olivencia, N., Varenius, E., Pérez-Torres, M., et al.: [Sub-arcsecond imaging of Arp 299-A at 150 MHz with LOFAR: Evidence for a starburst-driven outflow](http://adsabs.harvard.edu/abs/2018A&A...610L..18R), 2018, Astronomy and Astrophysics, 610, L18

68. Chiarucci, Simone, Wijnholds, Stefan J.: [Blind calibration of radio interferometric arrays using sparsity constraints and its implications for self-calibration](http://adsabs.harvard.edu/abs/2018MNRAS.474.1028C), 2018, Monthly Notices of the Royal Astronomical Society, 474, 1028

69. Wilber, A., Brüggen, M., Bonafede, A., et al.: [LOFAR discovery of an ultra-steep radio halo and giant head-tail radio galaxy in Abell 1132](http://adsabs.harvard.edu/abs/2018MNRAS.473.3536W), 2018, Monthly Notices of the Royal Astronomical Society, 473, 3536

70. Rajpurohit, K., Hoeft, M., van Weeren, R. J., et al.: [Deep VLA Observations of the Cluster 1RXS J0603.3+4214 in the Frequency Range of 1-2 GHz](http://adsabs.harvard.edu/abs/2018ApJ...852...65R), 2018, The Astrophysical Journal, 852, 65


2017
----

1. Pan, Hanjie, Simeoni, Matthieu, Hurley, Paul, et al.: [LEAP: Looking beyond pixels with continuous-space EstimAtion of Point sources](http://adsabs.harvard.edu/abs/2017A&A...608A.136P), 2017, Astronomy and Astrophysics, 608, A136

2. Vedantham, H. K., de Bruyn, A. G., Macquart, J. -P.: [A Dense Plasma Globule in the Solar Neighborhood](http://adsabs.harvard.edu/abs/2017ApJ...849L...3V), 2017, The Astrophysical Journal, 849, L3

3. Kontar, E. P., Yu, S., Kuznetsov, A. A., et al.: [Imaging spectroscopy of solar radio burst fine structures](http://adsabs.harvard.edu/abs/2017NatCo...8.1515K), 2017, Nature Communications, 8, 1515

4. Reid, Hamish A. S., Kontar, Eduard P.: [Imaging spectroscopy of type U and J solar radio bursts with LOFAR](http://adsabs.harvard.edu/abs/2017A&A...606A.141R), 2017, Astronomy and Astrophysics, 606, A141

5. Brienza, M., Godfrey, L., Morganti, R., et al.: [Search and modelling of remnant radio galaxies in the LOFAR Lockman Hole field](http://adsabs.harvard.edu/abs/2017A&A...606A..98B), 2017, Astronomy and Astrophysics, 606, A98

6. Morosan, D. E., Gallagher, P. T., Fallows, R. A., et al.: [The association of a J-burst with a solar jet](http://adsabs.harvard.edu/abs/2017A&A...606A..81M), 2017, Astronomy and Astrophysics, 606, A81

7. Hoang, D. N., Shimwell, T. W., Stroe, A., et al.: [Deep LOFAR observations of the merging galaxy cluster CIZA J2242.8+5301](http://adsabs.harvard.edu/abs/2017MNRAS.471.1107H), 2017, Monthly Notices of the Royal Astronomical Society, 471, 1107

8. de Gasperin, Francesco, Intema, Huib T., Shimwell, Timothy W., et al.: [Gentle reenergization of electrons in merging galaxy clusters](http://adsabs.harvard.edu/abs/2017SciA....3E1634D), 2017, Science Advances, 3, e1701634

9. Abbott, B. P., Abbott, R., Abbott, T. D., et al.: [Multi-messenger Observations of a Binary Neutron Star Merger](http://adsabs.harvard.edu/abs/2017ApJ...848L..12A), 2017, The Astrophysical Journal, 848, L12

10. Geyer, M., Karastergiou, A., Kondratiev, V. I., et al.: [Scattering analysis of LOFAR pulsar observations](http://adsabs.harvard.edu/abs/2017MNRAS.470.2659G), 2017, Monthly Notices of the Royal Astronomical Society, 470, 2659

11. Kokotanekov, G., Wise, M., Heald, G. H., et al.: [LOFAR MSSS: The scaling relation between AGN cavity power and radio luminosity at low radio frequencies](http://adsabs.harvard.edu/abs/2017A&A...605A..48K), 2017, Astronomy and Astrophysics, 605, A48

12. Bassa, C. G., Pleunis, Z., Hessels, J. W. T., et al.: [LOFAR Discovery of the Fastest-spinning Millisecond Pulsar in the Galactic Field](http://adsabs.harvard.edu/abs/2017ApJ...846L..20B), 2017, The Astrophysical Journal, 846, L20

13. Pleunis, Z., Bassa, C. G., Hessels, J. W. T., et al.: [A Millisecond Pulsar Discovery in a Survey of Unidentified Fermi γ-Ray Sources with LOFAR](http://adsabs.harvard.edu/abs/2017ApJ...846L..19P), 2017, The Astrophysical Journal, 846, L19

14. Swiggum, J. K., Kaplan, D. L., McLaughlin, M. A., et al.: [A Multiwavelength Study of Nearby Millisecond Pulsar PSR J1400-1431: Improved Astrometry and an Optical Detection of Its Cool White Dwarf Companion](http://adsabs.harvard.edu/abs/2017ApJ...847...25S), 2017, The Astrophysical Journal, 847, 25

15. Morabito, Leah K., Williams, W. L., Duncan, Kenneth J., et al.: [Investigating the unification of LOFAR-detected powerful AGN in the Boötes field](http://adsabs.harvard.edu/abs/2017MNRAS.469.1883M), 2017, Monthly Notices of the Royal Astronomical Society, 469, 1883

16. Calistro Rivera, G., Williams, W. L., Hardcastle, M. J., et al.: [The LOFAR window on star-forming galaxies and AGNs - curved radio SEDs and IR-radio correlation at 0&lt;z&lt;2.5](http://adsabs.harvard.edu/abs/2017MNRAS.469.3468C), 2017, Monthly Notices of the Royal Astronomical Society, 469, 3468

17. Harwood, Jeremy J., Hardcastle, Martin J., Morganti, Raffaella, et al.: [FR II radio galaxies at low frequencies - II. Spectral ageing and source dynamics](http://adsabs.harvard.edu/abs/2017MNRAS.469..639H), 2017, Monthly Notices of the Royal Astronomical Society, 469, 639

18. Venturi, T., Rossetti, M., Brunetti, G., et al.: [The two-component giant radio halo in the galaxy cluster Abell 2142](http://adsabs.harvard.edu/abs/2017A&A...603A.125V), 2017, Astronomy and Astrophysics, 603, A125

19. Salas, P., Oonk, J. B. R., van Weeren, R. J., et al.: [LOFAR observations of decameter carbon radio recombination lines towards Cassiopeia A](http://adsabs.harvard.edu/abs/2017MNRAS.467.2274S), 2017, Monthly Notices of the Royal Astronomical Society, 467, 2274

20. Clarke, A. O., Heald, G., Jarrett, T., et al.: [LOFAR MSSS: Discovery of a 2.56 Mpc giant radio galaxy associated with a disturbed galaxy group](http://adsabs.harvard.edu/abs/2017A&A...601A..25C), 2017, Astronomy and Astrophysics, 601, A25

21. Trinh, T. N. G., Scholten, O., Bonardi, A., et al.: [Thunderstorm electric fields probed by extensive air showers through their polarized radio emission](http://adsabs.harvard.edu/abs/2017PhRvD..95h3004T), 2017, Physical Review D, 95, 083004

22. Shulevski, A., Morganti, R., Harwood, J. J., et al.: [Radiative age mapping of the remnant radio galaxy B2 0924+30: the LOFAR perspective](http://adsabs.harvard.edu/abs/2017A&A...600A..65S), 2017, Astronomy and Astrophysics, 600, A65

23. Sabater, J., Sánchez-Expósito, S., Best, P., et al.: [Calibration of LOFAR data on the cloud](http://adsabs.harvard.edu/abs/2017A&C....19...75S), 2017, Astronomy and Computing, 19, 75

24. Morganti, R.: [Synergy with new radio facilities: From LOFAR to SKA](http://adsabs.harvard.edu/abs/2017AN....338..165M), 2017, Astronomische Nachrichten, 338, 165

25. Patil, A. H., Yatawatta, S., Koopmans, L. V. E., et al.: [Upper Limits on the 21 cm Epoch of Reionization Power Spectrum from One Night with LOFAR](http://adsabs.harvard.edu/abs/2017ApJ...838...65P), 2017, The Astrophysical Journal, 838, 65

26. Oonk, J. B. R., van Weeren, R. J., Salas, P., et al.: [Carbon and hydrogen radio recombination lines from the cold clouds towards Cassiopeia A](http://adsabs.harvard.edu/abs/2017MNRAS.465.1066O), 2017, Monthly Notices of the Royal Astronomical Society, 465, 1066

27. Shimwell, T. W., Röttgering, H. J. A., Best, P. N., et al.: [The LOFAR Two-metre Sky Survey. I. Survey description and preliminary data release](http://adsabs.harvard.edu/abs/2017A&A...598A.104S), 2017, Astronomy and Astrophysics, 598, A104

28. Van Eck, C. L., Haverkorn, M., Alves, M. I. R., et al.: [Faraday tomography of the local interstellar medium with LOFAR: Galactic foregrounds towards IC 342](http://adsabs.harvard.edu/abs/2017A&A...597A..98V), 2017, Astronomy and Astrophysics, 597, A98

29. Coughlan, Colm P., Ainsworth, Rachael E., Eislöffel, Jochen, et al.: [A LOFAR Detection of the Low-mass Young Star T Tau at 149 MHz](http://adsabs.harvard.edu/abs/2017ApJ...834..206C), 2017, The Astrophysical Journal, 834, 206

30. Bassa, C. G., Pleunis, Z., Hessels, J. W. T.: [Enabling pulsar and fast transient searches using coherent dedispersion](http://adsabs.harvard.edu/abs/2017A&C....18...40B), 2017, Astronomy and Computing, 18, 40

31. Turner, J. D., Griessmeier, J. -M., Zarka, P., et al.: [The search for radio emission from exoplanets using LOFAR low-frequency beam-formed observations: Data pipeline and preliminary results for the 55 Cnc system](http://adsabs.harvard.edu/abs/2017pre8.conf..301T), 2017, Planetary Radio Emissions VIII, None, 301

32. Dabrowski, B. P., Blaszkiewicz, L., Krankowski, A., et al.: [Low frequency solar scrutiny with the Polish LOFAR stations](http://adsabs.harvard.edu/abs/2017pre8.conf..437D), 2017, Planetary Radio Emissions VIII, None, 437

33. Morosan, D. E., Gallagher, P. T.: [Characteristics of type III radio bursts and solar S bursts](http://adsabs.harvard.edu/abs/2017pre8.conf..357M), 2017, Planetary Radio Emissions VIII, None, 357


2016
----

1. Mahony, E. K., Morganti, R., Prandoni, I., et al.: [The Lockman Hole project: LOFAR observations and spectral index properties of low-frequency radio sources](http://adsabs.harvard.edu/abs/2016MNRAS.463.2997M), 2016, Monthly Notices of the Royal Astronomical Society, 463, 2997

2. Burningham, Ben, Hardcastle, M., Nichols, J. D., et al.: [A LOFAR mini-survey for low-frequency radio emission from the nearest brown dwarfs](http://adsabs.harvard.edu/abs/2016MNRAS.463.2202B), 2016, Monthly Notices of the Royal Astronomical Society, 463, 2202

3. McKean, J. P., Godfrey, L. E. H., Vegetti, S., et al.: [LOFAR imaging of Cygnus A - direct detection of a turnover in the hotspot radio spectra](http://adsabs.harvard.edu/abs/2016MNRAS.463.3143M), 2016, Monthly Notices of the Royal Astronomical Society, 463, 3143

4. Patil, Ajinkya H., Yatawatta, Sarod, Zaroubi, Saleem, et al.: [Systematic biases in low-frequency radio interferometric data due to calibration: the LOFAR-EoR case](http://adsabs.harvard.edu/abs/2016MNRAS.463.4317P), 2016, Monthly Notices of the Royal Astronomical Society, 463, 4317

5. Mereghetti, S., Kuiper, L., Tiengo, A., et al.: [A Deep Campaign to Characterize the Synchronous Radio/X-Ray Mode Switching of PSR B0943+10](http://adsabs.harvard.edu/abs/2016ApJ...831...21M), 2016, The Astrophysical Journal, 831, 21

6. Jackson, N., Tagore, A., Deller, A., et al.: [LBCS: The LOFAR Long-Baseline Calibrator Survey](http://adsabs.harvard.edu/abs/2016A&A...595A..86J), 2016, Astronomy and Astrophysics, 595, A86

7. Scholten, O., Trinh, T. N. G., Bonardi, A., et al.: [Measurement of the circular polarization in radio emission from extensive air showers confirms emission mechanisms](http://adsabs.harvard.edu/abs/2016PhRvD..94j3010S), 2016, Physical Review D, 94, 103010

8. Hardcastle, M. J., Gürkan, G., van Weeren, R. J., et al.: [LOFAR/H-ATLAS: a deep low-frequency survey of the Herschel-ATLAS North Galactic Pole field](http://adsabs.harvard.edu/abs/2016MNRAS.462.1910H), 2016, Monthly Notices of the Royal Astronomical Society, 462, 1910

9. Crosley, M. K., Osten, R. A., Broderick, J. W., et al.: [The Search for Signatures of Transient Mass Loss in Active Stars](http://adsabs.harvard.edu/abs/2016ApJ...830...24C), 2016, The Astrophysical Journal, 830, 24

10. Morabito, Leah K., Deller, Adam T., Röttgering, Huub, et al.: [LOFAR VLBI studies at 55 MHz of 4C 43.15, a z = 2.4 radio galaxy](http://adsabs.harvard.edu/abs/2016MNRAS.461.2676M), 2016, Monthly Notices of the Royal Astronomical Society, 461, 2676

11. Fallows, R. A., Bisi, M. M., Forte, B., et al.: [Separating Nightside Interplanetary and Ionospheric Scintillation with LOFAR](http://adsabs.harvard.edu/abs/2016ApJ...828L...7F), 2016, The Astrophysical Journal, 828, L7

12. Varenius, E., Conway, J. E., Martí-Vidal, I., et al.: [Subarcsecond international LOFAR radio images of Arp 220 at 150 MHz. A kpc-scale star forming disk surrounding nuclei with shocked outflows](http://adsabs.harvard.edu/abs/2016A&A...593A..86V), 2016, Astronomy and Astrophysics, 593, A86

13. Williams, W. L., van Weeren, R. J., Röttgering, H. J. A., et al.: [LOFAR 150-MHz observations of the Boötes field: catalogue and source counts](http://adsabs.harvard.edu/abs/2016MNRAS.460.2385W), 2016, Monthly Notices of the Royal Astronomical Society, 460, 2385

14. Mulcahy, D. D., Fletcher, A., Beck, R., et al.: [Modelling the cosmic ray electron propagation in M 51](http://adsabs.harvard.edu/abs/2016A&A...592A.123M), 2016, Astronomy and Astrophysics, 592, A123

15. Mikhailov, K., van Leeuwen, J.: [The LOFAR search for radio pulsars and fast transients in M 33, M 81, and M 82](http://adsabs.harvard.edu/abs/2016A&A...593A..21M), 2016, Astronomy and Astrophysics, 593, A21

16. Broderick, J. W., Fender, R. P., Breton, R. P., et al.: [Low-radio-frequency eclipses of the redback pulsar J2215+5135 observed in the image plane with LOFAR](http://adsabs.harvard.edu/abs/2016MNRAS.459.2681B), 2016, Monthly Notices of the Royal Astronomical Society, 459, 2681

17. Mevius, M., van der Tol, S., Pandey, V. N., et al.: [Probing ionospheric structures using the LOFAR radio telescope](http://adsabs.harvard.edu/abs/2016RaSc...51..927M), 2016, Radio Science, 51, 927

18. Carbone, D., van der Horst, A. J., Wijers, R. A. M. J., et al.: [New methods to constrain the radio transient rate: results from a survey of four fields with LOFAR](http://adsabs.harvard.edu/abs/2016MNRAS.459.3161C), 2016, Monthly Notices of the Royal Astronomical Society, 459, 3161

19. Abbott, B. P., Abbott, R., Abbott, T. D., et al.: [Supplement: “Localization and Broadband Follow-up of the Gravitational-wave Transient GW150914” (2016, ApJL, 826, L13)](http://adsabs.harvard.edu/abs/2016ApJS..225....8A), 2016, The Astrophysical Journal Supplement Series, 225, 8

20. Shimwell, T. W., Luckin, J., Brüggen, M., et al.: [A plethora of diffuse steep spectrum radio sources in Abell 2034 revealed by LOFAR](http://adsabs.harvard.edu/abs/2016MNRAS.459..277S), 2016, Monthly Notices of the Royal Astronomical Society, 459, 277

21. Lyon, R. J., Stappers, B. W., Cooper, S., et al.: [Fifty Years of Pulsar Candidate Selection: From simple filters to a new principled real-time classification approach](http://adsabs.harvard.edu/abs/2016MNRAS.459.1104L), 2016, Monthly Notices of the Royal Astronomical Society, 459, 1104

22. Harwood, Jeremy J., Croston, Judith H., Intema, Huib T., et al.: [FR II radio galaxies at low frequencies - I. Morphology, magnetic field strength and energetics](http://adsabs.harvard.edu/abs/2016MNRAS.458.4443H), 2016, Monthly Notices of the Royal Astronomical Society, 458, 4443

23. Bilous, A. V., Kondratiev, V. I., Kramer, M., et al.: [A LOFAR census of non-recycled pulsars: average profiles, dispersion measures, flux densities, and spectra](http://adsabs.harvard.edu/abs/2016A&A...591A.134B), 2016, Astronomy and Astrophysics, 591, A134

24. Corstanje, A., Buitink, S., Enriquez, J. E., et al.: [Timing calibration and spectral cleaning of LOFAR time series data](http://adsabs.harvard.edu/abs/2016A&A...590A..41C), 2016, Astronomy and Astrophysics, 590, A41

25. Rossetto, L., Buitink, S., Corstanje, A., et al.: [Measurement of cosmic rays with LOFAR](http://adsabs.harvard.edu/abs/2016JPhCS.718e2035R), 2016, Journal of Physics Conference Series, 718, 052035

26. van Weeren, R. J., Williams, W. L., Hardcastle, M. J., et al.: [LOFAR Facet Calibration](http://adsabs.harvard.edu/abs/2016ApJS..223....2V), 2016, The Astrophysical Journal Supplement Series, 223, 2

27. Buitink, S., Corstanje, A., Falcke, H., et al.: [A large light-mass component of cosmic rays at 10<SUP>17</SUP>-10<SUP>17.5</SUP> electronvolts from radio observations](http://adsabs.harvard.edu/abs/2016Natur.531...70B), 2016, Nature, 531, 70

28. Stewart, A. J., Fender, R. P., Broderick, J. W., et al.: [LOFAR MSSS: detection of a low-frequency radio transient in 400 h of monitoring of the North Celestial Pole](http://adsabs.harvard.edu/abs/2016MNRAS.456.2321S), 2016, Monthly Notices of the Royal Astronomical Society, 456, 2321

29. Girard, J. N., Zarka, P., Tasse, C., et al.: [Imaging Jupiter's radiation belts down to 127 MHz with LOFAR](http://adsabs.harvard.edu/abs/2016A&A...587A...3G), 2016, Astronomy and Astrophysics, 587, A3

30. Vrbanec, Dijana, Ciardi, Benedetta, Jelić, Vibor, et al.: [Predictions for the 21 cm-galaxy cross-power spectrum observable with LOFAR and Subaru](http://adsabs.harvard.edu/abs/2016MNRAS.457..666V), 2016, Monthly Notices of the Royal Astronomical Society, 457, 666

31. van Weeren, R. J., Brunetti, G., Brüggen, M., et al.: [LOFAR, VLA, and Chandra Observations of the Toothbrush Galaxy Cluster](http://adsabs.harvard.edu/abs/2016ApJ...818..204V), 2016, The Astrophysical Journal, 818, 204

32. Marcote, B., Ribó, M., Paredes, J. M., et al.: [Orbital and superorbital variability of LS I +61 303 at low radio frequencies with GMRT and LOFAR](http://adsabs.harvard.edu/abs/2016MNRAS.456.1791M), 2016, Monthly Notices of the Royal Astronomical Society, 456, 1791

33. Pilia, M., Hessels, J. W. T., Stappers, B. W., et al.: [Wide-band, low-frequency pulse profiles of 100 radio pulsars with LOFAR](http://adsabs.harvard.edu/abs/2016A&A...586A..92P), 2016, Astronomy and Astrophysics, 586, A92

34. Thoudam, S., Buitink, S., Corstanje, A., et al.: [Measurement of the cosmic-ray energy spectrum above 10<SUP>16</SUP> eV with the LOFAR Radboud Air Shower Array](http://adsabs.harvard.edu/abs/2016APh....73...34T), 2016, Astroparticle Physics, 73, 34

35. Kondratiev, V. I., Verbiest, J. P. W., Hessels, J. W. T., et al.: [A LOFAR census of millisecond pulsars](http://adsabs.harvard.edu/abs/2016A&A...585A.128K), 2016, Astronomy and Astrophysics, 585, A128

36. Brienza, M., Godfrey, L., Morganti, R., et al.: [LOFAR discovery of a 700-kpc remnant radio galaxy at low redshift](http://adsabs.harvard.edu/abs/2016A&A...585A..29B), 2016, Astronomy and Astrophysics, 585, A29

37. Trinh, T. N. G., Scholten, O., Buitink, S., et al.: [Influence of atmospheric electric fields on the radio emission from extensive air showers](http://adsabs.harvard.edu/abs/2016PhRvD..93b3003T), 2016, Physical Review D, 93, 023003


2015
----

1. Orrù, E., van Velzen, S., Pizzo, R. F., et al.: [Wide-field LOFAR imaging of the field around the double-double radio galaxy B1834+620. A fresh view on a restarted AGN and doubeltjes](http://adsabs.harvard.edu/abs/2015A&A...584A.112O), 2015, Astronomy and Astrophysics, 584, A112

2. Zaroubi, S., Jelic, V., de Bruyn, A. G., et al.: [Galactic interstellar filaments as probed by LOFAR and Planck.](http://adsabs.harvard.edu/abs/2015MNRAS.454L..46Z), 2015, Monthly Notices of the Royal Astronomical Society, 454, L46

3. Breitling, F., Mann, G., Vocks, C., et al.: [The LOFAR Solar Imaging Pipeline and the LOFAR Solar Data Center](http://adsabs.harvard.edu/abs/2015A&C....13...99B), 2015, Astronomy and Computing, 13, 99

4. Jelić, V., de Bruyn, A. G., Pandey, V. N., et al.: [Linear polarization structures in LOFAR observations of the interstellar medium in the 3C 196 field](http://adsabs.harvard.edu/abs/2015A&A...583A.137J), 2015, Astronomy and Astrophysics, 583, A137

5. Shulevski, A., Morganti, R., Barthel, P. D., et al.: [AGN duty cycle estimates for the ultra-steep spectrum radio relic VLSS J1431.8+1331](http://adsabs.harvard.edu/abs/2015A&A...583A..89S), 2015, Astronomy and Astrophysics, 583, A89

6. Nelles, A., Hörandel, J. R., Karskens, T., et al.: [Calibrating the absolute amplitude scale for air showers measured at LOFAR](http://adsabs.harvard.edu/abs/2015JInst..10P1005N), 2015, Journal of Instrumentation, 10, P11005

7. Heald, G. H., Pizzo, R. F., Orrú, E., et al.: [The LOFAR Multifrequency Snapshot Sky Survey (MSSS). I. Survey description and first results](http://adsabs.harvard.edu/abs/2015A&A...582A.123H), 2015, Astronomy and Astrophysics, 582, A123

8. Ciardi, B., Inoue, S., Abdalla, F. B., et al.: [Simulating the 21 cm forest detectable with LOFAR and SKA in the spectra of high-z GRBs](http://adsabs.harvard.edu/abs/2015MNRAS.453..101C), 2015, Monthly Notices of the Royal Astronomical Society, 453, 101

9. Ghosh, Abhik, Koopmans, Léon V. E., Chapman, E., et al.: [A Bayesian analysis of redshifted 21-cm H I signal and foregrounds: simulations for LOFAR](http://adsabs.harvard.edu/abs/2015MNRAS.452.1587G), 2015, Monthly Notices of the Royal Astronomical Society, 452, 1587

10. Karastergiou, A., Chennamangalam, J., Armour, W., et al.: [Limits on fast radio bursts at 145 MHz with ARTEMIS, a real-time software backend](http://adsabs.harvard.edu/abs/2015MNRAS.452.1254K), 2015, Monthly Notices of the Royal Astronomical Society, 452, 1254

11. van Diepen, G. N. J.: [Casacore Table Data System and its use in the MeasurementSet](http://adsabs.harvard.edu/abs/2015A&C....12..174V), 2015, Astronomy and Computing, 12, 174

12. Asad, K. M. B., Koopmans, L. V. E., Jelić, V., et al.: [Polarization leakage in epoch of reionization windows - I. Low Frequency Array observations of the 3C196 field](http://adsabs.harvard.edu/abs/2015MNRAS.451.3709A), 2015, Monthly Notices of the Royal Astronomical Society, 451, 3709

13. Sobey, C., Young, N. J., Hessels, J. W. T., et al.: [LOFAR discovery of a quiet emission mode in PSR B0823+26](http://adsabs.harvard.edu/abs/2015MNRAS.451.2493S), 2015, Monthly Notices of the Royal Astronomical Society, 451, 2493

14. Karako-Argaman, C., Kaspi, V. M., Lynch, R. S., et al.: [Discovery and Follow-up of Rotating Radio Transients with the Green Bank and LOFAR Telescopes](http://adsabs.harvard.edu/abs/2015ApJ...809...67K), 2015, The Astrophysical Journal, 809, 67

15. Morosan, D. E., Gallagher, P. T., Zucca, P., et al.: [LOFAR tied-array imaging and spectroscopy of solar S bursts](http://adsabs.harvard.edu/abs/2015A&A...580A..65M), 2015, Astronomy and Astrophysics, 580, A65

16. Yoshiura, Shintaro, Shimabukuro, Hayato, Takahashi, Keitaro, et al.: [Sensitivity for 21 cm bispectrum from Epoch of Reionization](http://adsabs.harvard.edu/abs/2015MNRAS.451..266Y), 2015, Monthly Notices of the Royal Astronomical Society, 451, 266

17. Vedantham, H. K., Koopmans, L. V. E., de Bruyn, A. G., et al.: [Lunar occultation of the diffuse radio sky: LOFAR measurements between 35 and 80 MHz](http://adsabs.harvard.edu/abs/2015MNRAS.450.2291V), 2015, Monthly Notices of the Royal Astronomical Society, 450, 2291

18. Obrocka, M., Stappers, B., Wilkinson, P.: [Localising fast radio bursts and other transients using interferometric arrays](http://adsabs.harvard.edu/abs/2015A&A...579A..69O), 2015, Astronomy and Astrophysics, 579, A69

19. Shulevski, A., Morganti, R., Barthel, P. D., et al.: [The peculiar radio galaxy 4C 35.06: a case for recurrent AGN activity?](http://adsabs.harvard.edu/abs/2015A&A...579A..27S), 2015, Astronomy and Astrophysics, 579, A27

20. Metzger, Brian D., Williams, P. K. G., Berger, Edo: [Extragalactic Synchrotron Transients in the Era of Wide-field Radio Surveys. I. Detection Rates and Light Curve Characteristics](http://adsabs.harvard.edu/abs/2015ApJ...806..224M), 2015, The Astrophysical Journal, 806, 224

21. Swinbank, John D., Staley, Tim D., Molenaar, Gijs J., et al.: [The LOFAR Transients Pipeline](http://adsabs.harvard.edu/abs/2015A&C....11...25S), 2015, Astronomy and Computing, 11, 25

22. Nelles, A., Buitink, S., Corstanje, A., et al.: [The radio emission pattern of air showers as measured with LOFAR-a tool for the reconstruction of the energy and the shower maximum](http://adsabs.harvard.edu/abs/2015JCAP...05..018N), 2015, Journal of Cosmology and Astroparticle Physics, 2015, 018

23. Nelles, A., Schellart, P., Buitink, S., et al.: [Measuring a Cherenkov ring in the radio emission from air showers at 110-190 MHz with LOFAR](http://adsabs.harvard.edu/abs/2015APh....65...11N), 2015, Astroparticle Physics, 65, 11

24. Noutsos, A., Sobey, C., Kondratiev, V. I., et al.: [Pulsar polarisation below 200 MHz: Average profiles and propagation effects](http://adsabs.harvard.edu/abs/2015A&A...576A..62N), 2015, Astronomy and Astrophysics, 576, A62

25. Schellart, P., Trinh, T. N. G., Buitink, S., et al.: [Probing Atmospheric Electric Fields in Thunderstorms through Radio Emission from Cosmic-Ray-Induced Air Showers](http://adsabs.harvard.edu/abs/2015PhRvL.114p5001S), 2015, Physical Review Letters, 114, 165001

26. Garsden, H., Girard, J. N., Starck, J. L., et al.: [LOFAR sparse image reconstruction](http://adsabs.harvard.edu/abs/2015A&A...575A..90G), 2015, Astronomy and Astrophysics, 575, A90

27. Corstanje, A., Schellart, P., Nelles, A., et al.: [The shape of the radio wavefront of extensive air showers as measured with LOFAR](http://adsabs.harvard.edu/abs/2015APh....61...22C), 2015, Astroparticle Physics, 61, 22

28. Varenius, E., Conway, J. E., Martí-Vidal, I., et al.: [Subarcsecond international LOFAR radio images of the M82 nucleus at 118 MHz and 154 MHz](http://adsabs.harvard.edu/abs/2015A&A...574A.114V), 2015, Astronomy and Astrophysics, 574, A114

29. Moldón, J., Deller, A. T., Wucknitz, O., et al.: [The LOFAR long baseline snapshot calibrator survey](http://adsabs.harvard.edu/abs/2015A&A...574A..73M), 2015, Astronomy and Astrophysics, 574, A73

30. Nelles, Anna, Buitink, Stijn, Falcke, Heino, et al.: [A parameterization for the radio emission of air showers as predicted by CoREAS simulations and applied to LOFAR measurements](http://adsabs.harvard.edu/abs/2015APh....60...13N), 2015, Astroparticle Physics, 60, 13


2014
----

1. Fallows, R. A., Coles, W. A., McKay-Bukowski, D., et al.: [Broadband meter-wavelength observations of ionospheric scintillation](http://adsabs.harvard.edu/abs/2014JGRA..11910544F), 2014, Journal of Geophysical Research (Space Physics), 119, 10,544

2. Bilous, A. V., Hessels, J. W. T., Kondratiev, V. I., et al.: [LOFAR observations of PSR B0943+10: profile evolution and discovery of a systematically changing profile delay in bright mode](http://adsabs.harvard.edu/abs/2014A&A...572A..52B), 2014, Astronomy and Astrophysics, 572, A52

3. Patel, Prina, Abdalla, Filipe B., Bacon, David J., et al.: [Weak lensing measurements in simulations of radio images](http://adsabs.harvard.edu/abs/2014MNRAS.444.2893P), 2014, Monthly Notices of the Royal Astronomical Society, 444, 2893

4. Salvini, Stefano, Wijnholds, Stefan J.: [Fast gain calibration in radio astronomy using alternating direction implicit methods: Analysis and applications](http://adsabs.harvard.edu/abs/2014A&A...571A..97S), 2014, Astronomy and Astrophysics, 571, A97

5. Buitink, S., Corstanje, A., Enriquez, J. E., et al.: [Method for high precision reconstruction of air shower X<SUB>max</SUB> using two-dimensional radio intensity profiles](http://adsabs.harvard.edu/abs/2014PhRvD..90h2003B), 2014, Physical Review D, 90, 082003

6. Schellart, P., Buitink, S., Corstanje, A., et al.: [Polarized radio emission from extensive air showers measured with LOFAR](http://adsabs.harvard.edu/abs/2014JCAP...10..014S), 2014, Journal of Cosmology and Astroparticle Physics, 2014, 014

7. Dolch, T., Lam, M. T., Cordes, J., et al.: [A 24 Hr Global Campaign to Assess Precision Timing of the Millisecond Pulsar J1713+0747](http://adsabs.harvard.edu/abs/2014ApJ...794...21D), 2014, The Astrophysical Journal, 794, 21

8. van Weeren, R. J., Williams, W. L., Tasse, C., et al.: [LOFAR Low-band Antenna Observations of the 3C 295 and Boötes Fields: Source Counts and Ultra-steep Spectrum Sources](http://adsabs.harvard.edu/abs/2014ApJ...793...82V), 2014, The Astrophysical Journal, 793, 82

9. Coenen, Thijs, van Leeuwen, Joeri, Hessels, Jason W. T., et al.: [The LOFAR pilot surveys for pulsars and fast radio transients](http://adsabs.harvard.edu/abs/2014A&A...570A..60C), 2014, Astronomy and Astrophysics, 570, A60

10. Patil, Ajinkya H., Zaroubi, Saleem, Chapman, Emma, et al.: [Constraining the epoch of reionization with the variance statistic: simulations of the LOFAR case](http://adsabs.harvard.edu/abs/2014MNRAS.443.1113P), 2014, Monthly Notices of the Royal Astronomical Society, 443, 1113

11. Archibald, Anne M., Kondratiev, Vladislav I., Hessels, Jason W. T., et al.: [Millisecond Pulsar Scintillation Studies with LOFAR: Initial Results](http://adsabs.harvard.edu/abs/2014ApJ...790L..22A), 2014, The Astrophysical Journal, 790, L22

12. Jelić, V., de Bruyn, A. G., Mevius, M., et al.: [Initial LOFAR observations of epoch of reionization windows. II. Diffuse polarized emission in the ELAIS-N1 field](http://adsabs.harvard.edu/abs/2014A&A...568A.101J), 2014, Astronomy and Astrophysics, 568, A101

13. Mulcahy, D. D., Horneffer, A., Beck, R., et al.: [The nature of the low-frequency emission of M 51. First observations of a nearby galaxy with LOFAR](http://adsabs.harvard.edu/abs/2014A&A...568A..74M), 2014, Astronomy and Astrophysics, 568, A74

14. Morosan, D. E., Gallagher, P. T., Zucca, P., et al.: [LOFAR tied-array imaging of Type III solar radio bursts](http://adsabs.harvard.edu/abs/2014A&A...568A..67M), 2014, Astronomy and Astrophysics, 568, A67

15. Mikhailov, E., Kasparova, A., Moss, D., et al.: [Magnetic fields near the peripheries of galactic discs](http://adsabs.harvard.edu/abs/2014A&A...568A..66M), 2014, Astronomy and Astrophysics, 568, A66

16. Prasad, P., Wijnholds, S. J., Huizinga, F., et al.: [Real-time calibration of the AARTFAAC array for transient detection](http://adsabs.harvard.edu/abs/2014A&A...568A..48P), 2014, Astronomy and Astrophysics, 568, A48

17. Vasiliev, Evgenii O., Sethi, Shiv K.: [H I Absorption from the Epoch of Reionization and Primordial Magnetic Fields](http://adsabs.harvard.edu/abs/2014ApJ...786..142V), 2014, The Astrophysical Journal, 786, 142

18. Rubart, Matthias, Bacon, David, Schwarz, Dominik J.: [Impact of local structure on the cosmic radio dipole](http://adsabs.harvard.edu/abs/2014A&A...565A.111R), 2014, Astronomy and Astrophysics, 565, A111

19. Mesinger, Andrei, Ewall-Wice, Aaron, Hewitt, Jacqueline: [Reionization and beyond: detecting the peaks of the cosmological 21 cm signal](http://adsabs.harvard.edu/abs/2014MNRAS.439.3262M), 2014, Monthly Notices of the Royal Astronomical Society, 439, 3262

20. Roseboom, I. G., Best, P. N.: [Cosmic star formation probed via parametric stack-fitting of known sources to radio imaging](http://adsabs.harvard.edu/abs/2014MNRAS.439.1286R), 2014, Monthly Notices of the Royal Astronomical Society, 439, 1286

21. Vedantham, H. K., Koopmans, L. V. E., de Bruyn, A. G., et al.: [Chromatic effects in the 21 cm global signal from the cosmic dawn](http://adsabs.harvard.edu/abs/2014MNRAS.437.1056V), 2014, Monthly Notices of the Royal Astronomical Society, 437, 1056


2013
----

1. Pfrommer, Christoph: [Toward a Comprehensive Model for Feedback by Active Galactic Nuclei: New Insights from M87 Observations by LOFAR, Fermi, and H.E.S.S.](http://adsabs.harvard.edu/abs/2013ApJ...779...10P), 2013, The Astrophysical Journal, 779, 10

2. Schellart, P., Nelles, A., Buitink, S., et al.: [Detecting cosmic rays with the LOFAR radio telescope](http://adsabs.harvard.edu/abs/2013A&A...560A..98S), 2013, Astronomy and Astrophysics, 560, A98

3. Offringa, A. R., de Bruyn, A. G., Zaroubi, S., et al.: [The brightness and spatial distributions of terrestrial radio sources](http://adsabs.harvard.edu/abs/2013MNRAS.435..584O), 2013, Monthly Notices of the Royal Astronomical Society, 435, 584

4. Iacobelli, M., Haverkorn, M., Orrú, E., et al.: [Studying Galactic interstellar turbulence through fluctuations in synchrotron emission. First LOFAR Galactic foreground detection](http://adsabs.harvard.edu/abs/2013A&A...558A..72I), 2013, Astronomy and Astrophysics, 558, A72

5. Jones, P. B.: [PSR B1133+16: radio emission height and plasma composition.](http://adsabs.harvard.edu/abs/2013MNRAS.435L..11J), 2013, Monthly Notices of the Royal Astronomical Society, 435, L11

6. van Haarlem, M. P., Wise, M. W., Gunst, A. W., et al.: [LOFAR: The LOw-Frequency ARray](http://adsabs.harvard.edu/abs/2013A&A...556A...2V), 2013, Astronomy and Astrophysics, 556, A2

7. Fallows, R. A., Asgekar, A., Bisi, M. M., et al.: [The Dynamic Spectrum of Interplanetary Scintillation: First Solar Wind Observations on LOFAR](http://adsabs.harvard.edu/abs/2013SoPh..285..127F), 2013, Solar Physics, 285, 127

8. Tasse, C., van der Tol, S., van Zwieten, J., et al.: [Applying full polarization A-Projection to very wide field of view instruments: An imager for LOFAR](http://adsabs.harvard.edu/abs/2013A&A...553A.105T), 2013, Astronomy and Astrophysics, 553, A105

9. Prasad, P., Wijnholds, S. J.: [Amsterdam-ASTRON radio transient facility and analysis centre: towards a 24 x 7, all-sky monitor for the low-frequency array (LOFAR)](http://adsabs.harvard.edu/abs/2013RSPTA.37120234P), 2013, Philosophical Transactions of the Royal Society of London Series A, 371, 20120234

10. Hassall, T. E., Stappers, B. W., Weltevrede, P., et al.: [Differential frequency-dependent delay from the pulsar magnetosphere](http://adsabs.harvard.edu/abs/2013A&A...552A..61H), 2013, Astronomy and Astrophysics, 552, A61

11. Sotomayor-Beltran, C., Sobey, C., Hessels, J. W. T., et al.: [Calibrating high-precision Faraday rotation measurements for LOFAR and the next generation of low-frequency radio telescopes](http://adsabs.harvard.edu/abs/2013A&A...552A..58S), 2013, Astronomy and Astrophysics, 552, A58

12. Asgekar, A., Oonk, J. B. R., Yatawatta, S., et al.: [LOFAR detections of low-frequency radio recombination lines towards Cassiopeia A](http://adsabs.harvard.edu/abs/2013A&A...551L..11A), 2013, Astronomy and Astrophysics, 551, L11

13. Yatawatta, S., de Bruyn, A. G., Brentjens, M. A., et al.: [Initial deep LOFAR observations of epoch of reionization windows. I. The north celestial pole](http://adsabs.harvard.edu/abs/2013A&A...550A.136Y), 2013, Astronomy and Astrophysics, 550, A136

14. Offringa, A. R., de Bruyn, A. G., Zaroubi, S., et al.: [The LOFAR radio environment](http://adsabs.harvard.edu/abs/2013A&A...549A..11O), 2013, Astronomy and Astrophysics, 549, A11

15. Hermsen, W., Hessels, J. W. T., Kuiper, L., et al.: [Synchronous X-ray and Radio Mode Switches: A Rapid Global Transformation of the Pulsar Magnetosphere](http://adsabs.harvard.edu/abs/2013Sci...339..436H), 2013, Science, 339, 436


2012
----

1. de Gasperin, F., Orrú, E., Murgia, M., et al.: [M 87 at metre wavelengths: the LOFAR picture](http://adsabs.harvard.edu/abs/2012A&A...547A..56D), 2012, Astronomy and Astrophysics, 547, A56

2. Noorishad, P., Wijnholds, S. J., van Ardenne, A., et al.: [Redundancy calibration of phased-array stations](http://adsabs.harvard.edu/abs/2012A&A...545A.108N), 2012, Astronomy and Astrophysics, 545, A108

3. Buitink, S., Falcke, H., James, C., et al.: [Constraints on ultra-high-energy neutrino flux from radio observations of the Moon](http://adsabs.harvard.edu/abs/2012ASTRA...8...29B), 2012, Astrophysics and Space Sciences Transactions, 8, 29

4. Yatawatta, Sarod: [Reduced ambiguity calibration for LOFAR](http://adsabs.harvard.edu/abs/2012ExA....34...89Y), 2012, Experimental Astronomy, 34, 89

5. Hassall, T. E., Stappers, B. W., Hessels, J. W. T., et al.: [Wide-band simultaneous observations of pulsars: disentangling dispersion measure and profile variations](http://adsabs.harvard.edu/abs/2012A&A...543A..66H), 2012, Astronomy and Astrophysics, 543, A66

6. van Weeren, R. J., Röttgering, H. J. A., Rafferty, D. A., et al.: [First LOFAR observations at very low frequencies of cluster-scale non-thermal emission: the case of Abell 2256](http://adsabs.harvard.edu/abs/2012A&A...543A..43V), 2012, Astronomy and Astrophysics, 543, A43

7. Scaife, Anna M. M., Heald, George H.: [A broad-band flux scale for low-frequency radio telescopes](http://adsabs.harvard.edu/abs/2012MNRAS.423L..30S), 2012, Monthly Notices of the Royal Astronomical Society, 423, L30

8. Beck, Rainer: [Magnetic Fields in Galaxies](http://adsabs.harvard.edu/abs/2012SSRv..166..215B), 2012, Space Science Reviews, 166, 215

9. Offringa, A. R., van de Gronde, J. J., Roerdink, J. B. T. M.: [A morphological algorithm for improving radio-frequency interference detection](http://adsabs.harvard.edu/abs/2012A&A...539A..95O), 2012, Astronomy and Astrophysics, 539, A95

10. Singh, K., Mevius, M., Scholten, O., et al.: [Optimized trigger for ultra-high-energy cosmic-ray and neutrino observations with the low frequency radio array](http://adsabs.harvard.edu/abs/2012NIMPA.664..171S), 2012, Nuclear Instruments and Methods in Physics Research A, 664, 171

11. Tasse, Cyril, van Diepen, Ger, van der Tol, Sebastiaan, et al.: [LOFAR calibration and wide-field imaging](http://adsabs.harvard.edu/abs/2012CRPhy..13...28T), 2012, Comptes Rendus Physique, 13, 28


2011
----

1. Heald, George, Bell, Michael R., Horneffer, Andreas, et al.: [LOFAR: Recent Imaging Results and Future Prospects](http://adsabs.harvard.edu/abs/2011JApA...32..589H), 2011, Journal of Astrophysics and Astronomy, 32, 589

2. Wijnholds, S. J., Bregman, J. D., van Ardenne, A.: [Calibratability and its impact on configuration design for the LOFAR and SKA phased array radio telescopes](http://adsabs.harvard.edu/abs/2011RaSc...46.0F07W), 2011, Radio Science, 46, RS0F07

3. Kazemi, S., Yatawatta, S., Zaroubi, S., et al.: [Radio interferometric calibration using the SAGE algorithm](http://adsabs.harvard.edu/abs/2011MNRAS.414.1656K), 2011, Monthly Notices of the Royal Astronomical Society, 414, 1656

4. Wijnholds, Stefan J., van Cappellen, Wim A.: [In Situ Antenna Performance Evaluation of the LOFAR Phased Array Radio Telescope](http://adsabs.harvard.edu/abs/2011ITAP...59.1981W), 2011, IEEE Transactions on Antennas and Propagation, 59, 1981

5. Stappers, B. W., Hessels, J. W. T., Alexov, A., et al.: [Observing pulsars and fast transients with LOFAR](http://adsabs.harvard.edu/abs/2011A&A...530A..80S), 2011, Astronomy and Astrophysics, 530, A80

6. Thoudam, S., Aar, G. V., Akker, M. V. D., et al.: [An air shower array for LOFAR: LORA](http://adsabs.harvard.edu/abs/2011ASTRA...7..195T), 2011, Astrophysics and Space Sciences Transactions, 7, 195

7. Lofar Transients Key Science Project, van Leeuwen, Joeri, LOFAR Transients Key Science Project: [Neutron stars and gamma-ray bursts with LOFAR](http://adsabs.harvard.edu/abs/2011AdSpR..47.1441L), 2011, Advances in Space Research, 47, 1441

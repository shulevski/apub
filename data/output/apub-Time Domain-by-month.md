Title: ASTRON owned instruments and surveys Time Domain publications by month
Save_as: apub-Time Domain-by-month.html

[TOC]


2023-01
-------

1. Zhang, Jinge, Reid, Hamish A. S., Krupar, Vratislav, et al.: [Deriving Large Coronal Magnetic Loop Parameters Using LOFAR J Burst Observations](http://adsabs.harvard.edu/abs/2023SoPh..298....7Z), 2023, Solar Physics, 298, 7

2. Dabrowski, Bartosz, Mikuła, Katarzyna, Flisek, Paweł, et al.: [Interferometric imaging of the type IIIb and U radio bursts observed with LOFAR on 22 August 2017](http://adsabs.harvard.edu/abs/2023A&A...669A..52D), 2023, Astronomy and Astrophysics, 669, A52

3. van der Wateren, E., Bassa, C. G., Cooper, S., et al.: [The LOFAR Tied-Array All-Sky Survey: Timing of 35 radio pulsars and an overview of the properties of the LOFAR pulsar discoveries](http://adsabs.harvard.edu/abs/2023A&A...669A.160V), 2023, Astronomy and Astrophysics, 669, A160


2022-10
-------

1. Badman, Samuel T., Carley, Eoin, Cañizares, Luis Alberto, et al.: [Tracking a Beam of Electrons from the Low Solar Corona into Interplanetary Space with the Low Frequency Array, Parker Solar Probe, and 1 au Spacecraft](http://adsabs.harvard.edu/abs/2022ApJ...938...95B), 2022, The Astrophysical Journal, 938, 95


2022-09
-------

1. Liu, Hongyu, Zucca, Pietro, Cho, Kyung-Suk, et al.: [Interferometric Imaging, and Beam-Formed Study of a Moving Type-IV Radio Burst with LOFAR](http://adsabs.harvard.edu/abs/2022SoPh..297..115L), 2022, Solar Physics, 297, 115


2022-07
-------

1. Wu, Ziwei, Verbiest, Joris P. W., Main, Robert A., et al.: [Pulsar scintillation studies with LOFAR. I. The census](http://adsabs.harvard.edu/abs/2022A&A...663A.116W), 2022, Astronomy and Astrophysics, 663, A116


2022-06
-------

1. Zhang, PeiJin, Zucca, Pietro, Kozarev, Kamen, et al.: [Imaging of the Quiet Sun in the Frequency Range of 20-80 MHz](http://adsabs.harvard.edu/abs/2022ApJ...932...17Z), 2022, The Astrophysical Journal, 932, 17


2022-04
-------

1. Morosan, Diana E., Räsänen, Juska E., Kumari, Anshu, et al.: [Exploring the Circular Polarisation of Low-Frequency Solar Radio Bursts with LOFAR](http://adsabs.harvard.edu/abs/2022SoPh..297...47M), 2022, Solar Physics, 297, 47


2021-11
-------

1. Agar, C. H., Weltevrede, P., Bondonneau, L., et al.: [A broad-band radio study of PSR J0250+5854: the slowest spinning radio pulsar known](http://adsabs.harvard.edu/abs/2021MNRAS.508.1102A), 2021, Monthly Notices of the Royal Astronomical Society, 508, 1102


2021-10
-------

1. Grießmeier, J. -M., Smith, D. A., Theureau, G., et al.: [Follow-up of 27 radio-quiet gamma-ray pulsars at 110-190 MHz using the international LOFAR station FR606](http://adsabs.harvard.edu/abs/2021A&A...654A..43G), 2021, Astronomy and Astrophysics, 654, A43


2021-08
-------

1. Clarkson, Daniel L., Kontar, Eduard P., Gordovskyy, Mykola, et al.: [First Frequency-time-resolved Imaging Spectroscopy Observations of Solar Radio Spikes](http://adsabs.harvard.edu/abs/2021ApJ...917L..32C), 2021, The Astrophysical Journal, 917, L32

2. Scholten, O., Hare, B. M., Dwyer, J., et al.: [A distinct negative leader propagation mode](http://adsabs.harvard.edu/abs/2021NatSR..1116256S), 2021, Scientific Reports, 11, 16256


2021-06
-------

1. Maan, Yogesh, van Leeuwen, Joeri, Vohl, Dany: [Fourier domain excision of periodic radio frequency interference](http://adsabs.harvard.edu/abs/2021A&A...650A..80M), 2021, Astronomy and Astrophysics, 650, A80

2. Bailes, M., Bassa, C. G., Bernardi, G., et al.: [Multifrequency observations of SGR J1935+2154](http://adsabs.harvard.edu/abs/2021MNRAS.503.5367B), 2021, Monthly Notices of the Royal Astronomical Society, 503, 5367


2021-05
-------

1. Reid, Hamish A. S., Kontar, Eduard P.: [Fine structure of type III solar radio bursts from Langmuir wave motion in turbulent plasma](http://adsabs.harvard.edu/abs/2021NatAs...5..796R), 2021, Nature Astronomy, 5, 796

2. Miraval Zanon, A., D'Avanzo, P., Ridolfi, A., et al.: [Evidence of intra-binary shock emission from the redback pulsar PSR J1048+2339](http://adsabs.harvard.edu/abs/2021A&A...649A.120M), 2021, Astronomy and Astrophysics, 649, A120


2021-03
-------

1. Tiburzi, C., Shaifullah, G. M., Bassa, C. G., et al.: [The impact of solar wind variability on pulsar timing](http://adsabs.harvard.edu/abs/2021A&A...647A..84T), 2021, Astronomy and Astrophysics, 647, A84

2. Maguire, Ciara A., Carley, Eoin P., Zucca, Pietro, et al.: [LOFAR Observations of a Jet-driven Piston Shock in the Low Solar Corona](http://adsabs.harvard.edu/abs/2021ApJ...909....2M), 2021, The Astrophysical Journal, 909, 2


2021-01
-------

1. Turner, Jake D., Zarka, Philippe, Grießmeier, Jean-Mathias, et al.: [The search for radio emission from the exoplanetary systems 55 Cancri, υ Andromedae, and τ Boötis using LOFAR beam-formed observations](http://adsabs.harvard.edu/abs/2021A&A...645A..59T), 2021, Astronomy and Astrophysics, 645, A59


2020-12
-------

1. Mitra, P., Bonardi, A., Corstanje, A., et al.: [Reconstructing air shower parameters with LOFAR using event specific GDAS atmosphere](http://adsabs.harvard.edu/abs/2020APh...12302470M), 2020, Astroparticle Physics, 123, 102470

2. Donner, J. Y., Verbiest, J. P. W., Tiburzi, C., et al.: [Dispersion measure variability for 36 millisecond pulsars at 150 MHz with LOFAR](http://adsabs.harvard.edu/abs/2020A&A...644A.153D), 2020, Astronomy and Astrophysics, 644, A153


2020-11
-------

1. Mulrey, K., Buitink, S., Corstanje, A., et al.: [On the cosmic-ray energy scale of the LOFAR radio telescope](http://adsabs.harvard.edu/abs/2020JCAP...11..017M), 2020, Journal of Cosmology and Astroparticle Physics, 2020, 017

2. Rożko, K., Kijak, J., Chyży, K., et al.: [The Significance of Low-frequency Interferometric Observations for the GPS Pulsar Flux Estimation: The Case of J1740+1000](http://adsabs.harvard.edu/abs/2020ApJ...903..144R), 2020, The Astrophysical Journal, 903, 144


2020-10
-------

1. Nieder, L., Clark, C. J., Kandel, D., et al.: [Discovery of a Gamma-Ray Black Widow Pulsar by GPU-accelerated Einstein@Home](http://adsabs.harvard.edu/abs/2020ApJ...902L..46N), 2020, The Astrophysical Journal, 902, L46


2020-08
-------

1. Kuznetsov, Alexey A., Chrysaphi, Nicolina, Kontar, Eduard P., et al.: [Radio Echo in the Turbulent Corona and Simulations of Solar Drift-pair Radio Bursts](http://adsabs.harvard.edu/abs/2020ApJ...898...94K), 2020, The Astrophysical Journal, 898, 94


2020-07
-------

1. Zhang, PeiJin, Zucca, Pietro, Sridhar, Sarrvesh Seethapuram, et al.: [Interferometric imaging with LOFAR remote baselines of the fine structures of a solar type-IIIb radio burst](http://adsabs.harvard.edu/abs/2020A&A...639A.115Z), 2020, Astronomy and Astrophysics, 639, A115

2. Magdalenić, Jasmina, Marqué, Christophe, Fallows, Richard A., et al.: [Fine Structure of a Solar Type II Radio Burst Observed by LOFAR](http://adsabs.harvard.edu/abs/2020ApJ...897L..15M), 2020, The Astrophysical Journal, 897, L15


2020-05
-------

1. Polzin, E. J., Breton, R. P., Bhattacharyya, B., et al.: [Study of spider pulsar binary eclipses and discovery of an eclipse mechanism transition](http://adsabs.harvard.edu/abs/2020MNRAS.494.2948P), 2020, Monthly Notices of the Royal Astronomical Society, 494, 2948


2020-04
-------

1. Chrysaphi, Nicolina, Reid, Hamish A. S., Kontar, Eduard P.: [First Observation of a Type II Solar Radio Burst Transitioning between a Stationary and Drifting State](http://adsabs.harvard.edu/abs/2020ApJ...893..115C), 2020, The Astrophysical Journal, 893, 115

2. Lam, M. T., Lazio, T. J. W., Dolch, T., et al.: [On Frequency-dependent Dispersion Measures and Extreme Scattering Events](http://adsabs.harvard.edu/abs/2020ApJ...892...89L), 2020, The Astrophysical Journal, 892, 89


2020-03
-------

1. Bilous, A. V., Bondonneau, L., Kondratiev, V. I., et al.: [A LOFAR census of non-recycled pulsars: extending to frequencies below 80 MHz](http://adsabs.harvard.edu/abs/2020A&A...635A..75B), 2020, Astronomy and Astrophysics, 635, A75

2. Bondonneau, L., Grießmeier, J. -M., Theureau, G., et al.: [A census of the pulsar population observed with the international LOFAR station FR606 at low frequencies (25-80 MHz)](http://adsabs.harvard.edu/abs/2020A&A...635A..76B), 2020, Astronomy and Astrophysics, 635, A76

3. Zhang, PeiJin, Zucca, Pietro, Wang, ChuanBing, et al.: [The Frequency Drift and Fine Structures of Solar S-bursts in the High Frequency Band of LOFAR](http://adsabs.harvard.edu/abs/2020ApJ...891...89Z), 2020, The Astrophysical Journal, 891, 89

4. Hare, B. M., Scholten, O., Dwyer, J., et al.: [Radio Emission Reveals Inner Meter-Scale Structure of Negative Lightning Leader Steps](http://adsabs.harvard.edu/abs/2020PhRvL.124j5101H), 2020, Physical Review Letters, 124, 105101

5. Tan, C. M., Bassa, C. G., Cooper, S., et al.: [The LOFAR Tied-Array all-sky survey: Timing of 21 pulsars including the first binary pulsar discovered with LOFAR](http://adsabs.harvard.edu/abs/2020MNRAS.492.5878T), 2020, Monthly Notices of the Royal Astronomical Society, 492, 5878

6. Salas, P., Brentjens, M. A., Bordenave, D. D., et al.: [Tied-array holography with LOFAR](http://adsabs.harvard.edu/abs/2020A&A...635A.207S), 2020, Astronomy and Astrophysics, 635, A207


2020-02
-------

1. van Leeuwen, Joeri, Mikhailov, Klim, Keane, Evan, et al.: [LOFAR radio search for single and periodic pulses from M 31](http://adsabs.harvard.edu/abs/2020A&A...634A...3V), 2020, Astronomy and Astrophysics, 634, A3

2. Fallows, Richard A., Forte, Biagio, Astin, Ivan, et al.: [A LOFAR observation of ionospheric scintillation from two simultaneous travelling ionospheric disturbances](http://adsabs.harvard.edu/abs/2020JSWSC..10...10F), 2020, Journal of Space Weather and Space Climate, 10, 10

3. Xiao, Jiangping, Li, Xiangru, Lin, Haitao, et al.: [Pulsar candidate selection using pseudo-nearest centroid neighbour classifier](http://adsabs.harvard.edu/abs/2020MNRAS.492.2119X), 2020, Monthly Notices of the Royal Astronomical Society, 492, 2119


2020-01
-------

1. Michilli, D., Bassa, C., Cooper, S., et al.: [The LOFAR tied-array all-sky survey (LOTAAS): Characterization of 20 pulsar discoveries and their single-pulse behaviour](http://adsabs.harvard.edu/abs/2020MNRAS.491..725M), 2020, Monthly Notices of the Royal Astronomical Society, 491, 725

2. Quiroga-Nuñez, L. H., Intema, H. T., Callingham, J. R., et al.: [Differences in radio emission from similar M dwarfs in the binary system Ross 867-8](http://adsabs.harvard.edu/abs/2020A&A...633A.130Q), 2020, Astronomy and Astrophysics, 633, A130

3. Maguire, Ciara A., Carley, Eoin P., McCauley, Joseph, et al.: [Evolution of the Alfvén Mach number associated with a coronal mass ejection shock](http://adsabs.harvard.edu/abs/2020A&A...633A..56M), 2020, Astronomy and Astrophysics, 633, A56


2019-11
-------

1. Polzin, E. J., Breton, R. P., Stappers, B. W., et al.: [Long-term variability of a black widow's eclipses - A decade of PSR J2051-0827](http://adsabs.harvard.edu/abs/2019MNRAS.490..889P), 2019, Monthly Notices of the Royal Astronomical Society, 490, 889

2. Zhang, PeiJin, Yu, SiJie, Kontar, Eduard P., et al.: [On the Source Position and Duration of a Solar Type III Radio Burst Observed by LOFAR](http://adsabs.harvard.edu/abs/2019ApJ...885..140Z), 2019, The Astrophysical Journal, 885, 140

3. Kuznetsov, A. A., Kontar, E. P.: [First imaging spectroscopy observations of solar drift pair bursts](http://adsabs.harvard.edu/abs/2019A&A...631L...7K), 2019, Astronomy and Astrophysics, 631, L7


2019-09
-------

1. Nieder, L., Clark, C. J., Bassa, C. G., et al.: [Detection and Timing of Gamma-Ray Pulsations from the 707 Hz Pulsar J0952-0607](http://adsabs.harvard.edu/abs/2019ApJ...883...42N), 2019, The Astrophysical Journal, 883, 42

2. Hörandel, Jörg R.: [Radio detection of extensive air showers - Measuring the properties of cosmic rays with the radio technique at LOFAR and the Pierre Auger Observatory](http://adsabs.harvard.edu/abs/2019NPPP..306..108H), 2019, Nuclear and Particle Physics Proceedings, 306-308, 108


2019-07
-------

1. Tiburzi, C., Verbiest, J. P. W., Shaifullah, G. M., et al.: [On the usefulness of existing solar wind models for pulsar timing corrections](http://adsabs.harvard.edu/abs/2019MNRAS.487..394T), 2019, Monthly Notices of the Royal Astronomical Society, 487, 394


2019-06
-------

1. Krishnakumar, M. A., Maan, Yogesh, Joshi, B. C., et al.: [Multi-frequency Scatter-broadening Evolution of Pulsars. II. Scatter-broadening of Nearby Pulsars](http://adsabs.harvard.edu/abs/2019ApJ...878..130K), 2019, The Astrophysical Journal, 878, 130

2. Sanidas, S., Cooper, S., Bassa, C. G., et al.: [The LOFAR Tied-Array All-Sky Survey (LOTAAS): Survey overview and initial pulsar discoveries](http://adsabs.harvard.edu/abs/2019A&A...626A.104S), 2019, Astronomy and Astrophysics, 626, A104


2019-04
-------

1. Sobey, C., Bilous, A. V., Grießmeier, J. -M., et al.: [Low-frequency Faraday rotation measures towards pulsars using LOFAR: probing the 3D Galactic halo magnetic field](http://adsabs.harvard.edu/abs/2019MNRAS.484.3646S), 2019, Monthly Notices of the Royal Astronomical Society, 484, 3646

2. Hare, B. M., Scholten, O., Dwyer, J., et al.: [Needle-like structures discovered on positively charged lightning branches](http://adsabs.harvard.edu/abs/2019Natur.568..360H), 2019, Nature, 568, 360

3. Turner, Jake D., Grießmeier, Jean-Mathias, Zarka, Philippe, et al.: [The search for radio emission from exoplanets using LOFAR beam-formed observations: Jupiter as an exoplanet](http://adsabs.harvard.edu/abs/2019A&A...624A..40T), 2019, Astronomy and Astrophysics, 624, A40

4. Donner, J. Y., Verbiest, J. P. W., Tiburzi, C., et al.: [First detection of frequency-dependent, time-variable dispersion measures](http://adsabs.harvard.edu/abs/2019A&A...624A..22D), 2019, Astronomy and Astrophysics, 624, A22


2019-03
-------

1. Houben, L. J. M., Spitler, L. G., ter Veen, S., et al.: [Constraints on the low frequency spectrum of FRB 121102](http://adsabs.harvard.edu/abs/2019A&A...623A..42H), 2019, Astronomy and Astrophysics, 623, A42

2. Porayko, N. K., Noutsos, A., Tiburzi, C., et al.: [Testing the accuracy of the ionospheric Faraday rotation corrections through LOFAR observations of bright northern pulsars](http://adsabs.harvard.edu/abs/2019MNRAS.483.4100P), 2019, Monthly Notices of the Royal Astronomical Society, 483, 4100

3. Gordovskyy, Mykola, Kontar, Eduard, Browning, Philippa, et al.: [Frequency-Distance Structure of Solar Radio Sources Observed by LOFAR](http://adsabs.harvard.edu/abs/2019ApJ...873...48G), 2019, The Astrophysical Journal, 873, 48


2019-02
-------

1. Clarke, Brendan P., Morosan, Diana E., Gallagher, Peter T., et al.: [Properties and magnetic origins of solar S-bursts](http://adsabs.harvard.edu/abs/2019A&A...622A.204C), 2019, Astronomy and Astrophysics, 622, A204

2. Morosan, Diana E., Carley, Eoin P., Hayes, Laura A., et al.: [Multiple regions of shock-accelerated particles during a solar coronal mass ejection](http://adsabs.harvard.edu/abs/2019NatAs...3..452M), 2019, Nature Astronomy, 3, 452


2019-01
-------

1. ter Veen, S., Enriquez, J. E., Falcke, H., et al.: [The FRATS project: real-time searches for fast radio bursts and other fast transients with LOFAR at 135 MHz](http://adsabs.harvard.edu/abs/2019A&A...621A..57T), 2019, Astronomy and Astrophysics, 621, A57


2018-12
-------

1. Chrysaphi, Nicolina, Kontar, Eduard P., Holman, Gordon D., et al.: [CME-driven Shock and Type II Solar Radio Burst Band Splitting](http://adsabs.harvard.edu/abs/2018ApJ...868...79C), 2018, The Astrophysical Journal, 868, 79

2. Foster, Griffin, Karastergiou, Aris, Geyer, Marisa, et al.: [Verifying and reporting Fast Radio Bursts](http://adsabs.harvard.edu/abs/2018MNRAS.481.2612F), 2018, Monthly Notices of the Royal Astronomical Society, 481, 2612


2018-11
-------

1. Hermsen, W., Kuiper, L., Basu, R., et al.: [Discovery of synchronous X-ray and radio moding of PSR B0823+26](http://adsabs.harvard.edu/abs/2018MNRAS.480.3655H), 2018, Monthly Notices of the Royal Astronomical Society, 480, 3655

2. Michilli, D., Hessels, J. W. T., Lyon, R. J., et al.: [Single-pulse classifier for the LOFAR Tied-Array All-sky Survey](http://adsabs.harvard.edu/abs/2018MNRAS.480.3457M), 2018, Monthly Notices of the Royal Astronomical Society, 480, 3457


2018-10
-------

1. Tan, C. M., Bassa, C. G., Cooper, S., et al.: [LOFAR Discovery of a 23.5 s Radio Pulsar](http://adsabs.harvard.edu/abs/2018ApJ...866...54T), 2018, The Astrophysical Journal, 866, 54

2. Dąbrowski, B. P., Morosan, D. E., Fallows, R. A., et al.: [Observations of the Sun using LOFAR Bałdy station](http://adsabs.harvard.edu/abs/2018AdSpR..62.1895D), 2018, Advances in Space Research, 62, 1895

3. Błaszkiewicz, L. P., Lewandowski, W., Krankowski, A., et al.: [PL612 LOFAR station sensitivity measurements in the context of its application for pulsar observations](http://adsabs.harvard.edu/abs/2018AdSpR..62.1904B), 2018, Advances in Space Research, 62, 1904


2018-08
-------

1. Bilous, A. V.: [PSR B0943+10: low-frequency study of subpulse periodicity in the Bright mode with LOFAR](http://adsabs.harvard.edu/abs/2018A&A...616A.119B), 2018, Astronomy and Astrophysics, 616, A119

2. Sharykin, I. N., Kontar, E. P., Kuznetsov, A. A.: [LOFAR Observations of Fine Spectral Structure Dynamics in Type IIIb Radio Bursts](http://adsabs.harvard.edu/abs/2018SoPh..293..115S), 2018, Solar Physics, 293, 115

3. Gunst, André W., Kruithof, Gert H.: [Antenna data storage concept for phased array radio astronomical instruments](http://adsabs.harvard.edu/abs/2018ExA....45..351G), 2018, Experimental Astronomy, 45, 351


2018-07
-------

1. Zucca, P., Morosan, D. E., Rouillard, A. P., et al.: [Shock location and CME 3D reconstruction of a solar type II radio burst with LOFAR](http://adsabs.harvard.edu/abs/2018A&A...615A..89Z), 2018, Astronomy and Astrophysics, 615, A89

2. Kolotkov, Dmitrii Y., Nakariakov, Valery M., Kontar, Eduard P.: [Origin of the Modulation of the Radio Emission from the Solar Corona by a Fast Magnetoacoustic Wave](http://adsabs.harvard.edu/abs/2018ApJ...861...33K), 2018, The Astrophysical Journal, 861, 33

3. Verbiest, Joris P. W., Shaifullah, G. M.: [Measurement uncertainty in pulsar timing array experiments](http://adsabs.harvard.edu/abs/2018CQGra..35m3001V), 2018, Classical and Quantum Gravity, 35, 133001

4. Perera, B. B. P., Stappers, B. W., Babak, S., et al.: [Improving timing sensitivity in the microhertz frequency regime: limits from PSR J1713+0747 on gravitational waves produced by supermassive black hole binaries](http://adsabs.harvard.edu/abs/2018MNRAS.478..218P), 2018, Monthly Notices of the Royal Astronomical Society, 478, 218


2018-06
-------

1. Reid, Hamish A. S., Kontar, Eduard P.: [Solar type III radio burst time characteristics at LOFAR frequencies and the implications for electron beam transport](http://adsabs.harvard.edu/abs/2018A&A...614A..69R), 2018, Astronomy and Astrophysics, 614, A69

2. Shaifullah, G., Tiburzi, C., Osłowski, S., et al.: [Multifrequency behaviour of the anomalous events of PSR J0922+0638](http://adsabs.harvard.edu/abs/2018MNRAS.477L..25S), 2018, Monthly Notices of the Royal Astronomical Society, 477, L25

3. Lynch, Ryan S., Swiggum, Joseph K., Kondratiev, Vlad I., et al.: [The Green Bank North Celestial Cap Pulsar Survey. III. 45 New Pulsar Timing Solutions](http://adsabs.harvard.edu/abs/2018ApJ...859...93L), 2018, The Astrophysical Journal, 859, 93

4. Białkowski, Sławomir, Lewandowski, Wojciech, Kijak, Jarosław, et al.: [Mode switching characteristics of PSR B0329+54 at 150 MHz](http://adsabs.harvard.edu/abs/2018Ap&SS.363..110B), 2018, Astrophysics and Space Science, 363, 110


2018-05
-------

1. Polzin, E. J., Breton, R. P., Clarke, A. O., et al.: [The low-frequency radio eclipses of the black widow pulsar J1810+1744](http://adsabs.harvard.edu/abs/2018MNRAS.476.1968P), 2018, Monthly Notices of the Royal Astronomical Society, 476, 1968

2. Michilli, D., Hessels, J. W. T., Donner, J. Y., et al.: [Low-frequency pulse profile variation in PSR B2217+47: evidence for echoes from the interstellar medium](http://adsabs.harvard.edu/abs/2018MNRAS.476.2704M), 2018, Monthly Notices of the Royal Astronomical Society, 476, 2704


2018-04
-------

1. Cendes, Y., Prasad, P., Rowlinson, A., et al.: [RFI flagging implications for short-duration transients](http://adsabs.harvard.edu/abs/2018A&C....23..103C), 2018, Astronomy and Computing, 23, 103


2018-03
-------

1. Chen, Xingyao, Kontar, Eduard P., Yu, Sijie, et al.: [Fine Structures of Solar Radio Type III Bursts and Their Possible Relationship with Coronal Density Turbulence](http://adsabs.harvard.edu/abs/2018ApJ...856...73C), 2018, The Astrophysical Journal, 856, 73

2. Mann, G., Breitling, F., Vocks, C., et al.: [Tracking of an electron beam through the solar corona with LOFAR](http://adsabs.harvard.edu/abs/2018A&A...611A..57M), 2018, Astronomy and Astrophysics, 611, A57

3. Tan, C. M., Lyon, R. J., Stappers, B. W., et al.: [Ensemble candidate classification for the LOTAAS pulsar survey](http://adsabs.harvard.edu/abs/2018MNRAS.474.4571T), 2018, Monthly Notices of the Royal Astronomical Society, 474, 4571


2017-11
-------

1. Kontar, E. P., Yu, S., Kuznetsov, A. A., et al.: [Imaging spectroscopy of solar radio burst fine structures](http://adsabs.harvard.edu/abs/2017NatCo...8.1515K), 2017, Nature Communications, 8, 1515


2017-10
-------

1. Morosan, D. E., Gallagher, P. T., Fallows, R. A., et al.: [The association of a J-burst with a solar jet](http://adsabs.harvard.edu/abs/2017A&A...606A..81M), 2017, Astronomy and Astrophysics, 606, A81


2017-09
-------

1. Geyer, M., Karastergiou, A., Kondratiev, V. I., et al.: [Scattering analysis of LOFAR pulsar observations](http://adsabs.harvard.edu/abs/2017MNRAS.470.2659G), 2017, Monthly Notices of the Royal Astronomical Society, 470, 2659

2. Bassa, C. G., Pleunis, Z., Hessels, J. W. T., et al.: [LOFAR Discovery of the Fastest-spinning Millisecond Pulsar in the Galactic Field](http://adsabs.harvard.edu/abs/2017ApJ...846L..20B), 2017, The Astrophysical Journal, 846, L20

3. Pleunis, Z., Bassa, C. G., Hessels, J. W. T., et al.: [A Millisecond Pulsar Discovery in a Survey of Unidentified Fermi γ-Ray Sources with LOFAR](http://adsabs.harvard.edu/abs/2017ApJ...846L..19P), 2017, The Astrophysical Journal, 846, L19

4. Swiggum, J. K., Kaplan, D. L., McLaughlin, M. A., et al.: [A Multiwavelength Study of Nearby Millisecond Pulsar PSR J1400-1431: Improved Astrometry and an Optical Detection of Its Cool White Dwarf Companion](http://adsabs.harvard.edu/abs/2017ApJ...847...25S), 2017, The Astrophysical Journal, 847, 25


2017-01
-------

1. Bassa, C. G., Pleunis, Z., Hessels, J. W. T.: [Enabling pulsar and fast transient searches using coherent dedispersion](http://adsabs.harvard.edu/abs/2017A&C....18...40B), 2017, Astronomy and Computing, 18, 40

2. Turner, J. D., Griessmeier, J. -M., Zarka, P., et al.: [The search for radio emission from exoplanets using LOFAR low-frequency beam-formed observations: Data pipeline and preliminary results for the 55 Cnc system](http://adsabs.harvard.edu/abs/2017pre8.conf..301T), 2017, Planetary Radio Emissions VIII, None, 301

3. Dabrowski, B. P., Blaszkiewicz, L., Krankowski, A., et al.: [Low frequency solar scrutiny with the Polish LOFAR stations](http://adsabs.harvard.edu/abs/2017pre8.conf..437D), 2017, Planetary Radio Emissions VIII, None, 437

4. Morosan, D. E., Gallagher, P. T.: [Characteristics of type III radio bursts and solar S bursts](http://adsabs.harvard.edu/abs/2017pre8.conf..357M), 2017, Planetary Radio Emissions VIII, None, 357


2016-07
-------

1. Broderick, J. W., Fender, R. P., Breton, R. P., et al.: [Low-radio-frequency eclipses of the redback pulsar J2215+5135 observed in the image plane with LOFAR](http://adsabs.harvard.edu/abs/2016MNRAS.459.2681B), 2016, Monthly Notices of the Royal Astronomical Society, 459, 2681


2015-11
-------

1. Breitling, F., Mann, G., Vocks, C., et al.: [The LOFAR Solar Imaging Pipeline and the LOFAR Solar Data Center](http://adsabs.harvard.edu/abs/2015A&C....13...99B), 2015, Astronomy and Computing, 13, 99


2015-09
-------

1. Karastergiou, A., Chennamangalam, J., Armour, W., et al.: [Limits on fast radio bursts at 145 MHz with ARTEMIS, a real-time software backend](http://adsabs.harvard.edu/abs/2015MNRAS.452.1254K), 2015, Monthly Notices of the Royal Astronomical Society, 452, 1254


2015-08
-------

1. Sobey, C., Young, N. J., Hessels, J. W. T., et al.: [LOFAR discovery of a quiet emission mode in PSR B0823+26](http://adsabs.harvard.edu/abs/2015MNRAS.451.2493S), 2015, Monthly Notices of the Royal Astronomical Society, 451, 2493

2. Karako-Argaman, C., Kaspi, V. M., Lynch, R. S., et al.: [Discovery and Follow-up of Rotating Radio Transients with the Green Bank and LOFAR Telescopes](http://adsabs.harvard.edu/abs/2015ApJ...809...67K), 2015, The Astrophysical Journal, 809, 67

3. Morosan, D. E., Gallagher, P. T., Zucca, P., et al.: [LOFAR tied-array imaging and spectroscopy of solar S bursts](http://adsabs.harvard.edu/abs/2015A&A...580A..65M), 2015, Astronomy and Astrophysics, 580, A65


2015-07
-------

1. Obrocka, M., Stappers, B., Wilkinson, P.: [Localising fast radio bursts and other transients using interferometric arrays](http://adsabs.harvard.edu/abs/2015A&A...579A..69O), 2015, Astronomy and Astrophysics, 579, A69


2015-04
-------

1. Noutsos, A., Sobey, C., Kondratiev, V. I., et al.: [Pulsar polarisation below 200 MHz: Average profiles and propagation effects](http://adsabs.harvard.edu/abs/2015A&A...576A..62N), 2015, Astronomy and Astrophysics, 576, A62

2. Schellart, P., Trinh, T. N. G., Buitink, S., et al.: [Probing Atmospheric Electric Fields in Thunderstorms through Radio Emission from Cosmic-Ray-Induced Air Showers](http://adsabs.harvard.edu/abs/2015PhRvL.114p5001S), 2015, Physical Review Letters, 114, 165001


2014-12
-------

1. Bilous, A. V., Hessels, J. W. T., Kondratiev, V. I., et al.: [LOFAR observations of PSR B0943+10: profile evolution and discovery of a systematically changing profile delay in bright mode](http://adsabs.harvard.edu/abs/2014A&A...572A..52B), 2014, Astronomy and Astrophysics, 572, A52


2014-10
-------

1. Dolch, T., Lam, M. T., Cordes, J., et al.: [A 24 Hr Global Campaign to Assess Precision Timing of the Millisecond Pulsar J1713+0747](http://adsabs.harvard.edu/abs/2014ApJ...794...21D), 2014, The Astrophysical Journal, 794, 21

2. Coenen, Thijs, van Leeuwen, Joeri, Hessels, Jason W. T., et al.: [The LOFAR pilot surveys for pulsars and fast radio transients](http://adsabs.harvard.edu/abs/2014A&A...570A..60C), 2014, Astronomy and Astrophysics, 570, A60


2014-08
-------

1. Archibald, Anne M., Kondratiev, Vladislav I., Hessels, Jason W. T., et al.: [Millisecond Pulsar Scintillation Studies with LOFAR: Initial Results](http://adsabs.harvard.edu/abs/2014ApJ...790L..22A), 2014, The Astrophysical Journal, 790, L22

2. Morosan, D. E., Gallagher, P. T., Zucca, P., et al.: [LOFAR tied-array imaging of Type III solar radio bursts](http://adsabs.harvard.edu/abs/2014A&A...568A..67M), 2014, Astronomy and Astrophysics, 568, A67


2013-08
-------

1. Jones, P. B.: [PSR B1133+16: radio emission height and plasma composition.](http://adsabs.harvard.edu/abs/2013MNRAS.435L..11J), 2013, Monthly Notices of the Royal Astronomical Society, 435, L11


2013-07
-------

1. Fallows, R. A., Asgekar, A., Bisi, M. M., et al.: [The Dynamic Spectrum of Interplanetary Scintillation: First Solar Wind Observations on LOFAR](http://adsabs.harvard.edu/abs/2013SoPh..285..127F), 2013, Solar Physics, 285, 127


2013-04
-------

1. Hassall, T. E., Stappers, B. W., Weltevrede, P., et al.: [Differential frequency-dependent delay from the pulsar magnetosphere](http://adsabs.harvard.edu/abs/2013A&A...552A..61H), 2013, Astronomy and Astrophysics, 552, A61


2013-01
-------

1. Hermsen, W., Hessels, J. W. T., Kuiper, L., et al.: [Synchronous X-ray and Radio Mode Switches: A Rapid Global Transformation of the Pulsar Magnetosphere](http://adsabs.harvard.edu/abs/2013Sci...339..436H), 2013, Science, 339, 436


2012-07
-------

1. Hassall, T. E., Stappers, B. W., Hessels, J. W. T., et al.: [Wide-band simultaneous observations of pulsars: disentangling dispersion measure and profile variations](http://adsabs.harvard.edu/abs/2012A&A...543A..66H), 2012, Astronomy and Astrophysics, 543, A66


2011-06
-------

1. Stappers, B. W., Hessels, J. W. T., Alexov, A., et al.: [Observing pulsars and fast transients with LOFAR](http://adsabs.harvard.edu/abs/2011A&A...530A..80S), 2011, Astronomy and Astrophysics, 530, A80


2011-04
-------

1. Lofar Transients Key Science Project, van Leeuwen, Joeri, LOFAR Transients Key Science Project: [Neutron stars and gamma-ray bursts with LOFAR](http://adsabs.harvard.edu/abs/2011AdSpR..47.1441L), 2011, Advances in Space Research, 47, 1441

Title: Apertif publications by month
Save_as: apub-APERTIF-by-month.html

[TOC]


2023-02
-------

1. Offringa, A. R., Adebahr, B., Kutkin, A., et al.: [An interference detection strategy for Apertif based on AOFlagger 3](http://adsabs.harvard.edu/abs/2023A&A...670A.166O), 2023, Astronomy and Astrophysics, 670, A166


2022-11
-------

1. Adams, E. A. K., Adebahr, B., de Blok, W. J. G., et al.: [First release of Apertif imaging survey data](http://adsabs.harvard.edu/abs/2022A&A...667A..38A), 2022, Astronomy and Astrophysics, 667, A38

2. Kutkin, A. M., Oosterloo, T. A., Morganti, R., et al.: [Continuum source catalog for the first APERTIF data release](http://adsabs.harvard.edu/abs/2022A&A...667A..39K), 2022, Astronomy and Astrophysics, 667, A39

3. Dénes, H., Hess, K. M., Adams, E. A. K., et al.: [Characterising the Apertif primary beam response](http://adsabs.harvard.edu/abs/2022A&A...667A..40D), 2022, Astronomy and Astrophysics, 667, A40


2022-07
-------

1. Adebahr, B., Berger, A., Adams, E. A. K., et al.: [The Apertif science verification campaign. Characteristics of polarised radio sources](http://adsabs.harvard.edu/abs/2022A&A...663A.103A), 2022, Astronomy and Astrophysics, 663, A103


2022-05
-------

1. van Leeuwen, Joeri, Kooistra, Eric, Oostrum, Leon, et al.: [The Apertif Radio Transient System (ARTS): Design, Commissioning, Data Release, and Detection of the first 5 Fast Radio Bursts](http://adsabs.harvard.edu/abs/2022arXiv220512362V), 2022, arXiv e-prints, None, arXiv:2205.12362


2022-02
-------

1. van Cappellen, W. A., Oosterloo, T. A., Verheijen, M. A. W., et al.: [Apertif: Phased array feeds for the Westerbork Synthesis Radio Telescope. System overview and performance characteristics](http://adsabs.harvard.edu/abs/2022A&A...658A.146V), 2022, Astronomy and Astrophysics, 658, A146


2022-01
-------

1. Adebahr, B., Schulz, R., Dijkema, T. J., et al.: [Apercal-The Apertif calibration pipeline](http://adsabs.harvard.edu/abs/2022A&C....3800514A), 2022, Astronomy and Computing, 38, 100514


2021-06
-------

1. Boersma, O. M., van Leeuwen, J., Adams, E. A. K., et al.: [A search for radio emission from double-neutron star merger GW190425 using Apertif](http://adsabs.harvard.edu/abs/2021A&A...650A.131B), 2021, Astronomy and Astrophysics, 650, A131


2021-03
-------

1. Hess, K. M., Roberts, H., Dénes, H., et al.: [Apertif view of the OH megamaser IRAS 10597+5926: OH 18 cm satellite lines in wide-area H I surveys](http://adsabs.harvard.edu/abs/2021A&A...647A.193H), 2021, Astronomy and Astrophysics, 647, A193


2020-12
-------

1. Connor, L., van Leeuwen, J., Oostrum, L. C., et al.: [A bright, high rotation-measure FRB that skewers the M33 halo](http://adsabs.harvard.edu/abs/2020MNRAS.499.4716C), 2020, Monthly Notices of the Royal Astronomical Society, 499, 4716


2020-09
-------

1. Oosterloo, T. A., Vedantham, H. K., Kutkin, A. M., et al.: [Extreme intra-hour variability of the radio source J1402+5347 discovered with Apertif](http://adsabs.harvard.edu/abs/2020A&A...641L...4O), 2020, Astronomy and Astrophysics, 641, L4


2020-03
-------

1. Oostrum, L. C., Maan, Y., van Leeuwen, J., et al.: [Repeating fast radio bursts with WSRT/Apertif](http://adsabs.harvard.edu/abs/2020A&A...635A..61O), 2020, Astronomy and Astrophysics, 635, A61


2020-01
-------

1. Sclocco, Alessio, Vohl, Dany, van Nieuwpoort, Rob V.: [Real-Time RFI Mitigation for the Apertif Radio Transient System](http://adsabs.harvard.edu/abs/2020arXiv200103389S), 2020, arXiv e-prints, None, arXiv:2001.03389

2. Schoonderbeek, Gijs, Hut, Boudewijn, Kooistra, E., et al.: [Implementation of a Correlator onto a Hardware Beam-Former to Calculate Beam-Weights](http://adsabs.harvard.edu/abs/2020JAI.....950007S), 2020, Journal of Astronomical Instrumentation, 9, 2050007-912


2018-12
-------

1. Straal, Samayra M.: [Young hidden pulsars](http://adsabs.harvard.edu/abs/2018PhDT.......155S), 2018, Ph.D. Thesis, None, 

2. Connor, Liam, van Leeuwen, Joeri: [Applying Deep Learning to Fast Radio Burst Classification](http://adsabs.harvard.edu/abs/2018AJ....156..256C), 2018, The Astronomical Journal, 156, 256


2018-10
-------

1. Mikhailov, K., Sclocco, A.: [The Apertif Monitor for Bursts Encountered in Real-time (AMBER) auto-tuning optimization with genetic algorithms](http://adsabs.harvard.edu/abs/2018A&C....25..139M), 2018, Astronomy and Computing, 25, 139

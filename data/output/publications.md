Title: Publications
Save_as: publications.html

[TOC]

We request that scientific publications using data obtained from ASTRON instruments or surveys include one of the following acknowledgments:

*LOFAR, the Low Frequency Array designed and constructed by ASTRON, has facilities in several countries, that are owned by various parties (each with their own funding sources), and that are collectively operated by the International LOFAR Telescope (ILT) foundation under a joint scientific policy.*

*This work makes use of data from the Apertif system installed at the Westerbork Synthesis Radio Telescope owned by ASTRON.*

*Data taken with the Westerbork Synthesis Radio Telescope were the basis of this work. It is operated by ASTRON (Netherlands Institute for
Radio Astronomy) with support from the Netherlands Foundation for Scientific Research (NWO).*

## Publication database

The SDCO curates a list of scientific publications
pertaining to ASTRON.
The database contains 596 publications,
of which 591 are peer-reviewed.
It demonstrates the important impact of LOFAR data
on astronomical research.

You can access the publication list by instrument:

 * <a href="apub-lofar.html">LOFAR publications &raquo;</a>

Or by topic:

 * <a href="apub-imaging.html">Radio publications &raquo;</a>
 * <a href="apub-time-domain.html">Astrophysics publications &raquo;</a>

You also can mine the database yourself by accessing a spreadsheet of the publications: [astron-publications.xls](/data/astron-publications.xls)

If you spot an error, such as a missing entry,
please get in touch or open an issue in the <a href="https://support.astron.nl/sdchelpdesk">SDCO Helpdesk</a>.

Last update: 30 Mar 2023.

<hr/>

## Breakdown by year & instrument

The graph below shows the number of publications as a function
of year and instrument.
The publication count for LOFAR is 596
while that of WSRT is 0
and of APERTIF: 0.
The number of refereed papers is 591 for LOFAR and 0 for WSRT.

[![Publication rate by instrument and year](/images/apub/apub-publication-rate-without-extrapolation.png)](/images/apub/apub-publication-rate-without-extrapolation.png)

<hr/>

## Number of authors

The entries in the publication database have been authored and co-authored
by a total of 5445 unique author names.
We define the author name at "last name, first initial".
Slight variations in the spelling may increase the number of unique names,
while common names with the same first initial may result in undercounting.

[![Number of LOFAR, WSRT and APERTIF papers and unique authors over time](/images/apub/apub-author-count.png)](/images/apub/apub-author-count.png)

<hr/>

## Breakdown by subject

ASTRON data have been used for scientific applications
across radio astronomy research.
While 472 works relate to interferometry
(79%),
a total of 115
pertain to other areas of radio astronomy
(19%).

The graph below details the breakdown of ASTRON papers by science topic.

[![Publications by subject](/plots/apub-piechart.png)](/plots/apub-piechart.png)

<hr/>

## Most-cited publications

ASTRON publications have cumulatively been cited
18975 times.
The list below shows the most-cited publications,
based on the citation count obtained from NASA ADS.


1. MULTI-MESSENGER OBSERVATIONS OF A BINARY NEUTRON STAR MERGER  
Abbott, B. P., Abbott, R., Abbott, T. D., et al.    
[2017ApJ...848L..12A](http://adsabs.harvard.edu/abs/2017ApJ...848L..12A)
<span class="badge">2658 citations</span>

2. LOFAR: THE LOW-FREQUENCY ARRAY  
van Haarlem, M. P., Wise, M. W., Gunst, A. W., et al.    
[2013A&A...556A...2V](http://adsabs.harvard.edu/abs/2013A&A...556A...2V)
<span class="badge">1670 citations</span>

3. THE LOFAR TWO-METRE SKY SURVEY. I. SURVEY DESCRIPTION AND PRELIMINARY DATA RELEASE  
Shimwell, T. W., Röttgering, H. J. A., Best, P. N., et al.    
[2017A&A...598A.104S](http://adsabs.harvard.edu/abs/2017A&A...598A.104S)
<span class="badge">356 citations</span>

4. THE LOFAR TWO-METRE SKY SURVEY. II. FIRST DATA RELEASE  
Shimwell, T. W., Tasse, C., Hardcastle, M. J., et al.    
[2019A&A...622A...1S](http://adsabs.harvard.edu/abs/2019A&A...622A...1S)
<span class="badge">340 citations</span>

5. A MORPHOLOGICAL ALGORITHM FOR IMPROVING RADIO-FREQUENCY INTERFERENCE DETECTION  
Offringa, A. R., van de Gronde, J. J., Roerdink, J. B. T. M.    
[2012A&A...539A..95O](http://adsabs.harvard.edu/abs/2012A&A...539A..95O)
<span class="badge">269 citations</span>

6. UPPER LIMITS ON THE 21 CM EPOCH OF REIONIZATION POWER SPECTRUM FROM ONE NIGHT WITH LOFAR  
Patil, A. H., Yatawatta, S., Koopmans, L. V. E., et al.    
[2017ApJ...838...65P](http://adsabs.harvard.edu/abs/2017ApJ...838...65P)
<span class="badge">221 citations</span>

7. A BROAD-BAND FLUX SCALE FOR LOW-FREQUENCY RADIO TELESCOPES  
Scaife, Anna M. M., Heald, George H.    
[2012MNRAS.423L..30S](http://adsabs.harvard.edu/abs/2012MNRAS.423L..30S)
<span class="badge">212 citations</span>

8. OBSERVING PULSARS AND FAST TRANSIENTS WITH LOFAR  
Stappers, B. W., Hessels, J. W. T., Alexov, A., et al.    
[2011A&A...530A..80S](http://adsabs.harvard.edu/abs/2011A&A...530A..80S)
<span class="badge">206 citations</span>

9. LOFAR FACET CALIBRATION  
van Weeren, R. J., Williams, W. L., Hardcastle, M. J., et al.    
[2016ApJS..223....2V](http://adsabs.harvard.edu/abs/2016ApJS..223....2V)
<span class="badge">182 citations</span>

10. LOFAR 150-MHZ OBSERVATIONS OF THE BOÖTES FIELD: CATALOGUE AND SOURCE COUNTS  
Williams, W. L., van Weeren, R. J., Röttgering, H. J. A., et al.    
[2016MNRAS.460.2385W](http://adsabs.harvard.edu/abs/2016MNRAS.460.2385W)
<span class="badge">163 citations</span>

11. INITIAL DEEP LOFAR OBSERVATIONS OF EPOCH OF REIONIZATION WINDOWS. I. THE NORTH CELESTIAL POLE  
Yatawatta, S., de Bruyn, A. G., Brentjens, M. A., et al.    
[2013A&A...550A.136Y](http://adsabs.harvard.edu/abs/2013A&A...550A.136Y)
<span class="badge">143 citations</span>

12. LOFAR, VLA, AND CHANDRA OBSERVATIONS OF THE TOOTHBRUSH GALAXY CLUSTER  
van Weeren, R. J., Brunetti, G., Brüggen, M., et al.    
[2016ApJ...818..204V](http://adsabs.harvard.edu/abs/2016ApJ...818..204V)
<span class="badge">134 citations</span>

13. METHOD FOR HIGH PRECISION RECONSTRUCTION OF AIR SHOWER X<SUB>MAX</SUB> USING TWO-DIMENSIONAL RADIO INTENSITY PROFILES  
Buitink, S., Corstanje, A., Enriquez, J. E., et al.    
[2014PhRvD..90h2003B](http://adsabs.harvard.edu/abs/2014PhRvD..90h2003B)
<span class="badge">125 citations</span>

14. A LARGE LIGHT-MASS COMPONENT OF COSMIC RAYS AT 10<SUP>17</SUP>-10<SUP>17.5</SUP> ELECTRONVOLTS FROM RADIO OBSERVATIONS  
Buitink, S., Corstanje, A., Falcke, H., et al.    
[2016Natur.531...70B](http://adsabs.harvard.edu/abs/2016Natur.531...70B)
<span class="badge">122 citations</span>

15. SYNCHRONOUS X-RAY AND RADIO MODE SWITCHES: A RAPID GLOBAL TRANSFORMATION OF THE PULSAR MAGNETOSPHERE  
Hermsen, W., Hessels, J. W. T., Kuiper, L., et al.    
[2013Sci...339..436H](http://adsabs.harvard.edu/abs/2013Sci...339..436H)
<span class="badge">120 citations</span>

16. DETECTING COSMIC RAYS WITH THE LOFAR RADIO TELESCOPE  
Schellart, P., Nelles, A., Buitink, S., et al.    
[2013A&A...560A..98S](http://adsabs.harvard.edu/abs/2013A&A...560A..98S)
<span class="badge">110 citations</span>

17. SYSTEMATIC EFFECTS IN LOFAR DATA: A UNIFIED CALIBRATION STRATEGY  
de Gasperin, F., Dijkema, T. J., Drabent, A., et al.    
[2019A&A...622A...5D](http://adsabs.harvard.edu/abs/2019A&A...622A...5D)
<span class="badge">106 citations</span>

18. THE LOFAR TWO-METER SKY SURVEY: DEEP FIELDS DATA RELEASE 1. I. DIRECTION-DEPENDENT CALIBRATION AND IMAGING  
Tasse, C., Shimwell, T., Hardcastle, M. J., et al.    
[2021A&A...648A...1T](http://adsabs.harvard.edu/abs/2021A&A...648A...1T)
<span class="badge">104 citations</span>

19. THE LOFAR TWO-METRE SKY SURVEY. V. SECOND DATA RELEASE  
Shimwell, T. W., Hardcastle, M. J., Tasse, C., et al.    
[2022A&A...659A...1S](http://adsabs.harvard.edu/abs/2022A&A...659A...1S)
<span class="badge">104 citations</span>

20. A LOFAR CENSUS OF NON-RECYCLED PULSARS: AVERAGE PROFILES, DISPERSION MEASURES, FLUX DENSITIES, AND SPECTRA  
Bilous, A. V., Kondratiev, V. I., Kramer, M., et al.    
[2016A&A...591A.134B](http://adsabs.harvard.edu/abs/2016A&A...591A.134B)
<span class="badge">101 citations</span>
<hr/>

<!-- 
## Most-read publications

The read count shown below is obtained from the ADS API
and indicates the number of times the article has been downloaded
within the last 90 days.

<hr/>

-->

## Most-active authors

Below we list the most-active authors, defined as those with six or more first-author publications in our database.


 * Botteon, A (9 publications)

 * de Gasperin, F (7 publications)

 * Hoang, D (7 publications)

 * Heesen, V (7 publications)

 * Rajpurohit, K (6 publications)

 * Vedantham, H (6 publications)

 * Scholten, O (6 publications)

 * Morosan, D (6 publications)

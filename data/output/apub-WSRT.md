Title: Wsrt publications
Save_as: apub-WSRT.html

[TOC]


2023
----

1. Wang, Jing, Yang, Dong, Oh, S. -H., et al.: [FEASTS: IGM Cooling Triggered by Tidal Interactions through the Diffuse H I Phase around NGC 4631](http://adsabs.harvard.edu/abs/2023ApJ...944..102W), 2023, The Astrophysical Journal, 944, 102

2. Müller, Ancla, Frohn, Vanessa, Dirks, Lukas, et al.: [Multi-epoch variability of AT 2000ch (SN 2000ch) in NGC 3432. A radio continuum and optical study](http://adsabs.harvard.edu/abs/2023A&A...670A.130M), 2023, Astronomy and Astrophysics, 670, A130

3. Barkai, J. A., Verheijen, M. A. W., Talavera, E., et al.: [A comparative study of source-finding techniques in H I emission line cubes using SoFiA, MTObjects, and supervised deep learning](http://adsabs.harvard.edu/abs/2023A&A...670A..55B), 2023, Astronomy and Astrophysics, 670, A55


2022
----

1. Kamphuis, P., Jütte, E., Heald, G. H., et al.: [HALOGAS: Strong constraints on the neutral gas reservoir and accretion rate in nearby spiral galaxies](http://adsabs.harvard.edu/abs/2022A&A...668A.182K), 2022, Astronomy and Astrophysics, 668, A182

2. Liu, Yulan, Verbiest, Joris P. W., Main, Robert A., et al.: [Long-term scintillation studies of EPTA pulsars. I. Observations and basic results](http://adsabs.harvard.edu/abs/2022A&A...664A.116L), 2022, Astronomy and Astrophysics, 664, A116

3. Liu, K., Antoniadis, J., Bassa, C. G., et al.: [Detection of quasi-periodic micro-structure in three millisecond pulsars with the Large European Array for Pulsars](http://adsabs.harvard.edu/abs/2022MNRAS.513.4037L), 2022, Monthly Notices of the Royal Astronomical Society, 513, 4037

4. van der Wateren, E., Bassa, C. G., Clark, C. J., et al.: [Irradiated but not eclipsed, the case of PSR J0610−2100](http://adsabs.harvard.edu/abs/2022A&A...661A..57V), 2022, Astronomy and Astrophysics, 661, A57

5. Annibali, F., Bacchini, C., Iorio, G., et al.: [The Smallest Scale of Hierarchy Survey (SSH) - II. Extended star formation and bar-like features in the dwarf galaxy NGC 3741: recent merger or ongoing gas accretion?](http://adsabs.harvard.edu/abs/2022MNRAS.512.1781A), 2022, Monthly Notices of the Royal Astronomical Society, 512, 1781

6. Poulain, Mélina, Marleau, Francine R., Habas, Rebecca, et al.: [HI observations of the MATLAS dwarf and ultra-diffuse galaxies](http://adsabs.harvard.edu/abs/2022A&A...659A..14P), 2022, Astronomy and Astrophysics, 659, A14

7. Mall, G., Main, R. A., Antoniadis, J., et al.: [Modelling annual scintillation arc variations in PSR J1643-1224 using the Large European Array for Pulsars](http://adsabs.harvard.edu/abs/2022MNRAS.511.1104M), 2022, Monthly Notices of the Royal Astronomical Society, 511, 1104

8. Sarbadhicary, Sumit K., Martizzi, Davide, Ramirez-Ruiz, Enrico, et al.: [Testing the Momentum-driven Supernova Feedback Paradigm in M31](http://adsabs.harvard.edu/abs/2022ApJ...928...54S), 2022, The Astrophysical Journal, 928, 54

9. Yang, Jun, Chen, Yongjun, Gurvits, Leonid I., et al.: [Structural and spectral properties of Galactic plane variable radio sources](http://adsabs.harvard.edu/abs/2022MNRAS.511..280Y), 2022, Monthly Notices of the Royal Astronomical Society, 511, 280

10. Molnár, D. Cs., Serra, P., van der Hulst, T., et al.: [The Westerbork Coma Survey. A blind, deep, high-resolution H I survey of the Coma cluster](http://adsabs.harvard.edu/abs/2022A&A...659A..94M), 2022, Astronomy and Astrophysics, 659, A94

11. Nimmo, K., Hewitt, D. M., Hessels, J. W. T., et al.: [Milliarcsecond Localization of the Repeating FRB 20201124A](http://adsabs.harvard.edu/abs/2022ApJ...927L...3N), 2022, The Astrophysical Journal, 927, L3

12. Antoniadis, J., Arzoumanian, Z., Babak, S., et al.: [The International Pulsar Timing Array second data release: Search for an isotropic gravitational wave background](http://adsabs.harvard.edu/abs/2022MNRAS.510.4873A), 2022, Monthly Notices of the Royal Astronomical Society, 510, 4873

13. Wang, J., Shaifullah, G. M., Verbiest, J. P. W., et al.: [A comparative analysis of pulse time-of-arrival creation methods](http://adsabs.harvard.edu/abs/2022A&A...658A.181W), 2022, Astronomy and Astrophysics, 658, A181

14. Bilous, A. V., Grießmeier, J. M., Pennucci, T., et al.: [Dual-frequency single-pulse study of PSR B0950+08](http://adsabs.harvard.edu/abs/2022A&A...658A.143B), 2022, Astronomy and Astrophysics, 658, A143

15. Purver, M., Bassa, C. G., Cognard, I., et al.: [Removal and replacement of interference in tied-array radio pulsar observations using the spectral kurtosis estimator](http://adsabs.harvard.edu/abs/2022MNRAS.510.1597P), 2022, Monthly Notices of the Royal Astronomical Society, 510, 1597

16. Kirsten, F., Marcote, B., Nimmo, K., et al.: [A repeating fast radio burst source in a globular cluster](http://adsabs.harvard.edu/abs/2022Natur.602..585K), 2022, Nature, 602, 585


2021
----

1. Leisman, Lukas, Rhode, Katherine L., Ball, Catherine, et al.: [The ALFALFA Almost Dark Galaxy AGC 229101: A 2 Billion Solar Mass H I Cloud with a Very Low Surface Brightness Optical Counterpart](http://adsabs.harvard.edu/abs/2021AJ....162..274L), 2021, The Astronomical Journal, 162, 274

2. Di Teodoro, Enrico M., Peek, J. E. G.: [Radial Motions and Radial Gas Flows in Local Spiral Galaxies](http://adsabs.harvard.edu/abs/2021ApJ...923..220D), 2021, The Astrophysical Journal, 923, 220

3. Chen, S., Caballero, R. N., Guo, Y. J., et al.: [Common-red-signal analysis with 24-yr high-precision timing of the European Pulsar Timing Array: inferences in the stochastic gravitational-wave background search](http://adsabs.harvard.edu/abs/2021MNRAS.508.4970C), 2021, Monthly Notices of the Royal Astronomical Society, 508, 4970

4. Zhu, Ming, Yu, Haiyang, Wang, Jie, et al.: [FAST Discovery of a Long H I Accretion Stream toward M106](http://adsabs.harvard.edu/abs/2021ApJ...922L..21Z), 2021, The Astrophysical Journal, 922, L21

5. Hu, Wenkai, Cortese, Luca, Staveley-Smith, Lister, et al.: [The atomic hydrogen content of galaxies as a function of group-centric radius](http://adsabs.harvard.edu/abs/2021MNRAS.507.5580H), 2021, Monthly Notices of the Royal Astronomical Society, 507, 5580

6. Kramer, M., Stairs, I. H., Manchester, R. N., et al.: [Strong-Field Gravity Tests with the Double Pulsar](http://adsabs.harvard.edu/abs/2021PhRvX..11d1050K), 2021, Physical Review X, 11, 041050

7. McQuinn, Kristen B. W., Telidevara, Anjana K., Fuson, Jackson, et al.: [Galaxy Properties at the Faint End of the H I Mass Function](http://adsabs.harvard.edu/abs/2021ApJ...918...23M), 2021, The Astrophysical Journal, 918, 23

8. Berger, A., Adebahr, B., Herrera Ruiz, N., et al.: [Faint polarised sources in the Lockman Hole field at 1.4 GHz](http://adsabs.harvard.edu/abs/2021A&A...653A.155B), 2021, Astronomy and Astrophysics, 653, A155

9. Siewert, Thilo M., Schmidt-Rubart, Matthias, Schwarz, Dominik J.: [Cosmic radio dipole: Estimators and frequency dependence](http://adsabs.harvard.edu/abs/2021A&A...653A...9S), 2021, Astronomy and Astrophysics, 653, A9

10. Main, Robert, Lin, Rebecca, van Kerkwijk, Marten H., et al.: [Resolving the Emission Regions of the Crab Pulsar's Giant Pulses](http://adsabs.harvard.edu/abs/2021ApJ...915...65M), 2021, The Astrophysical Journal, 915, 65

11. Dallacasa, D., Orienti, M., Fanti, C., et al.: [VLBI images at 327 MHz of compact steep spectrum and GHz-peaked spectrum sources from the 3C and PW samples](http://adsabs.harvard.edu/abs/2021MNRAS.504.2312D), 2021, Monthly Notices of the Royal Astronomical Society, 504, 2312

12. Kirsten, F., Snelders, M. P., Jenkins, M., et al.: [Detection of two bright radio bursts from magnetar SGR 1935 + 2154](http://adsabs.harvard.edu/abs/2021NatAs...5..414K), 2021, Nature Astronomy, 5, 414

13. Combes, F., Gupta, N., Muller, S., et al.: [PKS 1830-211: OH and H I at z = 0.89 and the first MeerKAT UHF spectrum](http://adsabs.harvard.edu/abs/2021A&A...648A.116C), 2021, Astronomy and Astrophysics, 648, A116

14. Gault, Lexi, Leisman, Lukas, Adams, Elizabeth A. K., et al.: [VLA Imaging of H I-bearing Ultra-diffuse Galaxies from the ALFALFA Survey](http://adsabs.harvard.edu/abs/2021ApJ...909...19G), 2021, The Astrophysical Journal, 909, 19

15. Schulz, R., Morganti, R., Nyland, K., et al.: [Parsec-scale HI outflows in powerful radio galaxies](http://adsabs.harvard.edu/abs/2021A&A...647A..63S), 2021, Astronomy and Astrophysics, 647, A63

16. Busekool, E., Verheijen, M. A. W., van der Hulst, J. M., et al.: [The Ursa Major association of galaxies - VI: a relative dearth of gas-rich dwarf galaxies](http://adsabs.harvard.edu/abs/2021MNRAS.501.2608B), 2021, Monthly Notices of the Royal Astronomical Society, 501, 2608

17. Nunhokee, C. D., Bernardi, G., Manti, S., et al.: [Radio multifrequency observations of galaxy clusters. The Abell 399$-$401 pair](http://adsabs.harvard.edu/abs/2021arXiv210202900N), 2021, arXiv e-prints, None, arXiv:2102.02900

18. Chiang, I-Da, Sandstrom, Karin M., Chastenet, Jérémy, et al.: [Resolving the Dust-to-Metals Ratio and CO-to-H<SUB>2</SUB> Conversion Factor in the Nearby Universe](http://adsabs.harvard.edu/abs/2021ApJ...907...29C), 2021, The Astrophysical Journal, 907, 29


2020
----

1. Repetto, P.: [Tensorial solution of the Poisson equation and the dark matter amount and distribution of UGC 8490 and UGC 9753](http://adsabs.harvard.edu/abs/2020MNRAS.499.3381R), 2020, Monthly Notices of the Royal Astronomical Society, 499, 3381

2. Nikiel-Wroczyński, Błażej, Soida, Marian, Heald, George, et al.: [A Large-scale, Regular Intergalactic Magnetic Field Associated with Stephan's Quintet?](http://adsabs.harvard.edu/abs/2020ApJ...898..110N), 2020, The Astrophysical Journal, 898, 110

3. Gogate, A. R., Verheijen, M. A. W., Deshev, B. Z., et al.: [BUDHIES IV: Deep 21-cm neutral Hydrogen, optical, and UV imaging data of Abell 963 and Abell 2192 at z ≃ 0.2](http://adsabs.harvard.edu/abs/2020MNRAS.496.3531G), 2020, Monthly Notices of the Royal Astronomical Society, 496, 3531

4. Mancera Piña, Pavel E., Fraternali, Filippo, Oman, Kyle A., et al.: [Robust H I kinematics of gas-rich ultra-diffuse galaxies: hints of a weak-feedback formation scenario](http://adsabs.harvard.edu/abs/2020MNRAS.495.3636M), 2020, Monthly Notices of the Royal Astronomical Society, 495, 3636

5. Das, Sanskriti, Sardone, Amy, Leroy, Adam K., et al.: [Detection of the Diffuse H I Emission in the Circumgalactic Medium of NGC 891 and NGC 4565](http://adsabs.harvard.edu/abs/2020ApJ...898...15D), 2020, The Astrophysical Journal, 898, 15

6. Bak Nielsen, Ann-Sofie, Janssen, Gemma H., Shaifullah, Golam, et al.: [Timing stability of three black widow pulsars](http://adsabs.harvard.edu/abs/2020MNRAS.494.2591B), 2020, Monthly Notices of the Royal Astronomical Society, 494, 2591

7. Hu, Wenkai, Catinella, Barbara, Cortese, Luca, et al.: [The cosmic atomic hydrogen mass density as a function of mass and galaxy hierarchy from spectral stacking](http://adsabs.harvard.edu/abs/2020MNRAS.493.1587H), 2020, Monthly Notices of the Royal Astronomical Society, 493, 1587

8. Reynolds, T. N., Westmeier, T., Staveley-Smith, L., et al.: [H I asymmetries in LVHIS, VIVA, and HALOGAS galaxies](http://adsabs.harvard.edu/abs/2020MNRAS.493.5089R), 2020, Monthly Notices of the Royal Astronomical Society, 493, 5089

9. Lawrence, Al, Kerton, C. R., Struck, Curtis, et al.: [Revealing the Double Nucleus of NGC 4490](http://adsabs.harvard.edu/abs/2020ApJ...891...11L), 2020, The Astrophysical Journal, 891, 11

10. MAGIC Collaboration, Ahnen, M. L., Ansoldi, S., et al.: [Statistics of VHE γ-rays in temporal association with radio giant pulses from the Crab pulsar](http://adsabs.harvard.edu/abs/2020A&A...634A..25M), 2020, Astronomy and Astrophysics, 634, A25

11. Zhuravlev, Vladimir I., Yermolaev, Yu. I., Andrianov, A. S.: [Probing the ionosphere by the pulsar B0950+08 with help of RadioAstron ground-space baselines](http://adsabs.harvard.edu/abs/2020MNRAS.491.5843Z), 2020, Monthly Notices of the Royal Astronomical Society, 491, 5843

12. Annibali, F., Beccari, G., Bellazzini, M., et al.: [The Smallest Scale of Hierarchy survey (SSH) - I. Survey description](http://adsabs.harvard.edu/abs/2020MNRAS.491.5101A), 2020, Monthly Notices of the Royal Astronomical Society, 491, 5101

13. Aniano, G., Draine, B. T., Hunt, L. K., et al.: [Modeling Dust and Starlight in Galaxies Observed by Spitzer and Herschel: The KINGFISH Sample](http://adsabs.harvard.edu/abs/2020ApJ...889..150A), 2020, The Astrophysical Journal, 889, 150

14. Hobbs, G., Guo, L., Caballero, R. N., et al.: [A pulsar-based time-scale from the International Pulsar Timing Array](http://adsabs.harvard.edu/abs/2020MNRAS.491.5951H), 2020, Monthly Notices of the Royal Astronomical Society, 491, 5951

15. Oostrum, L. C., van Leeuwen, J., Maan, Y., et al.: [A search for pulsars in subdwarf B binary systems and discovery of giant-pulse emitting PSR J0533-4524](http://adsabs.harvard.edu/abs/2020MNRAS.492.4825O), 2020, Monthly Notices of the Royal Astronomical Society, 492, 4825


2019
----

1. Perera, B. B. P., DeCesar, M. E., Demorest, P. B., et al.: [The International Pulsar Timing Array: second data release](http://adsabs.harvard.edu/abs/2019MNRAS.490.4666P), 2019, Monthly Notices of the Royal Astronomical Society, 490, 4666

2. Marasco, A., Fraternali, F., Heald, G., et al.: [HALOGAS: the properties of extraplanar HI in disc galaxies](http://adsabs.harvard.edu/abs/2019A&A...631A..50M), 2019, Astronomy and Astrophysics, 631, A50

3. Hu, Wenkai, Hoppmann, Laura, Staveley-Smith, Lister, et al.: [An accurate low-redshift measurement of the cosmic neutral hydrogen density](http://adsabs.harvard.edu/abs/2019MNRAS.489.1619H), 2019, Monthly Notices of the Royal Astronomical Society, 489, 1619

4. Golovich, N., Dawson, W. A., Wittman, D. M., et al.: [Merging Cluster Collaboration: A Panchromatic Atlas of Radio Relic Mergers](http://adsabs.harvard.edu/abs/2019ApJ...882...69G), 2019, The Astrophysical Journal, 882, 69

5. Sil'chenko, Olga K., Moiseev, Alexei V., Egorov, Oleg V.: [The Gas Kinematics, Excitation, and Chemistry, in Connection with Star Formation, in Lenticular Galaxies](http://adsabs.harvard.edu/abs/2019ApJS..244....6S), 2019, The Astrophysical Journal Supplement Series, 244, 6

6. Dubeibe, F. L., Martínez-Sicacha, Sandra M., González, Guillermo A.: [Orbital dynamics in realistic galaxy models: NGC 3726, NGC 3877 and NGC 4010](http://adsabs.harvard.edu/abs/2019arXiv190709573D), 2019, arXiv e-prints, None, arXiv:1907.09573

7. González-Lópezlira, Rosa A., Mayya, Y. D., Loinard, Laurent, et al.: [Spectroscopy of NGC 4258 Globular Cluster Candidates: Membership Confirmation and Kinematics](http://adsabs.harvard.edu/abs/2019ApJ...876...39G), 2019, The Astrophysical Journal, 876, 39

8. Higgins, A. B., van der Horst, A. J., Starling, R. L. C., et al.: [Detailed multiwavelength modelling of the dark GRB 140713A and its host galaxy](http://adsabs.harvard.edu/abs/2019MNRAS.484.5245H), 2019, Monthly Notices of the Royal Astronomical Society, 484, 5245

9. McKee, J. W., Stappers, B. W., Bassa, C. G., et al.: [A detailed study of giant pulses from PSR B1937+21 using the Large European Array for Pulsars](http://adsabs.harvard.edu/abs/2019MNRAS.483.4784M), 2019, Monthly Notices of the Royal Astronomical Society, 483, 4784

10. Adebahr, B., Brienza, M., Morganti, R.: [Polarised structures in the radio lobes of B2 0258+35. Evidence of magnetic draping?](http://adsabs.harvard.edu/abs/2019A&A...622A.209A), 2019, Astronomy and Astrophysics, 622, A209

11. Driessen, L. N., Janssen, G. H., Bassa, C. G., et al.: [Scattering features and variability of the Crab pulsar](http://adsabs.harvard.edu/abs/2019MNRAS.483.1224D), 2019, Monthly Notices of the Royal Astronomical Society, 483, 1224

12. Zhu, W. W., Desvignes, G., Wex, N., et al.: [Tests of gravitational symmetries with pulsar binary J1713+0747](http://adsabs.harvard.edu/abs/2019MNRAS.482.3249Z), 2019, Monthly Notices of the Royal Astronomical Society, 482, 3249

13. Baan, Willem A.: [Implementing RFI Mitigation in Radio Science](http://adsabs.harvard.edu/abs/2019JAI.....840010B), 2019, Journal of Astronomical Instrumentation, 8, 1940010


2018
----

1. Prandoni, I., Guglielmino, G., Morganti, R., et al.: [The Lockman Hole Project: new constraints on the sub-mJy source counts from a wide-area 1.4 GHz mosaic](http://adsabs.harvard.edu/abs/2018MNRAS.481.4548P), 2018, Monthly Notices of the Royal Astronomical Society, 481, 4548

2. Caballero, R. N., Guo, Y. J., Lee, K. J., et al.: [Studying the Solar system with the International Pulsar Timing Array](http://adsabs.harvard.edu/abs/2018MNRAS.481.5501C), 2018, Monthly Notices of the Royal Astronomical Society, 481, 5501

3. de Ugarte Postigo, A., Thöne, C. C., Bensch, K., et al.: [The luminous host galaxy, faint supernova and rapid afterglow rebrightening of GRB 100418A](http://adsabs.harvard.edu/abs/2018A&A...620A.190D), 2018, Astronomy and Astrophysics, 620, A190

4. Hajduk, M., van Hoof, P. A. M., Śniadkowska, K., et al.: [Radio observations of planetary nebulae: no evidence for strong radial density gradients](http://adsabs.harvard.edu/abs/2018MNRAS.479.5657H), 2018, Monthly Notices of the Royal Astronomical Society, 479, 5657

5. Morabito, Leah K., Harwood, Jeremy J.: [Investigating the cause of the α-z relation](http://adsabs.harvard.edu/abs/2018MNRAS.480.2726M), 2018, Monthly Notices of the Royal Astronomical Society, 480, 2726

6. Law, C. J., Gaensler, B. M., Metzger, B. D., et al.: [Discovery of the Luminous, Decades-long, Extragalactic Radio Transient FIRST J141918.9+394036](http://adsabs.harvard.edu/abs/2018ApJ...866L..22L), 2018, The Astrophysical Journal, 866, L22

7. MichałowskI, Michał J., Xu, Dong, Stevens, Jamie, et al.: [The second-closest gamma-ray burst: sub-luminous GRB 111005A with no supernova in a super-solar metallicity environment](http://adsabs.harvard.edu/abs/2018A&A...616A.169M), 2018, Astronomy and Astrophysics, 616, A169

8. Pingel, N. M., Pisano, D. J., Heald, G., et al.: [A GBT Survey of the HALOGAS Galaxies and Their Environments. I. Revealing the Full Extent of H I around NGC 891, NGC 925, NGC 4414, and NGC 4565](http://adsabs.harvard.edu/abs/2018ApJ...865...36P), 2018, The Astrophysical Journal, 865, 36

9. Williams, Anna Louise: [The Coevolution of Magnetic Fields and Galaxies in Different Environments](http://adsabs.harvard.edu/abs/2018PhDT........85W), 2018, Ph.D. Thesis, None, 

10. Mihos, J. Christopher, Carr, Christopher T., Watkins, Aaron E., et al.: [BST1047+1156: An Extremely Diffuse and Gas-rich Object in the Leo I Group](http://adsabs.harvard.edu/abs/2018ApJ...863L...7M), 2018, The Astrophysical Journal, 863, L7

11. Archibald, Anne M., Gusinskaia, Nina V., Hessels, Jason W. T., et al.: [Universality of free fall from the orbital motion of a pulsar in a stellar triple system](http://adsabs.harvard.edu/abs/2018Natur.559...73A), 2018, Nature, 559, 73

12. Chupin, A., Beck, R., Frick, P., et al.: [Magnetic arms of NGC 6946 traced in Faraday cubes at low radio frequencies](http://adsabs.harvard.edu/abs/2018AN....339..440C), 2018, Astronomische Nachrichten, 339, 440

13. Gupta, N., Momjian, E., Srianand, R., et al.: [Discovery of OH Absorption from a Galaxy at z ∼ 0.05: Implications for Large Surveys with SKA Pathfinders](http://adsabs.harvard.edu/abs/2018ApJ...860L..22G), 2018, The Astrophysical Journal, 860, L22

14. Repetto, P., Martínez-García, E. E., Rosado, M., et al.: [General circular velocity relation of a test particle in a 3D gravitational potential: application to the rotation curves analysis and total mass determination of UGC 8490 and UGC 9753](http://adsabs.harvard.edu/abs/2018MNRAS.477..678R), 2018, Monthly Notices of the Royal Astronomical Society, 477, 678

15. Richards, Emily E., van Zee, L., Barnes, K. L., et al.: [Baryonic distributions in galaxy dark matter haloes - II. Final results](http://adsabs.harvard.edu/abs/2018MNRAS.476.5127R), 2018, Monthly Notices of the Royal Astronomical Society, 476, 5127

16. Nösel, S., Sharma, R., Massi, M., et al.: [Hour time-scale QPOs in the X-ray and radio emission of LS I +61°303](http://adsabs.harvard.edu/abs/2018MNRAS.476.2516N), 2018, Monthly Notices of the Royal Astronomical Society, 476, 2516

17. Heesen, V., Krause, M., Beck, R., et al.: [Radio haloes in nearby galaxies modelled with 1D cosmic ray transport using SPINNAKER](http://adsabs.harvard.edu/abs/2018MNRAS.476..158H), 2018, Monthly Notices of the Royal Astronomical Society, 476, 158

18. Gupta, N., Srianand, R., Farnes, J. S., et al.: [Revealing H I gas in emission and absorption on pc to kpc scales in a galaxy at z ∼ 0.017](http://adsabs.harvard.edu/abs/2018MNRAS.476.2432G), 2018, Monthly Notices of the Royal Astronomical Society, 476, 2432

19. Adams, Elizabeth A. K., Oosterloo, Tom A.: [Deep neutral hydrogen observations of Leo T with the Westerbork Synthesis Radio Telescope](http://adsabs.harvard.edu/abs/2018A&A...612A..26A), 2018, Astronomy and Astrophysics, 612, A26

20. Ball, Catherine, Cannon, John M., Leisman, Lukas, et al.: [The Enigmatic (Almost) Dark Galaxy Coma P: The Atomic Interstellar Medium](http://adsabs.harvard.edu/abs/2018AJ....155...65B), 2018, The Astronomical Journal, 155, 65

21. Kanekar, Nissim, Ghosh, Tapasi, Chengalur, Jayaram N.: [Stringent Constraints on Fundamental Constant Evolution Using Conjugate 18 cm Satellite OH Lines](http://adsabs.harvard.edu/abs/2018PhRvL.120f1302K), 2018, Physical Review Letters, 120, 061302


2017
----

1. Adebahr, B., Krause, M., Klein, U., et al.: [M 82 - A radio continuum and polarisation study. II. Polarisation and rotation measures](http://adsabs.harvard.edu/abs/2017A&A...608A..29A), 2017, Astronomy and Astrophysics, 608, A29

2. Ostorero, Luisa, Morganti, Raffaella, Diaferio, Antonaldo, et al.: [Correlation between X-Ray and Radio Absorption in Compact Radio Galaxies](http://adsabs.harvard.edu/abs/2017ApJ...849...34O), 2017, The Astrophysical Journal, 849, 34

3. Kalberla, P. M. W., Kerp, J., Haud, U., et al.: [H I anisotropies associated with radio-polarimetric filaments . Steep power spectra associated with cold gas](http://adsabs.harvard.edu/abs/2017A&A...607A..15K), 2017, Astronomy and Astrophysics, 607, A15

4. Andreev, N., Araya, E. D., Hoffman, I. M., et al.: [Long-term Variability of H<SUB>2</SUB>CO Masers in Star-forming Regions](http://adsabs.harvard.edu/abs/2017ApJS..232...29A), 2017, The Astrophysical Journal Supplement Series, 232, 29

5. Maccagni, F. M., Morganti, R., Oosterloo, T. A., et al.: [Kinematics and physical conditions of H I in nearby radio sources. The last survey of the old Westerbork Synthesis Radio Telescope](http://adsabs.harvard.edu/abs/2017A&A...604A..43M), 2017, Astronomy and Astrophysics, 604, A43

6. Leisman, Lukas, Haynes, Martha P., Janowiecki, Steven, et al.: [(Almost) Dark Galaxies in the ALFALFA Survey: Isolated H I-bearing Ultra-diffuse Galaxies](http://adsabs.harvard.edu/abs/2017ApJ...842..133L), 2017, The Astrophysical Journal, 842, 133

7. Chowdhury, Aditya, Chengalur, Jayaram N.: [Angular momentum content in gas-rich dwarf galaxies](http://adsabs.harvard.edu/abs/2017MNRAS.467.3856C), 2017, Monthly Notices of the Royal Astronomical Society, 467, 3856

8. Repetto, P., Martínez-García, Eric E., Rosado, M., et al.: [Mass content of UGC 6446 and UGC 7524 through H I rotation curves: deriving the stellar discs from stellar population synthesis models](http://adsabs.harvard.edu/abs/2017MNRAS.468..180R), 2017, Monthly Notices of the Royal Astronomical Society, 468, 180

9. Hermsen, W., Kuiper, L., Hessels, J. W. T., et al.: [Simultaneous X-ray and radio observations of the radio-mode-switching pulsar PSR B1822-09](http://adsabs.harvard.edu/abs/2017MNRAS.466.1688H), 2017, Monthly Notices of the Royal Astronomical Society, 466, 1688

10. Akamatsu, H., Mizuno, M., Ota, N., et al.: [Suzaku observations of the merging galaxy cluster Abell 2255: The northeast radio relic](http://adsabs.harvard.edu/abs/2017A&A...600A.100A), 2017, Astronomy and Astrophysics, 600, A100

11. Vargas, Carlos J., Heald, George, Walterbos, Rene A. M., et al.: [HALOGAS Observations of NGC 4559: Anomalous and Extra-planar HI and its Relation to Star Formation](http://adsabs.harvard.edu/abs/2017arXiv170309345V), 2017, arXiv e-prints, None, arXiv:1703.09345

12. Dutta, R., Srianand, R., Gupta, N., et al.: [H I 21-cm absorption survey of quasar-galaxy pairs: distribution of cold gas around z &lt; 0.4 galaxies](http://adsabs.harvard.edu/abs/2017MNRAS.465..588D), 2017, Monthly Notices of the Royal Astronomical Society, 465, 588

13. Popov, Mikhail V., Bartel, Norbert, Gwinn, Carl R., et al.: [PSR B0329+54: substructure in the scatter-broadened image discovered with RadioAstron on baselines up to 330 000 km](http://adsabs.harvard.edu/abs/2017MNRAS.465..978P), 2017, Monthly Notices of the Royal Astronomical Society, 465, 978

14. Emery, Deanna L., Bogdán, Ákos, Kraft, Ralph P., et al.: [A Spectacular Bow Shock in the 11 keV Galaxy Cluster Around 3C 438](http://adsabs.harvard.edu/abs/2017ApJ...834..159E), 2017, The Astrophysical Journal, 834, 159

15. Hess, Kelley M., Cluver, M. E., Yahya, Sahba, et al.: [H I in group interactions: HCG 44](http://adsabs.harvard.edu/abs/2017MNRAS.464..957H), 2017, Monthly Notices of the Royal Astronomical Society, 464, 957

16. Sorgho, A., Hess, K., Carignan, C., et al.: [H I observations of galaxies in the southern filament of the Virgo Cluster with the Square Kilometre Array Pathfinder KAT-7 and the Westerbork Synthesis Radio Telescope](http://adsabs.harvard.edu/abs/2017MNRAS.464..530S), 2017, Monthly Notices of the Royal Astronomical Society, 464, 530

#!/usr/bin/env python
from setuptools import setup


# PyPi requires reStructuredText instead of Markdown,
# so we convert our Markdown README for the long description
try:
   import pypandoc
   long_description = pypandoc.convert('README.md', 'rst')
except (IOError, ImportError):
   long_description = open('README.md').read()

# Command-line tools
entry_points = {'console_scripts': [
    'apub = apub:apub',
    'apub-update = apub:apub_update',
    'apub-add = apub:apub_add',
    'apub-delete = apub:apub_delete',
    'apub-import = apub:apub_import',
    'apub-export = apub:apub_export',
    'apub-plot = apub:apub_plot',
    'apub-citations = apub:apub_citations',
    'apub-spreadsheet = apub:apub_spreadsheet'
]}

setup(name='apub',
      version='0.1.0dev',
      description="A simple tool to keep track of the publications related "
                  "to the (radio) instruments and surveys owned by ASTRON.",
      long_description=long_description,
      author='Aleksandar Shulevski',
      author_email='shulevski@astron.nl',
      license='MIT',
      url='www.astron.nl/~shulevski',
      packages=['apub'],
      data_files=[('apub/templates', ['apub/templates/template.md', 'apub/templates/template-overview.md'])],
      install_requires=["jinja2",
                        "six",
                        "astropy",
                        "ads"],
      entry_points=entry_points,
      classifiers=[
          "Development Status :: 0.1 - Beta",
          "License :: OSI Approved :: MIT License",
          "Operating System :: OS Independent",
          "Programming Language :: Python",
          "Intended Audience :: Science/Research",
          "Topic :: Scientific/Engineering :: Astronomy",
          ],
      )
